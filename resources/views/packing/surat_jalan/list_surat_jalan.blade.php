<div class="table-responsive">
    <table class="table" id="table-list-suratjalan">
        <thead>
            <tr>
                <th>Date</th>
                <th>No Surat Jalan</th>
                <th>Subcont</th>
                <th style="width: 40%;">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $key => $val)
                <tr>
                    <td>{{ $val->created_at }}</td>
                    <td>{{ $val->no_suratjalan.' '.$val->no_suratjalan_info }}</td>
                    <td>{{ $val->subcont_name }}</td>
                    <td>
                        @if(Auth::user()->admin_role)
                            <button type="button" id="choose-suratjalan-cancel"
                                    data-suratjalan="{{ $val->no_suratjalan }}" data-suratjalaninfo="{{ $val->no_suratjalan_info }}"
                                    class="btn btn-default" title="cancel">
                                    <i class="icon-database-remove"></i>
                            </button>
                        @endif
                        @if ($val->is_global)    
                            <button type="button" id="choose-suratjalan-global"
                                    data-suratjalan="{{ $val->no_suratjalan }}"
                                    class="btn btn-default" title="print">
                                    <i class="icon-printer2"></i>
                            </button>
                        @else
                            <button type="button" id="choose-suratjalan"
                                    data-suratjalan="{{ $val->no_suratjalan }}"
                                    class="btn btn-default" title="print">
                                    <i class="icon-printer2"></i>
                            </button>
                        @endif
                        
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<script type="text/javascript">
    var table = $('#table-list-suratjalan').DataTable({
        "lengthChange": false,
        "pageLength": 5
        /*"columnDefs": [{
            "targets" : [0],
            "visible" : false,
            "searchable" : false
        }]*/
    });
</script>