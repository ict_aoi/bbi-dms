@extends('layouts.app', ['active' => 'packinglistglobal'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Cetak Surat Jalan Global</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('packing.suratJalanGlobal') }}"><i class="icon-list position-left"></i> Cetak</a></li>
            <li class="active">Surat Jalan Global</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<form id="main-form" method="POST">
    {{ csrf_field() }}
    <div class="panel panel-flat">
        <div class="panel-body loader-area">
            <div class="row">
                <fieldset>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Style <span class="text-danger">*</span></label>
                                    <select data-placeholder="select a state" name="style" id="style" class="form-control select-search">
                                        <option value=""></option>
                                        @foreach ($list_style as $list)
                                            <option value="{{ $list->style }}">{{ $list->style }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>QTY <span class="text-danger">*</span></label>
                                    <input type="text" name="qty" id="qty" class="form-control" onkeypress="return isNumberKey(event)">
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
			<div class="col-md-12">
				<div class="text-right">
					<button type="button" id="btnAdd" class="btn btn-primary">Add <i class="icon-plus3 position-right"></i></button>
				</div>
			</div>
        </div>
    </div>
</form>
<div class="panel panel-flat">
    <div class="panel-body loading-area">
        <div class="row">
            <div class="text-right">
                <button type="button" id="btnSave" class="btn btn-primary">Print Surat Jalan <i class="icon-floppy-disk position-right"></i></button>
            </div>
        </div>
        <div class="table-responsive">
            <div class="responsive" style="height: 30em; overflow: auto;">
                <table class="table datatable-save-state" id="table-list">
                    <thead>
                        <tr>
                            <th>STYLE</th>
                            <th>QTY</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<a href="{{ route('packing.updateUoms') }}" id="updateUoms"></a>
<a href="{{ route('packing.ajaxSetCompletedAllSuratJalanGlobal') }}" id="completed_all"></a>
<a href="{{ route('packing.printPreviewSuratJalanGlobal') }}" id="print_preview"></a>
<a href="{{ route('packing.ajaxGetNomorSuratJalan') }}" id="get_nomor"></a>
<a href="{{ route('packing.ajaxGetSuratJalan') }}" id="get_suratjalan"></a>
<a href="{{ route('packing.ajaxGetSuratJalanExist') }}" id="get_list"></a>
<a href="{{ route('packing.cancelSuratJalan') }}" id="cancel_suratjalan"></a>
@endsection

@section('page-modal')
 @include('packing.surat_jalan._index_global');   
@endsection

@section('page-js')
<script type="text/javascript" src="{{ url('js/dataTables.buttons.min.js') }}"></script> 
<script type="text/javascript">
//$(document).ready(function() {
    $('#style').select2({
        placeholder: "Select a State",
        minimumInputLength: 3,
        formatInputTooShort: function () {
            return "Enter Minimum 3 Character";
        },
    });

    //url
    var url = $('#form_filter').attr('action');

    var _token = $("input[name='_token']").val();

    //end of datatables

    // add
    $('#btnAdd').on('click', function(event) {
        event.preventDefault();

        var style = $('#style').val();
        var qty = $('#qty').val();

        if (style == '' || qty == '') {
            myalert('error', 'Oops Something wrong..!');
            return false;
        }

        var data = Array();
        $('#table-list tbody > tr').each(function(i, v) {
            data[i] = Array();
            $(this).children('td').each(function(ii, vv) {
                data[i][ii] = $(this).text();
            });
        });

        var i;
        for (i = 0; i < data.length; ++i) {
            if(data[i][0] == style){
                myalert('error', 'Style already exist..!');
                return false;
            }
        }

        $('#table-list tbody').append('<tr>'
                    +'<td>'+style+'</td>'
                    +'<td>'+qty+'</td>'
                    +'<td><button class="btn btn-sm btn-danger" onclick="deleterow(this);" title="delete"><i class="icon-trash"></i></button></td>'
                +'</tr>');
        clear();
    });

    // SAVE
    $('#btnSave').on('click', function(event) {
        event.preventDefault();

        var data = Array();

        $('#table-list tbody tr').each(function(i, v) {
            data[i] = Array();
            $(this).children('td').each(function(ii, vv) {
                data[i][ii] = $(this).text();
            });
        });

        if(data.length <= 0){
            myalert('error', 'Please check data first..!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url: $('#get_nomor').attr('href'),
            success: function(response){
                $('#no_suratjalan').val(response);
                $('#modal_list_').modal('show');
            },
            error: function(response){
                return false;
            }
        });

    });

    $(document).on('click', '.printSuratJalan', function(event){
        event.preventDefault();
        var data = [];

        $('.clref:checked').each(function(){
            data.push({
                id: $(this).data('id')
            });
        });

        if(data.length <= 0){
            myalert('error', 'Please check data first..!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url: $('#get_nomor').attr('href'),
            success: function(response){
                $('#no_suratjalan').val(response);
                $('#modal_list_').modal('show');
            },
            error: function(response){
                return false;
            }
        });

    });

    //button submit
    $('#btnSubmit').on('click', function(event){
        event.preventDefault();

        var no_suratjalan = $('#no_suratjalan').val();
        var no_suratjalan_info = $('#no_suratjalan_info').val();
        var subcont_id = $('#subcont_id').val();
        
        if($.trim(no_suratjalan) == '' || $.trim(no_suratjalan_info) == '' || $.trim(subcont_id) == '' ){
            myalert('error', 'Please completed all inputs..!');
            return false;
        }

        var data = Array();

        $('#table-list tbody tr').each(function(i, v) {
            data[i] = Array();
            $(this).children('td').each(function(ii, vv) {
                data[i][ii] = $(this).text();
            });
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#completed_all').attr('href'),
            data: {no_suratjalan:no_suratjalan, no_suratjalan_info:no_suratjalan_info, subcont_id:subcont_id, data: JSON.stringify(data)},
            beforeSend: function() {
                $('.loading-area').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('.loading-area').unblock();
                printPage(null, no_suratjalan);
                $('#modal_list_').modal('hide');
                $('#table-list tbody tr').remove();
            },
            error: function(response) {
                $('.loading-area').unblock();
                myalert('error','NOT GOOD');
            }
        });

    });

    //choose filter
    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'po') {
            if($('#filter_by_po').hasClass('hidden')) {
                $('#filter_by_po').removeClass('hidden');
            }

            $('#filter_by_style').addClass('hidden');
        }
        else if (this.value == 'style') {
            if($('#filter_by_style').hasClass('hidden')) {
                $('#filter_by_style').removeClass('hidden');
            }

            $('#filter_by_po').addClass('hidden');
        }
    });

    function printPage(data_id, no_suratjalan) {
        var url_print = $('#print_preview').attr('href');
        var data = [];

        $('.clref:checked').each(function() {
            data.push({
                id: $(this).data('headerid')
            });
        });

        if(data_id === null){
            var parameter = '?no_suratjalan='+no_suratjalan+'&data=' + JSON.stringify(data);
        }else{
            var parameter = '?no_suratjalan='+no_suratjalan+'&data=' + JSON.stringify(data_id);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : url_print + parameter,
            success: function(response) {
                window.open(url_print + parameter, '_blank');
                $('input:checkbox').removeAttr('checked');
                table.draw();
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
    }

    // reprint
    $('.rePrint').on('click', function(){
        $('#modal_list_printed').modal('show');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : $('#get_suratjalan').attr('href'),
            beforeSend: function() {
                $('#modal_list_printed > .modal-content').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#modal_list_printed > .modal-content').unblock();
                $('#listing_printed').html(response);
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
    });

    $('#listing_printed').on('click', '#choose-suratjalan', function(){
        var no_suratjalan = $(this).data('suratjalan');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : $('#get_list').attr('href'),
            data: {no_suratjalan:no_suratjalan},
            beforeSend: function() {
                $('#modal_list_printed > .modal-content').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#modal_list_printed').unblock();
                var data = [];

                $.each(response, function(i){
                    data.push({
                        'id' : response[i].formulir_id
                    });
                });

                printPage(data, no_suratjalan);

            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
    });

    //cancel surat jalan
    $('#listing_printed').on('click', '#choose-suratjalan-cancel', function(){
        var no_suratjalan = $(this).data('suratjalan');
        var no_suratjalaninfo = $(this).data('suratjalaninfo');

        bootbox.confirm("Are you sure cancel this delivery orders "+no_suratjalan+no_suratjalaninfo+" ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url : $('#cancel_suratjalan').attr('href'),
                    data: {no_suratjalan:no_suratjalan},
                    beforeSend: function() {
                        $('#modal_list_printed > .modal-content').block({
                            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: '10px 15px',
                                color: '#fff',
                                width: 'auto',
                                '-webkit-border-radius': 2,
                                '-moz-border-radius': 2,
                                backgroundColor: '#333'
                            }
                        });
                    },
                    success: function(response) {
                        $('#modal_list_printed > .modal-content').unblock();
                        myalert('success', 'GOOD..!');
                        $('#listing_printed').html(response);
                    },
                    error: function(response) {
                        myalert('error','NOT GOOD');
                    }
                });
            }
        });

    });

    function toggle(source) {
        checkboxes = document.getElementsByName('selector[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
          checkboxes[i].checked = source.checked;
        }
    }

    function clear() {
		$('#style').val('').trigger('change');
    	$('#qty').val('');
    }
    
    function deleterow(e) {
		$(e).parent().parent().remove();
	}
    
    //get parameter from url
    function getURLParameter(url) {
        var params = {};
        url.substring(1).replace(/[?&]+([^=&]+)=([^&]*)/gi,
                function (str, key, value) {
                     params[key] = value;
                });
        return params;
    }

//});
</script>
@endsection
