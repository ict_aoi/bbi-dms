<div id="modal_list_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-md">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title" id="title-suratjalan"><i class="position-left"></i>Surat Jalan</h5>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="tanggal" class="control-label col-sm-2">Tanggal</label>
                                <div class="col-sm-10">
                                    <p>{{ \Carbon\Carbon::now()->format('d F Y') }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="no_suratjalan" class="control-label col-sm-2">No.</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <input type="text" name="no_suratjalan" id="no_suratjalan" onkeypress="return isNumberKey(event)" class="form-control" required>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" name="no_suratjalan_info" id="no_suratjalan_info" class="form-control" value="/ BBI / GA / {{ \Carbon\Carbon::now()->format('Y') }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="subcont_id" class="control-label col-sm-2">Subcont</label>
                                <div class="col-sm-10">
                                    <select name="subcont_id" id="subcont_id" class="form-control" required>
                                        <option value=""></option>
                                        @foreach ($subcont as $key => $val)
                                            <option value="{{ $val->id }}">{{ $val->name }}</option>
                                        @endforeach   
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="btnSubmit">Submit</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_list_printed" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-md">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title" id="title-suratjalan"><i class="position-left"></i>Surat Jalan</h5>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <fieldset>
                        <div id="listing_printed" class="col-lg-12">
                            <!-- template pilihan -->
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>