@extends('layouts.app', ['active' => 'packinglist'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Cetak Surat Jalan</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('packing.index') }}"><i class="icon-list position-left"></i> Cetak</a></li>
            <li class="active">Surat Jalan</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('packing.ajaxGetDataSuratJalan') }}" id="form_filter">
            <div class="form-group" id="filter_by_style">
				<label><b>Choose Style</b></label>
				<div class="input-group col-md-12">
                    {{-- <input type="text" class="form-control text-uppercase" name="style" id="style" placeholder="Style"> --}}
                    <select data-placeholder="select a state" name="style" id="style" class="form-control select-search">
                        <option value=""></option>
                        @foreach ($list_style as $list)
                            <option value="{{ $list->style }}">{{ $list->style }}</option>
                        @endforeach
                    </select>
                    <div class="input-group-btn hidden">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
			</div>
            <div class="form-group hidden" id="filter_by_po">
				<label><b>Choose PO Number</b></label>
				<div class="input-group">
                    <input type="text" class="form-control text-uppercase" name="po" id="po" placeholder="PO Number">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
			</div>
            <div class="form-group hidden">
                <label class="radio-inline"><input type="radio" name="radio_status" value="po">Filter by PO Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="style">Filter by Style</label>
            </div>
        </form>
        <div class="form-group">
            <button class="btn btn-danger rePrint">List Surat Jalan</button>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body loading-area">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th><input type="checkbox" onclick="toggle(this)"></th>
                        <th>FORMULIR DATE</th>
                        <th>NO POLYBAG</th>
                        <th>PO</th>
                        <th>STYLE</th>
                        <th>QTY</th>
                        <th>SATUAN</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('packing.updateUoms') }}" id="updateUoms"></a>
<a href="{{ route('packing.ajaxSetCompletedAllSuratJalan') }}" id="completed_all"></a>
<a href="{{ route('packing.printPreviewSuratJalan') }}" id="print_preview"></a>
<a href="{{ route('packing.printPreviewSuratJalanGlobal') }}" id="print_preview_global"></a>
<a href="{{ route('packing.ajaxGetNomorSuratJalan') }}" id="get_nomor"></a>
<a href="{{ route('packing.ajaxGetSuratJalan') }}" id="get_suratjalan"></a>
<a href="{{ route('packing.ajaxGetSuratJalanExist') }}" id="get_list"></a>
<a href="{{ route('packing.cancelSuratJalan') }}" id="cancel_suratjalan"></a>
@endsection

@section('page-modal')
 @include('packing.surat_jalan._index');   
@endsection

@section('page-js')
<script type="text/javascript" src="{{ url('js/dataTables.buttons.min.js') }}"></script> 
<script type="text/javascript">
//$(document).ready(function() {
    $('#style').select2({
        placeholder: "Select a State",
        minimumInputLength: 3,
        formatInputTooShort: function () {
            return "Enter Minimum 3 Character";
        },
    });

    $('#style').on('change', function(){
        if($(this).val() != ''){
            $('#form_filter').submit();
        }
    });
    //url
    var url = $('#form_filter').attr('action');

    var _token = $("input[name='_token']").val();

    var table = $('#table-list').DataTable({
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        stateSave: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        buttons: [
                {
                    text: 'Print Surat Jalan',
                    className: 'btn btn-sm bg-primary printSuratJalan'
                }
            ],
        ajax: {
            url: url,
            //api
           data: function (d) {
                return $.extend({},d,{
                    "radio_status": $('input[name=radio_status]:checked').val(),
                    "po_number": $('#po').val(),
                    "style": $('#style').val(),
                    "filterby": $('.dataTables_filter input').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            //$('td', row).eq(1).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
            {data: 'formulir_created', name: 'formulir_created'},
            {data: 'no_polybag', name: 'no_polybag'},
            {data: 'po_number_edit', name: 'po_number_edit'},
            {data: 'style_edit', name: 'style_edit'},
            {data: 'total_qty', name: 'total_qty'},
            {data: 'uoms', name: 'uoms'}
        ],
    });
    //end of datatables

    table
    .on( 'preDraw', function () {
        Pace.start();
        loading_process();
    } )
    .on( 'draw.dt', function () {
        Pace.stop();
        $('#table-list').unblock();
    } );

    $(document).on('click', '.printSuratJalan', function(event){
        event.preventDefault();
        var data = [];

        $('.clref:checked').each(function(){
            data.push({
                id: $(this).data('id')
            });
        });

        if(data.length <= 0){
            myalert('error', 'Please check data first..!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url: $('#get_nomor').attr('href'),
            success: function(response){
                $('#no_suratjalan').val(response);
                $('#modal_list_').modal('show');
            },
            error: function(response){
                return false;
            }
        });

    });

    // update uoms
    $('#table-list').on('blur', '.uoms_filled', function() {
        var id = $(this).data('id');
        var uoms = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updateUoms').attr('href'),
            data: {id:id, uoms:uoms},
            success: function(response){
                $('#uoms_' + id).html(response);
            },
            error: function(response) {
                return false;
            }
        });
    });

    //filter (api)
    $('#form_filter').submit(function(event) {
        event.preventDefault();

        //check 
        if($('#style').val() == '') {
            alert('Please input style first');
            return false;
        }
        
        loading_process();
        table.draw();
    });

    //button submit
    $('#btnSubmit').on('click', function(event){
        event.preventDefault();

        var no_suratjalan = $('#no_suratjalan').val();
        var no_suratjalan_info = $('#no_suratjalan_info').val();
        var subcont_id = $('#subcont_id').val();
        
        if($.trim(no_suratjalan) == '' || $.trim(no_suratjalan_info) == '' || $.trim(subcont_id) == '' ){
            myalert('error', 'Please completed all inputs..!');
            return false;
        }

        var data = [];

        $('.clref:checked').each(function(){
            data.push({
                id: $(this).data('headerid')
            });
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#completed_all').attr('href'),
            data: {no_suratjalan:no_suratjalan, no_suratjalan_info:no_suratjalan_info, subcont_id:subcont_id, data: JSON.stringify(data)},
            beforeSend: function() {
                $('.loading-area').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('.loading-area').unblock();
                printPage(data, no_suratjalan);
                $('#modal_list_').modal('hide');
            },
            error: function(response) {
                $('.loading-area').unblock();
                myalert('error','NOT GOOD');
            }
        });

    });

    //choose filter
    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'po') {
            if($('#filter_by_po').hasClass('hidden')) {
                $('#filter_by_po').removeClass('hidden');
            }

            $('#filter_by_style').addClass('hidden');
        }
        else if (this.value == 'style') {
            if($('#filter_by_style').hasClass('hidden')) {
                $('#filter_by_style').removeClass('hidden');
            }

            $('#filter_by_po').addClass('hidden');
        }
    });

    function printPage(data_id, no_suratjalan) {
        var url_print = $('#print_preview').attr('href');
        var data = [];

        $('.clref:checked').each(function() {
            data.push({
                id: $(this).data('headerid')
            });
        });

        if(data_id === null){
            var parameter = '?no_suratjalan='+no_suratjalan+'&data=' + JSON.stringify(data);
        }else{
            var parameter = '?no_suratjalan='+no_suratjalan+'&data=' + JSON.stringify(data_id);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : url_print + parameter,
            success: function(response) {
                window.open(url_print + parameter, '_blank');
                $('input:checkbox').removeAttr('checked');
                table.draw();
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
    }
    function printPageGlobal(no_suratjalan) {
        var url_print = $('#print_preview_global').attr('href');
        var data = [];
        
        var parameter = '?no_suratjalan='+no_suratjalan+'&data=' + JSON.stringify(data);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : url_print + parameter,
            success: function(response) {
                window.open(url_print + parameter, '_blank');
                $('input:checkbox').removeAttr('checked');
                table.draw();
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
    }

    // reprint
    $('.rePrint').on('click', function(){
        $('#modal_list_printed').modal('show');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : $('#get_suratjalan').attr('href'),
            beforeSend: function() {
                $('#modal_list_printed > .modal-content').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#modal_list_printed > .modal-content').unblock();
                $('#listing_printed').html(response);
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
    });

    $('#listing_printed').on('click', '#choose-suratjalan', function(){
        var no_suratjalan = $(this).data('suratjalan');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : $('#get_list').attr('href'),
            data: {no_suratjalan:no_suratjalan},
            beforeSend: function() {
                $('#modal_list_printed > .modal-content').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#modal_list_printed').unblock();
                var data = [];

                $.each(response, function(i){
                    data.push({
                        'id' : response[i].formulir_id
                    });
                });

                printPage(data, no_suratjalan);

            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
    });

    $('#listing_printed').on('click', '#choose-suratjalan-global', function(){
        var no_suratjalan = $(this).data('suratjalan');

        printPageGlobal(no_suratjalan);
    });

    //cancel surat jalan
    $('#listing_printed').on('click', '#choose-suratjalan-cancel', function(){
        var no_suratjalan = $(this).data('suratjalan');
        var no_suratjalaninfo = $(this).data('suratjalaninfo');

        bootbox.confirm("Are you sure cancel this delivery orders "+no_suratjalan+no_suratjalaninfo+" ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url : $('#cancel_suratjalan').attr('href'),
                    data: {no_suratjalan:no_suratjalan},
                    beforeSend: function() {
                        $('#modal_list_printed > .modal-content').block({
                            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: '10px 15px',
                                color: '#fff',
                                width: 'auto',
                                '-webkit-border-radius': 2,
                                '-moz-border-radius': 2,
                                backgroundColor: '#333'
                            }
                        });
                    },
                    success: function(response) {
                        $('#modal_list_printed > .modal-content').unblock();
                        myalert('success', 'GOOD..!');
                        $('#listing_printed').html(response);
                    },
                    error: function(response) {
                        myalert('error','NOT GOOD');
                    }
                });
            }
        });

    });

    function toggle(source) {
        checkboxes = document.getElementsByName('selector[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
          checkboxes[i].checked = source.checked;
        }
    }
    
    //get parameter from url
    function getURLParameter(url) {
        var params = {};
        url.substring(1).replace(/[?&]+([^=&]+)=([^&]*)/gi,
                function (str, key, value) {
                     params[key] = value;
                });
        return params;
    }

//});
</script>
@endsection
