@extends('layouts.app', ['active' => 'formulirReject'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold text-danger">Formulir Pengiriman Artwork Reject</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('packing.formulir') }}"><i class="icon-list position-left"></i> Formulir</a></li>
            <li class="active text-danger">Pengiriman Artwork Reject</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('packing.ajaxGetDataFormulirReject') }}" id="form_filter">
            <div class="form-group hidden" id="filter_by_style">
				<label><b>Choose Style</b></label>
				<div class="input-group col-md-12">
                    {{-- <input type="text" class="form-control text-uppercase" name="style" id="style" placeholder="Style"> --}}
                    <select data-placeholder="Select a State..." name="style" id="style" class="form-control select-search">
                        <option value=""></option>
                        @foreach($list_style as $list)
                            <option value="{{ $list->style }}">{{ $list->style }}</option>
                        @endforeach
                    </select>
                    <div class="input-group-btn hidden">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
			</div>
            <div class="form-group" id="filter_by_po">
				<label><b>Choose PO Number</b></label>
				<div class="input-group col-md-12">
                    <select data-placeholder="Select a State..." name="po" id="po" class="form-control select-search">
                        <option value=""></option>
                        @foreach($list_po as $list)
                            <option value="{{ $list->po_number }}">{{ $list->po_number }}</option>
                        @endforeach
                    </select>
                    <div class="input-group-btn hidden">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
			</div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="po">Filter by PO Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="style">Filter by Style</label>
            </div>
        </form>
        <div class="form-group">
            <button class="btn btn-danger rePrint">List Formulir</button>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body loading-area">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table-list">
                <thead>
                    <tr>
                        <th><input type="checkbox" onclick="toggle(this)"></th>
                        {{-- <th>STYLE</th> --}}
                        <th>PO</th>
                        <th>ARTICLE</th>
                        <th>COLOUR</th>
                        <th>CUTT</th>
                        <th>KOMPONEN</th>
                        <th>SIZE</th>
                        <th>BUNDEL</th>
                        <th>BALANCE</th>
                        {{--  <th>QTY</th>  --}}
                        {{--  <th>KETERANGAN</th>  --}}
                        <th>TYPE</th>
                        <th>STICKER REJECT</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('packing.updateDescription') }}" id="updateDescription"></a>
<a href="{{ route('packing.updateQtyUsed') }}" id="updateQtyUsed"></a>
<a href="{{ route('packing.updateTypePlReject') }}" id="updateTypePl"></a>
<a href="{{ route('packing.updateBarcodeReject') }}" id="updateBarcodeReject"></a>
<a href="{{ route('packing.ajaxSetCompletedAllFormulirReject') }}" id="completed_all_formulir"></a>
<a href="{{ route('packing.printPreviewFormulirReject') }}" id="print_preview_formulir"></a>
<a href="{{ route('packing.ajaxGetFormulirReject') }}" id="get_formulir"></a>
<a href="{{ route('packing.ajaxGetBarcode') }}" id="get_barcode"></a>
<a href="{{ route('packing.cancelFormulirReject') }}" id="cancel_formulir"></a>
<a href="{{ route('packing.ajaxGetNomorPolyBag') }}" id="get_nomor"></a>
<a href="{{ route('cutting.printPreviewByFormulirReject') }}" id="get_panel_formulir"></a>
@endsection

@section('page-modal')
 @include('packing._packing_reject');   
@endsection

@section('page-js')
<script type="text/javascript" src="{{ url('js/dataTables.buttons.min.js') }}"></script> 
<script type="text/javascript">

    $('#po, #style').select2({
        placeholder: "Select a State",
        minimumInputLength: 3,
        formatInputTooShort: function () {
            return "Enter Minimum 3 Character";
        },
    });

    $('#po, #style').on('change', function(){
        if($(this).val() != ''){
            $('#form_filter').submit();
        }
    });
//$(document).ready(function() {
    //url
    var url = $('#form_filter').attr('action');

    var _token = $("input[name='_token']").val();

    var table = $('#table-list').DataTable({
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        stateSave: true,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        buttons: [
                {
                    text: 'Print Formulir',
                    className: 'btn btn-sm bg-primary printFormulir'
                }
            ],
        ajax: {
            url: url,
            //api
           data: function (d) {
                return $.extend({},d,{
                    "radio_status": $('input[name=radio_status]:checked').val(),
                    "po_number": $('#po').val(),
                    "style": $('#style').val(),
                    "filterby": $('.dataTables_filter input').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
           // $('td', row).eq(0).html(value);
            $('td', row).eq(1).css('min-width', '120px');
            $('td', row).eq(9).css('min-width', '120px');
            $('td', row).eq(10).css('min-width', '150px');
        },
        drawCallback: function() {
            $('.bareject_filled').select2();
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
         //   {data: 'style', name: 'style'},
            {data: 'po_number_edit', name: 'po_number_edit', orderable: false, searchable: false},
            {data: 'article_edit', name: 'article_edit', orderable: false, searchable: false},
            {data: 'color_edit', name: 'color_edit', orderable: false, searchable: false},
            {data: 'cut_number', name: 'cut_number', orderable: false, searchable: false},
            {data: 'name', name: 'master_komponen.name'},
            {data: 'size_edit', name: 'size_edit', orderable: false, searchable: false},
            {data: 'sticker_no', name: 'sticker_no', orderable: false, searchable: false},
            {data: 'balance', name: 'balance', orderable: false, searchable: false},
            //{data: 'qty_used_draft', name: 'qty_used_draft'},
          //  {data: 'description', name: 'description'},
            {data: 'type_pl_draft', name: 'type_pl_draft', orderable: false, searchable: false},
            {data: 'barcode_reject', name: 'barcode_reject', orderable: false, searchable: false}
        ],
    });
    //end of datatables

    table
    .on( 'preDraw', function () {
        Pace.start();
        loading_process();
    } )
    .on( 'draw.dt', function () {
        Pace.stop();
        $('#table-list').unblock();
    } );

//    $('#table-list').on('click', 'input[type="checkbox"]', function() {
//        var row =  table.row($(this).closest('tr'));
//        table.cell({ row: row.index(), column: 0 } ).data( this.checked ? 1 : 0 )
//        row.invalidate().draw()
//    })

    $(document).on('click', '.printFormulir', function(event){
        event.preventDefault();
        var data = [];

        $('.clref:checked').each(function(){
            data.push({
                barcode_id: $(this).val()
            });
        });

        if(data.length <= 0){
            myalert('error', 'Please check data first..!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : $('#get_nomor').attr('href'),
            success: function(response) {
                $('#no_polybag').val(response);
                var table_checked = $('#show-result-checked > tbody');
                table_checked.empty();

                var data = Array();

				$('#table-list tbody tr').each(function(i, v) {

					data[i] = Array();
					$(this).children('td').each(function(ii, vv) {

						data[i][ii] = $(this).text();

						/*if (ii == 9) {
							data[i][ii] = $(this).find('input:not([type=hidden])').val();
						}*/
                        
                        if (ii == 9) { //
							data[i][ii] = $(this).find('select option:selected').text();
                        }
                        
                        if (ii == 10) { //
							data[i][ii] = $(this).find('#sticker_reject').val();
						}
                        
                        if (ii == 0) { //
							if( $(this).find('#Inputselector').is(':checked') ) {
                                data[i][ii] = 1;
                            }else{
                                data[i][ii] = 0;
                            }
						}

					});
                });
                
                $.each(data, function(indexx, val){
                    if(val[0] == 1){
                        if(val[10] == null){
                            myalert('error', 'Please input sticker reject first..!');
                            $('#modal_list_checked').modal('hidden');
                            return false;
                        }
                        table_checked.append('<tr>'
                                                +'<td>'+val[1]+'</td>'
                                                +'<td>'+val[2]+'</td>'
                                                +'<td>'+val[3]+'</td>'
                                                +'<td>'+val[4]+'</td>'
                                                +'<td>'+val[6]+'</td>'
                                                +'<td>'+val[7]+'</td>'
                                                +'<td>'+val[9]+'</td>'
                                                +'<td>'+val[10]+'</td>'
                                               // +'<td>'+val[11]+'</td>'
                                              //  +'<td>'+val[12]+'</td>'
                                            +'</tr>');
                    }
                });

                $('#modal_list_checked').modal('show');
            },
            error: function(response) {
                myalert('error', response.responseJSON.message);
            }
        });

    });

    //button submit
    $('#btnSubmit').on('click', function(event){
        event.preventDefault();

        var no_polybag = $('#no_polybag').val();

        var data = [];

        $('.clref:checked').each(function(){
            data.push({
                barcode_id: $(this).val()
            });
        });

        if(data.length <= 0){
            myalert('error', 'Please check data first..!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#completed_all_formulir').attr('href'),
            data: {data: JSON.stringify(data), no_polybag: no_polybag},
            beforeSend: function() {
                $('.loading-area').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('.loading-area').unblock();

                $('#modal_list_checked').modal('hide');

                printPage(response);
            },
            error: function(response) {
                $('.loading-area').unblock();
                //myalert('error','NOT GOOD');
                myalert('error', response.responseJSON.message);
            }
        });

    });

    // update description
    $('#table-list').on('blur', '.desc_filled', function() {
        var barcodeid = $(this).data('barcode');
        var description = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updateDescription').attr('href'),
            data: {barcode_id:barcodeid, description:description},
            success: function(response){
                $('#desc_' + barcodeid).html(response);

            },
            error: function(response) {
                return false;
            }
        });
    });

    // update type pl
    $('#table-list').on('blur', '.typepl_filled', function() {
        var barcodeid = $('option:selected', this).data('barcode');
        var type_pl_draft = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updateTypePl').attr('href'),
            data: {barcode_id:barcodeid, type_pl_draft:type_pl_draft},
            success: function(response){
                $('#typepl_' + barcodeid).html(response);
            },
            error: function(response) {
                return false;
            }
        });
    });

    // update barcode reject
    $('#table-list').on('change.select2', '.bareject_filled', function() {
        //var barcodeid = $('option:selected', this).data('barcode');
        var barcodeid = $(this).select2().find(":selected").data("barcode");
        var sticker_from = $(this).select2().find(":selected").data("from");
        var sticker_to = $(this).select2().find(":selected").data("to");
        var barcode_reject = $(this).val();

        console.log(barcode_reject);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updateBarcodeReject').attr('href'),
            data: {barcode_id:barcodeid, barcode_reject:barcode_reject, sticker_from:sticker_from, sticker_to:sticker_to},
            success: function(response){
                console.log(response);
                //$('#bareject_' + barcodeid).html(response);
            },
            error: function(response) {
                return false;
            }
        });
    });

    //filter (api)
    $('#form_filter').submit(function(event) {
        event.preventDefault();

        //check 
        if($('input[name=radio_status]:checked').val() == 'po'){
            
            if($('#po').val() == '') {
                alert('Please input po number first');
                return false;
            }
        }else if($('input[name=radio_status]:checked').val() == 'style'){
            if($('#style').val() == '') {
                alert('Please input style first');
                return false;
            }
        }

        table.draw();
    })

    //choose filter
    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'po') {
            if($('#filter_by_po').hasClass('hidden')) {
                $('#filter_by_po').removeClass('hidden');
            }

            $('#filter_by_style').addClass('hidden');
        }
        else if (this.value == 'style') {
            if($('#filter_by_style').hasClass('hidden')) {
                $('#filter_by_style').removeClass('hidden');
            }

            $('#filter_by_po').addClass('hidden');
        }

        table.draw();
    });

    function printPage(formulir) {
        var url_print = $('#print_preview_formulir').attr('href');
        var data = [];

        $('.clref:checked').each(function() {
            data.push({
                barcode_id: $(this).val()
            });
        });

        //if(formulir === null){
        //    var parameter = '?data=' + JSON.stringify(data);
        //}else{
        //    var parameter = '?data=' + JSON.stringify(formulir);
        //}
        var parameter = '?data=' + formulir;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : url_print + parameter,
            success: function(response) {
                window.open(url_print + parameter, '_blank');
                $('input:checkbox').removeAttr('checked');
                table.draw();
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
    }

    // reprint
    $('.rePrint').on('click', function(){
        $('#modal_list_').modal('show');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : $('#get_formulir').attr('href'),
            beforeSend: function() {
                $('#modal_list_ > .modal-content').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#modal_list_ > .modal-content').unblock();
                $('#listing').html(response);
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
    });

    $('#listing').on('click', '#choose-formulir', function(){
        var date = $(this).data('date');
        var po = $(this).data('po');
        var style = $(this).data('style');
        var id = $(this).data('id');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : $('#get_barcode').attr('href'),
            data: {date:date, po:po, style:style, id:id},
            beforeSend: function() {
                $('#modal_list_ > .modal-content').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#modal_list_').unblock();
                var data = [];

               // $.each(response, function(i){
               //     data.push({
               //         'barcode_id' : response[i].barcode_id
               //     });
               // });

                printPage(id);

            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
    });
    $('#listing').on('click', '#choose-formulir-cancel', function(){
        var date = $(this).data('date');
        var po = $(this).data('po');
        var style = $(this).data('style');
        var id = $(this).data('id');

        bootbox.confirm("Are you sure cancel this formulir ?", function (result) {
            if (result) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url : $('#cancel_formulir').attr('href'),
                    data: {date:date, po:po, style:style, id:id},
                    beforeSend: function() {
                        $('#modal_list_ > .modal-content').block({
                            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: '10px 15px',
                                color: '#fff',
                                width: 'auto',
                                '-webkit-border-radius': 2,
                                '-moz-border-radius': 2,
                                backgroundColor: '#333'
                            }
                        });
                    },
                    success: function(response) {
                        $('#modal_list_ > .modal-content').unblock();
                        myalert('success', 'GOOD');
                        $('#listing').html(response);
        
                    },
                    error: function(response) {
                        //myalert('error','NOT GOOD');
                        myalert('error', response['responseJSON']);
                    }
                });

            }

        });

    });

    $('#listing').on('click', '#formulir-barcode', function(){
        var date = $(this).data('date');
        var po = $(this).data('po');
        var style = $(this).data('style');
        var id = $(this).data('id');

        var url_panel = $('#get_panel_formulir').attr('href');
        var paper = $('input[type=radio][name=radio_status_paper]:checked').val();
        
        var parameter = '?formulir_id='+id+'&paper='+paper;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : url_panel + parameter,
            beforeSend: function() {
                $('#modal_list_ > .modal-content').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#modal_list_').unblock();
                
                window.open(url_panel + parameter, '_blank');

            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
    });

    function upNumber(self){
        var balance = $(self).parent().find('input[name="txtBalance"]').val();

        if (parseInt(self.value) >parseInt(balance)) {
			myalert('error', 'Qty not accepted..!!');
			self.value = balance;

			return false;
		}
    }

    function doneNumber(self){
        var balance = $(self).parent().find('input[name="txtBalance"]').val();

        if ($.trim(self.value) == '') {
			myalert('error', 'Please enter Qty..!!');
            self.focus();

			return false;
        }

        var barcodeid = $(self).data('barcode');
        var qty_used_draft = self.value;
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updateQtyUsed').attr('href'),
            data: {barcode_id:barcodeid, qty_used_draft:qty_used_draft},
            success: function(response){
                $('#balance_' + barcodeid).html(response);
            },
            error: function(response) {
                return false;
            }
        });
    }

    function toggle(source) {
        checkboxes = document.getElementsByName('selector[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
          checkboxes[i].checked = source.checked;
        }
    }
    
    //get parameter from url
    function getURLParameter(url) {
        var params = {};
        url.substring(1).replace(/[?&]+([^=&]+)=([^&]*)/gi,
                function (str, key, value) {
                     params[key] = value;
                });
        return params;
    }

//});
</script>
@endsection
