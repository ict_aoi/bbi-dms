<div id="modal_list_checked" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title" id="title-formulir"><i class="position-left"></i>Create Formulir</h5>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="no_polybag" class="control-label col-sm-2">No Polybag</label>
                                <div class="col-sm-10">
                                    <input type="text" name="no_polybag" id="no_polybag" class="form-control text-uppercase" onkeypress="return isNumberKey(event)" required>
                                    {{--  onkeypress="return isNumberKey(event)"  --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table" id="show-result-checked">
                                    <thead>
                                        <tr>
                                            <th>Style</th>
                                            <th>PO</th>
                                            <th>Article</th>
                                            <th>Color</th>
                                            <th>Cutt</th>
                                            <th>Size</th>
                                            <th>Bundel</th>
                                            <th>Qty</th>
                                            {{--  <th>Ket</th>  --}}
                                            <th>Type</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="btnSubmit">Submit</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_list_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title" id="title-formulir"><i class="position-left"></i>Choose Formulir</h5>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <fieldset>
                        <div id="listing" class="col-lg-12">
                            <!-- template pilihan -->
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>