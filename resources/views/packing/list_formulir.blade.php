<div class="table-responsive">
    <div class="pull-right">
        <label class="radio-inline"><input type="radio" name="radio_status_paper" checked="checked" value="a3">Paper A3</label>
        <label class="radio-inline"><input type="radio" name="radio_status_paper" value="a4">Paper A4</label>
    </div>
    <table class="table" id="table-list-formulir">
        <thead>
            <tr>
                <th>Date</th>
                <th>No Polybag</th>
                <th>PO</th>
                <th>Style</th>
                <th>Qty</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $key => $val)
                <tr>
                    <td width="15%">{{ $val->formulir_created }}</td>
                    <td>{{ $val->no_polybag }}</td>
                    <td>{{ $val->po_number_edit }}</td>
                    <td>{{ $val->style }}</td>
                    <td>{{ $val->total }}</td>
                    <td width="30%">
                        @if(Auth::user()->admin_role)
                        <button type="button" id="choose-formulir-cancel"
                                data-id="{{ $val->formulir_artwork_header_id }}"
                                data-date="{{ $val->formulir_created }}"
                                data-po="{{ $val->po_number }}"
                                data-style="{{ $val->style }}"
                                class="btn btn-default" title="cancel">
                                <i class="icon-database-remove"></i>
                        </button>
                        @endif
                        <button type="button" id="choose-formulir"
                                data-id="{{ $val->formulir_artwork_header_id }}"
                                data-date="{{ $val->formulir_created }}"
                                data-po="{{ $val->po_number }}"
                                data-style="{{ $val->style }}"
                                class="btn btn-default" title="print">
                                <i class="icon-printer2"></i>
                        </button>
                        <button type="button" id="formulir-barcode"
                                data-id="{{ $val->formulir_artwork_header_id }}"
                                data-date="{{ $val->formulir_created }}"
                                data-po="{{ $val->po_number }}"
                                data-style="{{ $val->style }}"
                                class="btn btn-default" title="print panel">
                                <i class="icon-ship"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<script type="text/javascript">
    var table_ = $('#table-list-formulir').DataTable({
        "lengthChange": false,
        "pageLength": 5
       /* "columnDefs": [{
            "targets" : [0],
            "visible" : false,
            "searchable" : false
        }]*/
    });
</script>