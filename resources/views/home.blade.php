@extends('layouts.app', ['active' => 'dashboard'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Dashboard</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
    <div class="row">
        <div class="form-group col-md-12 hidden">
            <label><b>FILTER BY FREQUENCY</b> </label>
            <select data-placeholder="select trigger label" id="frequency" class="select form-control">
                <option value="daily">DAILY</option>
                <option value="weekly">WEEKLY</option>
                <option value="monthly">MONTHLY</option>
                <option value="yearly" selected="selected">YEARLY</option>
            </select>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="panel panel-body">
                <div class="media no-margin">
                    <div class="media-left media-middle">
                        <i class="icon-dropbox icon-3x text-success-400"></i>
                    </div>
    
                    <div class="media-body text-right">
                        <h3 class="no-margin text-semibold" id="total_preparation">{{ isset($count_preparation) ? $count_preparation : 0 }}</h3>
                        <span class="text-uppercase text-size-mini text-muted">Total Packing</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="panel panel-body">
                <div class="media no-margin">
                    <div class="media-left media-middle">
                        <i class="icon-office icon-3x text-indigo-400"></i>
                    </div>
    
                    <div class="media-body text-right">
                        <h3 class="no-margin text-semibold" id="total_sewing">{{ isset($count_sewing) ? $count_sewing : 0 }}</h3>
                        <span class="text-uppercase text-size-mini text-muted">Total Sewing</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="panel panel-body">
                <div class="media no-margin">
                    <div class="media-left media-middle">
                        <i class="icon-location4 icon-3x text-blue-400"></i>
                    </div>
    
                    <div class="media-body text-right">
                        <h3 class="no-margin text-semibold" id="total_inventory">{{ isset($count_inventory) ? $count_inventory : 0 }}</h3>
                        <span class="text-uppercase text-size-mini text-muted">Total Inventory</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="panel panel-body">
                <div class="media no-margin">
                    <div class="media-left media-middle">
                        <i class="icon-search4 icon-3x text-danger-400"></i>
                    </div>
    
                    <div class="media-body text-right">
                        <h3 class="no-margin text-semibold" id="total_qcinspect">{{ isset($count_qcinspect) ? $count_qcinspect : 0 }}</h3>
                        <span class="text-uppercase text-size-mini text-muted">Total QC Inspect</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-offset-6 col-md-offset-9 col-sm-6 col-md-3">
            <div class="panel panel-body">
                <div class="media no-margin">
                    <div class="media-left media-middle">
                        <i class="icon-truck icon-3x text-default-400"></i>
                    </div>
    
                    <div class="media-body text-right">
                        <h3 class="no-margin text-semibold" id="total_shipping">{{ isset($count_shipping) ? $count_shipping : 0 }}</h3>
                        <span class="text-uppercase text-size-mini text-muted">Total Shipping</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- TABLE -->
    <div class="panel panel-flat">
        <div class="panel-body">
            <form action="#" id="form_filter">
                <div class="form-group" id="filter_by_po">
                    <label><b>Choose PO Number</b></label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="po" id="po" placeholder="PO Number" value="">
                        <div class="input-group-btn">
                            <button type="button" id='filterPo' class="btn btn-primary">Filter</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel panel-flat">
            <div class="panel-heading">
                <div class="col-md-12" style="float:right">
                    <span class="label label-roundless label-danger" style="float:right">REJECTED</span>
                    <span class="label label-roundless label-primary" style="float:right">ON PROGRESS</span>
                    <span class="label label-roundless label-success" style="float:right">COMPLETED</span>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table datatable-button-html5-basic" id="table_po">
                        <thead>
                            <tr>
                                <th rowspan="2">#</th>
                                <th rowspan="2">PO NUMBER</th>
                                <th rowspan="2">PLAN REF NUMBER</th>
                                <th rowspan="2">TOTAL PACKAGE</th>
                                <th colspan="5" class="text-center">STATUS</th>
                                <th rowspan="2">ACTION</th>
                            </tr>
                            <tr>
                                <th style="text-align:center" class="col-xs-4">PREPARATION</th>
                                <th style="text-align:center" class="col-xs-4">SEWING</th>
                                <th style="text-align:center" class="col-xs-4">INVENTORY</th>
                                <th style="text-align:center" class="col-xs-4">QC INSPECT</th>
                                <th style="text-align:center" class="col-xs-4">SHIPPING</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
    </div>
@endsection

@section('page-modal')

@endsection

@section('page-js')
@endsection
