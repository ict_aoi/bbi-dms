<!-- MODAL DETAIL -->
<div id="modal_detail_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title" id="title-line-number"><i class="position-left"></i>Detail Surat Jalan</h5> :: <span id="title_detailx"></span>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <fieldset>
                        <div id="list" class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table" id="detailSuratJalan">
                                    <thead>
                                        <tr>
                                            <th>Style</th>
                                            <th>No polybag</th>
                                            <th>PO</th>
                                            <th>Article</th>
                                            <th>Color</th>
                                            <th>Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /MODAL DETAIL -->

<!-- MODAL ADD -->
<div id="modal_update_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('suratjalan.updateBcNo') }}" id="form-update" enctype="multipart/form-data">
			@csrf
            <div class="modal-content">
              	<div class="modal-body">
                    <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                  	<div class="panel-body loader-area">
                      	<fieldset>
                          	<legend class="text-semibold">
                              	<i class="icon-file-text2 position-left"></i>
                              	<span id="title"> Update Document Bea Cukai </span> :: <span id="title_detail"></span> <!-- title -->
                          	</legend>
                            <input type="hidden" name="no_suratjalan_update" id="no_suratjalan_update" class="form-control">
                            <div class="form-group">
                                <label class="col-lg-3 control-label text-semibold">No BC:</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="bc_no" id="bc_no" placeholder="No Bea Cukai">
                                </div>
                            </div>
                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">Type BC:</label>
                              	<div class="col-lg-9">
                                      <select data-placeholder="Select a State..." class="form-control select-search" name="type_bc" id="type_bc">
                                        <option value=""></option>
										@foreach($type_bc as $item)
											<option value="{{ $item }}">{{ $item }}</option>
										@endforeach
									</select>
                              	</div>
                          	</div>
                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">No Aju:</label>
                              	<div class="col-lg-9">
                                  	<input type="text" class="form-control" name="no_aju" id="no_aju" placeholder="No Aju">
                              	</div>
                          	</div>
                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">No Pendaftaran:</label>
                              	<div class="col-lg-9">
                                  	<input type="text" class="form-control" name="no_daftar_bc" id="no_daftar_bc" placeholder="No Pendaftaran BC">
                              	</div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label text-semibold">Tgl Dokumen:</label>
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                        <input type="text" class="form-control pickadate" placeholder="Tgl Dokumen" id="document_date" name="document_date">
                                    </div>
                                </div>
                            </div>
                          	<hr>
                          	<div class="form-group text-center">
                          		<button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
                          		<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                          	</div>
                      	</fieldset>
                  	</div>
              	</div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL ADD -->