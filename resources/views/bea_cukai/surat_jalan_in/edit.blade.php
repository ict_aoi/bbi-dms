@extends('layouts.app', ['active' => 'masterkomponen'])

@section('page-content')
<br>
<div class="breadcrumb-line breadcrumb-line-component">
    <ul class="breadcrumb">
        <li><a href="{{ route('suratjalanin.suratJalanIn') }}"><i class="icon-home2 position-left"></i>Edit Surat Jalan In</a></li>
    </ul>
</div>

<section class="panel">
	<div class="panel-body loader-area">
        <form action="{{ route('suratjalanin.updateSuratJalanIn') }}" id="main-form" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <input type="text" name="id" class="hidden" value="{{ $delivery_in->id }}" readonly>
            <div class="form-group">
                <label class="col-lg-3 control-label text-semibold">No Surat Jalan:</label>
                <div class="col-lg-9">
                    <input type="text" class="form-control" name="no_suratjalan" id="no_suratjalan" placeholder="No Surat Jalan">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label text-semibold">Subcont:</label>
                <div class="col-lg-9">
                    <select name="subcont_id" id="subcont_id" class="form-control" required>
                        <option value=""></option>
                        @foreach ($subcont as $key => $val)
                            <option value="{{ $val->id }}">{{ $val->name }}</option>
                        @endforeach   
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label text-semibold">No BC:</label>
                <div class="col-lg-9">
                    <input type="text" class="form-control" name="bc_no" id="bc_no" placeholder="No Bea Cukai">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label text-semibold">Type BC:</label>
                <div class="col-lg-9">
                    <select data-placeholder="Select a State..." class="form-control select-search" name="type_bc" id="type_bc">
                    <option value=""></option>
                    @foreach($type_bc as $item)
                        <option value="{{ $item }}">{{ $item }}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label text-semibold">No Aju:</label>
                <div class="col-lg-9">
                    <input type="text" class="form-control" name="no_aju" id="no_aju" placeholder="No Aju">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label text-semibold">No Pendaftaran:</label>
                <div class="col-lg-9">
                    <input type="text" class="form-control" name="no_daftar_bc" id="no_daftar_bc" placeholder="No Pendaftaran BC">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label text-semibold">Tgl Dokumen:</label>
                <div class="col-lg-9">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                        <input type="text" class="form-control pickadate" placeholder="Tgl Dokumen" id="document_date" name="document_date">
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-6">
                    <div class="text-center loader-area">
                        <button type="button" class="btn btn-success save-data" name="update">SAVE <i class="icon-floppy-disk position-right"></i></button>
                        <a class="btn btn-default" href="javascript:history.back()">Close <i class="icon-reload-alt position-right"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function () {

    var fd = $("#main-form").serializeArray();
    var url = $("#main-form").attr("href");
    $(".save-data").on('click', function(event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#main-form').attr('action'),
            data: $('#main-form').serialize(),
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $('.loader-area').unblock();
            },
            success: function(response) {
                console.log(response);
                myalert('success','GOOD');
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        })
    });

});
</script>
@endsection
