@extends('layouts.app', ['active' => 'suratjalanin'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Surat Jalan In</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('suratjalanin.suratJalanIn') }}"><i class="icon-home4 position-left"></i> Surat Jalan</a></li>
            <li class="active">Surat Jalan In</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	<div class="panel panel-flat">
	    <div class="panel-heading">
	        <div class="form-group">
	            <button type="button" class="btn btn-xs btn-primary add_new">Add Surat Jalan In <span class="icon-plus2"></span></button>
	        </div>
	    </div>
	    <div class="panel-body">
            <div class="table-responsive">
    	        <table class="table datatable-save-state" id="table_list">
    	            <thead>
    	                <tr>
    	                    <th style="width:10px;">#</th>
    	                    <th>Date</th>
    	                    <th>No. Surat Jalan</th>
    	                    <th>Subcont</th>
    	                    <th>No BC</th>
    	                    <th>Type BC</th>
    	                    <th>No Aju</th>
    	                    <th>No Pendaftaran</th>
    	                    <th>Tgl Dokumen</th>
    	                    <th>Qty</th>
    	                    <th style="width:10px;">ACTION</th>
    	                </tr>
    	            </thead>
    	        </table>
            </div>
	    </div>
	</div>

	<a href="{{ route('suratjalanin.ajaxGetDataSuratJalanIn') }}" id="get_data"></a>
@endsection

@section('page-modal')
    @include('bea_cukai.surat_jalan_in._index_modal')
    @include('bea_cukai.surat_jalan_in._update_modal')
@endsection

@section('page-js')
<script type="text/javascript">
$( document ).ready(function() {
    //datatables
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var url = $('#get_data').attr('href');
    var table = $('#table_list').DataTable({
        ajax: url,
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false  },
            {data: 'created_at', name: 'created_at'},
            {data: 'no_suratjalan', name: 'no_suratjalan'},
            {data: 'subcont_name', name: 'subcont_name'},
            {data: 'bc_no', name: 'bc_no'},
            {data: 'type_bc', name: 'type_bc'},
            {data: 'no_aju', name: 'no_aju'},
            {data: 'no_daftar_bc', name: 'no_daftar_bc'},
            {data: 'document_date', name: 'document_date'},
            {data: 'qty', name: 'qty'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
    //end of datatables

    //add
    $('.add_new').on('click',function(){
        $('#modal_add_new_').modal('show')
    });
    //end of add

    $('#modal_add_new_').on('shown.bs.modal', function(){
        $('input[name=name]').focus();
    });

    //delete
    $("#table_list").on("click", ".delete", function() {
        event.preventDefault();
        var id = $(this).data('id');
        if(id == 'kosong') {
            return false;
        }
        var token = $(this).data("token");
        bootbox.confirm("Are you sure delete this row ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "surat-jalan-in/delete/"+id,
                    type: "GET",
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": token,
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $(".loader-area").unblock();
                    },
                    success: function () {
                        myalert('success','Data has been deleted');
                        table.ajax.reload();
                    }
                });
            }
        });
    });
    //end of delete

    //add new
    $('#form-add').submit(function(event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-add').attr('action'),
            data: $('#form-add').serialize(),
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
            	$('.loader-area').unblock();
            },
            success: function(response) {
                myalert('success','GOOD');
                $('#form-add').trigger("reset");
                $('#modal_add_new_').trigger('toggle');
                $('#subcont_id').val('').trigger('change');
                $('#type_bc').val('').trigger('change');
                table.ajax.reload();
            },
            error: function(response) {
                myalert('error',response['responseJSON']);
            }
        })
    });

    //update
    $('#form-update').submit(function(event) {
        event.preventDefault();

        var formData = new FormData($(this)[0]);
        var parameter = ['id_update', 'no_suratjalan_update', 'subcont_id_update', 'bc_no_update', 'type_bc_update', 'no_aju_update', 'no_daftar_bc_update', 'document_date_update', 'qty_update'];
        var val = [
            $('#id_update').val(), 
            $('#no_suratjalan_update').val(),
            $('#subcont_id_update').val(),
            $('#bc_no_update').val(),
            $('#type_bc_update').val(),
            $('#no_aju_update').val(),
            $('#no_daftar_bc_update').val(),
            $('#document_date_update').val(),
            $('#qty_update').val()
        ]
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-update').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function(response) {
                myalert('success','GOOD');
                $('#no_suratjalan_update').focus();
                table.ajax.reload();
            },
            error: function(response) {
                myalert('error',response['responseJSON']);
            }
        })
    });
});

function edit(url){

    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();

        }
    })
    .done(function (response) {

        var document_date = response.document_date ? moment(response.document_date).format('DD MMMM, YYYY') : null;

        $('#id_update').val(response.id);
        $('#no_suratjalan_update').val(response.no_suratjalan);
        $('#subcont_id_update').val(response.subcont_id).trigger('change');
        $('#bc_no_update').val(response.bc_no).trigger('change');
        $('#type_bc_update').val(response.type_bc).trigger('change');
        $('#no_aju_update').val(response.no_aju);
        $('#no_daftar_bc_update').val(response.no_daftar_bc);
        $('#document_date_update').val(document_date).trigger('change');
        $('#qty_update').val(response.qty);

        $('#modal_edit_').modal();

    });
}

</script>
@endsection
