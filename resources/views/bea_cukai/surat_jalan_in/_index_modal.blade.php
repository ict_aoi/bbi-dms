<!-- MODAL ADD KOMPONEN -->
<div id="modal_add_new_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('suratjalanin.addSuratJalanIn') }}" id="form-add">
            <div class="modal-content">
              	<div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                  	<div class="panel-body loader-area">
						<fieldset>
							<legend class="text-semibold">
								<i class="icon-file-text2 position-left"></i>
								<span id="title"> Inser Document Bea Cukai In</span> :: <span id="title_detail"></span> <!-- title -->
							</legend>
								<div class="form-group">
									<label class="col-lg-3 control-label text-semibold">No Surat Jalan:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="no_suratjalan" id="no_suratjalan" placeholder="No Surat Jalan">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-3 control-label text-semibold">Subcont:</label>
									<div class="col-lg-9">
										<select name="subcont_id" id="subcont_id" data-placeholder="Select a State..." class="form-control select-search">
											<option value=""></option>
											@foreach ($subcont as $key => $val)
												<option value="{{ $val->id }}">{{ $val->name }}</option>
											@endforeach   
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label text-semibold">No BC:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="bc_no" id="bc_no" placeholder="No Bea Cukai">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label text-semibold">Type BC:</label>
									<div class="col-lg-9">
										<select data-placeholder="Select a State..." class="form-control select-search" name="type_bc" id="type_bc">
										<option value=""></option>
										@foreach($type_bc as $item)
											<option value="{{ $item }}">{{ $item }}</option>
										@endforeach
									</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label text-semibold">No Aju:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="no_aju" id="no_aju" placeholder="No Aju">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label text-semibold">No Pendaftaran:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="no_daftar_bc" id="no_daftar_bc" placeholder="No Pendaftaran BC">
									</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label text-semibold">Tgl Dokumen:</label>
								<div class="col-lg-9">
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-calendar"></i></span>
										<input type="text" class="form-control pickadate" placeholder="Tgl Dokumen" id="document_date" name="document_date">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label text-semibold">Qty:</label>
								<div class="col-lg-9">
									<div class="input-group">
										<input type="text" class="form-control" name="qty" id="qty" placeholder="Qty" onkeypress="return isNumberKey(event);">
									</div>
								</div>
							</div>
							<hr>
							<div class="form-group text-center">
								<button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
							</div>
						</fieldset>
                  	</div>
              	</div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL ADD KOMPONEN -->