<!-- MODAL EDIT -->
<div id="modal_edit_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('suratjalanin.updateSuratJalanIn') }}" id="form-update" enctype="multipart/form-data">
			@csrf
            <div class="modal-content">
              	<div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                  	<div class="panel-body loader-area">
                      	<fieldset>
                          	<legend class="text-semibold">
                              	<i class="icon-file-text2 position-left"></i>
                              	<span id="title"> UPDATE SURAT JALAN IN</span> <!-- title -->
                              </legend>
                              
                              <input type="hidden" class="form-control" name="id_update" id="id_update" placeholder="" readonly>

								<div class="form-group">
									<label class="col-lg-3 control-label text-semibold">No Surat Jalan:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="no_suratjalan_update" id="no_suratjalan_update" placeholder="No Surat Jalan">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-3 control-label text-semibold">Subcont:</label>
									<div class="col-lg-9">
										<select name="subcont_id_update" id="subcont_id_update" data-placeholder="Select a State..." class="form-control select-search">
											<option value=""></option>
											@foreach ($subcont as $key => $val)
												<option value="{{ $val->id }}">{{ $val->name }}</option>
											@endforeach   
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label text-semibold">No BC:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="bc_no_update" id="bc_no_update" placeholder="No Bea Cukai">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label text-semibold">Type BC:</label>
									<div class="col-lg-9">
										<select data-placeholder="Select a State..." class="form-control select-search" name="type_bc_update" id="type_bc_update">
										<option value=""></option>
										@foreach($type_bc as $item)
											<option value="{{ $item }}">{{ $item }}</option>
										@endforeach
									</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label text-semibold">No Aju:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="no_aju_update" id="no_aju_update" placeholder="No Aju">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label text-semibold">No Pendaftaran:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="no_daftar_bc_update" id="no_daftar_bc_update" placeholder="No Pendaftaran BC">
									</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label text-semibold">Tgl Dokumen:</label>
								<div class="col-lg-9">
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-calendar"></i></span>
										<input type="text" class="form-control pickadate" placeholder="Tgl Dokumen" id="document_date_update" name="document_date_update">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label text-semibold">Qty:</label>
								<div class="col-lg-9">
									<div class="input-group">
										<input type="text" class="form-control" name="qty_update" id="qty_update" placeholder="Qty" onkeypress="return isNumberKey(event);">
									</div>
								</div>
							</div>
                          	<hr>
                          	<div class="form-group text-center">
                          		<button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
                          		<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                          	</div>
                      	</fieldset>
                  	</div>
              	</div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL EDIT -->