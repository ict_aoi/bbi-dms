@extends('layouts.app', ['active' => 'updateSuratJalan'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Update Surat Jalan</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home4 position-left"></i> Update</a></li>
            <li class="active">Surat Jalan</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('suratjalan.ajaxGetDataSuratJalan') }}" id="form_filter">
            @csrf
            <div class="form-group" id="filter_by_date">
				<label><b>Select Date</b></label>
				<div class="input-group">
					<span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range" required>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
            </div>
            <div class="form-group hidden" id="filter_by_sj">
				<label><b>Enter Nomor SJ</b></label>
				<div class="input-group">
                    <select data-placeholder="Select a State..." name="no_suratjalan" id="no_suratjalan" class="form-control select-search">
                        <option value=""></option>
                        @foreach($list_sj as $list)
                            <option value="{{ $list->no_suratjalan }}">{{ $list->no_suratjalan_concat }}</option>
                        @endforeach
                    </select>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="date">Filter by Date</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="sj">Filter by Nomor SJ</label>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-2"> Factory :
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->id }}" {{ $val->id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table datatable-button-html5-basic" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>No.Surat Jalan</th>
                        <th>No. BC</th>
                        <th>Subcont</th>
                        <th>Type BC</th>
                        <th>No Aju</th>
                        <th>No Pendaftaran</th>
                        <th>Tgl Dokumen</th>
                        <th style="width: 10px;">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('suratjalan.exportSuratJalan') }}" id="export_data"></a>
<a href="{{ route('suratjalan.editBcNo') }}" id="editBcNo"></a>
<a href="{{ route('suratjalan.ajaxGetDetailSuratJalan') }}" id="get_detail_suratjalan"></a>
@endsection

@section('page-modal')
    @include('bea_cukai._index_modal')
@endsection

@section('page-js')
<script src="{{ url('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    //url
    var url = $('#form_filter').attr('action');
    var date = $('#date_range').val();
    var filter = null;

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        lengthMenu: [[10, 25, 50, 100, -1],[10, 25, 50, 100, "All"]],
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#export_data').attr('href')
                                            + '?date_range=' + date
                                            + '&orderby=' + column_name
                                            + '&direction=' + direction
                                            + '&filterby=' + filter
                                            + '&radio_status=' + $('input[name=radio_status]:checked').val()
                                            + '&no_suratjalan=' + $('#no_suratjalan').val()
                                            + '&factory_id=' + $('#factory_id').val();
                }
            }
       ],
        ajax: {
            url: url,
            type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "date_range": $('#date_range').val(),
                    "radio_status" : $('input[name=radio_status]:checked').val(),
                    "no_suratjalan" : $('#no_suratjalan').val(),
                    "factory_id": $('#factory_id').val(),
                    "filterby": $('.dataTables_filter input').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
           // $('td', row).eq(1).css('min-width', '150px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
            {data: 'no_suratjalan_concat', name: 'no_suratjalan_concat'},
            {data: 'bc_no', name: 'bc_no'},
            {data: 'subcont_name', name: 'subcont_name'},
            {data: 'type_bc', name: 'type_bc'},
            {data: 'no_aju', name: 'no_aju'},
            {data: 'no_daftar_bc', name: 'no_daftar_bc'},
            {data: 'document_date', name: 'document_date'},
            {data: 'action', name: 'action', sortable: false, orderable: false, searchable: false}
        ],
    });
    //end of datatables

    table
    .on( 'preDraw', function () {
        Pace.start();
    } )
    .on( 'draw.dt', function () {
        $('#table-list').unblock();
        Pace.stop();
    } );

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        //check location
        if($('#date_range').val() == '') {
            alert('Please select date range first');
            return false;
        }

        loading_process();

        table.draw();
    })

    $('#factory_id').on('change', function() {

        loading_process();
        table.draw();
    });

    //
    $('#date_range').on('change', function(){
        date = $(this).val();
    })

     //choose filter
     $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'sj') {
            if($('#filter_by_sj').hasClass('hidden')) {
                $('#filter_by_sj').removeClass('hidden');
            }

            $('#filter_by_date').addClass('hidden');
        }
        else if (this.value == 'date') {
            if($('#filter_by_date').hasClass('hidden')) {
                $('#filter_by_date').removeClass('hidden');
            }

            $('#filter_by_sj').addClass('hidden');
        }

        loading_process();
        table.draw();
    });

    // update no bc
    $('#table-list').on('blur', '.bc_filled', function() {
        var no_suratjalan = $(this).data('id');
        var bc_no = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updateBcNo').attr('href'),
            data: {no_suratjalan:no_suratjalan, bc_no:bc_no},
            success: function(response){
                $('#bc_' + no_suratjalan).html(response);

            },
            error: function(response) {
                return false;
            }
        });
    });

    //update
    $('#form-update').submit(function(event) {
        event.preventDefault();

        var formData = new FormData($(this)[0]);
        var parameter = ['no_suratjalan', 'type_bc', 'no_aju', 'no_daftar_bc', 'document_date', 'bc_no'];
        var val = [
            $('#no_suratjalan_update').val(), 
            $('#type_bc').val(), 
            $('#no_aju').val(), 
            $('#no_daftar_bc').val(), 
            $('#document_date').val(),
            $('#bc_no').val()
        ]
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-update').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
            },
            complete: function () {
            	$.unblockUI();
            },
            success: function(response) {
                myalert('success','GOOD');

                clear();
            },
            error: function(response) {
                console.log(response);
                myalert('error',response['responseJSON']);
            }
        })
    });

});

function detailSuratJalan(e){
    var no_suratjalan = $(e).data('suratjalan');
    var no_suratjalan_concat = $(e).data('suratjalanconcat');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'GET',
        url: $('#get_detail_suratjalan').attr('href'),
        data: {'no_suratjalan': no_suratjalan, '_token': $("input[name='_token']").val()},
        beforeSend: function(){
            $('#modal_detail_ > .modal-content').block({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    width: 'auto',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333'
                }
            });
        },
        complete: function() {
            $('#modal_detail_').modal('show');
        },
        success: function(response) {
            $('#modal_detail_ > .modal-content').unblock();
            //$('#list').html(response);
            $('#title_detailx').text(no_suratjalan_concat);
            $('#detailSuratJalan > tbody').empty();
            if(response.status == 'global'){
                
                $.each(response.data, function(i,v){
                    let desc = v.description ? v.description : '';
                    let style = v.is_washing ? v.style+' -WASHING '+desc : v.style;    
    
                    $('#detailSuratJalan > tbody').append('<tr>'
                                                                +'<td>'+style+'</td>'
                                                                +'<td>-</td>'
                                                                +'<td>-</td>'
                                                                +'<td>-</td>'
                                                                +'<td>-</td>'
                                                                +'<td>'+v.qty+'</td>'
                                                            +'</tr>');
                });

            }else{

                $.each(response.data, function(i,v){
    
                    $('#detailSuratJalan > tbody').append('<tr>'
                                                                +'<td>'+v.style_edit+'</td>'
                                                                +'<td>'+v.no_polybag+'</td>'
                                                                +'<td>'+v.po_number_edit+'</td>'
                                                                +'<td>'+v.article_edit+'</td>'
                                                                +'<td>'+v.color_edit+'</td>'
                                                                +'<td>'+v.total_qty/v.count_komponen+'</td>'
                                                            +'</tr>');
                });

            }
        },
        error: function(response) {
            console.log(response);
        }
    });
}

function editSuratJalan(e){
    var no_suratjalan = $(e).data('suratjalan');
    var no_suratjalan_concat = $(e).data('suratjalanconcat');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'GET',
        url: $('#editBcNo').attr('href'),
        data: {'no_suratjalan': no_suratjalan, '_token': $("input[name='_token']").val()},
        beforeSend: function(){
            $('#modal_update_ > .modal-content').block({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    width: 'auto',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333'
                }
            });
        },
        complete: function() {
            $('#modal_update_').modal('show');
        },
        success: function(response) {
            $('#modal_update_ > .modal-content').unblock();

            $('#title_detail').text(no_suratjalan_concat);
            $('#bc_no').val(response.bc_no);
            $('#no_suratjalan_update').val(response.no_suratjalan);
            $('#type_bc').val(response.type_bc).trigger('change');
            $('#no_aju').val(response.no_aju);
            $('#no_daftar_bc').val(response.no_daftar_bc);
            $('#document_date').val(response.document_date).trigger('change');
        },
        error: function(response) {
            console.log(response);
        }
    });
}

function clear(){
    $('#bc_no').val('');
    $('#no_suratjalan_update').val('');
    $('#type_bc').val('').trigger('change');
    $('#no_aju').val('');
    $('#no_daftar_bc').val('');
    $('#document_date').val(null).trigger('change');
}
</script>
@endsection
