@extends('layouts.app', ['active' => 'dashboard'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Dashboard</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<!-- STATUS -->
<div class="row">
    <div class="col-sm-6 col-md-4 hidden">
        <div class="panel panel-body">
            <div class="media no-margin">
                <div class="media-left media-middle">
                    <i class="icon-dropbox icon-3x text-grey-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="no-margin text-semibold" id="total_sticker_ready">{{ isset($count_sticker_ready) ? $count_sticker_ready : 0 }}</h3>
                    <span class="text-uppercase text-size-mini text-muted">Total Ready Bundle</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3 hidden">
        <div class="panel panel-body">
            <div class="media no-margin">
                <div class="media-left media-middle">
                    <i class="icon-dropbox icon-3x text-success-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="no-margin text-semibold" id="total_sticker">{{ isset($count_sticker) ? $count_sticker : 0 }}</h3>
                    <span class="text-uppercase text-size-mini text-muted">Total Scan Bundle</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4 pull-right">
        <div class="panel panel-body">
            <div class="media no-margin">
                <div class="media-left media-middle">
                    <i class="icon-truck icon-3x text-danger-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="no-margin text-semibold" id="total_out">{{ isset($count_out) ? $count_out : 0 }}</h3>
                    <span class="text-uppercase text-size-mini text-muted">Total Bundle Out</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4 pull-right">
        <div class="panel panel-body">
            <div class="media no-margin">
                <div class="media-left media-middle">
                    <i class="icon-location4 icon-3x text-blue-400"></i>
                </div>

                <div class="media-body text-right">
                    <h3 class="no-margin text-semibold" id="total_in">{{ isset($count_in) ? $count_in : 0 }}</h3>
                    <span class="text-uppercase text-size-mini text-muted">Total Bundle In</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /STATUS -->

<!-- TABLE -->
<div class="panel panel-flat">
    <div class="panel-body">
          <div class="form-group" id="filter_by_po">
            <label><b>Choose PO Number</b></label>
            <div class="input-group">
                <input type="text" class="form-control text-uppercase" name="po" id="po" placeholder="PO Number" value="">
                <div class="input-group-btn">
                    <button type="button" id='filterPo' value="po" class="btn btn-primary">Filter</button>
                </div>
            </div>
          </div>
          <div class="form-group hidden" id="filter_by_style">
                <label><b>Choose Style</b></label>
                <div class="input-group">
                    <input type="text" class="form-control text-uppercase" name="style" id="style" placeholder="Style">
                    <div class="input-group-btn">
                        <button type="submit" id='filterStyle' value="style" class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="po">Filter by PO Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="style">Filter by Style</label>
            </div>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="col-md-12" style="float:right">
            <span class="label label-roundless label-success" style="float:right">SCAN IN</span>
            <span class="label label-roundless label-danger" style="float:right">SCAN OUT</span>
            <span class="label label-roundless label-primary" style="float:right">READY</span>
            <span class="label label-roundless label-default" style="float:right">ONPROGRESS</span>
        </div>
    </div>
    <hr>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-button-html5-basic" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>PO NUMBER</th>
                        <th>STYLE</th>
                        <th>ARTICLE</th>
                        <th>SIZE</th>
                        <th>QTY</th>
                        <th class="text-center">STATUS</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('ajaxGetData') }}" id="po_get_data"></a>
@endsection

@section('page-modal')
    @include('dashboard/_dashboard')
@endsection

@section('page-js')
<script type="text/javascript">
    $(document).ready(function(){
        var url = $('#po_get_data').attr('href');
        //datatables
        $.extend( $.fn.dataTable.defaults, {
            scrollX: true,
            scrollY: "50vh",
            scrollCollapse: true,
            stateSave: true,
            autoWidth: false,
            autoLength: false,
            processing: true,
            serverSide: true,
            paging: false,
            ordering: false,
            info: false,
            searching: false
        });

        var table = $('#table-list').DataTable({
            ajax: {
                url: url,
                data: function (d) {
                    return $.extend({},d,{
                        "po": $('#po').val(),
                        "style": $('#style').val(),
                        "radio_status": $('input[type=radio][name=radio_status]:checked').val()
                    });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = table.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
                $('td', row).eq(6).css('min-width', '150px');
            },
            columnDefs: [
                {
                    className: 'dt-center'
                },
                {
                    targets: [6],
                    className: 'text-center'
                }
            ],
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'po_number', name: 'po_number'},
                {data: 'style_edit', name: 'style_edit'},
                {data: 'article_edit', name: 'article_edit'},
                {data: 'size', name: 'size'},
                {data: 'qty', name: 'qty', sortable: false, orderable: false, searchable: false},
                {data: 'status_distribusi', name: 'status_distribusi', sortable: false, orderable: false, searchable: false},
                {data: 'action', name: 'action', sortable: false, orderable: false, searchable: false}
            ],
        });

        //
        table
        .on( 'preDraw', function () {
            Pace.start();
        } )
        .on( 'draw.dt', function () {
            Pace.stop();
            $('#table-list').unblock();
        } );

        $('#filterPo').on('click', function(event) {
            event.preventDefault();
        
                var po = $('#po').val();
        
                if (po.length >= 3 ) {
                loading_process();
                //reload data table
                table.ajax.reload( null, false );
                }else{
                myalert('error','Please enter PO minimal 3 character');
                return false;
                }
        });
      
        $('#po').keyup(function(e) {
            var po = $('#po').val();
    
            if (e.keyCode == 13 || e.which == 13) {
                if (po.length >= 3 ) {
                loading_process();
                //reload data table
                table.ajax.reload( null, false );
                }else{
                myalert('error','Please enter PO minimal 3 character');
                return false;
                }
            }
        });

        //
        $('#filterStyle').on('click', function(event) {
            event.preventDefault();
        
                var style = $('#style').val();
        
                if (style.length >= 3 ) {
                    loading_process();
                    table.ajax.reload( null, false );
                }else{
                    myalert('error','Please enter Style minimal 3 character');
                    return false;
                }
        });
      
        $('#style').keyup(function(e) {
            var style = $('#style').val();
    
            if (e.keyCode == 13 || e.which == 13) {
                if (style.length >= 3 ) {
                    loading_process();
                    table.ajax.reload( null, false );
                }else{
                    myalert('error','Please enter Style minimal 3 character');
                    return false;
                }
            }
        });

          //filter choose
          $('input[type=radio][name=radio_status]').change(function(){
            if (this.value == 'po') {
                if($('#filter_by_po').hasClass('hidden')) {
                    $('#filter_by_po').removeClass('hidden');
                }

                $('#filter_by_style').addClass('hidden');

            }else if (this.value == 'style') {
                if($('#filter_by_style').hasClass('hidden')) {
                    $('#filter_by_style').removeClass('hidden');
                }
                $('#filter_by_po').addClass('hidden');
            }
          });

    });
</script>
@endsection
