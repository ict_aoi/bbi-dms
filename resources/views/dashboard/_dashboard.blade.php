<!-- MODAL -->
<div id="" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content modal-lg">
      <div class="modal-body">
        <form class="form-horizontal" action="#">
            <div class="panel-body">
                <fieldset>
                    <legend class="text-semibold">
                        <i class="icon-file-text2 position-left"></i><h4 id="title_"></h4>
                    </legend>
                </fieldset>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- /MODAL -->