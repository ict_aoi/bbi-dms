@extends('layouts.app', ['active' => 'dashboard'])

@section ('page-header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">PO DETAILS</span></h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home4 position-left"></i> Dashboard</a></li>
            <li class="active"><a href="#"> PO Details</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>BARCODE ID</th>
                        <th>STYLE</th>
                        <th>PO NUMBER</th>
                        <th>ARTICLE</th>
                        <th>COLOR</th>
                        <th>SIZE</th>
                        <th>KOMPONEN</th>
                        <th>CUT NUMBER</th>
                        <th>STICKER NO</th>
                        <th>PROSES</th>
                        <th>STATUS</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('ajaxPackageDataByOrderLine') }}" id="package_detail_get_data" data-style="{{ $style }}" data-size="{{ $size }}" data-orderline="{{ $c_orderline_id }}"></a>
@endsection

@section('page-js')
<script type="text/javascript">
$( document ).ready(function() {
    //datatables
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });
    var url = $('#package_detail_get_data').attr('href');
    var style = $('#package_detail_get_data').data('style');
    var size = $('#package_detail_get_data').data('size');
    var c_orderline_id = $('#package_detail_get_data').data('orderline');
    var table = $('#table-list').DataTable({
        ajax: {
            'url' : url,
            'data': {style: style, size: size, c_orderline_id: c_orderline_id}
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'barcode_id', name: 'barcode_id'},
            {data: 'style_edit', name: 'style_edit'},
            {data: 'po_number_edit', name: 'po_number_edit'},
            {data: 'article_edit', name: 'article_edit'},
            {data: 'color_edit', name: 'color_edit'},
            {data: 'size_edit', name: 'size_edit'},
            {data: 'komponen_name', name: 'komponen_name'},
            {data: 'cut_number', name: 'cut_number'},
            {data: 'sticker_no', name: 'sticker_no'},
            {data: 'current_process', name: 'current_process'},
            {data: 'current_status', name: 'current_status'}
        ]
    });

});

</script>
@endsection
