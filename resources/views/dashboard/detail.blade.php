@extends('layouts.app', ['active' => 'dashboard'])

@section ('page-header')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="position-left"></i> <span class="text-semibold">PO DETAILS</span> (po number : #{{$po_number}})</h4>
            <p class="position-left"></p>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home4 position-left"></i> Dashboard</a></li>
            <li class="active"><a href="#"> PO Details</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>BARCODE ID</th>
                        <th>PO NUMBER</th>
                        <th>ARTICLE</th>
                        <th>COLOR</th>
                        <th>SIZE</th>
                        <th>KOMPONEN</th>
                        <th>CUT NUMBER</th>
                        <th>STICKER NO</th>
                        <th>PROSES</th>
                        <th>STATUS</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('ajaxPackageData') }}" id="package_detail_get_data" data-ponumber = '{{ $po_number}}'></a>
@endsection

@section('page-js')
<script type="text/javascript">
$( document ).ready(function() {
    //datatables
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });
    var url = $('#package_detail_get_data').attr('href');
    var ponumber = $('#package_detail_get_data').data('ponumber');
    var table = $('#table-list').DataTable({
        ajax: {
            'url' : url,
            'data': {po_number: ponumber}
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'barcode_id', name: 'barcode_id'},
            {data: 'po_number', name: 'po_number'},
            {data: 'article', name: 'article'},
            {data: 'color', name: 'color'},
            {data: 'size', name: 'size'},
            {data: 'komponen', name: 'komponen'},
            {data: 'cut_number', name: 'cut_number'},
            {data: 'sticker_no', name: 'sticker_no'},
            {data: 'current_process', name: 'current_process'},
            {data: 'current_status', name: 'current_status'}
        ]
    });

});

</script>
@endsection
