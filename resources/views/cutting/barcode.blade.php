@extends('layouts.app', ['active' => 'barcoding'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Cutting Panel</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('cutting.barcode') }}"><i class="icon-list position-left"></i> Cutting</a></li>
            <li class="active">Panel</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('cutting.ajaxGetData') }}" id="form_filter">
            <div class="form-group" id="filter_by_po">
				<label><b>Choose PO Number</b></label>
				<div class="input-group">
                    <input type="text" class="form-control text-uppercase" name="po" id="po" placeholder="PO Number">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
            </div>
            <div class="form-group hidden" id="filter_by_style">
				<label><b>Choose Style</b></label>
				<div class="input-group">
                    <input type="text" class="form-control text-uppercase" name="style_" id="style_" placeholder="Style">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
            </div>
            <div class="form-group hidden" id="filter_by_promisedate">
				<label><b>Choose Promise Date (month/day/year)</b></label>
				<div class="input-group">
					<span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
			</div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="po">Filter by PO Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="fstyle">Filter by Style</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="promise">Filter by Delivery Date</label>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
        <span id="filter_date" style="float:right"><b></b></span>
    </div>
    <div class="panel-body loading-area">
        <table class="table datatable-save-state" id="table-po-list">
            <thead>
                <tr>
                    {{--  <th><input type="checkbox" onclick="toggle(this)"></th>  --}}
                    <th>#</th>
                    <th>PO Number</th>
                    <th>Style</th>
                    <th>Article</th>
                    <th>Color</th>
                    <th>PODD</th>
                    <th>Size</th>
                    <th>Qty</th>
                    <th>Is Completed</th>
                    <th>ACTION</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<a href="{{ route('cutting.ajaxSetCompletedAllFilterPo') }}" id="completed_all"></a>
<a href="{{ route('cutting.printPreview') }}" id="print_preview"></a>
<a href="{{ route('cutting.ajaxGetDataKomponen') }}" id="get_detail_komponen"></a>
<a href="{{ route('cutting.deleteSummaryDetail') }}" id="delete-summary-detail"></a>
<a href="{{ route('cutting.updatePoNumberEdit') }}" id="updatePoNumberEdit"></a>
@endsection

@section('page-modal')
    @include('cutting._upload_modal');
    @include('cutting._list_delete_modal');
@endsection

@section('page-js') 
<script type="text/javascript" src="{{ url('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript">
//$(document).ready(function() {
    //url
    var url = $('#form_filter').attr('action');

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#table-po-list').DataTable({
        buttons: [
                /*{
                    text: 'Print All',
                    className: 'btn btn-sm bg-primary printAll'
                }*/
            ],
        ajax: {
            url: url,
            //api
            data: {date_range: $('#date_range').val(), po_number: $('#po').val(), style: $('#style_').val(), radio_status: $('input[name=radio_status]:checked').val()}

        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(5).css('width', '120px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
        //    {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
            {data: null, sortable: false, orderable: false, searchable: false  },
            {data: 'poreference', name: 'poreference'},
            {data: 'style', name: 'style'},
            {data: 'article', name: 'article'},
            {data: 'color', name: 'color'},
            {data: 'datepromise', name: 'datepromise'},
            {data: 'size', name: 'size', searchable: false, orderable: false, sortable: false},
            {data: 'total_package', name: 'total_package', searchable: false, orderable: false, sortable: false},
            {data: 'is_completed', name: 'is_completed', searchable: false, orderable: false, sortable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
    });
    //end of datatables

    table
    .on( 'preDraw', function () {
        Pace.start();
    } )
    .on( 'draw.dt', function () {
        $('#table-po-list').unblock();
        Pace.stop();
    } );

    var _token = $("input[name='_token']").val();

    //
    $('#table-list').on('click','.ignore-click', function() {
        return false;
    });

    //print all
    $(document).on('click', '.printAll', function(event){
        event.preventDefault();

        var filter_po = $('#po').val();
        var style = null;
        var size = null;
        var color = null;
        var article = null;
        var data = [];

        $('.clref').each(function(){
            data.push({
                po_number: $(this).data('po'),
                style: $(this).data('style'),
                size: $(this).data('size'),
                color: $(this).data('color'),
                article: $(this).data('article')
            });
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#completed_all').attr('href'),
            data: {filter_po: filter_po, po_number: filter_po, style: style, size: size, color: color, article: article, data: JSON.stringify(data)},
            beforeSend: function() {
                $('.loading-area').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('.loading-area').unblock();
                printPage(filter_po);
            },
            error: function(response) {
                $('.loading-area').unblock();
                myalert('error','NOT GOOD');
            }
        });
        
    });

    $('#modal_upload_').on('shown.bs.modal',function(){
        $('#cut_number').focus();
    });

    //open modal upload (api)
    $('#table-po-list').on('click', '.upload', function() {
        //var url = $(this).attr('href');
        //var params = getURLParameter(url);
        //var ponumber = params['po_number'];
        var str = $(this).data('value');
        var str_sp = str.split("|");

        var ponumber = str_sp[0];
        var style = str_sp[1];
        var size = str_sp[2];
        var color = str_sp[3];
        var article = str_sp[4];
        var m_product_id = str_sp[5];
      //  var c_order_id = str_sp[6];
        var c_orderline_id = str_sp[6];

        $('#po_number').val(ponumber);
        $('#style').val(style);
        $('#size').val($('#size_' + c_orderline_id).val());
        $('#article').val($('#article_' + c_orderline_id).val());
        $('#color').val($('#color_' + c_orderline_id).val());

        $('#article_edit').val($('#article_edit_' + c_orderline_id).val());
        $('#color_edit').val($('#color_edit_' + c_orderline_id).val());

        $('#po_number_edit').val($('#po_number_edit_' + c_orderline_id).val());
        $('#style_edit').val(style);

        $('#size_edit').val($('#size_edit_' + c_orderline_id).val());

        $('#pcd').val($('#pcd_' + c_orderline_id).val());
        $('#podd').val($('#podd_' + c_orderline_id).val());
        $('#mo').val($('#mo_' + c_orderline_id).val());
        $('#city_name').val($('#ctyname_' + c_orderline_id).val());
        $('#kst_joborder').val($('#joborder_' + c_orderline_id).val());
        $('#qtyordered').val($('#qtyordered_' + c_orderline_id).val());
        $('#m_product_id').val($('#productid_' + c_orderline_id).val());
        $('#kode_product').val($('#productkode_' + c_orderline_id).val());
        $('#nama_product').val($('#productname_' + c_orderline_id).val());
        $('#dateordered').val($('#dateorder_' + c_orderline_id).val());
        $('#datepromised').val($('#datepromise_' + c_orderline_id).val());

        $('#buyer').val($('#buyer_' + c_orderline_id).val());
        $('#c_order_id').val($('#orderid_' + c_orderline_id).val());
        $('#c_orderline_id').val($('#orderlineid_' + c_orderline_id).val());
        $('#is_recycle').val($('#is_recycle_' + c_orderline_id).val());

        $('#title_po').text(ponumber);
        $('#modal_upload_').modal('show');
    });

    $('#qty, #sticker_from').on('blur', function(){
        var qty = $('#qty').val();  
        var sticker_from = $('#sticker_from').val();

        var sticker_to = parseInt(sticker_from)+parseInt(qty)-1 || 0;
        $('#sticker_to').val(sticker_to);
        $('#sticker_no').val(sticker_from+' - '+sticker_to);
    })

    //filter (api)
    $('#form_filter').submit(function(event){
        event.preventDefault();
        var date_range = $('#date_range').val();
        var po_number  = $('#po').val();
        var style  = $('#style_').val();
        var radio_status = $('input[name=radio_status]:checked').val();

        //check location
        if(date_range == '' && po_number == '') {
            myalert('error','Please select filter first');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : url,
            data: {date_range: date_range, po_number: po_number, style: style, radio_status: radio_status},
            beforeSend: function() {
                $('#table-po-list').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#table-po-list').unblock();

                if(radio_status == 'po') {
                    $('#filter_date').text('CURRENT FILTER: PO Number #' + po_number);
                }
                else if(radio_status == 'promise') {
                    $('#filter_date').text('CURRENT FILTER: ' + date_range);
                }
                else if(radio_status == 'fstyle') {
                    $('#filter_date').text('CURRENT FILTER: ' + style);
                }
                $('#filter_date').addClass('label-striped');
                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#table-po-list').unblock();
                myalert('error',response['responseJSON']);
            }
        });
    });

    //verify
    $('#form-upload').submit(function(event) {
        event.preventDefault();

        //check po_number
       /* if($('#po_number').val() == '') {
            myalert('error','PO Number not integrated');
            return false;
        }*/

        //check sticker to apakah masih 0 ? --> gak dipakai dulu
        /*if($('#sticker_to').val() <= 0){
            myalert('error','something wrong with no sticker');
            return false;
        }*/

        //sticker to tidak boleh lebih kecil dari sticker from --> gak dipakai dulu
        /*if($('#sticker_to').val() < $('#sticker_from').val()){
            myalert('error','something wrong with sticker from & sticker to');
            return false;
        }*/

        //total qty tidak boleh lebih kecil dari qty per bundle
        if( parseInt($('#total_qty').val()) < parseInt($('#qty').val())){
            myalert('error','something wrong (total qty < qty per bundle)');
            return false;
        }

        //bundle start tidak boleh lebih kecil dari sama dengan 0
        if($('#from').val() <= 0 ){
            myalert('error','something wrong (bundle start <= 0)');
            return false;
        }

        //
        var formData = new FormData($(this)[0]);
        var parameter = ['po_number', 'pcd', 'podd', 'mo', 'city_name', 'kst_joborder', 'qtyordered','style','article','size','color','cut_number','cut_info','lot','part','komponen','process_id','qty','sticker_from','sticker_to','sticker_no','cutting_date','bundle','qc','cutter','type_id','type_description','category','m_product_id','kode_product','nama_product','dateordered','datepromised', 'total_qty', 'from', 'color_edit', 'article_edit', 'style_edit', 'po_number_edit', 'c_order_id', 'c_orderline_id', 'size_edit', 'no_bundle', 'is_recycle'];
        var val = [
                    $('#po_number').val(),
                    $('#pcd').val(),
                    $('#podd').val(), 
                    $('#mo').val(),
                    $('#city_name').val(),
                    $('#kst_joborder').val(),
                    $('#qtyordered').val(),
                    $('#style').val(),
                    $('#article').val(),
                    $('#size').val(),
                    $('#color').val(),
                    $('#cut_number').val(),
                    $('#cut_info').val(),
                    $('#lot').val(),
                    $('#part').val(),
                    $('#komponen').val(),
                    $('#process_id').val(),
                    $('#qty').val(),
                    $('#sticker_from').val(),
                    $('#sticker_to').val(),
                    $('#sticker_no').val(),
                    $('#cutting_date').val(),
                    $('#bundle').val(),
                    $('#qc').val(),
                    $('#cutter').val(),
                    $('#type_id').val(),
                    $('#type_description').val(),
                    $('#category').val(),
                    $('#m_product_id').val(),
                    $('#kode_product').val(),
                    $('#nama_product').val(),
                    $('#dateordered').val(),
                    $('#datepromised').val(),
                    $('#total_qty').val(),
                    $('#from').val(),
                    $('#color_edit').val(),
                    $('#article_edit').val(),
                    $('#style_edit').val(),
                    $('#po_number_edit').val(),
                    $('#c_order_id').val(),
                    $('#c_orderline_id').val(),
                    $('#size_edit').val(),
                    $('#no_bundle').val(),
                    $('#is_recycle').val()
        ]

        for(var i = 0; i < parameter.length; i++) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#form-upload').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $('#form-upload .modal-body').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Uploading File</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#form-upload .modal-body').unblock();
                myalert('success', 'Good..!');
                $('#form_filter').submit();
                clear();
            },
            error: function(response) {
                $('#form-upload .modal-body').unblock();
                if(response['status'] == 422) {
                    myalert('error', response['responseJSON']);
                }
            }
        });
    });

    $('#modal_upload_').on('hidden.bs.modal', function(){
        $('#cut_number').val('');
        $('#cut_info').val('');
        $('#lot').val('');
        $('#part').val('');
        $('#komponen').val('');
        $('#process_id').val(null).trigger('change');
        $('#qty').val('');
        $('#sticker_from').val('');
        $('#sticker_to').val('');
        $('#sticker_no').val('');
      //  $('#cutting_date').val('');
        $('#bundle').val('');
        $('#qc').val('');
        $('#cutter').val('');
        $('#type_id').val(1).trigger('change');
        $('#type_description').val('');
        $('#total_qty').val('');
        $('#from').val('');
        $('#no_bundle').val('');
    });

    $('#type_id').change(function() {
        var type_text = $('#type_id :selected').text();

        if (type_text != 'Non'){
            $('.type-desc').removeClass('hidden');
        }else{
            $('.type-desc').addClass('hidden');
        }
    });

    //choose filter
    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'po') {
            if($('#filter_by_po').hasClass('hidden')) {
                $('#filter_by_po').removeClass('hidden');
            }

            $('#filter_by_promisedate').addClass('hidden');
            $('#filter_by_style').addClass('hidden');
        }
        else if (this.value == 'fstyle') {
            if($('#filter_by_style').hasClass('hidden')) {
                $('#filter_by_style').removeClass('hidden');
            }

            $('#filter_by_po').addClass('hidden');
            $('#filter_by_promisedate').addClass('hidden');
        }
        else if (this.value == 'promise') {
            if($('#filter_by_promisedate').hasClass('hidden')) {
                $('#filter_by_promisedate').removeClass('hidden');
            }

            $('#filter_by_po').addClass('hidden');
            $('#filter_by_style').addClass('hidden');
        }

        $('#form_filter').submit();
    });

    // listing
    $('#listing').on('click', '#choose-orderline', function(){
        var id = $(this).data('id');
        var c_orderline_id = $(this).data('orderline');
        var po_summary_id = $(this).data('posummaryid');
        var from = $(this).data('from');
        var qty = $(this).data('qty');
        var total = $(this).data('total');
        var style = $(this).data('style');

        var messages = "( from:"+from+" qty:"+qty+" total:"+total+" )";

        bootbox.confirm("Are you sure delete this ? "+messages, function(result){
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: $('#delete-summary-detail').attr('href'),
                   // type: "GET",
                    data: {
                        "id": id,
                        "c_orderline_id": c_orderline_id,
                        "po_summary_id": po_summary_id,
                        "style": style
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $(".loader-area").unblock();
                    },
                    success: function () {
                        myalert('success','Data succesfully deleted');

                        $('#modal_list_delete_').modal('hide');

                        $('#form_filter').submit();
                    }
                });
            }
        });
    });

    //clear
    function clear(){
        $('#total_qty').val('');
        $('#from').val('');
        $('#qty').val('');
        $('#sticker_from').val('');
        $('#sticker_to').val('');
        $('#sticker_no').val('');
    }

    function printPage(po_number) {
        var url_print = $('#print_preview').attr('href');
        var data = [];
        
        var parameter = '?filter_po=' + po_number;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : url_print + parameter,
            success: function(response) {
                window.open(url_print + parameter, '_blank');
                //$('input:checkbox').removeAttr('checked');
                table.draw();
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
    }

    function detailStyle(e){
        var style = $(e).data('style');

        //$('#modal_detail_').modal('show');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url: $('#get_detail_komponen').attr('href'),
            data: {'style': style, '_token': _token},
            beforeSend: function(){
                $('#modal_detail_ > .modal-content').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            complete: function() {
                $('#modal_detail_').modal('show');
            },
            success: function(response) {
                $('#modal_detail_ > .modal-content').unblock();
                $('#list').html(response);
            },
            error: function(response) {
                console.log(response);
            }
        });
    }

    function toggle(source) {
        checkboxes = document.getElementsByName('selector[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
          checkboxes[i].checked = source.checked;
        }
    }

    // list delete
    function deleteDetail(url){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url: url,
            beforeSend: function() {
                $('#modal_list_delete_ > .modal-content').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response){
                $('#modal_list_delete_ > .modal-content').unblock();
                $('#listing').html(response);
            }
        });

        
        $('#modal_list_delete_').modal('show');
    }

    // update po number edit
    $(document).on('blur', '.po_filled', function() {
        var id = $(this).data('id');
        var po_number_edit = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updatePoNumberEdit').attr('href'),
            data: {id:id, po_number_edit:po_number_edit},
            success: function(response){
                $('#po_' + id).html(response);

            },
            error: function(response) {
                return false;
            }
        });
    });

    //get parameter from url
    function getURLParameter(url) {
        var params = {};
        url.substring(1).replace(/[?&]+([^=&]+)=([^&]*)/gi,
                function (str, key, value) {
                     params[key] = value;
                });
        return params;
    }

    function loading(){
        $('#table-po-list').block({
            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: '10px 15px',
                color: '#fff',
                width: 'auto',
                '-webkit-border-radius': 2,
                '-moz-border-radius': 2,
                backgroundColor: '#333'
            }
        });
    }

//});
</script>
@endsection
