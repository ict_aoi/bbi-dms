<!-- MODAL -->
<div id="modal_list_delete_" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
            <div class="modal-content">
              	<div class="modal-body">
                    <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title" id="title"><i class="position-left"></i>Choose Detail</h5>
					</div>
                  	<div class="panel-body loader-area">
						<fieldset>
                            <input type="hidden" id="po_summary_detail_id" name="po_summary_detail_id">
                            <div id="listing" class="col-lg-12">
                                <!-- template pilihan -->
                            </div>
                        </fieldset>
                  	</div>
              	</div>
            </div>
    </div>
</div>
<!-- /MODAL -->