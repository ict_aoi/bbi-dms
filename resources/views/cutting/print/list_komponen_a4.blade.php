<style type="text/css">

    @page {
        margin: 0 20 0 20;
    }
    
    @font-face {
        font-family: 'impact' !important;
        src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
    }
    
    .template {
       font-size: 16 !important;
       //font-family: 'impact';
    }
    
    .page-break {
        page-break-after: always;
    }

    .separator {
        margin: 30 0 0 0;
    }

    .container {
        display: flex;
        flex-wrap: wrap;
    }

    .break {
        flex-basis: 100%;
        height: 0;
    }
    
    .print-friendly {
        //height: 30%;
        width: 100%;
        line-height: 0px;
        padding-top: 8px;
        margin-left: -5px;
        margin-right: -5px;
        font-size: 14px;
    }
    
    .print-friendly-single {
        height: 15%;
        width: 100%;
        //font-family: sans-serif;
        font-size: 14px !important;
        line-height: 6px;
        padding-top: 10px;
    }
    
    table.print-friendly tr td, table.print-friendly tr th,
    table.print-friendly-single tr td, table.print-friendly-single tr th, {
        page-break-inside: avoid;
        padding: 6px;
    }
    
    .barcode {
        float: left;
        margin-bottom: 20px;
        line-height: 16px;
        position:absolute;
        top: -4.8em;
        //margin-left: 230px;
        //transform: rotate(270deg);
        font-family: 'impact';
    }
    
    .img_barcode {
        display: block;
        padding: 0px;
    }
    
    .img_barcode > img {
        width: 166px;
        height: 40px;
    }
    
    .barcode_number {
        font-family: sans-serif;
        font-size: 14px !important;
    }
    
    .area_barcode {
        width: 25%;
    }
    
    p{
        line-height: 0.5;
    }

    .tg  {
        border-collapse:collapse;
        border-spacing:0;
        width: 100%;
        border-width: 1px;
        //padding-top: 15px;
        //margin-left: 5px;
        //margin-right: 5px;
        //line-height: 0.5;
        
    }
    tg, th, td
    {
        border: 1;
    }
    .tg td{
        font-family:sans-serif;
        font-size:14px;
        padding:2px 2px;
        border-style:solid;
        border-width:1px;
        overflow:hidden;
        word-break:normal;
        border-color:black;
    }
    .tg th{
        font-family:sans-serif;
        font-size:14px;
        font-weight:normal;
        padding:2px 2px;
        border-style:solid;
        border-width:1px;
        overflow:hidden;
        word-break:normal;
        border-color:black;
    }
    .tg .tg-cly1{
        text-align:left;
        vertical-align:middle
    }
    .tg .tg-nrix{
        text-align:center;
        vertical-align:middle
    }
    .tg .tg-nrix1{
        text-align:center;
        background-color: #a9a9a9;
        vertical-align:middle
    }
    .tg .tg-0lax{
        text-align:left;
        vertical-align:top
    }
    .clear{
        clear: left; 
        margin: 0.5em; 
        border-width: 0.1px;
    }

    .thing {
       // clear: left;
        //width: 100%;
        //height: 15px;
        //line-height: 10px;
        //padding: 1px solid #000;
        //word-break:break-all;
        float:left; display:inline-block; clear:both;
    }

    .left-half {
        position: absolute;
        left: 0px;
        padding-top: 15px;
        margin-left: -10px;
        width: 50%;
    }

    .right-half {
        position: absolute;
        padding-top: 15px;
        right: -20px;
        width: 50%;
    }

    .full-left {
        position: absolute;
        left: 0px;
        padding-top: 15px;
        margin-left: -10px;
        width: 100%;
    }
    
    </style>
        @php
            //$num=1;
            $j=1;
            $i=0;
        @endphp
        @foreach($chunk_new as $key => $val)
            <div class="full-left">
                <table class="tg">
                    {{--  style="width: 50%; float: left; padding-left: -10px;"  --}}
                    <tr style="border: 1pt solid black;">
                        <td class="tg-nrix" colspan="10"><u>CHECK LIST KOMPONEN</u></td>
                    </tr>
                    <tr>
                        <td class="tg-nrix" rowspan="5">NO</td>
                        <td class="tg-cly1">PO/SO</td>
                        <td class="tg-cly1" colspan="4">: {{ $list_komponen_header->po_number_edit }}</td>
                        <td class="tg-nrix" width="20%" colspan="2" rowspan="3">NO STICKER</td>
                        <td class="tg-nrix" width="20%" colspan="2" rowspan="3">: {{ $list_sticker_new[$i] }}</td>
                    </tr>
                    <tr>
                        <td class="tg-cly1">STYLE</td>
                        <td class="tg-cly1" colspan="3">: {{ $list_komponen_header->style_edit }}</td>
                        <td class="tg-cly1"></td>
                    </tr>
                    <tr>
                        <td class="tg-cly1">COLL</td>
                        <td class="tg-cly1" colspan="3">: {{ $list_komponen_header->color_edit }}</td>
                        <td class="tg-cly1"></td>
                    </tr>
                    <tr>
                        <td class="tg-cly1">JOB</td>
                        <td class="tg-cly1" colspan="4">: {{ $list_komponen_header->style_edit.' : '.$list_komponen_header->po_number_edit.' :'.$list_komponen_header->article_edit }}</td>
                        <td class="tg-nrix1" colspan="2">CUTTING</td>
                        <td class="tg-nrix1" colspan="2">DISTRIBUSI</td>
                    </tr>
                    <tr>
                        <td class="tg-nrix">SIZE</td>
                        <td class="tg-nrix">CUT</td>
                        <td class="tg-nrix">QTY</td>
                        <td class="tg-nrix" width="30%" colspan="2">KOMPONEN</td>
                        <td class="tg-nrix">STATUS</td>
                        <td class="tg-nrix">JUMLAH</td>
                        <td class="tg-nrix">STATUS</td>
                        <td class="tg-nrix">JUMLAH</td>
                    </tr>
                    @php
                        $num = 1;
                        $count = 0;
                        $plus = 69;
                    @endphp
                    @foreach($val as $key2 => $val2)
                        @php
                            $qty = ($val2->sticker_to-$val2->sticker_from)+1;
                            $size = $val2->size;
                            $cut_number = $val2->cut_number.' '.$val2->cut_info;
                            
                            if($num != 1){
                                $size = '';
                                $cut_number = '';
                                $qty = '';
                            }

                            $komponen = $val2->process_name ? $val2->komponen_name.' ('.$val2->process_name.')' : $val2->komponen_name;

                            $st_komponen = strlen($komponen);

                            if($st_komponen>=27){
                               //$count_komponen = $count_komponen+1;
                               $plus = $plus+19;
                            }

                        @endphp
                        <tr>
                            <td align="center">{{$num}}</td>
                            <td align="center"><b>{{$size}}</b></td>
                            <td align="center">{{$cut_number}}</td>
                            <td align="center">{{$qty}}</td>
                            <td colspan="2">: {{$komponen}}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        @php
                            $num++;
                            $count++;
                        @endphp
                    @endforeach
                </table>
            </div>
            @php
                $i++;
                $j++;
                $margin = (($count_komponen-1)*11)+$plus;    
            @endphp
            @if(($key+1) % 1 == 0)
            <br>
            <hr style="clear: left; margin: {{$margin}}px; border-width: 0px;" />
            {{--  <div class="thing"></div>  --}}
            @endif
            @if(($key + 1) % ($factories->list_komponen_break/2) == 0)
            <div class="page-break"></div>
            @endif
           
        @endforeach
    </div>
    