<style type="text/css">

    @page {
        margin: 0 20 0 20;
    }
    
    @font-face {
        font-family: 'impact' !important;
        src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
    }
    
    .template {
       font-size: 16 !important;
       //font-family: 'impact';
    }
    
    .page-break {
        page-break-after: always;
    }

    .separator {
        margin: 30 0 0 0;
        //float: left;
        //page-break-after: always;
        //display: inline-block;
        //word-wrap: break-word;
    }

    .container {
        display: flex;
        flex-wrap: wrap;
    }

    .break {
        flex-basis: 100%;
        height: 0;
    }
    
    .print-friendly {
        //height: 30%;
        width: 100%;
        //font-family: sans-serif;
        //font-size: 14px !important;
        line-height: 6px;
        padding-top: 10px;
    }
    
    .print-friendly-single {
        height: 15%;
        width: 100%;
        font-family: sans-serif;
        font-size: 14px !important;
        line-height: 6px;
        padding-top: 10px;
    }
    
    table.print-friendly tr td, table.print-friendly tr th,
    table.print-friendly-single tr td, table.print-friendly-single tr th, {
        page-break-inside: avoid;
        padding: 6px;
    }
    
    .barcode {
        float: left;
        margin-bottom: 20px;
        line-height: 16px;
        position:absolute;
        top: 12rem;
        margin-left: 230px;
        transform: rotate(270deg);
        font-family: 'impact';
    }
    
    .img_barcode {
        display: block;
        padding: 0px;
    }
    
    .img_barcode > img {
        width: 166px;
        height: 40px;
    }
    
    .barcode_number {
        font-family: sans-serif;
        font-size: 14px !important;
    }
    
    .area_barcode {
        width: 25%;
    }
    
    p{
        line-height: 1;
    }
    
    </style>
    
    @if(isset($data))
        @if(count($data) == 1)
        <table class="print-friendly-single" style="width: 25%;">
            <tr>
                <td colspan="3">
                    <div class="barcode">
                        <div class="img_barcode">
                            <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data[0]->barcode_id, 'C128') }}" alt="barcode"   />
                        </div>
                        <span class="barcode_number">{{ isset($data[0]) ? $data[0]->barcode_id : null }}</span >
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 30%;">CUT</td>
                <td colspan="2">: {{ $data[0]->cut_number.' '.$data[0]->cut_info }}</td>
            </tr>
            <tr>
                <td>KOMPONEN</td>
                <td colspan="2">: {{ $data[0]->komponen }}</td>
            </tr>
            <tr>
                <td>PART</td>
                <td colspan="2">: {{ $data[0]->part }}</td>
            </tr>
            <tr>
                <td>STYLE</td>
                <td colspan="2">: {{ $data[0]->style }}</td>
            </tr>
            <tr>
                <td>SO</td>
                <td colspan="2">: {{ $data[0]->po_number }}</td>
            </tr>
            <tr>
                <td>COLL</td>
                <td colspan="2">: {{ $data[0]->color }}</td>
            </tr>
            <tr>
                <td>ARTICLE</td>
                <td colspan="2">: {{ $data[0]->article }}</td>
            </tr>
            <tr>
                <td>LOT</td>
                <td colspan="2">: {{ $data[0]->lot }}</td>
            </tr>
            {{-- <div class="barcode">
                    <div class="img_barcode">
                        <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data[0]->barcode_id, 'C128') }}" alt="barcode"   />
                    </div>
                    <span class="barcode_number">{{ isset($data[0]) ? $data[0]->barcode_id : null }}</span >
                </div> --}}
            <tr>
                <td>SIZE</td>
                <td colspan="2">: {{ $data[0]->size }}</td>
            </tr>
            <tr>
                <td>QTY</td>
                <td colspan="2">: {{ $data[0]->qty }}</td>
            </tr>
            <tr>
                <td>NO STICKER</td>
                <td colspan="2">: {{ $data[0]->sticker_no }}</td>
            </tr>
            <tr>
                <td>TGL</td>
                <td>: {{ \Carbon\Carbon::parse($data[0]->cutting_date)->format('d/m') }}</td>
            </tr>
            <tr>
                <td>BUNDLE</td>
                <td>: {{ $data[0]->bundle }}</td>
            </tr>
            <tr>
                <td>QC</td>
                <td>: {{ $data[0]->qc }}</td>
            </tr>
            <tr>
                <td>CUTTER</td>
                <td>: {{ $data[0]->cutter }}</td>
            </tr>
        </table>
        @else
            @php($chunk = array_chunk($data->toArray(), 2))
            @foreach($chunk as $key => $value)
            <table class="print-friendly" style="width: 25%; float: left; padding-left: -10px;">
                
            </table>
            @if(($key+1) % 3 == 0 && ($key +1) != count($chunk))
            <div class="page-break"></div>
            @endif
            @endforeach
        @endif
    @else
    <table style="width:100%;">
        <tr>
            <td colspan="8"></td>
            <td></td>
        </tr>
    </table>
    @endif
    </div>
    