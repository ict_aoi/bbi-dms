<style type="text/css">

    @page {
        margin: 0 20 0 20;
    }
    
    @font-face {
        font-family: 'impact' !important;
        src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
    }
    
    .template {
       font-size: 16 !important;
       //font-family: 'impact';
    }
    
    .page-break {
        page-break-after: always;
    }

    .separator {
        margin: 30 0 0 0;
    }

    .container {
        display: flex;
        flex-wrap: wrap;
    }

    .break {
        flex-basis: 100%;
        height: 0;
    }
    
    .print-friendly {
        //height: 30%;
        width: 100%;
        line-height: 0px;
        padding-top: 8px;
        //margin-left: -5px;
        //margin-right: -5px;
        font-size: 14px;
        border-right: thick solid #E0DCDC;
        border-width: 0.2px;
        margin-right: 0.5em;
    }
    
    .print-friendly-single {
        height: 15%;
        width: 100%;
        //font-family: sans-serif;
        font-size: 14px !important;
        line-height: 6px;
        padding-top: 10px;
    }
    
    table.print-friendly tr td, table.print-friendly tr th,
    table.print-friendly-single tr td, table.print-friendly-single tr th, {
        page-break-inside: avoid;
        padding: 6px;
    }
    
    .barcode {
        float: left;
        margin-bottom: 20px;
        line-height: 16px;
        position:absolute;
        top: -3.7em;
        margin-left: -120px;
        //transform: rotate(270deg);
        font-family: 'impact';
    }
    
    .img_barcode {
        display: block;
        padding: 0px;
    }
    
    .img_barcode > img {
        width: 135px;
        height: 23px;
    }
    
    .barcode_number {
        font-family: sans-serif;
        font-size: 14px !important;
    }
    
    .area_barcode {
        width: 25%;
    }
    
    p{
        line-height: 0.5;
    }

    .verticalLine {
        border-right: thick solid #ff0000;
    }
    
    </style>
    
    @if(isset($data))
        @if(count($data) == 1)
        @php
            $type_name = '';
            
            if ($data[0]->type_name != 'Non') {
                $type_name = $data[0]->type_name;
            }
        @endphp
        <table class="print-friendly" style="width: 52%; float: left; padding-left: -10px;">
            {{--  <tr>
                <td colspan="3">
                    <div class="barcode">
                        <div class="img_barcode">
                            <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data[0]->barcode_id, 'C128') }}" alt="barcode"   />
                        </div>
                        <span class="barcode_number">{{ isset($data[0]) ? $data[0]->barcode_id : null }}</span >
                    </div>
                </td>
            </tr>  --}}
            <tr>
                <td style="width: 5.2rem;">CUT</td>
                <td colspan="2">: {{ $data[0]->cut_number.' '.$data[0]->cut_info }}</td>
            </tr>
            <tr>
                <td>KOMPONEN</td>
                @php
                    $proname = $data[0]->process_name != null ? ' ('.$data[0]->process_name.')' : '';
                @endphp
                <td colspan="2">: {{ strlen($data[0]->komponen_name.$proname) <= 46 ? $data[0]->komponen_name.$proname : substr($data[0]->komponen_name.$proname, 0, 46).'.. )' }}</td>
            </tr>
            <tr>
                <td>PART</td>
                <td style="width: 4.5rem;">: {{ $data[0]->part }}</td>
                <td align="right"><b style="padding-right: 45px;">{{ $data[0]->total.'X' }}</b></td>
            </tr>
            <tr>
                <td>STYLE</td>
                <td colspan="2">: {{ $data[0]->style_edit }} {{ $type_name }}</td>
            </tr>
            <tr>
                <td>SO</td>
                <td colspan="2">: {{ $data[0]->po_number_edit }}</td>
            </tr>
            <tr>
                <td>COLL</td>
                <td colspan="2">: {{ $data[0]->color_edit }}</td>
            </tr>
            <tr>
                <td>ARTICLE</td>
                <td colspan="2">: {{ $data[0]->article_edit }}</td>
            </tr>
            <tr>
                <td>LOT</td>
                <td colspan="2">: {{ $data[0]->lot }}</td>
            </tr>
            <tr>
                <td>SIZE</td>
                <td colspan="2">: {{ $data[0]->size_edit }}</td>
            </tr>
            <tr>
                <td>QTY</td>
                <td colspan="2">: {{ $data[0]->qty }}</td>
            </tr>
            <tr>
                <td>NO STICKER</td>
                <td colspan="2">: {{ $data[0]->sticker_no }}</td>
            </tr>
            <tr>
                <td>TGL</td>
                <td>: {{ \Carbon\Carbon::parse($data[0]->cutting_date)->format('d/m') }}</td>
            </tr>
            <tr>
                <td>BUNDLE</td>
                <td>: {{ $data[0]->bundle }}</td>
            </tr>
            <tr>
                <td>QC</td>
                <td>: {{ $data[0]->qc }}</td>
            </tr>
            <tr>
                <td>CUTTER</td>
                <td>: {{ $data[0]->cutter }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>
                    <div class="barcode">
                        <div class="img_barcode">
                            <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data[0]->barcode_id, 'C128') }}" alt="barcode"   />
                        </div>
                        <span class="barcode_number">{{ isset($data[0]) ? $data[0]->barcode_id : null }}</span >
                    </div>
                </td>
            </tr>
        </table>
        @else
            @php
                $num = 0;
            @endphp
            @foreach($data as $key => $value)
            @php
                $pss = 'left';
                $proname = $value->process_name != null ? ' ('.$value->process_name.')' : '';

                $komponen_name = $value->komponen_name;

                if(strlen($value->komponen_name.$proname)>30){
                    $spn = '<span style="font-size: 9px;">';
                }else{   
                    $spn = '<span style="font-size: 12px;">';
                }

                $type_name = '';
                
                if ($value->type_name != 'Non') {
                    $type_name = $value->type_name;
                }
            @endphp
            <table class="print-friendly" style="width: 52%; float: left; padding-left: -10px;">
                {{--  <tr>
                    <td colspan="3">
                        <div class="barcode">
                            <div class="img_barcode">
                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128') }}" alt="barcode"   />
                            </div>
                            <span class="barcode_number">{{ isset($value) ? $value->barcode_id : null }}</span >
                        </div>
                    </td>
                </tr>  --}}
                <tr>
                    <td style="width: 5.2rem;">CUT</td>
                    <td colspan="2">: {{ $value->cut_number.' '.$value->cut_info }}</td>
                </tr>
                <tr>
                    <td>KOMPONEN</td>
                    <td colspan="2">:@php echo $spn; @endphp {{ $value->komponen_name.$proname }}</td>
                </tr>
                <tr>
                    <td>PART</td>
                    <td style="width: 10rem;">: {{ $value->part }}</td>
                    <td align="right"><b style="padding-right: 45px;">{{ $value->total.'X' }}</b></td>
                </tr>
                <tr>
                    <td>STYLE</td>
                    <td colspan="2">: {{ $value->style_edit }} {{ $type_name }}</td>
                </tr>
                <tr>
                    <td>SO</td>
                    <td colspan="2">: {{ $value->po_number_edit }}</td>
                </tr>
                <tr>
                    <td>COLL</td>
                    <td colspan="2">: {{ $value->color_edit }}</td>
                </tr>
                <tr>
                    <td>ARTICLE</td>
                    <td colspan="2">: {{ $value->article_edit }}</td>
                </tr>
                <tr>
                    <td>LOT</td>
                    <td colspan="2">: {{ $value->lot }}</td>
                </tr>
                <tr>
                    <td><b>SIZE</b></td>
                    <td colspan="2"><b>: {{ $value->size_edit }}</b></td>
                </tr>
                <tr>
                    <td>QTY</td>
                    <td colspan="2">: {{ $value->qty }}</td>
                </tr>
                <tr>
                    <td>NO STICKER</td>
                    <td colspan="2">: {{ $value->sticker_no }}</td>
                </tr>
                <tr>
                    <td>TGL</td>
                    <td>: {{ \Carbon\Carbon::parse($value->cutting_date)->format('d/m') }}</td>
                </tr>
                <tr>
                    <td>BUNDLE</td>
                    <td>: {{ $value->bundle }}</td>
                </tr>
                <tr>
                    <td>QC</td>
                    <td>: {{ $value->qc }}</td>
                </tr>
                <tr>
                    <td>CUTTER</td>
                    <td>: {{ $value->cutter }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <div class="barcode">
                            <div class="img_barcode">
                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128') }}" alt="barcode"   />
                            </div>
                            <span class="barcode_number">{{ isset($value) ? $value->barcode_id : null }}</span >
                        </div>
                    </td>
                </tr>
            </table>
            @if(($num + 1) % 2 == 0 && $num != (count($data) - 1))
            <hr style="clear: left; margin: -0.47em; border-width: 0.1px;" />
            @endif
            @if (($num+1) == $count_komponen)
                <div class="page-break"></div>
                @php
                    $num = -1;
                @endphp
            @endif
            
            @php
                $num++;
            @endphp
            @endforeach
        @endif
    @else
    <table style="width:100%;">
        <tr>
            <td colspan="8"></td>
            <td></td>
        </tr>
    </table>
    @endif
    </div>
    