<style type="text/css">

    @page {
        margin: 0 20 0 20;
    }
    
    @font-face {
        font-family: 'impact' !important;
        src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
    }
    
    .template {
       font-size: 16 !important;
       //font-family: 'impact';
    }
    
    .page-break {
        page-break-after: always;
    }

    .separator {
        margin: 30 0 0 0;
    }

    .container {
        display: flex;
        flex-wrap: wrap;
    }

    .break {
        flex-basis: 100%;
        height: 0;
    }
    
    .print-friendly {
        //height: 30%;
        width: 100%;
        line-height: 0px;
        padding-top: 8px;
        margin-left: -5px;
        margin-right: -5px;
        font-size: 14px;
        border-right: thick solid #E0DCDC;
        border-width: 0.2px;
        margin-right: 1em;
    }
    
    .print-friendly-single {
        height: 15%;
        width: 100%;
        //font-family: sans-serif;
        font-size: 14px !important;
        line-height: 6px;
        padding-top: 10px;
    }
    
    table.print-friendly tr td, table.print-friendly tr th,
    table.print-friendly-single tr td, table.print-friendly-single tr th, {
        page-break-inside: avoid;
        padding: 7.6px;
    }
    
    .barcode {
        float: left;
        margin-bottom: 20px;
        line-height: 16px;
        position:absolute;
        top: -5.8em;
        //margin-left: 230px;
        //transform: rotate(270deg);
        font-family: 'impact';
    }
    
    .img_barcode {
        display: block;
        padding: 0px;
    }
    
    .img_barcode > img {
        width: 166px;
        height: 30px;
    }
    
    .barcode_number {
        font-family: sans-serif;
        font-size: 14px !important;
    }
    
    .area_barcode {
        width: 25%;
    }
    
    p{
        line-height: 0.5;
    }

    .tg  {
        border-collapse:collapse;
        border-spacing:0;
        width: 100%;
        border-width: 1px;
    }
    .tg td{
        font-family:sans-serif;
        font-size:14px;
        padding:2px 2px;
        border-style:solid;
        border-width:1px;
        overflow:hidden;
        word-break:normal;
        border-color:black;
    }
    .tg th{
        font-family:sans-serif;
        font-size:14px;
        font-weight:normal;
        padding:2px 2px;
        border-style:solid;
        border-width:1px;
        overflow:hidden;
        word-break:normal;
        border-color:black;
    }
    .tg .tg-cly1{
        text-align:left;
        vertical-align:middle
    }
    .tg .tg-nrix{
        text-align:center;
        vertical-align:middle
    }
    .tg .tg-nrix1{
        text-align:center;
        background-color: #a9a9a9;
        vertical-align:middle
    }
    .tg .tg-0lax{
        text-align:left;
        vertical-align:top
    }
    
    </style>
    
    @if(isset($data))
        @if(count($data) == 1)
        <table class="print-friendly" style="width: 26%; float: left; padding-left: -10px;">
            <tr>
                <td style="width: 5.2rem;">CUT</td>
                <td colspan="2">: {{ $data[0]->cut_number.' '.$data[0]->cut_info }}</td>
            </tr>
            <tr>
                <td>KOMPONEN</td>
                @php($proname = $data[0]->process_name != null ? ' ('.$data[0]->process_name.')' : '')
                <td colspan="2">: {{ strlen($data[0]->komponen_name.$proname) <= 46 ? $data[0]->komponen_name.$proname : substr($data[0]->komponen_name.$proname, 0, 46).'.. )' }}</td>
            </tr>
            <tr>
                <td>STYLE</td>
                <td colspan="2">: {{ $data[0]->style_edit }}</td>
            </tr>
            <tr>
                <td>LOT</td>
                <td colspan="2">: {{ $data[0]->po_number_edit }}</td>
            </tr>
            <tr>
                <td>COLL</td>
                <td colspan="2">: {{ $data[0]->color_edit }}</td>
            </tr>
            <tr>
                <td><b>SIZE</b></td>
                <td colspan="2"><b>: {{ $data[0]->size_edit }}</b></td>
            </tr>
            <tr>
                <td>NO BUNDLE</td>
                <td colspan="2">: {{ $data[0]->no_bundle }}</td>
            </tr>
            <tr>
                <td>QTY</td>
                <td colspan="2">: {{ $data[0]->qty }}</td>
            </tr>
            <tr>
                <td>NO STICKER</td>
                <td colspan="2">: {{ $data[0]->sticker_no }}</td>
            </tr>
            <tr>
                <td>TGL</td>
                <td>: {{ \Carbon\Carbon::parse($data[0]->cutting_date)->format('d/m') }}</td>
            </tr>
            <tr>
                <td>BUNDLE</td>
                <td>: {{ $data[0]->bundle }}</td>
            </tr>
            <tr>
                <td>CUTTER</td>
                <td>: {{ $data[0]->cutter }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>
                    <div class="barcode">
                        <div class="img_barcode">
                            <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data[0]->barcode_id, 'C128') }}" alt="barcode"   />
                        </div>
                        <span class="barcode_number">{{ isset($data[0]) ? $data[0]->barcode_id : null }}</span >
                    </div>
                </td>
            </tr>
        </table>
        @else
            @foreach($data as $key => $value)
            <table class="print-friendly" style="width: 26%; float: left; padding-left: -10px;">
                <tr>
                    <td style="width: 5.2rem;">CUT</td>
                    <td colspan="2">: {{ $value->cut_number.' '.$value->cut_info }}</td>
                </tr>
                <tr>
                    <td>KOMPONEN</td>
                    @php($proname = $value->process_name != null ? ' ('.$value->process_name.')' : '')
                    <td colspan="2">: {{ strlen($value->komponen_name.$proname) <= 46 ? $value->komponen_name.$proname : substr($value->komponen_name.$proname, 0, 46).'.. )' }}</td>
                </tr>
                <tr>
                    <td>STYLE</td>
                    <td colspan="2">: {{ $value->style_edit }}</td>
                </tr>
                <tr>
                    <td>LOT</td>
                    <td colspan="2">: {{ $value->po_number_edit }}</td>
                </tr>
                <tr>
                    <td>COLL</td>
                    <td colspan="2">: {{ $value->color_edit }}</td>
                </tr>
                <tr>
                    <td><b>SIZE</b></td>
                    <td colspan="2"><b>: {{ $value->size_edit }}</b></td>
                </tr>
                <tr>
                    <td>NO BUNDLE</td>
                    <td colspan="2">: {{ $value->no_bundle }}</td>
                </tr>
                <tr>
                    <td>QTY</td>
                    <td colspan="2">: {{ $value->qty }}</td>
                </tr>
                <tr>
                    <td>NO STICKER</td>
                    <td colspan="2">: {{ $value->sticker_no }}</td>
                </tr>
                <tr>
                    <td>TGL</td>
                    <td>: {{ \Carbon\Carbon::parse($value->cutting_date)->format('d/m') }}</td>
                </tr>
                <tr>
                    <td>BUNDLE</td>
                    <td>: {{ $value->bundle }}</td>
                </tr>
                <tr>
                    <td>CUTTER</td>
                    <td>: {{ $value->cutter }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <div class="barcode">
                            <div class="img_barcode">
                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128') }}" alt="barcode"   />
                            </div>
                            <span class="barcode_number">{{ isset($value) ? $value->barcode_id : null }}</span >
                        </div>
                    </td>
                </tr>
            </table>
            @if(($key + 1) % 4 == 0 && $key != (count($data) - 1))
            <hr style="clear: left; margin: -0.5em; border-width: 0.1px;" />
            @endif
            @endforeach
        @endif
    @else
    <table style="width:100%;">
        <tr>
            <td colspan="8"></td>
            <td></td>
        </tr>
    </table>
    @endif
    </div>
    