<div class="table-responsive">
    <table class="table" id="table-orderline">
        <thead>
            <tr>
                <th style="min-width: 120px;">PO</th>
                <th>Style</th>
                <th>Article</th>
                <th>Size</th>
                <th>From</th>
                <th>Qty</th>
                <th>Total</th>
                @if (\Auth::user()->is_nagai)
                <th>No.Bundle</th>
                @endif
                <th>Cut</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $key => $val)
                <tr>
                    <td>
                        <div id="po_{{$val->id}}">
                            <input type="text"
                                    data-id="{{$val->id}}"
                                    class="form-control po_filled"
                                    value="{{$val->po_number_edit}}">
                        </div>
                    </td>
                    <td>{{ $val->style_edit }}</td>
                    <td>{{ $val->article_edit ? $val->article_edit : $val->color_edit }}</td>
                    <td>{{ $val->size_edit }}</td>
                    <td>{{ $val->from }}</td>
                    <td>{{ $val->qty }}</td>
                    <td>{{ $val->total_qty }}</td>
                    @if (\Auth::user()->is_nagai)
                    <td>{{ $val->no_bundle }}</td>
                    @endif
                    <td>{{ $val->cut_number }}</td>
                    <td>
                        <button type="button" id="choose-orderline"
                                data-id="{{ $val->id }}"
                                data-orderline="{{ $val->c_orderline_id }}"
                                data-posummaryid="{{ $val->po_summary_id }}"
                                data-from="{{ $val->from }}"
                                data-qty="{{ $val->qty }}"
                                data-total="{{ $val->total_qty }}"
                                data-style="{{ $val->style }}"
                                title="delete"
                                class="btn btn-danger">
                                <i class="icon-close2"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<script type="text/javascript">
    var tables = $('#table-orderline').DataTable({  
        buttons: [
        ],
        "lengthChange": false,
        "pageLength": 5
        /*"columnDefs": [{
            "targets" : [0],
            "visible" : false,
            "searchable" : false
        }]*/
    });
</script>
