@extends('layouts.app', ['active' => 'checkkomponen'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Cutting Check List Komponen</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('cutting.checkKomponen') }}"><i class="icon-list position-left"></i> Cutting</a></li>
            <li class="active">Check List Komponen</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="row">
            <div class="form-group">
                <label class="col-sm-3 control-label">List komponen /page (A3):</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control text-right" id="list_komponen_break" placeholder="List komponen /page" value="{{$factories->list_komponen_break}}" onkeypress="return isNumberKey(event)" required>
                </div>
            </div>
        </div>
        <div class="row pull-right">
            <label class="radio-inline"><input type="radio" name="radio_paper" checked="checked" value="a3">Paper A3</label>
            <label class="radio-inline"><input type="radio" name="radio_paper" value="a4">Paper A4</label>
        </div>
    </div>
    <hr>
    <div class="panel-body">
        <form action="{{ route('cutting.getDataCheckKomponen') }}" id="form_filter">
            <div class="form-group" id="filter_by_po">
				<label><b>Choose PO Number</b></label>
				<div class="input-group">
                    <input type="text" class="form-control text-uppercase" name="po" id="po" placeholder="PO Number">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
            </div>
            <div class="form-group hidden" id="filter_by_style">
				<label><b>Choose Style</b></label>
				<div class="input-group">
                    <input type="text" class="form-control text-uppercase" name="style" id="style" placeholder="Style">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="po">Filter by PO Number</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="style">Filter by Style</label>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
        <span id="filter_date" style="float:right"><b></b></span>
    </div>
    <div class="panel-body loading-area">
        <table class="table datatable-save-state" id="table-po-list">
            <thead>
                <tr>
                    <th>#</th>
                    <th>No Sticker</th>
                    <th>PO Number</th>
                    <th>Style</th>
                    <th>Article</th>
                    <th>Color</th>
                    <th>Size</th>
                    <th>Cutt</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<a href="{{ route('cutting.printPreviewKomponen') }}" id="print_preview"></a>
<a href="{{ route('updateListKomponenBreak') }}" id="updateListKomponenBreak"></a>
@endsection

@section('page-modal')
@endsection

@section('page-js') 
<script type="text/javascript" src="{{ url('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript">
//$(document).ready(function() {
    //url
    var url = $('#form_filter').attr('action');

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#table-po-list').DataTable({
        buttons: [
              /*  {
                    text: 'Print All',
                    className: 'btn btn-sm bg-primary printAll'
                }*/
        ],
        ajax: {
            url: url,
            //api
            data: {po_number: $('#po').val(), style: $('#style').val(), radio_status: $('input[name=radio_status]:checked').val()}

        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
           // $('td', row).eq(5).css('width', '120px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
        //    {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
            {data: null, sortable: false, orderable: false, searchable: false  },
            {data: 'sticker_no', name: 'sticker_no'},
            {data: 'po_number', name: 'po_number'},
            {data: 'style', name: 'style'},
            {data: 'article', name: 'article'},
            {data: 'color', name: 'color'},
            {data: 'size', name: 'size'},
            {data: 'cut_number', name: 'cut_number'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
    });
    //end of datatables

    table
    .on( 'preDraw', function () {
        Pace.start();
    } )
    .on( 'draw.dt', function () {
        $('#table-po-list').unblock();
        Pace.stop();
    } );

    var _token = $("input[name='_token']").val();

    //
    $('#table-list').on('click','.ignore-click', function() {
        return false;
    });

    //print all
    $(document).on('click', '.printAll', function(event){
        event.preventDefault();

       var url_print = $('#print_preview').attr('href');
       var po_number = $.trim($('#po').val());
       var style = $.trim($('#style').val());

       window.open(url_print + '?radio_filter='+$('input[type=radio][name=radio_status]:checked').val()+'&po_number='+po_number+'&style='+style, '_blank');
        
    });
    
    //print
    $(document).on('click', '.set-completed', function(event){
        event.preventDefault();

        var url_print = $(this).attr('href');

        window.open(url_print + '&paper='+$('input[type=radio][name=radio_paper]:checked').val(), '_blank');
        
        
    });

    // update list komponen
    $('#list_komponen_break').on('blur', function(){
        var list_komponen_break = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updateListKomponenBreak').attr('href'),
            data: {list_komponen_break:list_komponen_break},
            success: function(response){
                $('#list_komponen_break').html(response);

            },
            error: function(response) {
                return false;
            }
        });
    })

    //filter (api)
    $('#form_filter').submit(function(event){
        event.preventDefault();
        var po_number  = $('#po').val();
        var style  = $('#style').val();
        var radio_status = $('input[name=radio_status]:checked').val();

        //check location
        /*if(date_range == '' && po_number == '') {
            myalert('error','Please select filter first');
            return false;
        }*/

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : url,
            data: {po_number: po_number, style: style, radio_status: radio_status},
            beforeSend: function() {
                $('#table-po-list').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#table-po-list').unblock();

                if(radio_status == 'po') {
                    $('#filter_date').text('CURRENT FILTER: PO Number #' + po_number);
                }
                else if(radio_status == 'promise') {
                    $('#filter_date').text('CURRENT FILTER: ' + date_range);
                }
                else if(radio_status == 'fstyle') {
                    $('#filter_date').text('CURRENT FILTER: ' + style);
                }
                $('#filter_date').addClass('label-striped');
                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#table-po-list').unblock();
                myalert('error',response['responseJSON']);
            }
        });
    });

    //choose filter
    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'po') {
            if($('#filter_by_po').hasClass('hidden')) {
                $('#filter_by_po').removeClass('hidden');
            }

            $('#filter_by_style').addClass('hidden');
        }
        else if (this.value == 'style') {
            if($('#filter_by_style').hasClass('hidden')) {
                $('#filter_by_style').removeClass('hidden');
            }

            $('#filter_by_po').addClass('hidden');
        }

        $('#form_filter').submit();
    });

    function printPage(po_number) {
        var url_print = $('#print_preview').attr('href');
        var data = [];
        
        var parameter = '?filter_po=' + po_number;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : url_print + parameter,
            success: function(response) {
                window.open(url_print + parameter, '_blank');
                //$('input:checkbox').removeAttr('checked');
                table.draw();
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
    }

    function toggle(source) {
        checkboxes = document.getElementsByName('selector[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
          checkboxes[i].checked = source.checked;
        }
    }

    //get parameter from url
    function getURLParameter(url) {
        var params = {};
        url.substring(1).replace(/[?&]+([^=&]+)=([^&]*)/gi,
                function (str, key, value) {
                     params[key] = value;
                });
        return params;
    }

    function loading(){
        $('#table-po-list').block({
            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: '10px 15px',
                color: '#fff',
                width: 'auto',
                '-webkit-border-radius': 2,
                '-moz-border-radius': 2,
                backgroundColor: '#333'
            }
        });
    }

//});
</script>
@endsection
