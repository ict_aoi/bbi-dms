<!-- MODAL EDIT -->
<div id="modal_edit_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('cutting.updateStyle') }}" id="form-update" enctype="multipart/form-data">
			@csrf
            <div class="modal-content">
              	<div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                  	<div class="panel-body loader-area">
                      	<fieldset>
                          	<legend class="text-semibold">
                              	<i class="icon-file-text2 position-left"></i>
                              	<span id="title"> UPDATE STYLE</span> <!-- title -->
                              </legend>
                              
                              <input type="hidden" class="form-control" name="komponenIdUpdate" id="komponenIdUpdate" placeholder="" readonly>

                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">Style:</label>
                              	<div class="col-lg-9">
                                  	<input type="text" class="form-control" name="styleUpdate" id="styleUpdate" placeholder="Style" readonly>
                              	</div>
                          	</div>
                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">Part:</label>
                              	<div class="col-lg-9">
                                  	<input type="text" class="form-control" name="partUpdate" id="partUpdate" placeholder="Part">
                              	</div>
                          	</div>
                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">Komponen:</label>
                              	<div class="col-lg-9">
                                  	<input type="text" class="form-control" name="komponenNameUpdate" id="komponenNameUpdate" placeholder="" readonly>
                              	</div>
                          	</div>
                          	<div class="form-group hidden">
                              	<label class="col-lg-3 control-label text-semibold">Komponen:</label>
                              	<div class="col-lg-9">
									  <select data-placeholder="Select a State..." class="form-control select-search" name="komponenUpdate" id="komponenUpdate">
										@foreach($komponen as $komp)
											<option value="{{ $komp->id }}">{{ $komp->name }}</option>
										@endforeach
									</select>
                              	</div>
                          	</div>
                          	<div class="form-group">
								<label class="col-lg-3 control-label">Proses:</label>
								<div class="col-lg-9">
									<select multiple data-placeholder="Select a State..." class="form-control select-search" name="process_id_update[]" id="process_id_update">
										@foreach($process as $proc)
											<option value="{{ $proc->id }}">{{ $proc->process_name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Total:</label>
								<div class="col-lg-9">
									<input type="text" class="form-control" name="totalUpdate" id="totalUpdate" placeholder="Total" value="1" onkeypress="return isNumberKey(event)">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Type:</label>
								<div class="col-lg-9">
									<select class="form-control" name="type_id_update" id="type_id_update">
										@foreach($types as $type)
											<option value="{{ $type->id }}">{{ $type->type_name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3">Inhouse ? :</label>
								<div class="col-lg-3">
									<input type="checkbox" name="is_inhouse_update" id="is_inhouse_update">
								</div>                                        
							</div>
                            <div class="form-group">
                                <label class="col-lg-3">Is Special ? :</label>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="is_special_update" id="is_special_update" >
                                </div>                                        
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3">Big Size ? :</label>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="is_big_size_update" id="is_big_size_update" >
                                </div>                                        
                            </div>
                          	<hr>
                          	<div class="form-group text-center">
                          		<button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
                          		<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                          	</div>
                      	</fieldset>
                  	</div>
              	</div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL EDIT -->