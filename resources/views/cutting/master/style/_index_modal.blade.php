<!-- MODAL ADD -->
<div id="modal_add_new_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('cutting.addStyle') }}" id="form-add" enctype="multipart/form-data">
			@csrf
            <div class="modal-content">
              	<div class="modal-body">
                    <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                  	<div class="panel-body loader-area">
                      	<fieldset>
                          	<legend class="text-semibold">
                              	<i class="icon-file-text2 position-left"></i>
                              	<span id="title"> ADD NEW STYLE</span> <!-- title -->
                          	</legend>

                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">Style:</label>
                              	<div class="col-lg-9">
                                  	<input type="text" class="form-control" name="modalstyle" id="modalstyle" placeholder="Style" readonly>
                              	</div>
                          	</div>
                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">Part:</label>
                              	<div class="col-lg-9">
                                  	<input type="text" class="form-control" name="part" id="part" placeholder="Part">
                              	</div>
                          	</div>
                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">Komponen:</label>
                              	<div class="col-lg-9">
									  <select data-placeholder="Select a State..." class="form-control select-search" name="komponen" id="komponen">
										@foreach($komponen as $komp)
											<option value="{{ $komp->id }}">{{ $komp->name }}</option>
										@endforeach
									</select>
                              	</div>
                          	</div>
                          	<div class="form-group">
								<label class="col-lg-3 control-label">Proses:</label>
								<div class="col-lg-9">
									<select multiple data-placeholder="Select a State..." class="form-control select-search" name="process_id[]" id="process_id">
										@foreach($process as $proc)
											<option value="{{ $proc->id }}">{{ $proc->process_name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Total:</label>
								<div class="col-lg-9">
									<input type="text" class="form-control" name="total" id="total" placeholder="Total" value="1" onkeypress="return isNumberKey(event)">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Type:</label>
								<div class="col-lg-9">
									<select class="form-control" name="type_id" id="type_id">
										@foreach($types as $type)
											<option value="{{ $type->id }}">{{ $type->type_name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3">Inhouse ? :</label>
								<div class="col-lg-3">
									<input type="checkbox" name="is_inhouse" id="is_inhouse" checked="checked">
								</div>                                        
							</div>
                            <div class="form-group">
                                <label class="col-lg-3">Is Special ? :</label>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="is_special" id="is_special" >
                                </div>                                        
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3">Big Size ? :</label>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="is_big_size" id="is_big_size" >
                                </div>                                        
                            </div>
                          	<hr>
                          	<div class="form-group text-center">
                          		<button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
                          		<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                          	</div>
                      	</fieldset>
                  	</div>
              	</div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL ADD -->