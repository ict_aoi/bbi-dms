@extends('layouts.app', ['active' => 'masterstyle'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">STYLE & KOMPONEN</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('cutting.masterStyle') }}"><i class="icon-home4 position-left"></i> STYLE</a></li>
            <li class="active">Master</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('cutting.ajaxGetDataMasterStyle') }}" id="form_filter">
            <div class="form-group hidden" id="filter_by_lcdate">
				<label><b>Choose Order Date (month/day/year)</b></label>
				<div class="input-group">
					<span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
			</div>
            <div class="form-group" id="filter_by_style">
				<label><b>Choose Style</b></label>
                    <div class="input-group col-md-12">
                        <select data-placeholder="Select a State..." name="style" id="style" class="form-control select-search">
                            <option value=""></option>
                            @foreach($styles as $list)
                                <option value="{{ $list->style }}">{{ $list->style }}</option>
                            @endforeach
                        </select>
                        <div class="input-group-btn hidden">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </div>
			</div>
            <div class="form-group hidden">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="style">Filter by Style</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="lc">Filter by LC Date</label>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading hidden">
        <div class="form-group">
            <button type="button" class="btn btn-xs btn-primary add_new">Add Style <span class="icon-plus2"></span></button>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table_list">
                <thead>
                    <tr>
                        <th style="width:10px;">#</th>
                        <th>STYLE</th>
                        <th>PART</th>
                        <th>KOMPONEN</th>
                        <th>TYPE</th>
                        <th>PROCESS</th>
                        <th>TOTAL</th>
                        <th style="width:10px;">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('cutting.ajaxGetDataGetListStyle') }}" id="get-style"></a>
<a href="{{ route('cutting.styleSync') }}" id="sync-style"></a>
@endsection

@section('page-modal')
    @include('cutting.master.style._index_modal')
    @include('cutting.master.style._update_modal')
    @include('cutting.master.style._sync_modal')
@endsection

@section('page-js')
<script type="text/javascript">

    $('#style').select2({
        placeholder: "Select a State",
        minimumInputLength: 3,
        formatInputTooShort: function () {
            return "Enter Minimum 3 Character";
        },
    });

    $('#style').on('change', function(){
        if($(this).val() != ''){
            $('#form_filter').submit();
        }
    });
//$( document ).ready(function() {

    var url = $('#form_filter').attr('action');
    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#table_list').DataTable({
        ajax: url,
        data: {
            date_range: $('#date_range').val(), 
            style: $('#style').val(), 
            radio_status: $('input[name=radio_status]:checked').val()
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false },
            {data: 'style', name: 'style'},
            {data: 'part', name: 'part'},
            {data: 'komponen_name', name: 'komponen_name'},
            {data: 'type_name', name: 'type_name'},
            {data: 'process_name', name: 'process_name'},
            {data: 'total', name: 'total'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
    //end of datatables

    //filter (api)
    $('#form_filter').submit(function(event){
        event.preventDefault();
        var date_range = $('#date_range').val();
        var style  = $('#style').val();
        var radio_status = $('input[name=radio_status]:checked').val();

        //check location
        if(date_range == '' && style == '') {
            myalert('error','Please select filter first');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : url,
            data: {date_range: date_range, style: style, radio_status: radio_status},
            beforeSend: function() {
                $('#table_list').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#table_list').unblock();
                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#table_list').unblock();
                myalert('error',response['responseJSON']);
            }
        });
    });

    //add new
    $('.add_new').on('click',function(){
        $('#modal_add_new_').modal('show');
    });
    //end of add new
    
    $('.syncStyle').on('click',function(){
        $('#modal_sync_').modal('show');
    });

    $('#modal_add_new_').on('shown.bs.modal', function(){
        $('input[name=part]').focus();
    });

    $('#modal_edit_').on('shown.bs.modal', function(){
        $('input[name=partUpdate]').focus();
    });

    $('#modal_add_new_').on('hidden.bs.modal', function(){
        $('#form-add').trigger("reset");
        $('#process_id').val(null).trigger('change');
        $('#total').val(1);
        $('#part').val('');

        $('#form_filter').submit();
    });

    $('#modal_edit_').on('hidden.bs.modal', function(){
        $('#form-update').trigger("reset");
        $('#process_id_update').val(null).trigger('change');
        $('#totalUpdate').val(1);
        $('#partUpdate').val('');

        $('#form_filter').submit();
    });

    //open modal upload (api)
    $('#table_list').on('click', '.upload', function() {
        var str = $(this).data('value');

        $('input[name=modalstyle]').val(str);
        $('#modal_add_new_').modal('show');
    });

    //open modal sync
    $('#table_list').on('click', '.upload_sync', function(){
        var str = $(this).data('value');

        $('#style_sync').val(str);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url: $('#get-style').attr('href'),
            beforeSend: function() {
                $('#modal_sync_ > .modal-content').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response){
                $('#modal_sync_ > .modal-content').unblock();
                $('#listing').html(response);
            }
        });

        
        $('#modal_sync_').modal('show');
    });

    $('#listing').on('click','#choose-style', function(){
        var style = $(this).data('style');
        var style_id = $(this).data('id');

        bootbox.confirm("Are you sure sync style " +$('#style_sync').val() + " like " +style+ "  ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: $('#sync-style').attr('href'),
                   // type: "GET",
                    data: {
                        "style_sync": $('#style_sync').val(),
                        "style": style,
                        "style_id": style_id
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $(".loader-area").unblock();
                    },
                    success: function () {
                        myalert('success','Data synchronized succesfully');

                        $('#form_filter').submit();
                    }
                });
            }
        });
    });
    
   /* $('#table_list').on('click', '.edits', function() {
        var str = $(this).data('value');
        var str_sp = str.split("|");

        $('input[name=modalstyle]').val(str);
        $('#modal_edit_').modal('show');
    });*/

    //delete
    $('#table_list').on('click', '.deleteStyle', function() {
        event.preventDefault();
        var id = $(this).data('id');
        var style = $(this).data('style');
        var komponen = $(this).data('komponen');

        if(id == 'kosong' || style == 'kosong' || komponen == 'kosong') {
            return false;
        }
        var token = $(this).data("token");
        bootbox.confirm("Are you sure delete this row ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "master-style/delete-style/"+id+"/"+komponen,
                    type: "GET",
                    data: {
                        "id": id,
                        "style": style,
                        "komponen": komponen,
                        "_method": 'DELETE',
                        "_token": token,
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $(".loader-area").unblock();
                    },
                    success: function () {
                        myalert('success','Data has been deleted');
                        //table.ajax.reload();

                        $('#form_filter').submit();
                    }
                });
            }
        });
    });
    //end of delete

    //add new
    $('#form-add').submit(function(event) {
        event.preventDefault();

        if($('#total').val() < 1){
            myalert('error', 'total must be > 0..!');
            return false;
        }

        let is_big_size = false;
        let is_special = false;

        if ($('#is_big_size').is(':checked')) {
            is_big_size = true;
        }else{
            is_big_size = false;
        }

        if ($('#is_special').is(':checked')) {
            is_special = true;
        }else{
            is_special = false;
        }

        var formData = new FormData($(this)[0]);
        var parameter = ['modalstyle', 'part', 'komponen', 'process_id', 'total', 'is_inhouse', 'is_big_size', 'is_special'];
        var val = [
            $('#modalstyle').val(), 
            $('#part').val(), 
            $('#komponen').val(), 
            $('#process_id').val(), 
            $('#total').val(),
            $('#is_inhouse').val(),
            is_big_size,
            is_special
        ]
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-add').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
            	$('.loader-area').unblock();
            },
            success: function(response) {
                myalert('success','GOOD');
                $('#process_id').val(null).trigger('change');
                $('#total').val(1);
                $('#part').val('').focus();
                //table.ajax.reload();

                //$('#form_filter').submit();
            },
            error: function(response) {
                console.log(response);
                myalert('error',response['responseJSON']);
            }
        })
    });

    //update
    $('#form-update').submit(function(event) {
        event.preventDefault();

        if($('#totalUpdate').val() < 1){
            myalert('error', 'total must be > 0..!');
            return false;
        }

        let is_big_size = false;
        let is_special = false;

        if ($('#is_big_size_update').is(':checked')) {
            is_big_size = true;
        }else{
            is_big_size = false;
        }

        if ($('#is_special_update').is(':checked')) {
            is_special = true;
        }else{
            is_special = false;
        }
        
        var formData = new FormData($(this)[0]);
        var parameter = ['styleUpdate', 'partUpdate', 'komponenUpdate', 'process_id_update', 'totalUpdate', 'is_inhouse_update', 'is_big_size_update', 'is_special_update'];
        var val = [
            $('#styleUpdate').val(), 
            $('#partUpdate').val(), 
            $('#komponenIdUpdate').val(), 
            $('#process_id_update').val(), 
            $('#totalUpdate').val(),
            $('#is_inhouse_update').val(),
            is_big_size,
            is_special
        ]
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-update').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
            },
            complete: function () {
            	$.unblockUI();
            },
            success: function(response) {
                myalert('success','GOOD');
                $('#process_id_update').val(null).trigger('change');
                $('#totalUpdate').val(1);
                $('#partUpdate').val('').focus();

                //table.ajax.reload();

                //$('#form_filter').submit();
            },
            error: function(response) {
                console.log(response);
                myalert('error',response['responseJSON']);
            }
        })
    });

    //choose filter
    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'style') {
            if($('#filter_by_style').hasClass('hidden')) {
                $('#filter_by_style').removeClass('hidden');
            }

            $('#filter_by_lcdate').addClass('hidden');
        }
        else if (this.value == 'lc') {
            if($('#filter_by_lcdate').hasClass('hidden')) {
                $('#filter_by_lcdate').removeClass('hidden');
            }

            $('#filter_by_style').addClass('hidden');
        }
    });

    function edit(url){

        $.ajax({
			type: "get",
			url: url,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();

			}
		})
		.done(function (response) {
            console.log(response);
            var process = response.process;

            $('#styleUpdate').val(response.style);
            $('#partUpdate').val(response.part);
            //$('#komponenUpdate option[value='+response.komponen+']').prop('selected', true).trigger('change');
            $('#totalUpdate').val(response.total);

            $('#komponenIdUpdate').val(response.komponen);
            $('#komponenNameUpdate').val(response.komponen_name);

            $('#type_id_update').val(response.type_id);

            if(response.is_inhouse){
                $('#is_inhouse_update').prop('checked', true);
            }else{
                $('#is_inhouse_update').prop('checked', false);
            }

            if (response.is_big_size) {
                $('#is_big_size_update').prop('checked', true);
            }else{
                $('#is_big_size_update').prop('checked', false);
            }

            if (response.is_special) {
                $('#is_special_update').prop('checked', true);
            }else{
                $('#is_special_update').prop('checked', false);
            }

            if(process != null){
                $.each(process.split(","), function(i, v){
                    $('#process_id_update option[value='+ v +']').prop('selected', true).trigger('change');
                });
            }else{
                $('#process_id_update').val(null).trigger('change');
            }

			$('#modal_edit_').modal();

		});
    }

//});

</script>
@endsection
