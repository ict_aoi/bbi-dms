@extends('layouts.app', ['active' => 'masterprocess'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">PROCESS</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('cutting.masterProcess') }}"><i class="icon-home4 position-left"></i> Cutting</a></li>
            <li class="active">Master Process</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	<div class="panel panel-flat">
	    <div class="panel-heading">
	        <div class="form-group">
	            <button type="button" class="btn btn-xs btn-primary add_new">Add Process <span class="icon-plus2"></span></button>
	        </div>
	    </div>
	    <div class="panel-body">
            <div class="table-responsive">
    	        <table class="table datatable-save-state" id="table_list">
    	            <thead>
    	                <tr>
    	                    <th style="width:10px;">#</th>
    	                    <th>NAME</th>
    	                    <th style="width:10px;">ACTION</th>
    	                </tr>
    	            </thead>
    	        </table>
            </div>
	    </div>
	</div>

	<a href="{{ route('cutting.ajaxGetDataMasterProcess') }}" id="get_data"></a>
@endsection

@section('page-modal')
    @include('cutting.master.process._index_modal')
    @include('cutting.master.process._update_modal')
@endsection

@section('page-js')
<script type="text/javascript">
$( document ).ready(function() {
    //datatables
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var url = $('#get_data').attr('href');
    var table = $('#table_list').DataTable({
        ajax: url,
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false  },
            {data: 'process_name', name: 'process_name'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
    //end of datatables

    //add
    $('.add_new').on('click',function(){
        $('#modal_add_new_').modal('show')
    });
    //end of add

    $('#modal_add_new_').on('shown.bs.modal', function(){
        $('input[name=process_name]').focus();
    });

    $('#modal_edit_').on('shown.bs.modal', function(){
        $('input[name=process_name_update]').focus();
    });

    $('#modal_edit_').on('hidden.bs.modal', function(){
        $('#form-update').trigger("reset");
        $('#id_update').val(null).trigger('change');
        $('#locator_name_update').val('');

        table.draw();
    });

    //delete
    $("#table_list").on("click", ".delete", function() {
        event.preventDefault();
        var id = $(this).data('id');
        if(id == 'kosong') {
            return false;
        }
        var token = $(this).data("token");
        bootbox.confirm("Are you sure delete this row ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "master-process/delete/"+id,
                    type: "GET",
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": token,
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $(".loader-area").unblock();
                    },
                    success: function () {
                        myalert('success','Data has been deleted');
                        table.ajax.reload();
                    }
                });
            }
        });
    });
    //end of delete

    //add new
    $('#form-add').submit(function(event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-add').attr('action'),
            data: $('#form-add').serialize(),
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
            	$('.loader-area').unblock();
            },
            success: function(response) {
                myalert('success','GOOD');
                $('#form-add').trigger("reset");
                $('#modal_add_new_').trigger('toggle');
                table.ajax.reload();
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        })
    });

    //update
    $('#form-update').submit(function(event) {
        event.preventDefault();

        var formData = new FormData($(this)[0]);
        var parameter = ['id_update', 'process_name_update'];
        var val = [
            $('#id_update').val(), 
            $('#process_name_update').val()
        ]
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-update').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
            },
            complete: function () {
            	$.unblockUI();
            },
            success: function(response) {
                myalert('success','GOOD');
                $('#process_name_update').focus();
            },
            error: function(response) {
                console.log(response);
                myalert('error',response['responseJSON']);
            }
        })
    });
});

function edit(url){

    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();

        }
    })
    .done(function (response) {

        var process_name = response.process_name;
        var id = response.id;

        $('#id_update').val(id);
        $('#process_name_update').val(process_name);

        $('#modal_edit_').modal();

    });
}

</script>
@endsection
