<!-- MODAL ADD SUBCONT -->
<div id="modal_add_new_subcont_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('cutting.addSubcont') }}" id="form-add">
            <div class="modal-content">
              	<div class="modal-body">
                    <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                  	<div class="panel-body loader-area">
                      	<fieldset>
                          	<legend class="text-semibold">
                              	<i class="icon-file-text2 position-left"></i>
                              	<span id="title"> ADD NEW SUBCONT</span> <!-- title -->
                          	</legend>

                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">Name:</label>
                              	<div class="col-lg-9">
                                  	<input type="text" class="form-control" name="name" placeholder="Subcont Name">
                              	</div>
                          	</div>
                          	
                          	<hr>
                          	<div class="form-group text-center">
                          		<button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
                          		<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                          	</div>
                      	</fieldset>
                  	</div>
              	</div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL ADD SUBCONT -->