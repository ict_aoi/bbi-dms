<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th>Style</th>
                <th>Part</th>
                <th>Komponen</th>
                <th>total</th>
                <th>process</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $key => $val)
                <tr>
                    <td>{{ $val->style }}</td>
                    <td>{{ $val->part }}</td>
                    <td>{{ $val->komponen_name }}</td>
                    <td>{{ $val->total }}</td>
                    <td>{{ $val->process_name }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
