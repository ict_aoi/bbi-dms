@extends('layouts.app', ['active' => 'barcoding'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Cutting Panel Detail </span>(po number : #{{$po_number}} / style : {{$style}} / size : {{$size}} / color : {{$color}} / article : {{$article}} )</h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('cutting.barcode') }}"><i class="icon-list position-left"></i> Cutting</a></li>
            <li class="active">Panel Detail</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
    <div class="panel-heading">
        {{--  <span id="filter_date" style="float:right"><b></b></span>  --}}
    </div>
    <div class="panel-body loading-area">
        <div class="table-responsive">
            <table class="table" id="table-list">
                <thead>
                    <tr>
                        <th><input type="checkbox" onclick="toggle(this)"></th>
                        <th>Barcode Id</th>
                        <th>Color</th>
                        <th>Article</th>
                        <th>Komponen</th>
                        <th>Cutt</th>
                        <th>No Sticker</th>
                        <th>Is Completed</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('cutting.ajaxPackageData') }}" id="package_detail_get_data" data-ponumber = "{{ $po_number}}" data-style = "{{ $style }}" data-size="{{ $size }}" data-color="{{ $color }}" data-article="{{ $article }}"></a>
<a href="{{ route('cutting.ajaxSetCompletedAll') }}" id="completed_all"></a>
<a href="{{ route('cutting.printPreview') }}" id="print_preview"></a>
@endsection

@section('page-modal')
@endsection

@section('page-js')
<script type="text/javascript" src="{{ url('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript">
   // $( document ).ready(function() {
        //datatables
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            autoLength: false,
            processing: true,
            serverSide: true,
            stateSave: true,
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            }
        });
        var url = $('#package_detail_get_data').attr('href');
        var ponumber = $('#package_detail_get_data').data('ponumber');
        var style = $('#package_detail_get_data').data('style');
        var size = $('#package_detail_get_data').data('size');
        var color = $('#package_detail_get_data').data('color');
        var article = $('#package_detail_get_data').data('article');

        var table = $('#table-list').DataTable({
            buttons: [
                {
                    text: 'Print All',
                    className: 'btn btn-sm bg-primary printAll'
                }
            ],
            ajax: {
                'url' : url,
                'data': {po_number: ponumber, style: style, size: size, color:color, article:article}
            },
            columns: [
                {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
                {data: 'barcode_id', name: 'barcode_id'},
                {data: 'color', name: 'color'},
                {data: 'article', name: 'article'},
                {data: 'komponen_name', name:'komponen_name'},
                {data: 'cut_number', name: 'cut_number'},
                {data: 'sticker_no', name: 'sticker_no', sortable: false, orderable: false, searchable: false},
                {data: 'is_printed', name: 'is_printed', sortable: false, orderable: false, searchable: false},
                {data: 'action', name: 'action', sortable: false, orderable: false, searchable: false}
            ]
        });
        //end of datatables

    //});

    //print all
    $(document).on('click', '.printAll', function(event){
        event.preventDefault();
        var ponumber = $('#package_detail_get_data').data('ponumber');
        var style = $('#package_detail_get_data').data('style');
        var size = $('#package_detail_get_data').data('size');
        var color = $('#package_detail_get_data').data('color');
        var article = $('#package_detail_get_data').data('article');

        var data = [];

        $('.clplanref:checked').each(function() {
            data.push({
                barcode_id: $(this).val()
            });
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#completed_all').attr('href'),
            data: {po_number: ponumber, style: style, size: size, color: color, article: article, data: JSON.stringify(data)},
            beforeSend: function() {
                $('.loading-area').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('.loading-area').unblock();
                printPage(null, ponumber, style, size, color, article);
            },
            error: function(response) {
                $('.loading-area').unblock();
                myalert('error','NOT GOOD');
            }
        });

    });

    //open window
    $('#table-list').on('click', '.set-completed', function(event) {
        event.preventDefault();
        var this_url = $(this).attr('href');
        var params = getURLParameter(this_url);
        var barcodeid = params['barcodeid'];
        var current = params['current_is_printed'];
        var ponumber = $('#package_detail_get_data').data('ponumber');
        var style = $('#package_detail_get_data').data('style');
        var size = $('#package_detail_get_data').data('size');
        var color = $('#package_detail_get_data').data('color');
        var article = $('#package_detail_get_data').data('article');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : this_url,
            data: {barcodeid: barcodeid, current_is_printed: current},
            beforeSend: function() {
                $('.loading-area').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                table.draw();
                $('.loading-area').unblock();
                printPage(barcodeid, ponumber, style, size, color, article);
            },
            error: function(response) {
                $('.loading-area').unblock();
                myalert('error','NOT GOOD');
            }
        });

    });

    function toggle(source) {
        checkboxes = document.getElementsByName('selector[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
          checkboxes[i].checked = source.checked;
        }
    }

    function printPage(barcode_id, po_number, style, size, color, article) {
        var url_print = $('#print_preview').attr('href');
        var data = [];

        $('.clplanref:checked').each(function() {
            data.push({
                barcode_id: $(this).val()
            });
        });

        if(barcode_id === null) {
            var parameter = '?po_number=' + po_number +
                            '&style=' + style +
                            '&size=' + size +
                            '&color=' + color +
                            '&article=' + article +
                            '&data=' + JSON.stringify(data);
        }
        else {
            var parameter = '?barcode_id=' + barcode_id + '&po_number=' + po_number +
                            '&style=' + style +
                            '&size=' + size +
                            '&color=' + color +
                            '&article=' + article +
                            '&data=' + JSON.stringify(data);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : url_print + parameter,
            success: function(response) {
                window.open(url_print + parameter, '_blank');
                $('input:checkbox').removeAttr('checked');
                table.draw();
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
    }

    //get parameter from url
    function getURLParameter(url) {
        var params = {};
        url.substring(1).replace(/[?&]+([^=&]+)=([^&]*)/gi,
                function (str, key, value) {
                     params[key] = value;
                });
        return params;
    }
</script>
@endsection
