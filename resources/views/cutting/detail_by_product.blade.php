@extends('layouts.app', ['active' => 'barcoding'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Cutting Panel Detail </span>(po number : {{$po_number}} / style : {{$style}} / size : {{$size}} / color : {{$color}} / article : {{$article}} )</h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('cutting.barcode') }}"><i class="icon-list position-left"></i> Cutting</a></li>
            <li class="active">Panel Detail</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label class="col-sm-4 control-label">List komponen /page (A3):</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control text-right" id="list_komponen_break" placeholder="List komponen /page" value="{{$factories->list_komponen_break}}" onkeypress="return isNumberKey(event)" required>
                    </div>
                </div>
            </div>
            {{--  <div class="col-md-2">
                <div class="form-group">
                    <div class="col-lg-9">
                        <select class="form-control" name="type_id" id="type_id">
                            <option value="0">All Type</option>
                            @foreach($types as $type)
                                <option value="{{ $type->id }}">{{ $type->type_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>  --}}
            <div class="col-md-2">
                <div class="form-group">
                        <label>break /komponen ? </label>&nbsp;<input type="checkbox" name="komponenBreak" id="komponenBreak" checked>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                        <label>List komponen /part ? </label>&nbsp;<input type="checkbox" name="listPart" id="listPart">
                </div>
            </div>
        </div>
        <div class="row pull-right">
            <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="a3">Paper A3</label>
            <label class="radio-inline"><input type="radio" name="radio_status" value="a4">Paper A4</label>
        </div>
        <div class="row pull-left">
            @php
                if(app('request')->input('status_print')=='all_print'){
                    $checked1 = ' checked="checked"';
                }else{
                    $checked1 = ' ';
                }

                if(app('request')->input('status_print')=='print_complete'){
                    $checked2 = ' checked="checked"';
                }else{
                    $checked2 = ' ';
                }

                if(app('request')->input('status_print')=='print_onprogress'){
                    $checked3 = ' checked="checked"';
                }else{
                    $checked3 = ' ';
                }
            @endphp
            <label class="radio-inline"><input type="radio" name="radio_status_printed" {{$checked1}} value="all_print">All Print Status</label>
            <label class="radio-inline"><input type="radio" name="radio_status_printed" {{$checked2}} value="print_complete">Print completed</label>
            <label class="radio-inline"><input type="radio" name="radio_status_printed" {{$checked3}} value="print_onprogress">Print onprogress</label>
        </div>
        <input type="hidden" name="status_printer" id="status_printer" value='all_print'>
    </div>
    <hr>
    <div class="panel-body loading-area">
        <div class="table-responsive">
            <table class="table" id="table-list">
                <thead>
                    <tr>
                        <th><input type="checkbox" onclick="toggle(this)"></th>
                        <th>Barcode Id</th>
                        <th>Color</th>
                        <th>Article</th>
                        <th>{{\Auth::user()->is_nagai ? 'No.Bundle' : 'Lot'}}</th>
                        <th>Komponen</th>
                        <th>Cutt</th>
                        <th>No Sticker</th>
                        <th>Qty</th>
                        <th>Is Completed</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('cutting.ajaxPackageDataByProduct') }}" id="package_detail_get_data" data-ponumber = "{{ $po_number}}" data-style = "{{ $style }}" data-size="{{ $size }}" data-color="{{ $color }}" data-article="{{ $article }}" data-product="{{ $m_product_id }}" data-orderline="{{ $c_orderline_id }}" data-statusprint="{{ $status_print }}"></a>
<a href="{{ route('cutting.ajaxSetCompletedAll') }}" id="completed_all"></a>
<a href="{{ route('cutting.printPreview') }}" id="print_preview"></a>
<a href="{{ route('cutting.printPreviewKomponen') }}" id="print_preview_komponen"></a>
<a href="{{ route('updateListKomponenBreak') }}" id="updateListKomponenBreak"></a>
@endsection

@section('page-modal')
@endsection

@section('page-js')
<script type="text/javascript" src="{{ url('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript">

    var getUrlParameters = function getUrlParameters(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
    
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
    
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };
   // $( document ).ready(function() {
        //datatables
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            autoLength: false,
            processing: true,
            serverSide: true,
            stateSave: true,
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            }
        });
        var url = $('#package_detail_get_data').attr('href');
        var ponumber = $('#package_detail_get_data').data('ponumber');
        var style = $('#package_detail_get_data').data('style');
        var size = $('#package_detail_get_data').data('size');
        var color = $('#package_detail_get_data').data('color');
        var article = $('#package_detail_get_data').data('article');
        var product = $('#package_detail_get_data').data('product');
        var orderline = $('#package_detail_get_data').data('orderline');
        var status_print = $('#package_detail_get_data').data('statusprint');
        //console.log(getUrlParameters('status_print'));
        var table = $('#table-list').DataTable({
            buttons: [
                {
                    text: 'Print All',
                    className: 'btn btn-sm bg-primary printAll'
                }
            ],
            ajax: {
                'url' : url,
                'data': {po_number: ponumber, style: style, size: size, color:color, article:article, m_product_id: product, c_orderline_id: orderline, status_print: status_print}
            },
            columns: [
                {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
                {data: 'barcode_id', name: 'barcode_id', sortable: false, orderable: false, searchable: false},
                {data: 'color_edit', name: 'color_edit', sortable: false, orderable: false, searchable: false},
                {data: 'article_edit', name: 'article_edit', sortable: false, orderable: false, searchable: false},
                {data: 'lot', name: 'lot', sortable: false, orderable: false, searchable: false},
                {data: 'komponen_name', name:'komponen_name', sortable: false, orderable: false, searchable: false},
                {data: 'cut_number', name: 'cut_number'},
                {data: 'sticker_no', name: 'sticker_no', sortable: false, orderable: false, searchable: false},
                {data: 'qty', name: 'qty', sortable: false, orderable: false, searchable: false},
                {data: 'is_printed', name: 'is_printed', sortable: false, orderable: false, searchable: false},
                {data: 'action', name: 'action', sortable: false, orderable: false, searchable: false}
            ]
        });
        //end of datatables

    //});

    //print all
    $(document).on('click', '.printAll', function(event){
        event.preventDefault();
        var ponumber = $('#package_detail_get_data').data('ponumber');
        var style = $('#package_detail_get_data').data('style');
        var size = $('#package_detail_get_data').data('size');
        var color = $('#package_detail_get_data').data('color');
        var article = $('#package_detail_get_data').data('article');
        var product = $('#package_detail_get_data').data('product');
        var orderline = $('#package_detail_get_data').data('orderline');

        var paper = $('input[type=radio][name=radio_status]:checked').val();

        var part = 0;
        if($('#listPart').is(':checked')) {
            part = 1;
        }

        var komponen_break = 0;
        if($('#komponenBreak').is(':checked')) {
            komponen_break = 1;
        }

        var data = [];

        $('.clplanref:checked').each(function() {
            data.push({
                barcode_id: $(this).val()
            });
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#completed_all').attr('href'),
            data: {po_number: ponumber, style: style, size: size, color: color, article: article, m_product_id: product, c_orderline_id: orderline, data: JSON.stringify(data)},
            beforeSend: function() {
                $('.loading-area').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('.loading-area').unblock();
                printPage(null, ponumber, style, size, color, article, product, orderline, paper, part, komponen_break);
            },
            error: function(response) {
                $('.loading-area').unblock();
                myalert('error','NOT GOOD');
            }
        });

    });

    //open window
    $('#table-list').on('click', '.set-completed', function(event) {
        event.preventDefault();
        var this_url = $(this).attr('href');
        var params = getURLParameter(this_url);
        var barcodeid = params['barcodeid'];
        var current = params['current_is_printed'];
        var ponumber = $('#package_detail_get_data').data('ponumber');
        var style = $('#package_detail_get_data').data('style');
        var size = $('#package_detail_get_data').data('size');
        var color = $('#package_detail_get_data').data('color');
        var article = $('#package_detail_get_data').data('article');
        var product = $('#package_detail_get_data').data('product');
        var orderline = $('#package_detail_get_data').data('orderline');

        var paper = $('input[type=radio][name=radio_status]:checked').val();

        var part = 0;
        if($('#listPart').is(':checked')) {
            part = 1;
        }

        var komponen_break = 0;
        if($('#komponenBreak').is(':checked')) {
            komponen_break = 1;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : this_url,
            data: {barcodeid: barcodeid, current_is_printed: current},
            beforeSend: function() {
                $('.loading-area').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                table.draw();
                $('.loading-area').unblock();
                printPage(barcodeid, ponumber, style, size, color, article, product, orderline, paper, part, komponen_break);
            },
            error: function(response) {
                $('.loading-area').unblock();
                myalert('error','NOT GOOD');
            }
        });

    });

    $('input[type=radio][name=radio_status_printed]').change(function() {
        //window.location.href = window.location.href + "&status_print="+$(this).val();
        //table.draw();
        var queries = {};
        $.each(document.location.search.substr(1).split('&'), function(c,q){
            var i = q.split('=');
            queries[i[0].toString()] = unescape(i[1].toString()); // change escaped characters in actual format
        });

        // modify your parameter value and reload page using below two lines
        queries['status_print']=$(this).val();

        document.location.href="?"+$.param(queries); // it reload page
        //history.pushState({}, '', "?"+$.param(queries)); // it change url but not reload page
        //table.draw();
    });

    function toggle(source) {
        checkboxes = document.getElementsByName('selector[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
          checkboxes[i].checked = source.checked;
        }
    }

    function printPage(barcode_id, po_number, style, size, color, article, m_product_id, c_orderline_id, paper, part, komponen_break) {
        var url_print = $('#print_preview').attr('href');
        var url_print_komponen = $('#print_preview_komponen').attr('href');
        var data = [];

        $('.clplanref:checked').each(function() {
            data.push({
                barcode_id: $(this).val()
            });
        });

        if(barcode_id === null) {
            var parameter = '?po_number=' + po_number +
                            '&style=' + style +
                            '&size=' + size +
                            '&color=' + color +
                            '&article=' + article +
                            '&m_product_id=' + m_product_id +
                            '&c_orderline_id=' + c_orderline_id +
                            '&paper=' + paper +
                            '&part=' + part +
                            '&komponen_break=' + komponen_break +
                            '&data=' + JSON.stringify(data);
        }
        else {
            var parameter = '?barcode_id=' + barcode_id + '&po_number=' + po_number +
                            '&style=' + style +
                            '&size=' + size +
                            '&color=' + color +
                            '&article=' + article +
                            '&m_product_id=' + m_product_id +
                            '&c_orderline_id=' + c_orderline_id +
                            '&paper=' + paper +
                            '&part=' + part +
                            '&komponen_break=' + komponen_break +
                            '&data=' + JSON.stringify(data);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : url_print + parameter,
            success: function(response) {
                window.open(url_print + parameter, '_blank');
                window.open(url_print_komponen + parameter, '_blank');
                $('.clplanref input:checkbox').removeAttr('checked');
                $('#listPart').removeAttr('checked');
                $('#komponenBreak').attr('checked');
                table.draw();
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
    }

    // update list komponen
    $('#list_komponen_break').on('blur', function(){
        var list_komponen_break = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updateListKomponenBreak').attr('href'),
            data: {list_komponen_break:list_komponen_break},
            success: function(response){
                $('#list_komponen_break').html(response);

            },
            error: function(response) {
                return false;
            }
        });
    })

    //get parameter from url
    function getURLParameter(url) {
        var params = {};
        url.substring(1).replace(/[?&]+([^=&]+)=([^&]*)/gi,
                function (str, key, value) {
                     params[key] = value;
                });
        return params;
    }
</script>
@endsection
