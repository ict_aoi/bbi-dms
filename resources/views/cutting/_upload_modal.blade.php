<!-- MODAL UPLOAD -->
<div id="modal_upload_" class="modal fade" role="dialog">
    <form class="form-horizontal" action="{{ route('cutting.generateBarcode') }}" id="form-upload" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content modal-lg">
                <div class="modal-body">
                    <div class="panel-body">
                        <fieldset>
                            <legend class="text-semibold">
                                <h4 id="title_upload"><i class="icon-upload4 position-left"></i>INFORMASI #<span id='title_po'></span></h4>

                            </legend>
                            <div class="row">
                                <!-- INFO FIELD -->
                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="text-semibold"><i class="icon-reading position-left"></i> Info</legend>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Delivery Date:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="datepromised" name="datepromised" placeholder="" readonly required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Size:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control text-uppercase" id="size_edit" name="size_edit" placeholder="" required autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Style:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control text-uppercase" id="style_edit" name="style_edit" placeholder="" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">PO:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control text-uppercase" id="po_number_edit" name="po_number_edit" placeholder="" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Color:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control text-uppercase" id="color_edit" name="color_edit" placeholder="" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Article:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="article_edit" name="article_edit" placeholder="" autocomplete="off">
                                            </div>
                                        </div>

                                        <legend class="text-semibold"><i class="icon-truck position-left"></i> Cutting Data</legend>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="col-lg-3 control-label">Cut No:</label>
                                                    <div class="col-lg-9">
                                                        <input type="text" class="form-control text-uppercase text-right" id="cut_number" placeholder="cut number" onkeypress="return isNumberKey(event)" required autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="col-lg-3 control-label">Cut info:</label>
                                                    <div class="col-lg-9">
                                                        <input type="text" class="form-control text-uppercase" id="cut_info" placeholder="cut info" autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if (!\Auth::user()->is_nagai)
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Lot:</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control text-uppercase" id="lot" placeholder="Lot" autocomplete="off">
                                                </div>
                                            </div>
                                        @endif
                                        <div class="form-group hidden">
                                            <label class="col-lg-3 control-label">Part:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control text-uppercase" id="part" placeholder="Part" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group hidden">
                                            <label class="col-lg-3 control-label">Komponen:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control text-uppercase" id="komponen" placeholder="Komponen" autocomplete="off">
                                            </div>
                                        </div>

                                    </fieldset>
                                </div>
                                <!-- END OF INFO FIELD -->

                                <!-- CUTTING FIELD -->
                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="text-semibold"><i class="icon-train position-left"></i> Proses Cutting</legend>

                                        <div class="form-group hidden">
                                            <label class="col-lg-3 control-label">Proses:</label>
                                            <div class="col-lg-9">
                                                <select multiple data-placeholder="Select a State..." class="form-control select-search" name="process_id[]" id="process_id">
                                                    @foreach($process as $proc)
                                                        <option value="{{ $proc->id }}">{{ $proc->process_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Total Qty:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control text-right" id="total_qty" placeholder="Total Qty" onkeypress="return isNumberKey(event)" required autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="col-lg-3 control-label">Qty Per bundle:</label>
                                                    <div class="col-lg-9">
                                                        <input type="text" class="form-control text-right" id="qty" placeholder="Qty" onkeypress="return isNumberKey(event)" required autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="col-lg-3 control-label">Bundle Start:</label>
                                                    <div class="col-lg-9">
                                                        <input type="text" class="form-control text-right" id="from" placeholder="bundle Start" onkeypress="return isNumberKey(event)" required autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- sementara diganti -->
                                        <div class="form-group hidden">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="col-lg-3 control-label">Stiker From:</label>
                                                    <div class="col-lg-9">
                                                        <input type="text" class="form-control text-right" id="sticker_from" placeholder="Stiker From" onkeypress="return isNumberKey(event)">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="col-lg-3 control-label">Stiker To:</label>
                                                    <div class="col-lg-9">
                                                        <input type="text" class="form-control text-right" id="sticker_to" placeholder="Stiker To" onkeypress="return isNumberKey(event)" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group hidden">
                                            <label class="col-lg-3 control-label">No Stiker:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="sticker_no" placeholder="No Stiker" readonly>
                                            </div>
                                        </div>
                                        <!-- end sementara diganti -->
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Tanggal:</label>
                                            <div class="col-lg-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                                                    <input type="text" class="form-control pickadate" name="cutting_date" id="cutting_date" value="{{ \Carbon\Carbon::now()->format('j F, Y') }}" required>
                                                </div>
                                            </div>
                                        </div>
                                        {{--  @if (\Auth::user()->is_nagai)  --}}
                                            <div class="form-group {{\Auth::user()->is_nagai ? ' ' : ' hidden'}}">
                                                <label class="col-lg-3 control-label">No. Bundle:</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control text-uppercase" id="no_bundle" placeholder="No Bundle" autocomplete="off">
                                                </div>
                                            </div>
                                        {{--  @endif  --}}
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Bundle:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control text-uppercase" id="bundle" placeholder="Bundle" required autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">QC:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control text-uppercase" id="qc" placeholder="QC" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Cutter:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control text-uppercase" id="cutter" placeholder="Cutter" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group hidden">
                                            <label class="col-lg-3">Inhouse ? :</label>
                                            <div class="col-lg-3">
                                                <input type="checkbox" name="is_inhouse">
                                            </div>                                        
                                        </div>
                                        <div class="form-group hidden">
                                            <label class="col-lg-3 control-label">Type:</label>
                                            <div class="col-lg-9">
                                                <select class="form-control" name="type_id" id="type_id">
                                                    @foreach($types as $type)
                                                        <option value="{{ $type->id }}">{{ $type->type_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group type-desc hidden">
                                            <label class="col-lg-3 control-label">Type Desc:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control text-uppercase" id="type_description" placeholder="Description">
                                            </div>
                                        </div>
                                        <div class="form-group hidden">
                                            <div class="text-right">
                                                <button type="button" id="btnAddKomponen" class="btn btn-primary">Add <i class="icon-plus3 position-right"></i></button>
                                            </div>
                                        </div>

                                    </fieldset>
                                </div>
                                <!-- END OF CUTTING FIELD -->
                            </div>

                            <input type="hidden" name="style" id="style">
                            <input type="hidden" name="po_number" id="po_number">
                            <input type="hidden" name="size" id="size">
                            <input type="hidden" name="pcd" id="pcd">
                            <input type="hidden" name="podd" id="podd">
                            <input type="hidden" name="mo" id="mo">
                            <input type="hidden" name="city_name" id="city_name">
                            <input type="hidden" name="kst_joborder" id="kst_joborder">
                            <input type="hidden" name="qtyordered" id="qtyordered">
                            <input type="hidden" name="category" id="category">
                            <input type="hidden" name="m_product_id" id="m_product_id">
                            <input type="hidden" name="kode_product" id="kode_product">
                            <input type="hidden" name="nama_product" id="nama_product">
                            <input type="hidden" name="dateordered" id="dateordered">
                            {{--  <input type="hidden" name="datepromised" id="datepromised">  --}}
                            <input type="hidden" name="color" id="color">
                            <input type="hidden" name="article" id="article">
                            <input type="hidden" name="buyer" id="buyer">
                            <input type="hidden" name="c_order_id" id="c_order_id">
                            <input type="hidden" name="c_orderline_id" id="c_orderline_id">
                            <input type="hidden" name="is_recycle" id="is_recycle">
                        </fieldset>
                    </div>
                </div>
                <div class="responsive hidden" style="height: 20em; overflow: auto;">
                    <table class="table datatable-save-state" id="table-list-komponen">
                            <thead>
                                <tr>
                                    <td>Cut</td>
                                    <td>No Stiker</td>
                                    <td>Part</td>
                                    <td>Komponen</td>
                                    <td>Bundle</td>
                                    <td>Act</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /MODAL UPLOAD-->

<!-- MODAL DETAIL STYLE -->
<div id="modal_detail_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title" id="title-line-number"><i class="position-left"></i>Detail Komponen</h5>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <fieldset>
                        <div id="list" class="col-lg-12">
                            <!-- template pilihan -->
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /MODAL DETAIL STYLE -->
