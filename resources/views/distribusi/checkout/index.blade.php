@extends('layouts.app', ['active' => 'distribusiout'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">DISTRIBUSI CHECK OUT</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('distribusi.checkout') }}"><i class="icon-home4 position-left"></i> Distribusi</a></li>
            <li class="active">Check Out</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <form action="{{ route('distribusi.postCheckout') }}" id="form-check">
            @csrf
            <div class="row">
                <div class="col-md-10">
                    <input type="text" class="form-control input-new" id="distribusi-checkout" name="distribusiCheckout" placeholder="#Scan ID" autofocus></input>
                    <input type="hidden" id="_barcodeid"></input>
                    <input type="hidden" id="_status"></input>
                </div>
                <div class="col-md-2">
                    <input type="text" id="count_scan" name="count_scan" value="0" style="text-align: center; font-size: 28px;"
                           class="form-control" readonly="readonly"
                           placeholder="">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="completed">COMPLETED</label>
                        <label class="radio-inline"><input type="radio" name="radio_status" value="cancel">CANCEL</label>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="show-result">
                <thead>
                    <tr>
                        <th>Barcode Id</th>
                        <th>PO Number</th>
                        <th>Style</th>
                        <th>Size</th>
                        <th>Qty</th>
                        <th>Color</th>
                        <th>Article</th>
                        <th>Komponen</th>
                        <th>Cutt</th>
                        <th>No Sticker</th>
                        <th>Proses</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script type="text/javascript">
$(document).ready(function() {
    $('#distribusi-checkout').focus();

    //scan package
    $('.input-new').keypress(function(event) {
        if(event.which == 13) {
            event.preventDefault();
            var barcodeid = $('#distribusi-checkout').val();
            var status = $('input[name=radio_status]:checked').val();

            $('#_barcodeid').val(barcodeid);
            $('#_status').val(status);
            if($('#distribusi-checkout').val() == '') {
                $('#distribusi-checkout').val('');
                $('#distribusi-checkout').focus();
                $('#_barcodeid').val('');
                $('#_status').val('');
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: $('#form-check').attr('action'),
                data: {barcodeid: barcodeid, status: status},
                beforeSend: function() {
                    $('#distribusi-checkout').val('');
                    $('#distribusi-checkout').focus();
                    $('#_barcodeid').val('');
                    $('#_status').val('');
                },
                success: function(response){
                    $('#show-result > tbody').prepend(response);
                    var count_s = +$('#count_scan').val()+1;

                    $('#count_scan').val(count_s);
                },
                error: function(response){
                    if(response['status'] == 422) {
                        myalert('error', response['responseJSON']);
                    }
                    $('#distribusi-checkout').val('');
                }
            });
        }
    });
});
</script>
@endsection
