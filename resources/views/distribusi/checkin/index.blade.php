@extends('layouts.app', ['active' => 'distribusiin'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">DISTRIBUSI CHECK IN</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('distribusi.checkin') }}"><i class="icon-home4 position-left"></i> Distribusi</a></li>
            <li class="active">Check In</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="row">
            <div class="form-group">
                <label class="col-lg-2 control-label text-semibold">No.SJ :: No.BC:</label>
                <div class="col-lg-10">
                    <select name="delivery_in_id" id="delivery_in_id" data-placeholder="Select a State..." class="form-control select-search3">
                        <option value=""></option>
                        @foreach ($delivery_in as $key => $val)
                            <option value="{{ $val->id }}" data-qty="{{$val->qty}}">{{ $val->no_suratjalan }} :: {{ $val->bc_no }} ( total qty : {{$val->qty}} )</option>
                        @endforeach   
                    </select>
                </div>
            </div>
        </div>
        <hr>
        <form action="{{ route('distribusi.postCheckin') }}" id="form-check">
            @csrf
            <div class="row">
                <div class="col-md-10">
                    <input type="text" class="form-control input-new" id="distribusi-checkin" name="distribusiCheckin" placeholder="#Scan ID" autofocus></input>
                    <input type="hidden" id="_barcodeid"></input>
                    <input type="hidden" id="_status"></input>
                </div>
                <div class="col-md-2">
                    <input type="hidden" id="count_scan" name="count_scan" value="0" style="text-align: center; font-size: 28px;"
                           class="form-control" readonly="readonly"
                           placeholder="">
                    <input type="text" id="style" name="style"
                           class="form-control" readonly="readonly"
                           placeholder="#STYLE GLOBAL">
                </div>
            </div>
            <br>
            <div class="row hidden">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="onprogress">ON PROGRESS</label>
                        <label class="radio-inline"><input type="radio" name="radio_status" value="cancel">CANCEL</label>
                    </div>
                </div>
            </div>
            <input type="hidden" id="delivery_reject_id" name="delivery_reject_id"></input>
        </form>
    </div>
    <div class="panel-body">
        <button type="button" id="btnMove" class="btn btn-warning btn-raised legitRipple" title="move to another SJ"><i class="icon-move-horizontal position-left"></i> Move</button>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table_list">
                <thead>
                    <tr>
                        <th><input type="checkbox" onclick="toggle(this)"></th>
                        <th>Barcode Id</th>
                        <th>Style</th>
                        <th>Cut</th>
                        <th>komponen</th>
                        <th>No.Sticker</th>
                        <th>Size</th>
                        <th>Qty</th>
                        <th>Date</th>
                        {{--  <th>Action</th>  --}}
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('distribusi.ajaxGetDeliveryReject') }}" id="get-delivery-reject"></a>
<a href="{{ route('distribusi.checkinDeliveryReject') }}" id="delivery-reject-checkin"></a>
<a href="{{ route('suratjalanin.ajaxGetDataDeliveryIn') }}" id="get-delivery-in-detail"></a>
<a href="{{ route('suratjalanin.updateQty') }}" id="updateQty"></a>
<a href="{{ route('distribusi.ajaxGetDeliveryInMove') }}" id="get-delivery-in-move"></a>
<a href="{{ route('distribusi.checkinDeliveryMove') }}" id="delivery-move-checkin"></a>
<a href="{{ route('suratjalanin.exportDataDeliveryIn') }}" id="export_data"></a>
@endsection

@section('page-modal')
    @include('distribusi.checkin._index')
@endsection

@section('page-js')
<script src="{{ url('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.select-search3').select2({
        placeholder: "Select a State",
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Enter Minimum 2 Character";
        },
    });

    $('#distribusi-checkin').focus();
    var _token = $("input[name='_token']").val();
    //datatables
   /* $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });*/

    var url = $('#get-delivery-in-detail').attr('href');
    var table = $('#table_list').DataTable({
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#export_data').attr('href')
                                            + '?delivery_in_id=' + $('#delivery_in_id').val()
                                            + '&orderby=' + column_name
                                            + '&direction=' + direction
                                            + '&filterby=' + filter;
                }
            }
       ],
        ajax: {
            url: url,
            type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "delivery_in_id": $('#delivery_in_id').val(),
                    "filterby": $('.dataTables_filter input').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
          //  $('td', row).eq(0).html(value);
        },
        columns: [
           // {data: null, sortable: false, orderable: false, searchable: false  },
            {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
            {data: 'barcode_id', name: 'barcode_id'},
            {data: 'style_edit', name: 'style_edit'},
            {data: 'cut_number', name: 'cut_number'},
            {data: 'komponen_name', name: 'komponen_name'},
            {data: 'sticker_no', name: 'sticker_no'},
            {data: 'size_edit', name: 'size_edit'},
            {data: 'qty', name: 'qty', orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'}
           // {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
    //end of datatables

    // update qty
    $('#table_list').on('blur', '.qty_filled', function() {
        var id = $(this).data('id');
        var qty = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#updateQty').attr('href'),
            data: {id:id, qty:qty},
            success: function(response){
                $('#qty_' + id).html(response);

            },
            error: function(response) {
                myalert('error', response['responseJSON'])
                table.draw();
                return false;
            }
        });
    });
    // end update qty

    //delete
    $("#table_list").on("click", ".delete", function() {
        event.preventDefault();
        var id = $(this).data('id');
        if(id == 'kosong') {
            return false;
        }
        var token = $(this).data("token");
        bootbox.confirm("Are you sure delete this row ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "delivery-in-detail/delete/"+id,
                    type: "GET",
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": token,
                    },
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        myalert('success','Data has been deleted');
                        table.ajax.reload();
                    }
                });
            }
        });
    });
    //end of delete

    $('#delivery_in_id').on('change', function(){
        table.draw();
    });

    //scan package
    $('.input-new').keypress(function(event) {
        if(event.which == 13) {
            event.preventDefault();
            var barcodeid = $('#distribusi-checkin').val();
            var status = $('input[name=radio_status]:checked').val();

            $('#_barcodeid').val(barcodeid);
            $('#_status').val(status);
            if($('#distribusi-checkin').val() == '') {
                $('#distribusi-checkin').val('');
                $('#distribusi-checkin').focus();
                $('#_barcodeid').val('');
                $('#_status').val('');
                return false;
            }

            //check no sj
            if($('#delivery_in_id').val() == '') {
                $('#distribusi-checkin').val('');
                $('#distribusi-checkin').focus();
                $('#_barcodeid').val('');
                $('#_status').val('');
                myalert('error', 'Please Choose "No SJ/No BC" First!');
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: $('#form-check').attr('action'),
                data: {barcodeid: barcodeid, status: status, delivery_in_id: $('#delivery_in_id').val()},
                beforeSend: function() {
                    $('#distribusi-checkin').val('');
                    $('#distribusi-checkin').focus();
                    $('#_barcodeid').val('');
                    $('#_status').val('');
                },
                success: function(response){
                    $('#show-result > tbody').prepend(response);
                    var count_s = +$('#count_scan').val()+1;

                    $('#count_scan').val(count_s);

                    table.draw();
                },
                error: function(response){
                    if(response['status'] == 422) {
                        myalert('error', response['responseJSON']);
                    }
                    $('#distribusi-checkin').val('');
                }
            });
        }
    });

    //choose style global
    $('#style').on('click',function(){
        var delivery_in_id = $('#delivery_in_id').val();

        if(delivery_in_id == ''){
            myalert('error', 'please choose no BC first..!');
            return false;
        }

        $('#modal_detail_').modal('show');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url: $('#get-delivery-reject').attr('href'),
            beforeSend: function() {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function(response){
                $.unblockUI();
                $('#lists').html(response);
                
            },
            error: function(response){
                $.unblockUI();
                console.log(response);
                myalert('error', 'Not Good');
            }
        });
    });

    //
    $('#lists').on('click','#choose-style', function(){
        var id = $(this).data('id');
        var style = $(this).data('style');
        var qty = $(this).data('qty');
        var delivery_in_id = $('#delivery_in_id').val();

        var tr = $(this).parent().parent();
        var qty_filled = $(tr).find('.qty_filled').val();

        if(delivery_in_id == ''){
            myalert('error', 'please choose no BC first..!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#delivery-reject-checkin').attr('href'),
            data: {delivery_reject_id:id, delivery_in_id:delivery_in_id, qty:qty_filled},
            beforeSend: function() {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function(response){
                $('#modal_detail_').modal('hide');
                $('.input-new').focus();
                table.draw();
            },
            error: function(response){
                $.unblockUI();
                myalert('error', response['responseJSON']);
            }
        });

       // $('#delivery_reject_id').val(id);
       // $('#style').val(style);
        
    })

    //
    $('#list_sj').on('click','#choose-delivery_in', function(){
        var delivery_in_id_new = $(this).data('id');

        var delivery_in_id_old = $('#delivery_in_id').val();

        var data = [];
        $('.clplanref:checked').each(function(){
            data.push({
                id: $(this).val()
            });
        });

        if(data.length <= 0){
            myalert('error', 'Please select barcode/style first..!');
            return false;
        }


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#delivery-move-checkin').attr('href'),
            data: {delivery_in_id_old:delivery_in_id_old, delivery_in_id_new:delivery_in_id_new, data:JSON.stringify(data)},
            beforeSend: function() {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function(response){
                $('#modal_detail_sj').modal('hide');
                $('.input-new').focus();
                table.draw();
            },
            error: function(response){
                $.unblockUI();
                myalert('error', response['responseJSON']);
            }
        });
        
    });

    $('#btnMove').on('click', function(){
        var data = [];
        $('.clplanref:checked').each(function(){
            data.push({
                id: $(this).val()
            });
        });

        if(data.length <= 0){
            myalert('error', 'Please select barcode/style first..!');
            return false;
        }

        $('#modal_detail_sj').modal('show');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url: $('#get-delivery-in-move').attr('href'),
            data: {delivery_in_id: $('#delivery_in_id').val() },
            beforeSend: function() {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function(response){
                $.unblockUI();
                $('#list_sj').html(response);
                
            },
            error: function(response){
                $.unblockUI();
                console.log(response);
                myalert('error', 'Not Good');
            }
        });

    });
});

function toggle(source) {
    checkboxes = document.getElementsByName('selector[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = source.checked;
    }
}
</script>
@endsection
