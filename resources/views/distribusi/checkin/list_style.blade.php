<div class="table-responsive">
<table class="table" id="table-lines">
    <thead>
        <tr>
            <th>Date</th>
            <th>No SJ</th>
            <th>Style</th>
            <th>Qty</th>
            <th>Subcont</th>
            <th width="10%">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($datax as $key => $val)
            <tr>
                <td>{{ $val->created_at }}</td>
                <td>{{ $val->no_suratjalan }}</td>
                <td>{{ $val->style }}</td>
                {{--  <td>{{ $val->qty }}</td>  --}}
                <td><input type="text"
                    class="form-control qty_filled"
                    value="{{$val->qty}}" onkeypress="return isNumberKey(event)"></td>
                <td>{{ $val->subcont_name }}</td>
                <td>
                    <button type="button" id="choose-style"
                            data-id="{{ $val->id }}"
                            data-style="{{ $val->style }}"
                            data-qty="{{ $val->qty }}"
                            class="btn btn-default">
                            SELECT
                    </button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

</div>
<script type="text/javascript">
    var tablex = $('#table-lines').DataTable({
        "lengthChange": false,
        "pageLength": 5
        /*"columnDefs": [{
             "targets" : [0],
             "visible" : false,
             "searchable" : false
        }]*/
    });
</script>
