<div class="table-responsive">
<table class="table" id="table-lines">
    <thead>
        <tr>
            <th>No SJ</th>
            <th>No. BC</th>
            <th>Total Qty</th>
            <th width="10%">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($dataxx as $key => $val)
            <tr>
                <td>{{ $val->no_suratjalan }}</td>
                <td>{{ $val->bc_no }}</td>
                <td>{{ $val->qty }}</td>
                <td>
                    <button type="button" id="choose-delivery_in"
                            data-id="{{ $val->id }}"
                            class="btn btn-default">
                            SELECT
                    </button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

</div>
<script type="text/javascript">
    var tablex = $('#table-lines').DataTable({
        "lengthChange": false,
        "pageLength": 5
        /*"columnDefs": [{
             "targets" : [0],
             "visible" : false,
             "searchable" : false
        }]*/
    });
</script>
