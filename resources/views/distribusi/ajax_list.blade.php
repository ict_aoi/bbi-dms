<tr>
    <td>{{ $data['barcode_id'] }}</td>
    <td>{{ $po_number_edit }}</td>
    <td>{{ $style_edit }}</td>
    <td>{{ $size_edit }}</td>
    <td>{{ $qty }}</td>
    <td>{{ $color_edit }}</td>
    <td>{{ $article_edit }}</td>
    <td>{{ $komponen_name }}</td>
    <td>{{ $cut_number }}</td>
    <td>{{ $sticker_no }}</td>
    <td><span class="label label-flat border-grey text-grey-600">{{ $data['process_to'] }}</span></td>
    <td>
        @if($data['status_to'] == 'completed')
        <span class="label label-success">COMPLETED</span>
        @elseif($data['status_to'] == 'onprogress')
        <span class="label label-primary">ON PROGRESS</span>
        @elseif($data['status_to'] == 'cancel')
        <span class="label label-default">CANCEL</span>
        @endif
    </td>
</tr>