<div class="sidebar sidebar-main sidebar-default">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="category-content">
                <div class="sidebar-user-material-content">
                    @if(auth::user()->photo)
                        <img src="{{ route('accountSetting.showAvatar', auth::user()->photo) }}" class="img-circle" alt="profile_photo" id="avatar_image">
                    @else 
                        @if(strtoupper(auth::user()->sex) == 'LAKI')
                            <a href="#"><img src="{{ asset('images/male_avatar.png') }}" class="img-circle" alt="avatar_male"></a>
                        @else
                            <a href="#"><img src="{{ asset('images/female_avatar.png') }}" class="img-circle" alt="avatar_female"></a>
                        @endif
                    @endif
                    <h6>{{ Auth::user()->name }}</h6>
                    <span class="text-size-small">{{ Auth::user()->nik }}</span>
                </div>
                                            
                <div class="sidebar-user-material-menu">
                    <a href="#user-nav" data-toggle="collapse"><span>My account</span> <i class="caret"></i></a>
                </div>
            </div>
            
            <div class="navigation-wrapper collapse" id="user-nav">
                <ul class="navigation">
                    <li class="divider"></li>
                    @permission(['menu-factory'])
                    <li class="{{ $active == 'factory_setting' ? 'active' : '' }}"><a href="{{ route('factorySetting') }}"><i class="icon-cog5"></i> <span>Factory settings</span></a></li>
                    @endpermission
                    <li class="{{ $active == 'account_setting' ? 'active' : '' }}"><a href="{{ route('accountSetting') }}"><i class="icon-cog5"></i> <span>Account settings</span></a></li>
                    <li><a href="{{ route('logout') }}"
							onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
							<i class="icon-switch2"></i> Logout</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
                            </form>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    @permission(['menu-dashboard'])
                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li class="{{ $active == 'dashboard' ? 'active' : '' }}">
                        <a href="{{ route('dashboard') }}"><i class="icon-home4"></i> <span>Dashboard</span></a>
                    </li>
                    @endpermission
                    <li class="navigation-header"><span>Transactions</span> <i class="icon-archive" title="Transaction"></i></li>
                    <!-- MENU ADMIN -->
                    @permission(['menu-user','menu-role','menu-permission'])
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-users"></i> <span>User Management</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            @permission('menu-permission')
                                <li class="{{ $active == 'permission' ? 'active' : '' }}"><a href="{{ route('permission.index') }}" class="legitRipple">Permission</a></li>
                            @endpermission
                            @permission('menu-role')
                                <li class="{{ $active == 'role' ? 'active' : '' }}"><a href="{{ route('role.index') }}" class="legitRipple">Role</a></li>
                            @endpermission
                            @permission('menu-user')
                                <li class="{{ $active == 'user' ? 'active' : '' }}"><a href="{{ route('user.index') }}" class="legitRipple">User</a></li>
                            @endpermission
                        </ul>
                    </li>
                    @endpermission
                    @permission(['menu-master', 'menu-subcont'])
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-list"></i> <span>Master</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            @permission('menu-subcont')
                            <li class="{{ isset($active) && $active == 'mastersubcont' ? 'active' : '' }}">
                                    <a href="{{ route('cutting.masterSubcont') }}" class="legitRipple">Subcont</a>
                            </li>
                            @endpermission
                            @permission('menu-master')
                            <li class="{{ isset($active) && $active == 'masterprocess' ? 'active' : '' }}">
                                    <a href="{{ route('cutting.masterProcess') }}" class="legitRipple">Process</a>
                            </li>
                            <li class="{{ isset($active) && $active == 'masterkomponen' ? 'active' : '' }}">
                                    <a href="{{ route('cutting.masterKomponen') }}" class="legitRipple">Komponen</a>
                            </li>
                            <li class="{{ isset($active) && $active == 'masterstyle' ? 'active' : '' }}">
                                    <a href="{{ route('cutting.masterStyle') }}" class="legitRipple">Style</a>
                            </li>
                            @endpermission
                        </ul>
                    </li>
                    @endpermission
                    <!-- END OF MENU ADMIN -->
                    <!-- MENU BARCODE -->
                    @permission(['menu-barcoding'])
                    <li class="active">
                        <a href="#" class="has-ul legitRipple"><i class="icon-station"></i> <span>Cutting</span></a>
                        <ul class="hidden-ul" style="display: block;">
                            <li class="{{ isset($active) && $active == 'barcoding' ? 'active' : '' }}">
                                    <a href="{{ route('cutting.barcode') }}" class="legitRipple">Generate Barcode</a>
                            </li>
                            <li class="{{ isset($active) && $active == 'checkkomponen' ? 'active' : '' }}">
                                    <a href="{{ route('cutting.checkKomponen') }}" class="legitRipple">List Check Komponen</a>
                            </li>
                        </ul>
                    </li>
                    @endpermission
                    <!-- END MENU BARCODE -->
                    <!-- MENU DISTRIBUSI -->
                    @permission(['menu-distribusi-out', 'menu-distribusi-in'])
                    <li class="active">
                        <a href="#" class="has-ul legitRipple"><i class="icon-link2"></i> <span>Distribusi</span></a>
                        <ul class="hidden-ul" style="display: block;">
                            @permission(['menu-distribusi-out'])
                            <li class="{{ isset($active) && $active == 'distribusiout' ? 'active' : '' }}">
                                <a href="{{ route('distribusi.checkout') }}" class="legitRipple">Check Out</a>
                            </li>
                            @endpermission
                            @permission(['menu-distribusi-in'])
                            <li class="{{ isset($active) && $active == 'distribusiin' ? 'active' : '' }}">
                                <a href="{{ route('distribusi.checkin') }}" class="legitRipple">Check In</a>
                            </li>
                            @endpermission
                        </ul>
                    </li>
                    @endpermission
                    <!-- END  -->
                    <!-- MENU PACKING LIST -->
                    @permission(['menu-packinglist'])
                    <li class="active">
                        <a href="#" class="has-ul legitRipple"><i class="icon-library2"></i> <span>Packing List</span></a>
                        <ul class="hidden-ul" style="display: block;">
                            <li class="{{ isset($active) && $active == 'formulir' ? 'active' : '' }}">
                                <a href="{{ route('packing.formulir') }}" class="legitRipple">Formulir</a>
                            </li>
                            <li class="{{ isset($active) && $active == 'formulirReject' ? 'active' : '' }} hidden">
                                <a href="{{ route('packing.formulirReject') }}" class="legitRipple">Formulir Reject</a>
                            </li>
                            <li class="{{ isset($active) && $active == 'packinglist' ? 'active' : '' }}">
                                <a href="{{ route('packing.index') }}" class="legitRipple">Surat Jalan</a>
                            </li>
                            <li class="{{ isset($active) && $active == 'packinglistglobal' ? 'active' : '' }}">
                                <a href="{{ route('packing.suratJalanGlobal') }}" class="legitRipple">Surat Jalan Global</a>
                            </li>
                        </ul>
                    </li>
                    @endpermission
                    @permission(['menu-update-surat-jalan'])
                    <li class="{{ $active == 'updateSuratJalan' ? 'active' : '' }}">
                        <a href="{{ route('suratjalan.list') }}"><i class="icon-grid4"></i> <span>Update Surat Jalan Out</span></a>
                    </li>
                    <li class="{{ $active == 'suratjalanin' ? 'active' : '' }}">
                        <a href="{{ route('suratjalanin.suratJalanIn') }}"><i class="icon-grid4"></i> <span>Insert Surat Jalan IN</span></a>
                    </li>
                    @endpermission
                    <!-- END  -->
                    <li class="navigation-header"><span>Report</span> <i class="icon-archive" title="Report"></i></li>
                    <!-- MENU ADMIN -->
                    @permission(['menu-report'])
                    <li class="{{ $active == 'reportpreparation' ? 'active' : '' }}">
                        <a href="{{ route('report.preparation') }}"><i class="icon-ticket"></i> <span>Preparation</span></a>
                    </li>
                    <li class="{{ $active == 'reportdistribusi' ? 'active' : '' }}">
                        <a href="{{ route('report.distribusi') }}"><i class="icon-circle-up2"></i> <span>Distribusi</span></a>
                    </li>
                    <li class="{{ $active == 'reportdistribusimonitoring' ? 'active' : '' }}">
                        <a href="{{ route('report.distribusiMonitoring') }}"><i class="icon-list"></i> <span>Distribusi Monitoring Out</span></a>
                    </li>
                    <li class="{{ $active == 'reportdistribusimonitoringin' ? 'active' : '' }}">
                        <a href="{{ route('report.distribusiMonitoringIn') }}"><i class="icon-list"></i> <span>Distribusi Monitoring In</span></a>
                    </li>
                    <li class="{{ $active == 'reportformulir' ? 'active' : '' }}">
                        <a href="{{ route('report.formulir') }}"><i class="icon-file-check2"></i> <span>Formulir</span></a>
                    </li>
                    <li class="{{ $active == 'reportsuratjalan' ? 'active' : '' }}">
                        <a href="{{ route('report.suratjalan') }}"><i class="icon-truck"></i> <span>Monitoring Surat Jalan Out</span></a>
                    </li>
                    <li class="{{ $active == 'reportsuratjalanin' ? 'active' : '' }}">
                        <a href="{{ route('report.suratjalanin') }}"><i class="icon-upload7"></i> <span>Monitoring Surat Jalan In</span></a>
                    </li>
                    <li class="{{ $active == 'reportsuratjalaninexim' ? 'active' : '' }}">
                        <a href="{{ route('report.suratjalaninexim') }}"><i class="icon-link2"></i> <span>Monitoring Surat Jalan In EXIM</span></a>
                    </li>
                    @endpermission
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>