@extends('layouts.app',['active' => 'factory_setting'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Factory Setting</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Factory Setting</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
         {!!
            Form::open([
                'role' => 'form',
                'url' => route('factorySetting.updateFactory'),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            ])
        !!}
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Last Delivery Number</label>
                        <input type="text" name="no_suratjalan" value="{{ $factories->no_suratjalan }}" class="form-control" onkeypress="return isNumberKey(event)" autocomplete="off" required>
                    </div>

                    <div class="col-md-6">
                        <label>Last Polybag Number</label>
                        <input type="text" name="no_polybag" value="{{ $factories->no_polybag }}" class="form-control" onkeypress="return isNumberKey(event)" autocomplete="off" required>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>List Komponen /page (A3)</label>
                        <input type="text" name="list_komponen_break" value="{{ $factories->list_komponen_break }}" class="form-control" onkeypress="return isNumberKey(event)" autocomplete="off" required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label class="display-block">Logo Factory</label>
                        <input type="file" class="file-styled" name="logo_factory" accept="image/*">
                        <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
                    </div>
                    <div class="col-md-6">
                        <img id="logo_preview" src="{{ url('images') }}/{{ $factories->logo_factory }}" alt="" height="80" width="200">
                    </div>
                </div>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('page-js')
<script>
    $(function() {
        $(".file-styled").uniform({
            fileButtonClass: 'action btn bg-warning'
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('#logo_preview').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
        
    $("input[name=logo_factory]").change(function() {
        readURL(this);
    });

</script>
@endsection
