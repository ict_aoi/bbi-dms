<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>DMS | Distribusi Management System</title>

        <!-- Styles -->
        <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">
        @yield('page-css')
    </head>
    <body>
        @include('includes.main_navbar')
        @include('includes.top_notif')
        <div class="page-container">
            <div class="page-content">
                @include('includes.side_navbar')
                <div class="content-wrapper">
                    @yield('page-header')
                    <div class="content">
                            @yield('page-content')
                            @yield('page-modal')
                            <script src="{{ mix('js/backend.js') }}"></script>
                            <script src="{{ mix('js/notification.js') }}"></script>
                            <script src="{{ mix('js/swith.js') }}"></script>
                            <script src="{{ mix('js/datepicker.js') }}"></script>
                            <!-- Scripts -->
                            <script type="text/javascript">
                                $('.select-search2').select2({
                                    placeholder: "Select a State",
                                    minimumInputLength: 3,
                                    formatInputTooShort: function () {
                                        return "Enter Minimum 3 Character";
                                    },
                                });
                                
                                function myalert(type, text) {
                                    var color = '#66BB6A';
                                    var title = 'Good Job!';
                                    if(type == 'error') {
                                        color = '#EF5350';
                                        title = 'Oops...';
                                    }
                                    swal({
                                        title: title,
                                        text: text,
                                        confirmButtonColor: color,
                                        type: type
                                    });
                                }
                        
                                function isNumberDot(evt) {
                                    var theEvent = evt || window.event;
                                    var key = theEvent.keyCode || theEvent.which;
                                    key = String.fromCharCode(key);
                                    if (key.length == 0) return;
                                    var regex = /^[0-9.\b]+$/;
                                    if (!regex.test(key)) {
                                        theEvent.returnValue = false;
                                        if (theEvent.preventDefault) theEvent.preventDefault();
                                    }
                                }

                                function isNumberKey(evt)
                                {
                                    var charCode = (evt.which) ? evt.which : evt.keyCode
                                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                                    return false;
                                    return true;
                                }

                                // loading
                                function loading_process() {
                                    $('#table-list').block({
                                        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                                        overlayCSS: {
                                            backgroundColor: '#fff',
                                            opacity: 0.8,
                                            cursor: 'wait'
                                        },
                                        css: {
                                            border: 0,
                                            padding: '10px 15px',
                                            color: '#fff',
                                            width: 'auto',
                                            '-webkit-border-radius': 2,
                                            '-moz-border-radius': 2,
                                            backgroundColor: '#333'
                                        }
                                    });
                                }

                                function formatNumber(num) {
                                    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
                                }
                            </script>
                            @yield('page-js')
                        <div class="footer text-muted">
                            &copy; 2019. <a href="#">Distribusi Management System</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
