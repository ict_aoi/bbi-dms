@extends('layouts.app', ['active' => 'reportdistribusimonitoring'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Distribusi Monitoring Out</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home4 position-left"></i> Report</a></li>
            <li class="active">Distribusi Monitoring Out</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('report.ajaxGetDataDistribusiMonitoring') }}" id="form_filter">
            @csrf
            <div class="form-group hidden" id="filter_by_date">
				<label><b>Select Date</b></label>
				<div class="input-group">
					<span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range" required>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
            </div>
            <div class="form-group" id="filter_by_date_out">
				<label><b>Select Date</b></label>
				<div class="input-group">
					<span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range_out" id="date_range_out" required>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
            </div>
            <div class="form-group hidden" id="filter_by_date_in">
				<label><b>Select Date</b></label>
				<div class="input-group">
					<span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range_in" id="date_range_in" required>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
            </div>
            <div class="form-group hidden" id="filter_by_po">
				<label><b>Enter PO Number</b></label>
				<div class="input-group">
                    <select data-placeholder="Select a State..." name="po_number" id="po_number" class="form-control select-search2">
                        <option value=""></option>
                        @foreach($list_po as $list)
                            <option value="{{ $list->po_number_edit }}">{{ $list->po_number_edit }}</option>
                        @endforeach
                    </select>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
            </div>
            <div class="form-group">
                <label class="radio-inline hidden"><input type="radio" name="radio_status" value="date">All</label>
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="out">Filter by Date</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="po">Filter by PO Number</label>
                <label class="radio-inline hidden"><input type="radio" name="radio_status" value="in">Distribusi In</label>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-2"> Factory :
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->id }}" {{ $val->id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table datatable-button-html5-basic" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>TANGGAL</th>
                        <th>STYLE</th>
                        <th>COLOR</th>
                        <th>CUT</th>
                        <th>SIZE</th>
                        <th>TOTAL</th>
                        <th>NO STICKER</th>
                        <th>PROCESS</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('report.exportDistribusiMonitoring') }}" id="export_data"></a>
<a href="{{ route('report.ajaxDistribusiMonitoringOut') }}" id="get_data"></a>
<a href="{{ route('report.ajaxDistribusiMonitoringOutProcess') }}" id="get_data_process"></a>
@endsection

@section('page-modal')
    @include('report.distribusi._monitoring_modal')
@endsection

@section('page-js')
<script src="{{ url('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    //url
    var url = $('#form_filter').attr('action');
    var date = $('#date_range').val();
    var filter = null;

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        //serverSide: true,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        serverSide: true,
        buttons: [
            {
                text: 'Export to PDF',
                className: 'btn btn-sm bg-success exportPdf',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                }
            },
            {
                text: 'Export to PDF /Process',
                className: 'btn btn-sm bg-primary exportPdfProcess',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                }
            }
       ],
        ajax: {
            url: url,
            type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "date_range": $('#date_range').val(),
                    "date_range_out": $('#date_range_out').val(),
                    "date_range_in": $('#date_range_in').val(),
                    "radio_status" : $('input[name=radio_status]:checked').val(),
                    "po_number" : $('#po_number').val(),
                    "factory_id": $('#factory_id').val(),
                    "filterby": $('.dataTables_filter input').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
           // $('td', row).eq(1).css('min-width', '150px');
        },
        footerCallback: function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            var total_qty = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer by showing the total with the reference of the column index
            $( api.column( 6 ).footer() ).html(formatNumber(total_qty));

        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'tanggal', name: 'tanggal'},
            {data: 'style_edit', name: 'style_edit'},
            {data: 'color_edit', name: 'color_edit'},
            {data: 'cut_number', name: 'cut_number'},
            {data: 'size_edit', name: 'size_edit'},
            {data: 'qty', name: 'qty'},
            {data: 'sticker_no', name: 'sticker_no'},
            {data: 'process_name', name: 'process_name'}
        ],
    });
    //end of datatables

    table
    .on( 'preDraw', function () {
        Pace.start();
    } )
    .on( 'draw.dt', function () {
        $('#table-list').unblock();
        Pace.stop();
    } );

    $('#form_filter').submit(function(event) {
        event.preventDefault();


        //check location
        if($('#date_range').val() == '') {
            alert('Please select date range first');
            return false;
        }

        loading_process();

        table.draw();
    })

    $('#factory_id').on('change', function() {

        loading_process();
        table.draw();
    });

    //
    $('#date_range').on('change', function(){
        date = $(this).val();
    });

     //choose filter
     $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'po') {
            if($('#filter_by_po').hasClass('hidden')) {
                $('#filter_by_po').removeClass('hidden');
            }

            $('#filter_by_date').addClass('hidden');
            $('#filter_by_date_out').addClass('hidden');
            $('#filter_by_date_in').addClass('hidden');
        }
        else if (this.value == 'date') {
            if($('#filter_by_date').hasClass('hidden')) {
                $('#filter_by_date').removeClass('hidden');
            }

            $('#filter_by_po').addClass('hidden');
            $('#filter_by_date_out').addClass('hidden');
            $('#filter_by_date_in').addClass('hidden');
        }
        else if (this.value == 'out') {
            if($('#filter_by_date_out').hasClass('hidden')) {
                $('#filter_by_date_out').removeClass('hidden');
            }

            $('#filter_by_po').addClass('hidden');
            $('#filter_by_date').addClass('hidden');
            $('#filter_by_date_in').addClass('hidden');
        }
        else if (this.value == 'in') {
            if($('#filter_by_date_in').hasClass('hidden')) {
                $('#filter_by_date_in').removeClass('hidden');
            }

            $('#filter_by_po').addClass('hidden');
            $('#filter_by_date').addClass('hidden');
            $('#filter_by_date_out').addClass('hidden');
        }

        loading_process();
        table.draw();
    });

    $(document).on('click', '.exportPdf', function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url: $('#get_data').attr('href'),
            data: {
                date_range_out:$('#date_range_out').val(), 
                radio_status : $('input[name=radio_status]:checked').val(),
                po_number:$('#po_number').val()
            },
            beforeSend: function() {
                $('#modal_list_ > .modal-content').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response){
                $('#modal_list_ > .modal-content').unblock();
                $('#listing').html(response);
            }
        });

        
        $('#modal_list_').modal('show');
    });

    $(document).on('click', '.exportPdfProcess', function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url: $('#get_data_process').attr('href'),
            data: {
                date_range_out:$('#date_range_out').val(), 
                radio_status : $('input[name=radio_status]:checked').val(),
                po_number:$('#po_number').val()
            },
            beforeSend: function() {
                $('#modal_list_ > .modal-content').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response){
                $('#modal_list_ > .modal-content').unblock();
                $('#listing').html(response);
            }
        });

        
        $('#modal_list_').modal('show');
    });

    $('#listing').on('click', '#choose-distribusi-out', function(){
        var style = $(this).data('style');
        var color = $(this).data('color');
        var po = $(this).data('po');

        var url_print = $('#export_data').attr('href');

        var parameter = '?radio_status=' + $('input[name=radio_status]:checked').val()
                        + '&po_number=' + po
                        + '&date_range_out=' + $('#date_range_out').val()
                        //+ '&date_range_in=' + $('#date_range_in').val()
                        + '&style=' + style
                        //+ '&po=' + po
                        + '&color=' + color
                        + '&process_name='
                        + '&factory_id=' + $('#factory_id').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : url_print + parameter,
            success: function(response) {
                window.open(url_print + parameter, '_blank');
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
        

    });
    // process
    $('#listing').on('click', '#choose-distribusi-out-process', function(){
        var style = $(this).data('style');
        var color = $(this).data('color');
        var po = $(this).data('po');
        var process_name = $(this).data('process');

        var url_print = $('#export_data').attr('href');

        var parameter = '?radio_status=' + $('input[name=radio_status]:checked').val()
                        + '&po_number=' + po
                        + '&date_range_out=' + $('#date_range_out').val()
                        //+ '&date_range_in=' + $('#date_range_in').val()
                        + '&style=' + style
                        //+ '&po=' + po
                        + '&color=' + color
                        + '&process_name=' + process_name
                        + '&factory_id=' + $('#factory_id').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url : url_print + parameter,
            success: function(response) {
                window.open(url_print + parameter, '_blank');
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        });
        

    });

});

</script>
@endsection
