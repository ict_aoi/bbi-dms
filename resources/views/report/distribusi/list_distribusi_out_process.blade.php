<div class="table-responsive">
    <table class="table" id="table-listingx">
        <thead>
            <tr>
                <th>PO</th>
                <th>Style</th>
                <th>Color</th>
                <th>Process</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $key => $val)
                <tr>
                    <td>{{ $val->po_number_edit }}</td>
                    <td>{{ $val->style_edit }}</td>
                    <td>{{ $val->color_edit }}</td>
                    <td>{{ $val->process_name }}</td>
                    <td>
                        <button type="button" id="choose-distribusi-out-process"
                                data-po="{{ $val->po_number_edit }}"
                                data-style="{{ $val->style_edit }}"
                                data-color="{{ $val->color_edit }}"
                                data-process="{{ $val->process_name }}"
                                title="print"
                                class="btn btn-warning">
                                <i class="icon-printer4"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<script type="text/javascript">
    var tabless = $('#table-listingx').DataTable({  
        buttons: [
        ],
        "lengthChange": false,
        "pageLength": 5
        /*"columnDefs": [{
            "targets" : [0],
            "visible" : false,
            "searchable" : false
        }]*/
    });
</script>
