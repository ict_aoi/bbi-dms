<!-- MODAL -->
<div id="modal_list_" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
            <div class="modal-content">
              	<div class="modal-body">
                    <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title" id="title"><i class="position-left"></i>Choose Detail</h5>
					</div>
                  	<div class="panel-body loader-area">
						<fieldset>
                            <div id="listing" class="col-lg-12">
                                <!-- template pilihan -->
                            </div>
                        </fieldset>
                  	</div>
              	</div>
            </div>
    </div>
</div>
<!-- /MODAL -->