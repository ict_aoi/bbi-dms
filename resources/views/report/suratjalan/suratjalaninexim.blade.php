@extends('layouts.app', ['active' => 'reportsuratjalaninexim'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Monitoring Surat Jalan In Exim</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home4 position-left"></i> Report</a></li>
            <li class="active">Monitoring Surat Jalan In Exim</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('report.ajaxGetDataSuratJalanInExim') }}" id="form_filter">
            @csrf
            <div class="form-group hidden" id="filter_by_date">
				<label><b>Select Date</b></label>
				<div class="input-group">
					<span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range" required>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
            </div>
            <div class="form-group hidden" id="filter_by_po">
				<label><b>Enter PO Number</b></label>
				<div class="input-group">
                    <input type="text" class="form-control" name="po_number" id="po_number">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
            </div>
            <div class="form-group">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="all">All</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="waiting">Waiting Checkin</label>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-2"> Factory :
                <select class="form-control" name="factory_id" id="factory_id">
                    @foreach($factory as $key => $val)
                        <option value="{{ $val->id }}" {{ $val->id == Auth::user()->factory_id ? "selected" : "" }}>{{ $val->factory_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table datatable-button-html5-basic" id="table-list">
                <thead>
                    <tr>
                        <th style="width:10px;">#</th>
                        <th>Tgl Dokumen</th>
                        <th>No. Surat Jalan</th>
                        <th>Subcont</th>
                        <th>No BC</th>
                        <th>Type BC</th>
                        <th>No Aju</th>
                        <th>No Pendaftaran</th>
                        <th>Qty Exim</th>
                        <th>Status Cutting</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('report.exportSuratJalanInExim') }}" id="export_data"></a>
@endsection

@section('page-js')
<script src="{{ url('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    //url
    var url = $('#form_filter').attr('action');
    var date = $('#date_range').val();
    var filter = null;

    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        lengthMenu: [[10, 25, 50, 100, -1],[10, 25, 50, 100, "All"]],
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#export_data').attr('href')
                                            + '?date_range=' + date
                                            + '&orderby=' + column_name
                                            + '&direction=' + direction
                                            + '&filterby=' + filter
                                            + '&radio_status=' + $('input[name=radio_status]:checked').val()
                                            + '&po_number=' + $('#po_number').val()
                                            + '&factory_id=' + $('#factory_id').val();
                }
            }
       ],
        ajax: {
            url: url,
            type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "date_range": $('#date_range').val(),
                    "radio_status" : $('input[name=radio_status]:checked').val(),
                    "po_number" : $('#po_number').val(),
                    "factory_id": $('#factory_id').val(),
                    "filterby": $('.dataTables_filter input').val(),
                    "_token": _token
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(7).css('min-width', '150px');
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'document_date', name: 'document_date'},
            {data: 'no_suratjalan', name: 'no_suratjalan'},
            {data: 'subcont_name', name: 'subcont.name'},
            {data: 'bc_no', name: 'bc_no'},
            {data: 'type_bc', name: 'type_bc'},
            {data: 'no_aju', name: 'no_aju'},
            {data: 'no_daftar_bc', name: 'no_daftar_bc'},
            {data: 'qty', name: 'qty'},
            {data: 'status', name: 'status', orderable: false, searchable: false}
        ],
    });
    //end of datatables

    table
    .on( 'preDraw', function () {
        Pace.start();
    } )
    .on( 'draw.dt', function () {
        $('#table-list').unblock();
        Pace.stop();
    } );

    $('#form_filter').submit(function(event) {
        event.preventDefault();

        //check location
        if($('#date_range').val() == '') {
            alert('Please select date range first');
            return false;
        }

        loading_process();

        table.draw();
    })

    $('#factory_id').on('change', function() {

        loading_process();
        table.draw();
    });

    //
    $('#date_range').on('change', function(){
        date = $(this).val();
    })

     //choose filter
     $('input[type=radio][name=radio_status]').change(function() {

        loading_process();
        table.draw();
    });

});
</script>
@endsection
