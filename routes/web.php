<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//lOGIN
Route::get('/', function () {
    if(Auth::check()) {

        return redirect()->route('dashboard');
    }
    return redirect('/login');
});

//LOGOUT DARURAT ONLY
Route::get('/logoutdarurat', function() {
    if(Auth::check()) {
        Auth::logout();
    }
    return redirect('/');
});

Route::get('/home', function() {
    if(Auth::check()) {
        return redirect('/');
    }
    return redirect('/login');
});

Auth::routes();

Route::middleware('auth')->group(function () {
    //DASHBOARD
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/dashboard/get-data', 'DashboardController@getData')->name('ajaxGetData');
    Route::get('/dashboard/po_number/{po_number}/package', 'DashboardController@viewPackage')->name('viewPackage');
    Route::get('/dashboard/id/{po_summary_id}/package', 'DashboardController@viewPackageById')->name('viewPackageById');
    Route::get('/dashboard/style/{style}/size/{size}/c_orderline_id/{c_orderline_id}/package', 'DashboardController@viewPackageByOrderLine')->name('viewPackageByOrderLine');

    Route::get('/dashboard/po-number/package/get-data', 'DashboardController@viewPackageData')->name('ajaxPackageData');
    Route::get('/dashboard/po-summary-id/package/get-data', 'DashboardController@viewPackageDataById')->name('ajaxPackageDataById');
    Route::get('/dashboard/orderline/package/get-data', 'DashboardController@viewPackageDataByOrderLine')->name('ajaxPackageDataByOrderLine');

    Route::get('/account-setting', 'HomeController@accountSetting')->name('accountSetting');
    Route::put('/account-setting/{id}', 'HomeController@updateAccount')->name('accountSetting.updateAccount');
    Route::get('/account-setting/{id}/show-avatar', 'HomeController@showAvatar')->name('accountSetting.showAvatar');
    
    Route::get('/factory-setting', 'HomeController@factorySetting')->name('factorySetting');
    Route::post('/factory-setting', 'HomeController@updateFactory')->name('factorySetting.updateFactory');
    // update list komponen break
    Route::post('/list-komponen-break', 'DashboardController@updateListKomponenBreak')->name('updateListKomponenBreak');

    // view file
    Route::get('/documentation', 'HomeController@showDoc')->name('documentation');

    //ROUTE ADMIN
    Route::prefix('admin')->group(function () {
        Route::middleware('role:admin-ict')->group(function () {

            //user management
            Route::get('/user', 'Admin\UserController@index')->name('user.index');
            Route::get('/user/getdatatables', 'Admin\UserController@getDatatables')->name('user.getDatatables');
            Route::get('/user/add-user', 'Admin\UserController@create')->name('user.create');
            Route::get('/user/edit-user/{id}', 'Admin\UserController@edit')->name('user.edit');
            Route::post('/user/user-store', 'Admin\UserController@store')->name('user.store');
            Route::post('/user/update-user', 'Admin\UserController@update')->name('user.update');
            Route::get('/user/edit-user/{id}/reset-password', 'Admin\UserController@resetPassword')->name('user.resetPassword');
            Route::get('/user/delete-user/{id}', 'Admin\UserController@destroy')->name('user.delete');
            Route::get('/user-role', 'Admin\UserController@role')->name('user.role');

            Route::prefix('/permissions')->middleware(['permission:menu-permission'])->group(function(){
                Route::get('', 'Admin\PermissionController@index')->name('permission.index');
                Route::get('data', 'Admin\PermissionController@data')->name('permission.data');
                Route::post('store', 'Admin\PermissionController@store')->name('permission.store');
                Route::get('edit/{id}', 'Admin\PermissionController@edit')->name('permission.edit');
                Route::put('update/{id}', 'Admin\PermissionController@update')->name('permission.update');
                Route::delete('delete/{id}', 'Admin\PermissionController@destroy')->name('permission.destroy');
            });
            
            Route::prefix('/roles')->middleware(['permission:menu-role'])->group(function(){
                Route::get('', 'Admin\RoleController@index')->name('role.index');
                Route::get('data', 'Admin\RoleController@data')->name('role.data');
                Route::get('edit/{id}', 'Admin\RoleController@edit')->name('role.edit');
                Route::get('edit/{id}/permission-role', 'Admin\RoleController@dataPermission')->name('role.dataPermission');
                Route::post('store', 'Admin\RoleController@store')->name('role.store');
                Route::post('store/permission', 'Admin\RoleController@storePermission')->name('role.storePermission');
                Route::post('delete/{role_id}/{permission_id}/permission-role', 'Admin\RoleController@destroyPermissionRole')->name('role.destroyPermissionRole');
                Route::put('update/{id}', 'Admin\RoleController@update')->name('role.update');
                Route::delete('delete/{id}', 'Admin\RoleController@destroy')->name('role.destroy');
            });
        });
    });

    //ROUTE CUTTING
    Route::prefix('cutting')->group(function () {

        //cutting
        Route::get('/barcode', 'Cutting\CuttingController@barcode')->name('cutting.barcode');
        Route::get('/get-data', 'Cutting\CuttingController@getData')->name('cutting.ajaxGetData');
        Route::post('/barcode/modal', 'Cutting\CuttingController@modalBarcode')->name('cutting.modalBarcode');
        Route::post('/barcode/generate', 'Cutting\CuttingController@generateBarcode')->name('cutting.generateBarcode');
        // check list komponen
        Route::get('/check-komponen', 'Cutting\CuttingController@checkKomponen')->name('cutting.checkKomponen');
        Route::get('/check-komponen/get-data', 'Cutting\CuttingController@getDataCheckKomponen')->name('cutting.getDataCheckKomponen');

        Route::get('/get-data-komponen', 'Cutting\CuttingController@getDataKomponen')->name('cutting.ajaxGetDataKomponen');

        Route::get('/barcode/po-number/{po_number}/style/{style}/size/{size}/package', 'Cutting\CuttingController@viewPackage')->name('cutting.viewPackageDetail');
        Route::get('/barcode/m_product_id/{m_product_id}/c_orderline_id/{c_orderline_id}/style/{style}/size/{size}/package', 'Cutting\CuttingController@viewPackageByProduct')->name('cutting.viewPackageByProduct');

        Route::get('/barcode/po-number/package/get-data', 'Cutting\CuttingController@viewPackageData')->name('cutting.ajaxPackageData');
        Route::get('/barcode/product/package/get-data', 'Cutting\CuttingController@viewPackageDataByProduct')->name('cutting.ajaxPackageDataByProduct');
        
        Route::post('/barcode/po-number/package/set-completed', 'Cutting\CuttingController@setCompleted')->name('cutting.ajaxSetCompleted');

        Route::post('/barcode/po-number/package/set-completed-all', 'Cutting\CuttingController@setCompletedAll')->name('cutting.ajaxSetCompletedAll');
        
        Route::get('/barcode/po-number/package/print-preview', 'Cutting\CuttingController@printPreview')->name('cutting.printPreview');
        Route::get('/barcode/po-number/package/print-preview-komponen', 'Cutting\CuttingController@printPreviewKomponen')->name('cutting.printPreviewKomponen');

        Route::get('/barcode/formulir/package/print-preview', 'Cutting\CuttingController@printPreviewByFormulir')->name('cutting.printPreviewByFormulir');

        Route::get('/barcode/formulir/package/print-preview-reject', 'Cutting\CuttingController@printPreviewByFormulirReject')->name('cutting.printPreviewByFormulirReject');
        
        Route::post('/barcode/po-number/package/set-completed-all-filter-po', 'Cutting\CuttingController@setCompletedAllFilterPo')->name('cutting.ajaxSetCompletedAllFilterPo');

        //master subcont
        Route::get('/master-subcont', 'Cutting\CuttingController@masterSubcont')->name('cutting.masterSubcont');
        Route::get('/master-subcont/get-data', 'Cutting\CuttingController@ajaxGetDataMasterSubcont')->name('cutting.ajaxGetDataMasterSubcont');
        Route::post('/master-subcont/add-subcont', 'Cutting\CuttingController@addSubcont')->name('cutting.addSubcont');
        Route::get('/master-subcont/edit-subcont/{id}', 'Cutting\CuttingController@editSubcont')->name('cutting.editSubcont');
        Route::post('/master-subcont/update-subcont', 'Cutting\CuttingController@updateSubcont')->name('cutting.updateSubcont');
        Route::get('/master-subcont/delete-subcont/{id}', 'Cutting\CuttingController@deleteSubcont')->name('cutting.deleteSubcont');
        
        //master style
        Route::get('/master-style', 'Cutting\CuttingController@masterStyle')->name('cutting.masterStyle');
        Route::get('/master-style/get-data', 'Cutting\CuttingController@ajaxGetDataMasterStyle')->name('cutting.ajaxGetDataMasterStyle');
        Route::post('/master-style/add-style', 'Cutting\CuttingController@addStyle')->name('cutting.addStyle');
        Route::get('/master-style/edit-style/{id}/{komponen}', 'Cutting\CuttingController@editStyle')->name('cutting.editStyle');
        Route::post('/master-style/update-style', 'Cutting\CuttingController@updateStyle')->name('cutting.updateStyle');
        Route::get('/master-style/delete-style/{id}/{komponen}', 'Cutting\CuttingController@deleteStyle')->name('cutting.deleteStyle');
        Route::get('/master-style/get-list-data', 'Cutting\CuttingController@ajaxGetDataGetListStyle')->name('cutting.ajaxGetDataGetListStyle');

        // style sync
        Route::get('/style/sync', 'Cutting\CuttingController@styleSync')->name('cutting.styleSync');
        
        //master komponen
        Route::get('/master-komponen', 'Cutting\CuttingController@masterKomponen')->name('cutting.masterKomponen');
        Route::get('/master-komponen/get-data', 'Cutting\CuttingController@ajaxGetDataMasterKomponen')->name('cutting.ajaxGetDataMasterKomponen');
        Route::post('/master-komponen/add-komponen', 'Cutting\CuttingController@addKomponen')->name('cutting.addKomponen');
        Route::get('/master-komponen/edit-komponen/{id}', 'Cutting\CuttingController@editKomponen')->name('cutting.editKomponen');
        Route::post('/master-komponen/update-komponen', 'Cutting\CuttingController@updateKomponen')->name('cutting.updateKomponen');
        Route::get('/master-komponen/delete-komponen/{id}', 'Cutting\CuttingController@deleteKomponen')->name('cutting.deleteKomponen');
        
        //master process
        Route::get('/master-process', 'Cutting\CuttingController@masterProcess')->name('cutting.masterProcess');
        Route::get('/master-process/get-data', 'Cutting\CuttingController@ajaxGetDataMasterProcess')->name('cutting.ajaxGetDataMasterProcess');
        Route::post('/master-process/add', 'Cutting\CuttingController@addProcess')->name('cutting.addProcess');
        Route::get('/master-process/edit/{id}', 'Cutting\CuttingController@editProcess')->name('cutting.editProcess');
        Route::post('/master-process/update', 'Cutting\CuttingController@updateProcess')->name('cutting.updateProcess');
        Route::get('/master-process/delete/{id}', 'Cutting\CuttingController@deleteProcess')->name('cutting.deleteProcess');

        // ajax po summary detail
        Route::get('/list-orderline', 'Cutting\CuttingController@ajaxGetDataSummaryDetail')->name('cutting.ajaxGetDataSummaryDetail');
        // delete summary detail
        Route::get('/list-orderline/delete', 'Cutting\CuttingController@deleteSummaryDetail')->name('cutting.deleteSummaryDetail');

        // update po number edit
        Route::post('/update-po-number-edit', 'Cutting\CuttingController@updatePoNumberEdit')->name('cutting.updatePoNumberEdit');
     });

     //ROUTE DISTRIBUSI
    Route::prefix('distribusi')->group(function () {
        // checkout
        Route::get('/checkout', 'Distribusi\DistribusiController@checkout')->name('distribusi.checkout');
        Route::post('/checkout', 'Distribusi\DistribusiController@postCheckout')->name('distribusi.postCheckout');

        // checkin
        Route::get('/checkin', 'Distribusi\DistribusiController@checkin')->name('distribusi.checkin');
        Route::post('/checkin', 'Distribusi\DistribusiController@postCheckin')->name('distribusi.postCheckin');

        // get data delivery reject
        Route::get('/delivery-reject/get-data', 'Distribusi\DistribusiController@ajaxGetDeliveryReject')->name('distribusi.ajaxGetDeliveryReject');
        
        // get data delivery in move
        Route::get('/delivery-in-move/get-data', 'Distribusi\DistribusiController@ajaxGetDeliveryInMove')->name('distribusi.ajaxGetDeliveryInMove');

        Route::post('/delivery-reject/checkin', 'Distribusi\DistribusiController@checkinDeliveryReject')->name('distribusi.checkinDeliveryReject');

        // move sj
        Route::post('/delivery-move/checkin', 'Distribusi\DistribusiController@checkinDeliveryMove')->name('distribusi.checkinDeliveryMove');

    });

    //ROUTE PACKING
    Route::prefix('packing')->group(function () {
        //surat jalan
        Route::get('/packing-list', 'Packing\PackingListController@index')->name('packing.index');
        //surat jalan global
        Route::get('/packing-list-global', 'Packing\PackingListController@suratJalanGlobal')->name('packing.suratJalanGlobal');

        Route::get('/list-surat-jalan', 'Packing\PackingListController@listSuratJalan')->name('packing.ajaxGetSuratJalan');
        Route::get('/get-data-surat-jalan', 'Packing\PackingListController@getDataSuratJalan')->name('packing.ajaxGetDataSuratJalan');
        // update uoms
        Route::post('/update-uoms', 'Packing\PackingListController@updateUoms')->name('packing.updateUoms');

        // cancel surat jalan
        Route::post('/surat-jalan/cancel', 'Packing\PackingListController@cancelSuratJalan')->name('packing.cancelSuratJalan');

        // formulir pengiriman artwork
        Route::get('/formulir', 'Packing\PackingListController@formulir')->name('packing.formulir');
        Route::get('/get-data-formulir', 'Packing\PackingListController@getDataFormulir')->name('packing.ajaxGetDataFormulir');
        // update description
        Route::post('/update-description', 'Packing\PackingListController@updateDescription')->name('packing.updateDescription');
        // update qty used
        Route::post('/update-qty-used', 'Packing\PackingListController@updateQtyUsed')->name('packing.updateQtyUsed');
        // update type pl formulir
        Route::post('/update-type-pl', 'Packing\PackingListController@updateTypePl')->name('packing.updateTypePl');
        // update type pl formulir reject
        Route::post('/update-type-pl-reject', 'Packing\PackingListController@updateTypePlReject')->name('packing.updateTypePlReject');
        // update barcode reject
        Route::post('/update-barcode-reject', 'Packing\PackingListController@updateBarcodeReject')->name('packing.updateBarcodeReject');

        // print formulir
        Route::post('/formulir/set-completed-all', 'Packing\PackingListController@setCompletedAllFormulir')->name('packing.ajaxSetCompletedAllFormulir');
        // print formulir reject
        Route::post('/formulir/set-completed-all-reject', 'Packing\PackingListController@setCompletedAllFormulirReject')->name('packing.ajaxSetCompletedAllFormulirReject');

        Route::get('/formulir/print-preview', 'Packing\PackingListController@printPreviewFormulir')->name('packing.printPreviewFormulir');
        // print formulir reject
        Route::get('/formulir/print-preview-reject', 'Packing\PackingListController@printPreviewFormulirReject')->name('packing.printPreviewFormulirReject');


        // formulir pengiriman artwork reject
        Route::get('/formulir-reject', 'Packing\PackingListController@formulirReject')->name('packing.formulirReject');
        Route::get('/get-data-formulir-reject', 'Packing\PackingListController@getDataFormulirReject')->name('packing.ajaxGetDataFormulirReject');

        // print surat jalan
        Route::post('/surat-jalan/set-completed-all', 'Packing\PackingListController@setCompletedAllSuratJalan')->name('packing.ajaxSetCompletedAllSuratJalan');

        Route::get('/surat-jalan/print-preview', 'Packing\PackingListController@printPreviewSuratJalan')->name('packing.printPreviewSuratJalan');

        // print surat jalan global
        Route::post('/surat-jalan/set-completed-all-global', 'Packing\PackingListController@setCompletedAllSuratJalanGlobal')->name('packing.ajaxSetCompletedAllSuratJalanGlobal');

        Route::get('/surat-jalan/print-preview-global', 'Packing\PackingListController@printPreviewSuratJalanGlobal')->name('packing.printPreviewSuratJalanGlobal');
        

        Route::get('/formulir/list', 'Packing\PackingListController@listFormulir')->name('packing.ajaxGetFormulir');
        Route::get('/formulir/list/barcode', 'Packing\PackingListController@listFormulirBarcode')->name('packing.ajaxGetFormulirBarcode');

        Route::get('/formulir/list/barcode-exist', 'Packing\PackingListController@listFormulirBarcodeExist')->name('packing.ajaxGetBarcode');

        // cancel formulir
        Route::post('/formulir/cancel', 'Packing\PackingListController@cancelFormulir')->name('packing.cancelFormulir');
        // cancel formulir reject
        Route::get('/formulir/list-reject', 'Packing\PackingListController@listFormulirReject')->name('packing.ajaxGetFormulirReject');
        Route::post('/formulir/cancel-reject', 'Packing\PackingListController@cancelFormulirReject')->name('packing.cancelFormulirReject');
        
        // get no surat jalan
        Route::get('/surat-jalan/get-nomor', 'Packing\PackingListController@getNomorSuratJalan')->name('packing.ajaxGetNomorSuratJalan');

        // get formulir from no surat jalan
        Route::get('/surat-jalan/list-exist', 'Packing\PackingListController@listSuratJalanExist')->name('packing.ajaxGetSuratJalanExist');

        // get no polybag
        Route::get('/polybag/get-nomor', 'Packing\PackingListController@getNomorPolyBag')->name('packing.ajaxGetNomorPolyBag');
       
    });

    // ROUTE SURAT JALAN
    Route::prefix('surat-jalan')->group(function(){
        //list 
        Route::get('/list', 'SuratJalan\SuratJalanController@list')->name('suratjalan.list');
        Route::get('/list/export', 'SuratJalan\SuratJalanController@exportSuratJalan')->name('suratjalan.exportSuratJalan');
        Route::post('/get-list-surat-jalan', 'SuratJalan\SuratJalanController@getDataSuratJalan')->name('suratjalan.ajaxGetDataSuratJalan');

        // update bc no
        Route::get('/edit-bc', 'SuratJalan\SuratJalanController@editBcNo')->name('suratjalan.editBcNo');
        Route::post('/update-bc', 'SuratJalan\SuratJalanController@updateBcNo')->name('suratjalan.updateBcNo');

        // get detail surat jalan
        Route::get('/detail', 'SuratJalan\SuratJalanController@getDetailSuratJalan')->name('suratjalan.ajaxGetDetailSuratJalan');

        // surat jalan in
        Route::get('/surat-jalan-in', 'SuratJalan\SuratJalanController@suratJalanIn')->name('suratjalanin.suratJalanIn');
        Route::get('/surat-jalan-in/get-data', 'SuratJalan\SuratJalanController@ajaxGetDataSuratJalanIn')->name('suratjalanin.ajaxGetDataSuratJalanIn');
        Route::post('/surat-jalan-in/add', 'SuratJalan\SuratJalanController@addSuratJalanIn')->name('suratjalanin.addSuratJalanIn');
        Route::get('/surat-jalan-in/edit/{id}', 'SuratJalan\SuratJalanController@editSuratJalanIn')->name('suratjalanin.editSuratJalanIn');
        Route::post('/surat-jalan-in/update', 'SuratJalan\SuratJalanController@updateSuratJalanIn')->name('suratjalanin.updateSuratJalanIn');
        Route::get('/surat-jalan-in/delete/{id}', 'SuratJalan\SuratJalanController@deleteSuratJalanIn')->name('suratjalanin.deleteSuratJalanIn');
        
        // get data delivery in 
        Route::post('/delivery-in/get-data', 'SuratJalan\SuratJalanController@ajaxGetDataDeliveryIn')->name('suratjalanin.ajaxGetDataDeliveryIn');
        Route::get('/delivery-in/export', 'SuratJalan\SuratJalanController@exportDataDeliveryIn')->name('suratjalanin.exportDataDeliveryIn');

        Route::post('/delivery-in-detail/delete/{id}', 'SuratJalan\SuratJalanController@deleteDeliveryInDetail')->name('suratjalanin.deleteDeliveryInDetail');

        //update qty
        Route::post('/delivery-in-detail/update-qty', 'SuratJalan\SuratJalanController@updateQty')->name('suratjalanin.updateQty');
    });

    //ROUTE REPORT
    Route::prefix('report')->group(function () {
        //preparation
        Route::get('/preparation', 'Report\PreparationController@index')->name('report.preparation');
        Route::get('/preparation/export', 'Report\PreparationController@exportPreparation')->name('report.exportPreparation');
        Route::post('/get-report-preparation', 'Report\PreparationController@getDataPreparation')->name('report.ajaxGetDataPreparation');
        
        //distribusi
        Route::get('/distribusi', 'Report\DistribusiController@index')->name('report.distribusi');
        Route::get('/distribusi/export', 'Report\DistribusiController@exportDistribusi')->name('report.exportDistribusi');
        Route::post('/get-report-distribusi', 'Report\DistribusiController@getDataDistribusi')->name('report.ajaxGetDataDistribusi');
        
        //distribusi monitoring
        Route::get('/distribusi-monitoring', 'Report\DistribusiController@monitoring')->name('report.distribusiMonitoring');
        Route::get('/distribusi-monitoring/export', 'Report\DistribusiController@exportDistribusiMonitoringPdf')->name('report.exportDistribusiMonitoring');
        Route::post('/get-report-distribusi-monitoring', 'Report\DistribusiController@getDataDistribusiMonitoring')->name('report.ajaxGetDataDistribusiMonitoring');

        //distribusi monitoring in
        Route::get('/distribusi-monitoring-in', 'Report\DistribusiController@monitoringIn')->name('report.distribusiMonitoringIn');
        Route::get('/distribusi-monitoring-in/export', 'Report\DistribusiController@exportDistribusiMonitoringIn')->name('report.exportDistribusiMonitoringIn');
        Route::post('/get-report-distribusi-monitoring-in', 'Report\DistribusiController@getDataDistribusiMonitoringIn')->name('report.ajaxGetDataDistribusiMonitoringIn');

        // ajax get monitoring
        Route::get('/get-distribusi-monitoring-out', 'Report\DistribusiController@getDistribusiMonitoringOut')->name('report.ajaxDistribusiMonitoringOut');

        // ajax get monitoring per process
        Route::get('/get-distribusi-monitoring-out-process', 'Report\DistribusiController@getDistribusiMonitoringOutProcess')->name('report.ajaxDistribusiMonitoringOutProcess');

        //formulir
        Route::get('/formulir', 'Report\FormulirController@index')->name('report.formulir');
        Route::get('/formulir/export', 'Report\FormulirController@exportFormulir')->name('report.exportFormulir');
        Route::post('/get-report-formulir', 'Report\FormulirController@getDataFormulir')->name('report.ajaxGetDataFormulir');

        //surat jalan
        Route::get('/surat-jalan', 'Report\SuratJalanController@index')->name('report.suratjalan');
        Route::get('/surat-jalan/export', 'Report\SuratJalanController@exportSuratJalan')->name('report.exportSuratJalan');
        Route::post('/get-report-surat-jalan', 'Report\SuratJalanController@getDataSuratJalan')->name('report.ajaxGetDataSuratJalan');

        //surat jalan in
        Route::get('/surat-jalan-in', 'Report\SuratJalanController@suratjalanin')->name('report.suratjalanin');
        Route::get('/surat-jalan-in/export', 'Report\SuratJalanController@exportSuratJalanIn')->name('report.exportSuratJalanIn');
        Route::post('/get-report-surat-jalan-in', 'Report\SuratJalanController@getDataSuratJalanIn')->name('report.ajaxGetDataSuratJalanIn');
        
        //surat jalan in exim
        Route::get('/surat-jalan-in-exim', 'Report\SuratJalanController@suratjalaninexim')->name('report.suratjalaninexim');
        Route::get('/surat-jalan-in-exim/export', 'Report\SuratJalanController@exportSuratJalanInExim')->name('report.exportSuratJalanInExim');
        Route::post('/get-report-surat-jalan-in-exim', 'Report\SuratJalanController@getDataSuratJalanInExim')->name('report.ajaxGetDataSuratJalanInExim');
    });
    
});
