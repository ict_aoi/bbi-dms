<?php

namespace App\Pdf;

use TJGazel\LaraFpdf\LaraFpdf;
use Carbon\Carbon;

class FormulirArtwork extends LaraFpdf
{
    //
    private $data;
    private $factories;
    private $data_arr;

    public function __construct($data, $factories, $data_arr)
    {
        $this->data = $data;
        $this->factories = $factories;
        $this->data_arr = $data_arr;
        parent::__construct('P','mm', array(210, 297)); //A4 aslinya 210x297
        $this->SetTitle('Cetak Formulir Artwork', true);
        $this->SetAuthor('TJGazel', true);
        $this->AddPage();
        $this->AliasNbPages();
        $this->Body();
    }

    public function Header($from=0, $to=33)
    {
        // fixed all pages
        $this->SetLineWidth(0.5); //default 0.2 mm
        $this->Rect(8, 8, 195, 278);
        
        $this->Image(public_path().'/images/'.$this->factories->logo_factory, 30, 10, -400);
        
        $this->SetFont('Arial', 'B', '14');
        
        $this->setXY(110, 8);
        $this->Cell(93, 12, 'FORMULIR', 1, 0, 'C');
        $this->setXY(110, 20);
        $this->SetFont('Arial', '', '11');
        $this->Cell(93, 12, 'PENGIRIMAN ARTWORK', 1, 0, 'C');
        
        $this->Line(8, 32, 203, 32);
        $this->Line(110, 8, 110, 32);

        $this->setXY(10, 33);
        $this->Cell(20, 6, '# '.$this->data[0]->no_polybag, 0, 0);

        $this->SetFont('Arial','BI',8);
        // Print right page number
        // $this->ln();
        // $this->Cell(0,10,'Page '.$this->PageNo()."/{nb}",0,0,'R');
        
        $this->SetFont('Arial', '', '10');
        $this->setXY(8, 40);
        $this->Cell(25, 6, 'STYLE', 1, 0, 'C');
        $this->Cell(25, 6, 'PO', 1, 0, 'C');
        $this->Cell(22, 6, 'ARTICLE', 1, 0, 'C');
        $this->Cell(22, 6, 'COLOUR', 1, 0, 'C');
        $this->Cell(15, 6, 'CUTT', 1, 0, 'C');
        $this->Cell(15, 6, 'SIZE', 1, 0, 'C');
        $this->Cell(22, 6, 'BUNDEL', 1, 0, 'C');
        $this->Cell(15, 6, 'QTY', 1, 0, 'C');
        $this->Cell(34, 6, 'KETERANGAN', 1, 0, 'C');
        
        $this->SetFont('Arial', '', '9');
        $this->setXY(10, 46);
        for ($i=$from; $i < $to; $i++) {
            $this->Cell(-2);
            $this->Cell(25, 6, '', 1, 0);
            $this->Cell(25, 6, '', 1, 0);
            $this->Cell(22, 6, '', 1, 0);
            $this->Cell(22, 6, '', 1, 0, 'C');
            $this->Cell(15, 6, '', 1, 0, 'C');
            $this->Cell(15, 6, '', 1, 0, 'C');
            $this->Cell(22, 6, '', 1, 0, 'C');
            $this->Cell(15, 6, '', 1, 0, 'R');
            $this->Cell(34, 6, '', 1, 0, 'C');
            $this->ln();
        }

    }

    public function Body()
    {
        $this->Detail();

        if(count($this->data) > 33){
            $page_count = ceil((count($this->data) - 33) /33);
            for($i = 0; $i < $page_count; $i++){
                $this->AddPage('P');
                $this->Detail(33 + ($i * 33), 33);
            }
        }
    }

    public function Detail($from = 0, $to = 33)
    {
        $this->SetFont('Arial', '', '9');

        $no = $from+1;
        $this->setXY(8, 46);
        // $this->Cell(25, 6, $this->data[0]->style_edit, 0, 0);
        // $this->Cell(25, 6, $this->data[0]->po_number_edit, 0, 0);
        // $this->Cell(22, 6, $this->data[0]->article_edit, 0, 0);
        // $this->Cell(22, 6, $this->data[0]->color_edit, 0, 0, 'C');
        $this->setXY(10, 46);

        $rows = array();
        $inc = 0;
        foreach ($this->data_arr as $val) {
            $num = 1;
            foreach($val as $row){
                $row->num = $num;
                $rows[] = $row;
                

                $inc++;
                $num++;
             }
        }
        
        // foreach ($this->data_arr as $key => $val) {
            foreach(array_slice($rows, $from, $to) as $key2 => $row){
            // $num = 1;
            // foreach($val as $key1 => $row){
            // foreach($val->slice($from, $to) as $key1 => $row){

                $style_edit = $row->style_edit;
                $po_number_edit = $row->po_number_edit;
                $article_edit = $row->article_edit;
                $color_edit = $row->color_edit;

                if ($row->num != 1) {
                    $style_edit = '';
                    $po_number_edit = '';
                    $article_edit = '';
                    $color_edit = '';
                }
            
                $this->Cell(-2);
                $this->Cell(25, 6, $style_edit, 1, 0);
                $this->Cell(25, 6, $po_number_edit, 1, 0);
                $this->Cell(22, 6, $article_edit, 1, 0);
                $this->Cell(22, 6, $color_edit, 1, 0, 'C');
                $this->Cell(15, 6, $row->cut_number, 1, 0, 'C');
                $this->Cell(15, 6, $row->size_edit, 1, 0, 'C');
                $this->Cell(22, 6, $row->sticker_no, 1, 0, 'C');
                $this->Cell(15, 6, $row->qty_formulir, 1, 0, 'R');
                $this->Cell(34, 6, $row->description, 1, 0, 'C');
                $this->Ln();
                
                $num++;
            //  }
        }
        
    }

    public function Footer()
    {
        // Go to 1.0 cm from bottom
        $this->SetY(-10);
        // Select Arial italic 8
        $this->SetFont('Arial','I',8);
        // Print current and total page numbers
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
        // fixed all pages        
        $this->SetY(-40);
        $this->SetFont('Arial', '', '11');
        $this->Cell(92);
        $this->Cell(30, 6, 'Jabatan', 1, 0, 'C');
        $this->Cell(37, 6, 'Tanda Tangan', 1, 0, 'C');
        $this->Cell(34, 6, 'Tanggal', 1, 0, 'C');
        $this->ln();
        $this->Cell(49);
        $this->Cell(43, 6, 'Dibuat Oleh', 1, 0);
        $this->Cell(30, 6, '', 1, 0, 'C');
        $this->Cell(37, 6, '', 1, 0, 'C');
        $this->Cell(34, 6, Carbon::parse($this->data[0]->formulir_created)->format('d/m/Y'), 1, 0, 'C');
        $this->ln();
        $this->Cell(49);
        $this->Cell(43, 6, 'Diperiksa Oleh', 1, 0);
        $this->Cell(30, 6, '', 1, 0, 'C');
        $this->Cell(37, 6, '', 1, 0, 'C');
        $this->Cell(34, 6, '', 1, 0, 'C');
        $this->ln();
        $this->Cell(49);
        $this->Cell(43, 6, 'Diterima Oleh', 1, 0);
        $this->Cell(30, 6, '', 1, 0, 'C');
        $this->Cell(37, 6, '', 1, 0, 'C');
        $this->Cell(34, 6, '', 1, 0, 'C');

        $this->SetY(-10);
        $this->SetFont('Arial', 'B', '10');
        
        $this->Cell(193, 6, 'FR.03-01-DIS/00', 0, 0, 'R');
    }
}
