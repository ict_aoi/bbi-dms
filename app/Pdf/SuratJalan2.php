<?php

namespace App\Pdf;

use TJGazel\LaraFpdf\LaraFpdf;
use Carbon\Carbon;

class SuratJalan2 extends LaraFpdf
{
    //
    private $data_header;
    private $data;
    private $no_suratjalan;
    private $style;
    private $data_arr;
    private $factories;
    private $type_name;

    public function __construct($data_header,$data,$no_suratjalan,$style, $data_arr, $factories, $type_name)
    {
        $this->data_header = $data_header;
        $this->data = $data;
        $this->no_suratjalan = $no_suratjalan;
        $this->style = $style;
        $this->type_name = $type_name;
        $this->data_arr = $data_arr;
        $this->factories = $factories;
        parent::__construct('L','mm', array(210, 148)); //A5 aslinya 148x210
        $this->SetTitle('Cetak Surat Jalan', true);
        $this->SetAuthor('TJGazel', true);
        $this->AddPage();
        $this->AliasNbPages();
        $this->Body();
    }

    public function Header($from=0, $to=9)
    {
        // fixed all pages
        // $this->SetLineWidth(0.5); //default 0.2 mm
        // $this->Rect(8, 8, 195, 145);
        
        // $this->Image(public_path().'/images/'.$this->factories->logo_factory, 10, 10, -400);
        
        // $this->SetLineWidth(0.2); //default 0.2 mm
        $this->SetFont('Arial', '', '12');
        
        $this->setXY(107, 19);
        // $this->Cell(25, 6, 'Tanggal', 0);
        // $this->Cell(5, 6, ':', 0);
        $this->Cell(25, 6, '', 0);
        $this->Cell(5, 6, '', 0);
        $this->Cell(50, 6, Carbon::parse($this->data_header->created_at)->format('d F Y'), 0);
        /////////////////////////////
        $this->setXY(107, 24);
        $this->Cell(25, 6, '', 0);
        $this->Cell(5, 6, '', 0);
        $this->Cell(50, 6, $this->no_suratjalan, 0);


        $this->SetFont('Arial', 'B', '9');
        $this->setXY(184, 24.2);
        // $this->Cell(25, 6, 'No', 0);
        // $this->Cell(5, 6, ':', 0);
        //$this->Cell(25, 6, Carbon::parse($this->data_header->created_at)->format('y'), 0);
        $this->Cell(5, 6, '', 0);
        $this->SetFont('Arial', 'B', '11');
        $this->Cell(20, 6, '', 0);
        $this->SetFont('Arial', '', '12');
        $this->Cell(30, 6, '', 0);
        $this->setXY(100, 33);
        // $this->Cell(25, 6, 'Kepada Yth.', 0);
        // $this->Cell(5, 6, ':', 0);
        $this->Cell(25, 6, '', 0);
        $this->Cell(5, 6, '', 0);
        $this->Cell(50, 6, $this->data_header->name, 0);
        $this->setXY(120, 23.2);
        $this->MultiCell(80,6,'',0,'C');

        // $this->setXY(80, 42);
        $this->ln();
        $this->SetFont('Arial', 'B', '12');
        // $this->Cell(190, 6, 'SURAT JALAN', 0, 0, 'C');
        $this->SetLineWidth(0.5); //default 0.2 mm
        $this->SetFont('Arial', 'I', '12');
        // $this->setXY(8, 49);
        // // $this->Cell(195, 6, 'S # '. $this->style, 1, 0);
        // // $this->Line(8, 49, 203, 49);
        // // $this->SetLineWidth(0.2); //default 0.2 mm

        // $this->setXY(8, 49);
        // $this->Cell(15, 6, 'NO', 1, 0, 'C');
        // $this->Cell(100, 6, 'URAIAN BARANG', 1, 0, 'C');
        // $this->Cell(40, 6, 'JUMLAH', 1, 0, 'C');
        // $this->Cell(40, 6, 'SATUAN', 1, 0, 'C');
        
        $this->setXY(18, 53.2);
        // for ($i=$from; $i < $to; $i++) {
            // $this->Cell(-2);
            // $this->Cell(15, 7, '', 0, 0, 'C');
            $style_type = $this->style;

            if ($this->type_name != 'Non') {
                $style_type = $this->style.' '.$this->type_name;
            }

            $this->Cell(70, 7, 'S # '. $style_type, 0, 0, 'C');
            // $this->Cell(40, 7, '', 0, 0, 'R');
            // $this->Cell(40, 7, '', 0, 0, 'C');
            // $this->ln();
        // }

        $this->setXY(10, 66);
        for ($i=$from; $i < $to; $i++) {
            $this->Cell(-2);
            $this->Cell(20, 6.8, '', 0, 0, 'C');
            $this->Cell(33, 6.8, '', 0, 0);
            $this->Cell(50, 6.8, '', 0, 0);
            $this->Cell(40, 6.8, '', 0, 0, 'R');
            $this->Cell(40, 6.8, '', 0, 0, 'C');
            $this->ln();
        }

        // $this->Line(8, 116, 203, 116);
        // $this->Line(73, 116, 73, 153);

    }

    public function Body()
    {
        $this->Detail();

        if(count($this->data) > 9){
            $page_count = ceil((count($this->data) - 9) / 9);
            for($i = 0; $i < $page_count; $i++){
                $this->AddPage('L');
                $this->Detail(9 + ($i * 9), 9);
            }
        }
    }

    public function Detail($from = 0, $to = 9)
    {
         $this->SetFont('Arial', '', '12');

         $no = $from+1;
         
         $this->SetXY(10,65);
        //  foreach ($this->data_arr as $key => $value) {
         foreach ($this->data as $key => $row) {
             $num = 1;
            //  foreach($value as $key2 => $row){
            //  foreach(array_slice($this->data, $from, $to) as $row){
                $po_number_edit = $row->po_number_edit;

                // if ($num != 1) { //gak jadi
                //     $po_number_edit = '';
                // }

                $this->Cell(-2);
                $this->Cell(20, 6.3, $row->no_polybag, 0, 0, 'C');
                if (strlen($po_number_edit)>12) {

                    $this->SetFont('Arial', 'B', '8');
                }
                $this->Cell(33, 6.3, $po_number_edit, 0, 0);
                $this->SetFont('Arial', '', '7.5');
                $this->Cell(50, 6.3,'  #art:'.$row->article_edit.' #coll:'. $row->color_edit, 0, 0);
                $this->SetFont('Arial', '', '12');
                $this->Cell(40, 6.3, number_format($row->total_qty/$row->count_komponen), 0, 0, 'R');
                $this->Cell(40, 6.3, $row->uoms, 0, 0, 'C');
                $this->Ln();  
    
                $no++;
                $num++;
            //  }
         }
        
    }

    public function Footer()
    {
        // fixed all pages
        $this->SetY(-39);
        $this->SetFont('Arial', '', '11');
        
        // $this->Cell(63, 6, 'Diterima Oleh :', 0, 0, 'C');
        // $this->Cell(63, 6, 'Disetujui Oleh :', 0, 0, 'C');
        // $this->Cell(63, 6, 'Dikirim Oleh :', 0, 0, 'C');
        // $this->Ln(20);
        // $this->Cell(63, 6, '......................', 0, 0, 'C');
        // $this->Cell(63, 6, '......................', 0, 0, 'C');
        // $this->Cell(63, 6, '......................', 0, 0, 'C');
        // $this->Ln();
        // $this->Cell(63, 6, 'Nama Jelas', 0, 0, 'C');
    }
}
