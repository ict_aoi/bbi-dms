<?php

namespace App\Pdf;

use TJGazel\LaraFpdf\LaraFpdf;
use Carbon\Carbon;

class FormulirArtwork2 extends LaraFpdf
{
    //
    private $data;
    private $factories;
    private $data_arr;
    private $total_qty;
    private $count_set;

    public function __construct($data, $factories, $data_arr, $total_qty, $count_set)
    {
        $this->data = $data;
        $this->factories = $factories;
        $this->data_arr = $data_arr;
        $this->total_qty = $total_qty;
        $this->count_set = $count_set;
        parent::__construct('P','mm', array(210, 297)); //A4 aslinya 210x297
        $this->SetTitle('Cetak Formulir Artwork', true);
        $this->SetAuthor('TJGazel', true);
        $this->AddPage();
        $this->AliasNbPages();
        $this->Body();
    }

    public function Header($from=0, $to=29)
    {
        // fixed all pages
        // $this->SetLineWidth(0.5); //default 0.2 mm
        // $this->Rect(8, 8, 195, 278);
        
        // $this->Image(public_path().'/images/'.$this->factories->logo_factory, 30, 10, -400);
        
        $this->SetFont('Arial', 'B', '14');
        
        // $this->setXY(110, 8);
        // $this->Cell(93, 12, 'FORMULIR', 1, 0, 'C');
        // $this->setXY(110, 20);
        // $this->SetFont('Arial', '', '11');
        // $this->Cell(93, 12, 'PENGIRIMAN ARTWORK', 1, 0, 'C');
        
        // $this->Line(8, 32, 203, 32);
        // $this->Line(110, 8, 110, 32);

        $this->setXY(8, 33); //33
        if ($this->data[0]->type_pl == 1) {
            $this->Cell(20, 6, '# '.$this->data[0]->no_polybag, 0, 0);
        }

        $this->setXY(40, 33); //33
        $this->Cell(20, 6, $this->data[0]->cut_info, 0, 0);
        
        $this->SetFont('Arial', 'B', '12');
        $this->setXY(100, 33); //33
        $this->Cell(20, 6, 'Total Qty : '.$this->total_qty, 0, 0);
        
        if ($this->count_set > 1) {
            // $this->SetFont('Arial', '', '11');

            $this->setXY(140, 33); //33
            $this->Cell(20, 6, 'Total Set : '.$this->total_qty/$this->count_set, 0, 0);
            // .'  ** PCS : '.$this->total_qty/$this->count_set
        }

        $this->SetFont('Arial','BI',8);
        
        $this->SetFont('Arial', '', '10');
        
        $this->SetFont('Arial', '', '9.5');
        $this->setXY(8, 48);
        $this->Cell(-2);
        // style + type name
        $style_type = $this->data[0]->style_edit;

        if ($this->data[0]->type_name != 'Non') {
            $style_type = $this->data[0]->style_edit.' '.$this->data[0]->type_name;
        }

        if (strlen($style_type)>10) {
            // $this->SetFont('Arial', '', '8');
            $this->Cell(21, 7.5, substr($style_type, 0, 10), 0, 0);
            $this->setXY(8, 55);
            $this->Cell(21, 7.5, substr($style_type, 10, 20), 0, 0);
            $this->setXY(30, 48);
        }else {
            $this->Cell(21, 7.5, $style_type, 0, 0);
        }
        $this->SetFont('Arial', '', '10');
        
        if (strlen($this->data[0]->po_number_edit)>10) {
            // $this->SetFont('Arial', '', '8');
            $this->Cell(21, 7.5, substr($this->data[0]->po_number_edit,0,10), 0, 0);
            $this->setXY(29, 73.5);
            $this->Cell(21, 7.5, substr($this->data[0]->po_number_edit,10,20), 0, 0);
            $this->setXY(50, 48);
        }else {
            $this->Cell(21, 7.5, $this->data[0]->po_number_edit, 0, 0);
        }
        $this->SetFont('Arial', '', '8');
        // $this->Cell(15, 7.5, $this->data[0]->article_edit, 0, 0);
        if (strlen($this->data[0]->article_edit)>7) {
            // $this->SetFont('Arial', '', '6');
            $this->Cell(17, 7.5, substr($this->data[0]->article_edit, 0, 7), 0, 0, 'C');
            $this->setXY(50, 67);
            $this->Cell(17, 7.5, substr($this->data[0]->article_edit, 7, 14), 0, 0, 'C');
        }else{
            $this->Cell(17, 7.5, $this->data[0]->article_edit, 0, 0, 'C');
        }
        $this->setXY(65, 48);
        $this->SetFont('Arial', '', '7.5');
        if (strlen($this->data[0]->color_edit)>12) {
            // $this->SetFont('Arial', '', '6');
            $this->Cell(20, 7.5, substr($this->data[0]->color_edit, 0, 12), 0, 0, 'L');
            $this->setXY(65, 55);
            $this->Cell(20, 7.5, substr($this->data[0]->color_edit, 12, 24), 0, 0, 'L');
        }else{
            $this->Cell(20, 7.5, $this->data[0]->color_edit, 0, 0, 'C');
        }
        $this->SetFont('Arial', '', '9');
        for ($i=$from; $i < $to; $i++) {
            $this->Cell(-2);
            $this->Cell(21, 7.5, '', 0, 0);
            $this->Cell(21, 7.5, '', 0, 0);
            $this->Cell(17, 7.5, '', 0, 0);
            $this->Cell(20, 7.5, '', 0, 0, 'C');
            $this->Cell(12.5, 7.5, '', 0, 0, 'C');
            $this->Cell(12.5, 7.5, '', 0, 0, 'C');
            $this->Cell(22, 7.5, '', 0, 0, 'C');
            $this->Cell(14, 7.5, '', 0, 0, 'C');
            $this->Cell(40, 7.5, '', 0, 0, 'C');
            $this->ln();
        }

    }

    public function Body()
    {
        $this->Detail();

        if(count($this->data) > 29){
            $page_count = ceil((count($this->data) - 29) /29);
            for($i = 0; $i < $page_count; $i++){
                $this->AddPage('P');
                $this->Detail(29 + ($i * 29), 29);
            }
        }
    }

    public function Detail($from = 0, $to = 29)
    {
        $this->SetFont('Arial', '', '10');

        $no = $from+1;

        $this->setXY(10, 48);

        $rows = array();
        $inc = 0;
        foreach ($this->data_arr as $val) {
            $num = 1;
            foreach($val as $row){
                $row->num = $num;
                $rows[] = $row;
                

                $inc++;
                $num++;
             }
        }
        
        // foreach ($this->data_arr as $key => $val) {
            foreach(array_slice($rows, $from, $to) as $key2 => $row){
            // $num = 1;
            // foreach($val as $key1 => $row){
            // foreach($val->slice($from, $to) as $key1 => $row){

                $style_edit = $row->style_edit;
                $po_number_edit = $row->po_number_edit;
                $article_edit = $row->article_edit;
                $color_edit = $row->color_edit;

                if ($row->num != 1) {
                    $style_edit = '';
                    $po_number_edit = '';
                    $article_edit = '';
                    $color_edit = '';
                }
            
                //$this->Cell(-2);
                // $this->Cell(25, 7, $style_edit, 0, 0);
                // $this->Cell(27, 7, $po_number_edit, 0, 0);
                // $this->Cell(20, 7, $article_edit, 0, 0);
                // $this->Cell(22, 7, $color_edit, 0, 0, 'C');
                $this->Cell(21, 7.5, '', 0, 0);
                $this->Cell(21, 7.5, '', 0, 0);
                $this->Cell(17, 7.5, '', 0, 0);
                $this->Cell(20, 7.5, '', 0, 0, 'C');
                $this->Cell(12.5, 7.5, $row->cut_number, 0, 0, 'C');
                $this->SetFont('Arial', '', '10');
                $this->Cell(12.5, 7.5, $row->size_edit, 0, 0, 'C');
                $this->SetFont('Arial', '', '9');
                $this->Cell(22, 7.5, $row->sticker_no, 0, 0, 'C');
                $this->SetFont('Arial', '', '10');
                $this->Cell(14, 7.5, $row->qty_formulir, 0, 0, 'C');
                $this->SetFont('Arial', '', '6');
                $this->Cell(40, 7.5, $row->komponen_name.'('.$row->process_name.')', 0, 0, 'L');
                $this->SetFont('Arial', '', '10');
                $this->Ln();
                
                $num++;
            //  }
        }
        
    }

    public function Footer()
    {
        // Go to 1.0 cm from bottom
        $this->SetY(-10);
        // Select Arial italic 8
        $this->SetFont('Arial','I',8);
        // Print current and total page numbers
        // $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
        // fixed all pages        
        $this->SetY(-48);
        $this->SetFont('Arial', '', '11');
        // $this->Cell(92);
        // $this->Cell(30, 6, 'Jabatan', 1, 0, 'C');
        // $this->Cell(37, 6, 'Tanda Tangan', 1, 0, 'C');
        // $this->Cell(34, 6, 'Tanggal', 1, 0, 'C');
        // $this->ln();
        $this->Cell(49);
        // $this->Cell(43, 6, 'Dibuat Oleh', 1, 0);
        // $this->Cell(30, 6, '', 1, 0, 'C');
        // $this->Cell(37, 6, '', 1, 0, 'C');
        // $this->Cell(34, 6, Carbon::parse($this->data[0]->formulir_created)->format('d/m/Y'), 0, 0, 'C');
        // $this->ln();
        // $this->Cell(49);
        // $this->Cell(43, 6, 'Diperiksa Oleh', 1, 0);
        // $this->Cell(30, 6, '', 1, 0, 'C');
        // $this->Cell(37, 6, '', 1, 0, 'C');
        // $this->Cell(34, 6, '', 1, 0, 'C');
        // $this->ln();
        // $this->Cell(49);
        // $this->Cell(43, 6, 'Diterima Oleh', 1, 0);
        // $this->Cell(30, 6, '', 1, 0, 'C');
        // $this->Cell(37, 6, '', 1, 0, 'C');
        // $this->Cell(34, 6, '', 1, 0, 'C');

        // $this->SetY(-10);
        // $this->SetFont('Arial', 'B', '10');
        
        // $this->Cell(193, 6, 'FR.03-01-DIS/00', 0, 0, 'R');
    }
}
