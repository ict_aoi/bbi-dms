<?php

namespace App\Pdf;

use TJGazel\LaraFpdf\LaraFpdf;
use \Milon\Barcode\DNS1D;

class Barcode extends LaraFpdf
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
        $this->DNS1D = new DNS1D();
        //$this->DNS1D->setStorPath(__DIR__."/cache/");
        parent::__construct('L','mm', array(297, 420)); //a3
        // $this->SetA4();
        $this->SetTitle('Cetak Barcode', true);
        $this->SetAuthor('TJGazel', true);
        $this->AddPage();
        $this->AliasNbPages();
        $this->Body();
    }

    public function Header()
    {
        // fixed all pages
        // $this->SetLineWidth(0.5); //default 0.2 mm
        // $this->Rect(5, 5, 148, 200);

        // $this->Line(8, 37, 81, 37);
        // $this->Line(8, 37.5, 81, 37.5);

        // $this->Rect(94, 32, 6, 6); //kotak
        // $this->Rect(120, 32, 6, 6);

    }

    public function Body()
    {
        $this->Detail();

        if(count($this->data) > 14){
            $page_count = ceil((count($this->data) - 15) / 15);
            for($i = 0; $i < $page_count; $i++){
                $this->AddPage('L');
                $this->Detail(15 + ($i * 15), 15);
            }
        }
    }

    public function Detail($from = 0, $to = 4)
    {
         $this->SetFont('Arial', '', '11');
         $this->Cell(62, 6, $this->DNS1D->getBarcodeHTML('2132131', 'C128'), 1, 0, 'C');
         //\Storage::disk('public')->put('test.png',base64_decode(DNS2D::getBarcodePNG("4", "PDF417")));


         $no = $from+1;

        //  for ($i=0; $i < $to; $i++) { 
        //     $this->Cell(-3);
        //     $this->Cell(12, 6, $no, 1, 0, 'C');
        //     $this->Cell(62, 6, '', 1);
        //     $this->Cell(20, 6, '', 1, 0, 'C');
        //     $this->Cell(20, 6, '', 1, 0, 'R');
        //     $this->Cell(30, 6, '', 1);
        //     $this->Ln();

        //     $no++;
        //  }
        //  $this->SetXY(10,78);
        //  foreach($this->data->slice($from, $to) as $row){
        //     $this->Cell(-3);
        //     $this->Cell(30, 6, "", 0);
        //     $this->Ln();  
        //  }
        
    }

    public function Footer()
    {
        // fixed all pages
        // $this->SetY(-39);
        // $this->SetFont('Arial', '', '10');
        
        // $this->Cell(71);
        // $this->Cell(70, 6, 'Semarang, '. \Carbon\Carbon::now()->format('d/m/Y'), 1, 0, 'C');
        // $this->Ln();

        // $this->Cell(71);
        // $this->Cell(35, 6, 'diterima oleh', 1, 0, 'C');
        // $this->Cell(35, 6, 'dikeluarkan oleh', 1, 0, 'C');

        // $this->Ln(20);
        // $this->Cell(71);
        // $this->Cell(35, 6, '( ............................ )', 'B', 0, 'C');
        // $this->Cell(35, 6, '( ............................ )', 'B', 0, 'C');

        // $this->Line(81, 196, 151, 196);

        // $this->Line(81, 183, 81, 203);
        // $this->Line(116, 183, 116, 203);
        // $this->Line(151, 183, 151, 203);
    }
}
