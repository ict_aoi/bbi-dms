<?php

namespace App\Pdf;

use TJGazel\LaraFpdf\LaraFpdf;
use Carbon\Carbon;

class MonitoringDistribusi extends LaraFpdf
{
    //
    private $data;
    private $factories;
    private $radio_status;
    private $data_arr;
    private $style;
    private $color;
    private $size_1;
    private $size_2;
    private $size_3;
    private $size_4;
    private $size_5;
    private $size_6;
    private $size_7;
    private $size_8;
    private $size_9;
    private $po_number;
    private $process_name;

    public function __construct($data, $data_arr, $factories, $radio_status, $style, $color, $size_1, $size_2, $size_3, $size_4, $size_5, $size_6, $size_7, $size_8, $size_9, $po_number, $process_name)
    {
        $this->data = $data;
        $this->factories = $factories;
        $this->radio_status = $radio_status;
        $this->data_arr = $data_arr;
        $this->style = $style;
        $this->color = $color;
        $this->size_1 = $size_1;
        $this->size_2 = $size_2;
        $this->size_3 = $size_3;
        $this->size_4 = $size_4;
        $this->size_5 = $size_5;
        $this->size_6 = $size_6;
        $this->size_7 = $size_7;
        $this->size_8 = $size_8;
        $this->size_9 = $size_9;
        $this->po_number = $po_number;
        $this->process_name = $process_name;
        parent::__construct('P','mm', array(210, 297)); //A4 aslinya 210x297
        $this->SetTitle('Formulir Monitoring Distribusi', true);
        $this->SetAuthor('TJGazel', true);
        $this->SetAutoPageBreak(false);
        $this->AddPage();
        $this->AliasNbPages();
        $this->Body();
    }

    public function Header($from=0, $to=38)
    {
        // fixed all pages
        $this->SetLineWidth(0.5); //default 0.2 mm
        $this->Rect(8, 8, 195, 278);
        
        $this->Image(public_path().'/images/'.$this->factories->logo_factory, 20, 8, -500);
        
        $this->SetFont('Arial', 'B', '14');
        
        $this->setXY(70, 8);
        $this->Cell(133, 8, 'FORMULIR', 1, 0, 'C');
        $this->setXY(70, 16);
        $this->SetFont('Arial', '', '11');
        $this->Cell(133, 8, 'MONITORING DISTRIBUSI', 1, 0, 'C');
        
        $this->Line(8, 24, 203, 24);
        // $this->Line(110, 8, 110, 32);
        
        $this->SetFont('Arial','B',9);

        $this->setXY(10, 45); 
        $this->Cell(20, 6, $this->process_name, 0, 0);
        
        $this->SetFont('Arial','',8);

        $this->setXY(10, 24);
        $this->Cell(26, 6, 'NO ORDER ', 0, 0);
        $this->Cell(5, 6, ':', 0, 0);
        $this->Cell(5, 6, $this->po_number, 0, 0);

        $this->ln();
        $this->Cell(26, 6, 'STYLE ', 0, 0);
        $this->Cell(5, 6, ':', 0, 0);
        $this->Cell(5, 6, $this->style, 0, 0);
        
        $this->ln();
        $this->Cell(26, 6, 'COLOUR ', 0, 0);
        $this->Cell(5, 6, ':', 0, 0);
        $this->Cell(5, 6, $this->color, 0, 0);
        
        $this->setXY(120, 24);
        $this->Cell(26, 6, 'DELIVERY ', 0, 0);
        $this->Cell(5, 6, ':', 0, 0);
        $this->Cell(5, 6, '', 0, 0);

        $this->ln();
        $this->Cell(110);
        $this->Cell(26, 6, 'QTY ', 0, 0);
        $this->Cell(5, 6, ':', 0, 0);
        $this->Cell(5, 6, '', 0, 0);
        
        $this->ln();
        $this->Cell(110);
        $this->Cell(26, 6, 'COUNTRY ', 0, 0);
        $this->Cell(5, 6, ':', 0, 0);
        $this->Cell(5, 6, '', 0, 0);
        
        $this->SetLineWidth(0.2); //default 0.2 mm
        
        $this->Line(42, 30, 80, 30);
        $this->Line(42, 36, 80, 36);
        $this->Line(42, 42, 80, 42);
        
        $this->Line(152, 30, 190, 30);
        $this->Line(152, 36, 190, 36);
        $this->Line(152, 42, 190, 42);
        
        // Print right page number
        // $this->ln();
        // $this->Cell(0,10,'Page '.$this->PageNo()."/{nb}",0,0,'R');
        
        $this->SetFont('Arial', '', '8');
        $this->setXY(8, 50);
        $this->Cell(20, 8, 'TANGGAL', 1, 0, 'C');
        $this->Cell(10, 8, 'CUTT', 1, 0, 'C');
        $this->Cell(15, 4, $this->size_1, 1, 0, 'C');
        $this->Cell(15, 4, $this->size_2, 1, 0, 'C');
        $this->Cell(15, 4, $this->size_3, 1, 0, 'C');
        $this->Cell(15, 4, $this->size_4, 1, 0, 'C');
        $this->Cell(15, 4, $this->size_5, 1, 0, 'C');
        $this->Cell(15, 4, $this->size_6, 1, 0, 'C');
        $this->Cell(15, 4, $this->size_7, 1, 0, 'C');
        $this->Cell(15, 4, $this->size_8, 1, 0, 'C');
        $this->Cell(15, 4, $this->size_9, 1, 0, 'C');
        $this->Cell(15, 4, 'TOTAL', 1, 0, 'C');
        $this->MultiCell(15, 4, 'No Sticker', 1, 'C');

        $this->setXY(38, 54);
        $this->Cell(15, 4, '', 1, 0, 'C');
        $this->Cell(15, 4, '', 1, 0, 'C');
        $this->Cell(15, 4, '', 1, 0, 'C');
        $this->Cell(15, 4, '', 1, 0, 'C');
        $this->Cell(15, 4, '', 1, 0, 'C');
        $this->Cell(15, 4, '', 1, 0, 'C');
        $this->Cell(15, 4, '', 1, 0, 'C');
        $this->Cell(15, 4, '', 1, 0, 'C');
        $this->Cell(15, 4, '', 1, 0, 'C');
        $this->Cell(15, 4, '', 1, 0, 'C');
        
        $this->SetFont('Arial', '', '9');
        $this->setXY(10, 58);
        for ($i=$from; $i < $to; $i++) {
            $this->Cell(-2);
            $this->Cell(20, 6, '', 1, 0);
            $this->Cell(10, 6, '', 1, 0);
            $this->Cell(15, 6, '', 1, 0, 'C');
            $this->Cell(15, 6, '', 1, 0, 'C');
            $this->Cell(15, 6, '', 1, 0, 'C');
            $this->Cell(15, 6, '', 1, 0, 'C');
            $this->Cell(15, 6, '', 1, 0, 'C');
            $this->Cell(15, 6, '', 1, 0, 'C');
            $this->Cell(15, 6, '', 1, 0, 'C');
            $this->Cell(15, 6, '', 1, 0, 'C');
            $this->Cell(15, 6, '', 1, 0, 'C');
            $this->Cell(15, 6, '', 1, 0, 'C');
            $this->Cell(15, 6, '', 1, 0, 'C');
            $this->ln();
        }

        // garis miring
        $this->Line(38, 64, 53, 58);
        $this->Line(38, 70, 53, 64);
        $this->Line(38, 76, 53, 70);
        $this->Line(38, 82, 53, 76);
        $this->Line(38, 88, 53, 82);
        $this->Line(38, 94, 53, 88);
        $this->Line(38, 100, 53, 94);
        $this->Line(38, 106, 53, 100);
        $this->Line(38, 112, 53, 106);
        $this->Line(38, 118, 53, 112);
        $this->Line(38, 124, 53, 118);
        $this->Line(38, 130, 53, 124);
        $this->Line(38, 136, 53, 130);
        $this->Line(38, 142, 53, 136);
        $this->Line(38, 148, 53, 142);
        $this->Line(38, 154, 53, 148);
        $this->Line(38, 160, 53, 154);
        $this->Line(38, 166, 53, 160);
        $this->Line(38, 172, 53, 166);
        $this->Line(38, 178, 53, 172);
        $this->Line(38, 184, 53, 178);
        $this->Line(38, 190, 53, 184);
        $this->Line(38, 196, 53, 190);
        $this->Line(38, 202, 53, 196);
        $this->Line(38, 208, 53, 202);
        $this->Line(38, 214, 53, 208);
        $this->Line(38, 220, 53, 214);
        $this->Line(38, 226, 53, 220);
        $this->Line(38, 232, 53, 226);
        $this->Line(38, 238, 53, 232);
        $this->Line(38, 244, 53, 238);
        $this->Line(38, 250, 53, 244);
        $this->Line(38, 256, 53, 250);
        $this->Line(38, 262, 53, 256);
        $this->Line(38, 268, 53, 262);
        $this->Line(38, 274, 53, 268);
        $this->Line(38, 280, 53, 274);
        $this->Line(38, 286, 53, 280);

        // kolom ke 2
        $this->Line(53, 64, 68, 58);
        $this->Line(53, 70, 68, 64);
        $this->Line(53, 76, 68, 70);
        $this->Line(53, 82, 68, 76);
        $this->Line(53, 88, 68, 82);
        $this->Line(53, 94, 68, 88);
        $this->Line(53, 100, 68, 94);
        $this->Line(53, 106, 68, 100);
        $this->Line(53, 112, 68, 106);
        $this->Line(53, 118, 68, 112);
        $this->Line(53, 124, 68, 118);
        $this->Line(53, 130, 68, 124);
        $this->Line(53, 136, 68, 130);
        $this->Line(53, 142, 68, 136);
        $this->Line(53, 148, 68, 142);
        $this->Line(53, 154, 68, 148);
        $this->Line(53, 160, 68, 154);
        $this->Line(53, 166, 68, 160);
        $this->Line(53, 172, 68, 166);
        $this->Line(53, 178, 68, 172);
        $this->Line(53, 184, 68, 178);
        $this->Line(53, 190, 68, 184);
        $this->Line(53, 196, 68, 190);
        $this->Line(53, 202, 68, 196);
        $this->Line(53, 208, 68, 202);
        $this->Line(53, 214, 68, 208);
        $this->Line(53, 220, 68, 214);
        $this->Line(53, 226, 68, 220);
        $this->Line(53, 232, 68, 226);
        $this->Line(53, 238, 68, 232);
        $this->Line(53, 244, 68, 238);
        $this->Line(53, 250, 68, 244);
        $this->Line(53, 256, 68, 250);
        $this->Line(53, 262, 68, 256);
        $this->Line(53, 268, 68, 262);
        $this->Line(53, 274, 68, 268);
        $this->Line(53, 280, 68, 274);
        $this->Line(53, 286, 68, 280);

        // kolom ke 3
        $this->Line(68, 64, 83, 58);
        $this->Line(68, 70, 83, 64);
        $this->Line(68, 76, 83, 70);
        $this->Line(68, 82, 83, 76);
        $this->Line(68, 88, 83, 82);
        $this->Line(68, 94, 83, 88);
        $this->Line(68, 100, 83, 94);
        $this->Line(68, 106, 83, 100);
        $this->Line(68, 112, 83, 106);
        $this->Line(68, 118, 83, 112);
        $this->Line(68, 124, 83, 118);
        $this->Line(68, 130, 83, 124);
        $this->Line(68, 136, 83, 130);
        $this->Line(68, 142, 83, 136);
        $this->Line(68, 148, 83, 142);
        $this->Line(68, 154, 83, 148);
        $this->Line(68, 160, 83, 154);
        $this->Line(68, 166, 83, 160);
        $this->Line(68, 172, 83, 166);
        $this->Line(68, 178, 83, 172);
        $this->Line(68, 184, 83, 178);
        $this->Line(68, 190, 83, 184);
        $this->Line(68, 196, 83, 190);
        $this->Line(68, 202, 83, 196);
        $this->Line(68, 208, 83, 202);
        $this->Line(68, 214, 83, 208);
        $this->Line(68, 220, 83, 214);
        $this->Line(68, 226, 83, 220);
        $this->Line(68, 232, 83, 226);
        $this->Line(68, 238, 83, 232);
        $this->Line(68, 244, 83, 238);
        $this->Line(68, 250, 83, 244);
        $this->Line(68, 256, 83, 250);
        $this->Line(68, 262, 83, 256);
        $this->Line(68, 268, 83, 262);
        $this->Line(68, 274, 83, 268);
        $this->Line(68, 280, 83, 274);
        $this->Line(68, 286, 83, 280);
        
        // kolom ke 4
        $this->Line(83, 64, 98, 58);
        $this->Line(83, 70, 98, 64);
        $this->Line(83, 76, 98, 70);
        $this->Line(83, 82, 98, 76);
        $this->Line(83, 88, 98, 82);
        $this->Line(83, 94, 98, 88);
        $this->Line(83, 100, 98, 94);
        $this->Line(83, 106, 98, 100);
        $this->Line(83, 112, 98, 106);
        $this->Line(83, 118, 98, 112);
        $this->Line(83, 124, 98, 118);
        $this->Line(83, 130, 98, 124);
        $this->Line(83, 136, 98, 130);
        $this->Line(83, 142, 98, 136);
        $this->Line(83, 148, 98, 142);
        $this->Line(83, 154, 98, 148);
        $this->Line(83, 160, 98, 154);
        $this->Line(83, 166, 98, 160);
        $this->Line(83, 172, 98, 166);
        $this->Line(83, 178, 98, 172);
        $this->Line(83, 184, 98, 178);
        $this->Line(83, 190, 98, 184);
        $this->Line(83, 196, 98, 190);
        $this->Line(83, 202, 98, 196);
        $this->Line(83, 208, 98, 202);
        $this->Line(83, 214, 98, 208);
        $this->Line(83, 220, 98, 214);
        $this->Line(83, 226, 98, 220);
        $this->Line(83, 232, 98, 226);
        $this->Line(83, 238, 98, 232);
        $this->Line(83, 244, 98, 238);
        $this->Line(83, 250, 98, 244);
        $this->Line(83, 256, 98, 250);
        $this->Line(83, 262, 98, 256);
        $this->Line(83, 268, 98, 262);
        $this->Line(83, 274, 98, 268);
        $this->Line(83, 280, 98, 274);
        $this->Line(83, 286, 98, 280);
        
        // kolom ke 5
        $this->Line(98, 64, 113, 58);
        $this->Line(98, 70, 113, 64);
        $this->Line(98, 76, 113, 70);
        $this->Line(98, 82, 113, 76);
        $this->Line(98, 88, 113, 82);
        $this->Line(98, 94, 113, 88);
        $this->Line(98, 100, 113, 94);
        $this->Line(98, 106, 113, 100);
        $this->Line(98, 112, 113, 106);
        $this->Line(98, 118, 113, 112);
        $this->Line(98, 124, 113, 118);
        $this->Line(98, 130, 113, 124);
        $this->Line(98, 136, 113, 130);
        $this->Line(98, 142, 113, 136);
        $this->Line(98, 148, 113, 142);
        $this->Line(98, 154, 113, 148);
        $this->Line(98, 160, 113, 154);
        $this->Line(98, 166, 113, 160);
        $this->Line(98, 172, 113, 166);
        $this->Line(98, 178, 113, 172);
        $this->Line(98, 184, 113, 178);
        $this->Line(98, 190, 113, 184);
        $this->Line(98, 196, 113, 190);
        $this->Line(98, 202, 113, 196);
        $this->Line(98, 208, 113, 202);
        $this->Line(98, 214, 113, 208);
        $this->Line(98, 220, 113, 214);
        $this->Line(98, 226, 113, 220);
        $this->Line(98, 232, 113, 226);
        $this->Line(98, 238, 113, 232);
        $this->Line(98, 244, 113, 238);
        $this->Line(98, 250, 113, 244);
        $this->Line(98, 256, 113, 250);
        $this->Line(98, 262, 113, 256);
        $this->Line(98, 268, 113, 262);
        $this->Line(98, 274, 113, 268);
        $this->Line(98, 280, 113, 274);
        $this->Line(98, 286, 113, 280);
        
        // kolom ke 6
        $this->Line(113, 64, 128, 58);
        $this->Line(113, 70, 128, 64);
        $this->Line(113, 76, 128, 70);
        $this->Line(113, 82, 128, 76);
        $this->Line(113, 88, 128, 82);
        $this->Line(113, 94, 128, 88);
        $this->Line(113, 100, 128, 94);
        $this->Line(113, 106, 128, 100);
        $this->Line(113, 112, 128, 106);
        $this->Line(113, 118, 128, 112);
        $this->Line(113, 124, 128, 118);
        $this->Line(113, 130, 128, 124);
        $this->Line(113, 136, 128, 130);
        $this->Line(113, 142, 128, 136);
        $this->Line(113, 148, 128, 142);
        $this->Line(113, 154, 128, 148);
        $this->Line(113, 160, 128, 154);
        $this->Line(113, 166, 128, 160);
        $this->Line(113, 172, 128, 166);
        $this->Line(113, 178, 128, 172);
        $this->Line(113, 184, 128, 178);
        $this->Line(113, 190, 128, 184);
        $this->Line(113, 196, 128, 190);
        $this->Line(113, 202, 128, 196);
        $this->Line(113, 208, 128, 202);
        $this->Line(113, 214, 128, 208);
        $this->Line(113, 220, 128, 214);
        $this->Line(113, 226, 128, 220);
        $this->Line(113, 232, 128, 226);
        $this->Line(113, 238, 128, 232);
        $this->Line(113, 244, 128, 238);
        $this->Line(113, 250, 128, 244);
        $this->Line(113, 256, 128, 250);
        $this->Line(113, 262, 128, 256);
        $this->Line(113, 268, 128, 262);
        $this->Line(113, 274, 128, 268);
        $this->Line(113, 280, 128, 274);
        $this->Line(113, 286, 128, 280);
        
        // kolom ke 7
        $this->Line(128, 64, 143, 58);
        $this->Line(128, 70, 143, 64);
        $this->Line(128, 76, 143, 70);
        $this->Line(128, 82, 143, 76);
        $this->Line(128, 88, 143, 82);
        $this->Line(128, 94, 143, 88);
        $this->Line(128, 100, 143, 94);
        $this->Line(128, 106, 143, 100);
        $this->Line(128, 112, 143, 106);
        $this->Line(128, 118, 143, 112);
        $this->Line(128, 124, 143, 118);
        $this->Line(128, 130, 143, 124);
        $this->Line(128, 136, 143, 130);
        $this->Line(128, 142, 143, 136);
        $this->Line(128, 148, 143, 142);
        $this->Line(128, 154, 143, 148);
        $this->Line(128, 160, 143, 154);
        $this->Line(128, 166, 143, 160);
        $this->Line(128, 172, 143, 166);
        $this->Line(128, 178, 143, 172);
        $this->Line(128, 184, 143, 178);
        $this->Line(128, 190, 143, 184);
        $this->Line(128, 196, 143, 190);
        $this->Line(128, 202, 143, 196);
        $this->Line(128, 208, 143, 202);
        $this->Line(128, 214, 143, 208);
        $this->Line(128, 220, 143, 214);
        $this->Line(128, 226, 143, 220);
        $this->Line(128, 232, 143, 226);
        $this->Line(128, 238, 143, 232);
        $this->Line(128, 244, 143, 238);
        $this->Line(128, 250, 143, 244);
        $this->Line(128, 256, 143, 250);
        $this->Line(128, 262, 143, 256);
        $this->Line(128, 268, 143, 262);
        $this->Line(128, 274, 143, 268);
        $this->Line(128, 280, 143, 274);
        $this->Line(128, 286, 143, 280);
        
        // kolom ke 8
        $this->Line(143, 64, 158, 58);
        $this->Line(143, 70, 158, 64);
        $this->Line(143, 76, 158, 70);
        $this->Line(143, 82, 158, 76);
        $this->Line(143, 88, 158, 82);
        $this->Line(143, 94, 158, 88);
        $this->Line(143, 100, 158, 94);
        $this->Line(143, 106, 158, 100);
        $this->Line(143, 112, 158, 106);
        $this->Line(143, 118, 158, 112);
        $this->Line(143, 124, 158, 118);
        $this->Line(143, 130, 158, 124);
        $this->Line(143, 136, 158, 130);
        $this->Line(143, 142, 158, 136);
        $this->Line(143, 148, 158, 142);
        $this->Line(143, 154, 158, 148);
        $this->Line(143, 160, 158, 154);
        $this->Line(143, 166, 158, 160);
        $this->Line(143, 172, 158, 166);
        $this->Line(143, 178, 158, 172);
        $this->Line(143, 184, 158, 178);
        $this->Line(143, 190, 158, 184);
        $this->Line(143, 196, 158, 190);
        $this->Line(143, 202, 158, 196);
        $this->Line(143, 208, 158, 202);
        $this->Line(143, 214, 158, 208);
        $this->Line(143, 220, 158, 214);
        $this->Line(143, 226, 158, 220);
        $this->Line(143, 232, 158, 226);
        $this->Line(143, 238, 158, 232);
        $this->Line(143, 244, 158, 238);
        $this->Line(143, 250, 158, 244);
        $this->Line(143, 256, 158, 250);
        $this->Line(143, 262, 158, 256);
        $this->Line(143, 268, 158, 262);
        $this->Line(143, 274, 158, 268);
        $this->Line(143, 280, 158, 274);
        $this->Line(143, 286, 158, 280);
        
        // kolom ke 8
        $this->Line(158, 64, 173, 58);
        $this->Line(158, 70, 173, 64);
        $this->Line(158, 76, 173, 70);
        $this->Line(158, 82, 173, 76);
        $this->Line(158, 88, 173, 82);
        $this->Line(158, 94, 173, 88);
        $this->Line(158, 100, 173, 94);
        $this->Line(158, 106, 173, 100);
        $this->Line(158, 112, 173, 106);
        $this->Line(158, 118, 173, 112);
        $this->Line(158, 124, 173, 118);
        $this->Line(158, 130, 173, 124);
        $this->Line(158, 136, 173, 130);
        $this->Line(158, 142, 173, 136);
        $this->Line(158, 148, 173, 142);
        $this->Line(158, 154, 173, 148);
        $this->Line(158, 160, 173, 154);
        $this->Line(158, 166, 173, 160);
        $this->Line(158, 172, 173, 166);
        $this->Line(158, 178, 173, 172);
        $this->Line(158, 184, 173, 178);
        $this->Line(158, 190, 173, 184);
        $this->Line(158, 196, 173, 190);
        $this->Line(158, 202, 173, 196);
        $this->Line(158, 208, 173, 202);
        $this->Line(158, 214, 173, 208);
        $this->Line(158, 220, 173, 214);
        $this->Line(158, 226, 173, 220);
        $this->Line(158, 232, 173, 226);
        $this->Line(158, 238, 173, 232);
        $this->Line(158, 244, 173, 238);
        $this->Line(158, 250, 173, 244);
        $this->Line(158, 256, 173, 250);
        $this->Line(158, 262, 173, 256);
        $this->Line(158, 268, 173, 262);
        $this->Line(158, 274, 173, 268);
        $this->Line(158, 280, 173, 274);
        $this->Line(158, 286, 173, 280);

        // kolom ke 9
        $this->Line(173, 64, 188, 58);
        $this->Line(173, 70, 188, 64);
        $this->Line(173, 76, 188, 70);
        $this->Line(173, 82, 188, 76);
        $this->Line(173, 88, 188, 82);
        $this->Line(173, 94, 188, 88);
        $this->Line(173, 100, 188, 94);
        $this->Line(173, 106, 188, 100);
        $this->Line(173, 112, 188, 106);
        $this->Line(173, 118, 188, 112);
        $this->Line(173, 124, 188, 118);
        $this->Line(173, 130, 188, 124);
        $this->Line(173, 136, 188, 130);
        $this->Line(173, 142, 188, 136);
        $this->Line(173, 148, 188, 142);
        $this->Line(173, 154, 188, 148);
        $this->Line(173, 160, 188, 154);
        $this->Line(173, 166, 188, 160);
        $this->Line(173, 172, 188, 166);
        $this->Line(173, 178, 188, 172);
        $this->Line(173, 184, 188, 178);
        $this->Line(173, 190, 188, 184);
        $this->Line(173, 196, 188, 190);
        $this->Line(173, 202, 188, 196);
        $this->Line(173, 208, 188, 202);
        $this->Line(173, 214, 188, 208);
        $this->Line(173, 220, 188, 214);
        $this->Line(173, 226, 188, 220);
        $this->Line(173, 232, 188, 226);
        $this->Line(173, 238, 188, 232);
        $this->Line(173, 244, 188, 238);
        $this->Line(173, 250, 188, 244);
        $this->Line(173, 256, 188, 250);
        $this->Line(173, 262, 188, 256);
        $this->Line(173, 268, 188, 262);
        $this->Line(173, 274, 188, 268);
        $this->Line(173, 280, 188, 274);
        $this->Line(173, 286, 188, 280);


    }

    public function Body()
    {
        $this->Detail();
        // $this->AddPage('P');
        // $this->Cell(20, 6, count($this->data), 1, 0);

        // $this->SetAutoPageBreak(true);
        if(count($this->data) > 38){
            $page_count = ceil((count($this->data) - 38) /38);
            for($i = 0; $i < $page_count; $i++){
                $this->AddPage('P');
                $this->Detail(38 + ($i * 38), 38);
            }
        }
    }

    public function Detail($from = 0, $to = 38)
    {
        $this->SetFont('Arial', '', '9');

        $no = $from+1;
        $this->setXY(10, 58);

        $rows = array();
        $inc = 0;
        foreach ($this->data_arr as $val) {
            $num = 1;
            foreach($val as $row){
                $row->num = $num;
                $rows[] = $row;
                

                $inc++;
                $num++;
             }
        }

        foreach(array_slice($rows, $from, $to) as $key2 => $row){
                    
            if ($this->radio_status == 'out') {
                $tanggal = Carbon::parse($row->created_at)->format('d/m/Y');
            }elseif ($this->radio_status == 'in') {
                $tanggal = Carbon::parse($row->scanin_date)->format('d/m/Y');
            }else{
                $tanggal = Carbon::parse($row->created_at)->format('d/m/Y');
            }

            $cut_number = $row->cut_number;
            
            if ($row->num != 1) {
                $tanggal = '';
                // $cut_number = '';
            }

            $t1 = isset($row->{$this->size_1}) ? $row->{$this->size_1} : 0;
            $t2 = isset($row->{$this->size_2}) ? $row->{$this->size_2} : 0;
            $t3 = isset($row->{$this->size_3}) ? $row->{$this->size_3} : 0;
            $t4 = isset($row->{$this->size_4}) ? $row->{$this->size_4} : 0;
            $t5 = isset($row->{$this->size_5}) ? $row->{$this->size_5} : 0;
            $t6 = isset($row->{$this->size_6}) ? $row->{$this->size_6} : 0;
            $t7 = isset($row->{$this->size_7}) ? $row->{$this->size_7} : 0;
            $t8 = isset($row->{$this->size_8}) ? $row->{$this->size_8} : 0;
            $t9 = isset($row->{$this->size_9}) ? $row->{$this->size_9} : 0;

            $tot = $t1+$t2+$t3+$t4+$t5+$t6+$t7+$t8+$t9;
        
            $this->Cell(-2);
            $this->Cell(20, 6, $tanggal, 1, 0);
            $this->Cell(10, 6, $cut_number, 1, 0, 'C');
            $this->Cell(15, 6, isset($row->{$this->size_1}) && $row->{$this->size_1} > 0 ? $row->{$this->size_1} : '', 1, 0, 'L');
            $this->Cell(15, 6, isset($row->{$this->size_2}) && $row->{$this->size_2} > 0 ? $row->{$this->size_2} : '', 1, 0, 'L');
            $this->Cell(15, 6, isset($row->{$this->size_3}) && $row->{$this->size_3} > 0 ? $row->{$this->size_3} : '', 1, 0, 'L');
            $this->Cell(15, 6, isset($row->{$this->size_4}) && $row->{$this->size_4} > 0 ? $row->{$this->size_4} : '', 1, 0, 'L');
            $this->Cell(15, 6, isset($row->{$this->size_5}) && $row->{$this->size_5} > 0 ? $row->{$this->size_5} : '', 1, 0, 'L');
            $this->Cell(15, 6, isset($row->{$this->size_6}) && $row->{$this->size_6} > 0 ? $row->{$this->size_6} : '', 1, 0, 'L');
            $this->Cell(15, 6, isset($row->{$this->size_7}) && $row->{$this->size_7} > 0 ? $row->{$this->size_7} : '', 1, 0, 'L');
            $this->Cell(15, 6, isset($row->{$this->size_8}) && $row->{$this->size_8} > 0 ? $row->{$this->size_8} : '', 1, 0, 'L');
            $this->Cell(15, 6, isset($row->{$this->size_9}) && $row->{$this->size_9} > 0 ? $row->{$this->size_9} : '', 1, 0, 'L');
            $this->Cell(15, 6, '', 1, 0, 'L');
            $this->Cell(15, 6, $row->sticker_no, 1, 0, 'C');
            $this->ln();
        }
        
    }

    public function Footer()
    {
        // Go to 1.0 cm from bottom
        $this->SetY(-10);
        // Select Arial italic 8
        $this->SetFont('Arial','I',8);
        // Print current and total page numbers
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');

        // fixed all pages        
        // $this->SetY(-40);
        // $this->SetFont('Arial', '', '11');
        // $this->Cell(92);
        // $this->Cell(30, 6, 'Jabatan', 1, 0, 'C');
        // $this->Cell(37, 6, 'Tanda Tangan', 1, 0, 'C');
        // $this->Cell(34, 6, 'Tanggal', 1, 0, 'C');
        // $this->ln();
        // $this->Cell(49);
        // $this->Cell(43, 6, 'Dibuat Oleh', 1, 0);
        // $this->Cell(30, 6, '', 1, 0, 'C');
        // $this->Cell(37, 6, '', 1, 0, 'C');
        // $this->Cell(34, 6, Carbon::parse($this->data[0]->formulir_created)->format('d/m/Y'), 1, 0, 'C');
        // $this->ln();
        // $this->Cell(49);
        // $this->Cell(43, 6, 'Diperiksa Oleh', 1, 0);
        // $this->Cell(30, 6, '', 1, 0, 'C');
        // $this->Cell(37, 6, '', 1, 0, 'C');
        // $this->Cell(34, 6, '', 1, 0, 'C');
        // $this->ln();
        // $this->Cell(49);
        // $this->Cell(43, 6, 'Diterima Oleh', 1, 0);
        // $this->Cell(30, 6, '', 1, 0, 'C');
        // $this->Cell(37, 6, '', 1, 0, 'C');
        // $this->Cell(34, 6, '', 1, 0, 'C');

        // $this->SetY(-10);
        // $this->SetFont('Arial', 'B', '10');
        
        // $this->Cell(193, 6, 'FR.03-01-DIS/00', 0, 0, 'R');
    }
}
