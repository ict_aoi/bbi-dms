<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Http\Controllers\Integration\IntegrationController;

use App\Models\Scheduler;
use Carbon\Carbon;

class DailyUpdateBreakdownSize extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dailyUpdateBreakdownSize:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Daily Breakdown Size';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $schedular = Scheduler::where('job', 'SYNC_DAILY_BREAKDOWN_SIZE_UPDATE')
                            ->where('status', 'queue')
                            ->first();

        if (!empty($schedular)) {
            $this->info('SYNC DAILY BREAKDOWN SIZE UPDATE AT '.carbon::now());
            $this->setStatus($schedular, 'ongoing');
            $this->setStartJob($schedular);
            IntegrationController::BreakdownSizeUpdate();
            $this->setStatus($schedular, 'done');
            $this->setEndJob($schedular);
            $this->info('DONE SYNC DAILY BREAKDOWN SIZE UPDATE AT '.carbon::now());
        }else {
            $is_schedule_on_going = Scheduler::where('job', 'SYNC_DAILY_BREAKDOWN_SIZE_UPDATE')
                                            ->where('status', 'ongoing')
                                            ->exists();

            $data_schedule_on_going = Scheduler::where('job', 'SYNC_DAILY_BREAKDOWN_SIZE_UPDATE')
                                            ->where('status', 'ongoing')
                                            ->first();

            if (!$is_schedule_on_going) {
                $this->syncJob();
            } else {
                $date_on_going = Carbon::parse($data_schedule_on_going->created_at)->format('Y-m-d');
                $now = Carbon::parse(Carbon::now()->format('Y-m-d'));

                $length = $now->diffInDays($date_on_going);
                if ($length>0) {
                    Scheduler::where('job', 'SYNC_DAILY_BREAKDOWN_SIZE_UPDATE')
                                            ->where('status', 'ongoing')
                                            ->update([
                                                'status' => 'onrestart'
                                            ]);
                    $this->handle();
                }else {
                    $this->info('SYNC DAILY BREAKDOWN SIZE UPDATE SEDANG BERJALAN');
                }

            }
            
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }

    private function syncJob()
    {
        $new_scheduler = Scheduler::create([
            'job' => 'SYNC_DAILY_BREAKDOWN_SIZE_UPDATE',
            'status' => 'ongoing'
        ]);
        $this->info('SYNC DAILY BREAKDOWN SIZE UPDATE JOB AT'.Carbon::now());
        $this->setStartJob($new_scheduler);
        IntegrationController::BreakdownSizeUpdate();
        $this->setStatus($new_scheduler, 'done');
        $this->setEndJob($new_scheduler);
        $this->info('DONE SYNC DAILY BREAKDOWN SIZE UPDATE AT '.carbon::now());
    }
}
