<?php

namespace App\Http\Controllers\Packing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Auth;
use Carbon\Carbon;
use DataTables;

use App\Models\Factories;

use App\Pdf\SuratJalan2;
use App\Pdf\SuratJalan2Global;
use App\Pdf\FormulirArtwork2;
use App\Pdf\FormulirArtwork2Reject;

class PackingListController extends Controller
{
    // surat jalan
    public function index()
    {
        $list_style = DB::table('po_summary')
                    ->select('style')
                    ->whereNull('deleted_at')
                    ->groupBy('style')
                    ->get()
                    ->toArray();

        $subcont = DB::table('subcont')
                        ->wherenull('deleted_at')
                        ->get();

        return view('packing.surat_jalan.index')->with([
                                                        'subcont'=>$subcont,
                                                        'list_style'=>$list_style
                                                        ]);

    }

    // surat jalan global
    public function suratJalanGlobal()
    {
        $list_style = DB::table('breakdown_size')
                    ->select('style')
                    // ->whereNull('deleted_at')
                    ->groupBy('style')
                    ->get()
                    ->toArray();

        $subcont = DB::table('subcont')
                        ->wherenull('deleted_at')
                        ->get();

        return view('packing.surat_jalan.index_global')->with([
                                                        'subcont'=>$subcont,
                                                        'list_style'=>$list_style
                                                        ]);

    }

    // get list surat jalan
    public function listSuratJalan()
    {
        $data = DB::table('delivery_header')
                    ->select(DB::raw('delivery_header.no_suratjalan, delivery_header.no_suratjalan_info, delivery_header.subcont_id, delivery_header.created_by, delivery_header.deleted_at, delivery_header.created_at, subcont.name AS subcont_name, users.name AS user_name, users.factory_id, factories.factory_name, delivery_header.is_global'))
                    ->join('subcont', 'subcont.id', '=', 'delivery_header.subcont_id')
                    ->join('users', 'users.id', '=', 'delivery_header.created_by')
                    ->join('factories', 'factories.id', '=', 'users.factory_id')
                    ->where('users.factory_id', Auth::user()->factory_id)
                    ->whereNull('delivery_header.deleted_at')
                    ->whereNull('subcont.deleted_at')
                    ->whereNull('users.deleted_at')
                    ->whereNull('factories.deleted_at')
                    ->get();

        return view('packing.surat_jalan.list_surat_jalan')->with('data', $data);

    }

    // formulir
    public function formulir()
    {
        $list_po = DB::table('po_summary_detail')
                    ->select(DB::raw('po_number_edit as po_number'))
                    ->whereNull('deleted_at')
                    ->groupBy('po_number_edit')
                    ->get()
                    ->toArray();

        // $result_po = array_reduce($list_po, function($carry, $item) { 
        //     if(!isset($carry[$item->po_number])) {
        //         $carry[$item->po_number] = $item;
        //     } 
            
        //     return $carry;
        // });

        // $result_po = $result_po !=null ? array_values($result_po) : array();
        
       return view('packing.formulir')->with('list_po', $list_po);

    }

    // formulir reject
    public function formulirReject()
    {
        $list_po = DB::table('po_summary')
                    ->select('po_number')
                    ->whereNull('deleted_at')
                    ->groupBy('po_number')
                    ->get()
                    ->toArray();

        $list_style = DB::table('po_summary')
                    ->select('style')
                    ->whereNull('deleted_at')
                    ->groupBy('style')
                    ->get()
                    ->toArray();
        
       return view('packing.formulir_reject')->with([
                                                    'list_po'=> $list_po,
                                                    'list_style'=> $list_style
                                                    ]);

    }

    // get list formulir
    public function listFormulir()
    {
        $data = DB::table('mancing_formulir_exist')
                    ->where('factory_id', Auth::user()->factory_id)
                    ->where('type_pl', 1)
                    ->get();

        return view('packing.list_formulir')->with('data', $data);

    }

    // get list formulir reject
    public function listFormulirReject()
    {
        $data = DB::table('mancing_formulir_exist')
                    ->where('factory_id', Auth::user()->factory_id)
                    ->where('type_pl', '!=', 1)
                    ->get();

        return view('packing.list_formulir_reject')->with('data', $data);

    }

    // get list barcode formulir
    public function listFormulirBarcodeExist(Request $request)
    {
        $date = $request->date;
        $po = $request->po;
        $style = $request->style;

        $data = DB::table('cutting_detail')
                    ->select('cutting_detail.barcode_id', 'po_summary.po_number', 'po_summary.style')
                    ->join('formulir_artwork', 'formulir_artwork.barcode_id', '=', 'cutting_detail.barcode_id')
                    ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail.po_summary_detail_id')
                    ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                    ->where('po_summary.po_number', $po)
                    ->where('po_summary.style', $style)
                    ->where('formulir_artwork.created_at', $date)
                    ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
                    ->whereNull('cutting_detail.deleted_at');
        
        $data = $data->get();

        return response()->json($data, 200);
    }
    // get list formulir from no surat jalan
    public function listSuratJalanExist(Request $request)
    {
        $no_suratjalan = $request->no_suratjalan;

        $data = DB::table('delivery_detail')
                    ->where('no_suratjalan', $no_suratjalan)
                    ->whereNull('deleted_at');
        
        $data = $data->get();

        return response()->json($data, 200);
    }

    public function getDataFormulir(Request $request)
    {
        //filtering
        if($request->radio_status == 'po') {

            if($request->po_number == null) {
                $po_number = 'null';
            }
            else {
                $po_number = trim($request->po_number);
            }

            $data = DB::table('cutting_detail')
                ->select(
                        'cutting_detail.barcode_id',
                        'cutting_detail.po_summary_detail_id',
                        'cutting_detail.cut_number',
                        'cutting_detail.cut_info',
                        'cutting_detail.lot',
                        'cutting_detail.komponen',
                        'cutting_detail.part',
                        'cutting_detail.qty',
                        'cutting_detail.sticker_from',
                        'cutting_detail.sticker_to',
                        'cutting_detail.sticker_no',
                        'cutting_detail.cutting_date',
                        'cutting_detail.bundle_id',
                        'cutting_detail.bundle',
                        'cutting_detail.qc',
                        'cutting_detail.cutter',
                        'cutting_detail.current_process',
                        'cutting_detail.current_status',
                        'cutting_detail.process',
                        'cutting_detail.already_process',
                        'cutting_detail.deleted_at',
                        'cutting_detail.is_printed',
                        'cutting_detail.is_printed_formulir',
                        'cutting_detail.is_printed_sj',
                        'cutting_detail.description',
                        'cutting_detail.created_at',
                        'cutting_detail.updated_at',
                        'cutting_detail.uoms',
                        'po_summary.style',
                        'po_summary_detail.po_number',
                        'po_summary_detail.plan_ref',
                        'po_summary_detail.size',
                        'po_summary_detail.color',
                        'po_summary_detail.article',
                        'po_summary_detail.factory_id',
                        'po_summary_detail.mo',
                        'po_summary_detail.qtyordered',
                        'po_summary_detail.kst_joborder',
                        'po_summary_detail.type_id',
                        'po_summary_detail.type_description',
                        'po_summary_detail.category',
                        'po_summary_detail.m_product_id',
                        'po_summary_detail.kode_product',
                        'po_summary_detail.nama_product',
                        'po_summary_detail.from',
                        'po_summary_detail.total_qty',
                        'po_summary_detail.color_edit',
                        'po_summary_detail.article_edit',
                        'po_summary_detail.style_edit',
                        'po_summary_detail.po_number_edit',
                        'po_summary_detail.size_edit',
                        'cutting_detail.qty_used',
                        'cutting_detail.qty_used_draft',
                        DB::raw('master_komponen.name AS komponen_name')
                )
                ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail.po_summary_detail_id')
                ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                ->join('master_komponen', 'master_komponen.id', '=', 'cutting_detail.komponen')
                ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
                // ->where('po_summary.po_number', 'like', '%'.$po_number.'%')
                // ->where('po_summary.po_number', $po_number)
                ->where('po_summary_detail.po_number_edit', $po_number)
                ->where('cutting_detail.current_process', 'distribusi')
                ->where('cutting_detail.current_status', 'completed')
                ->where('cutting_detail.is_printed', true)
                ->where(DB::raw('cutting_detail.qty-cutting_detail.qty_used'), '>', 0)
                // ->where('cutting_detail.is_printed_formulir', false)
                ->whereNull('cutting_detail.deleted_at')
                ->orderBy('po_summary.po_number','asc')
                ->orderBy('po_summary_detail.size','asc')
                ->orderBy('cutting_detail.sticker_from','asc');
                
        }elseif ($request->radio_status == 'style') {

            if(empty($request->style)) {
                $style = 'null';
            }
            else {
                $style = $request->style;
            }

            $data = DB::table('cutting_detail')
                ->select(
                        'cutting_detail.barcode_id',
                        'cutting_detail.po_summary_detail_id',
                        'cutting_detail.cut_number',
                        'cutting_detail.cut_info',
                        'cutting_detail.lot',
                        'cutting_detail.komponen',
                        'cutting_detail.part',
                        'cutting_detail.qty',
                        'cutting_detail.sticker_from',
                        'cutting_detail.sticker_to',
                        'cutting_detail.sticker_no',
                        'cutting_detail.cutting_date',
                        'cutting_detail.bundle_id',
                        'cutting_detail.bundle',
                        'cutting_detail.qc',
                        'cutting_detail.cutter',
                        'cutting_detail.current_process',
                        'cutting_detail.current_status',
                        'cutting_detail.process',
                        'cutting_detail.already_process',
                        'cutting_detail.deleted_at',
                        'cutting_detail.is_printed',
                        'cutting_detail.is_printed_formulir',
                        'cutting_detail.is_printed_sj',
                        'cutting_detail.description',
                        'cutting_detail.created_at',
                        'cutting_detail.updated_at',
                        'cutting_detail.uoms',
                        'po_summary.style',
                        'po_summary_detail.po_number',
                        'po_summary_detail.plan_ref',
                        'po_summary_detail.size',
                        'po_summary_detail.color',
                        'po_summary_detail.article',
                        'po_summary_detail.factory_id',
                        'po_summary_detail.mo',
                        'po_summary_detail.qtyordered',
                        'po_summary_detail.kst_joborder',
                        'po_summary_detail.type_id',
                        'po_summary_detail.type_description',
                        'po_summary_detail.category',
                        'po_summary_detail.m_product_id',
                        'po_summary_detail.kode_product',
                        'po_summary_detail.nama_product',
                        'po_summary_detail.from',
                        'po_summary_detail.total_qty',
                        'po_summary_detail.color_edit',
                        'po_summary_detail.article_edit',
                        'po_summary_detail.style_edit',
                        'po_summary_detail.po_number_edit',
                        'po_summary_detail.size_edit',
                        'cutting_detail.qty_used',
                        'cutting_detail.qty_used_draft',
                        DB::raw('master_komponen.name AS komponen_name')
                )
                ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail.po_summary_detail_id')
                ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                ->join('master_komponen', 'master_komponen.id', '=', 'cutting_detail.komponen')
                ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
                ->where('po_summary.style', $style)
                ->where('cutting_detail.current_process', 'distribusi')
                ->where('cutting_detail.current_status', 'completed')
                ->where('cutting_detail.is_printed', true)
                ->where(DB::raw('cutting_detail.qty-cutting_detail.qty_used'), '>', 0)
                ->whereNull('cutting_detail.deleted_at')
                ->orderBy('po_summary.po_number','asc')
                ->orderBy('po_summary_detail.size','asc')
                ->orderBy('cutting_detail.sticker_from','asc');
        }

        $types = DB::table('type_packing_list')
                    ->where('id',1)
                    ->whereNull('deleted_at')
                    ->get();

        return Datatables::of($data)
                ->addColumn('checkbox', function ($data) {
                    return '<input type="checkbox" class="clref" name="selector[]" id="Inputselector" value="'.$data->barcode_id.'" data-ponumber="'.$data->po_number.'" data-style="'.$data->style.'" data-size="'.$data->size.'" data-description="'.$data->description.'">';
                })
                ->editColumn('description', function($data){
                    return '<div id="desc_'.$data->barcode_id.'">
                                <input type="text"
                                        data-barcode="'.$data->barcode_id.'"
                                        class="form-control desc_filled"
                                        value="'.$data->description.'">
                            </div>';
                })
                ->editColumn('type_pl_draft', function($data) use($types){

                    $testing = '';
                    foreach ($types as $key => $val){

                       $testing .= '<option data-barcode='.$data->barcode_id.' value='.$val->id.'>'.$val->type_name.'</option>';
                    }

                    return '<div id="typepl_'.$data->barcode_id.'">
                                <select class="form-control typepl_filled" id="type_pl_draft" name="type_pl_draft">
                                '.$testing.'
                                </select>
                            </div>';
                })
                ->editColumn('balance', function($data){
                    $balance = 0;
                    $balance = $data->qty - $data->qty_used;

                    return $balance;
                })
                ->editColumn('cut_number', function($data){

                    return $data->cut_number.' '.$data->cut_info;
                })
                ->editColumn('qty_used_draft', function($data){
                    $balance = 0;
                    $qty_default = 0;

                    $balance = $data->qty - $data->qty_used;
                    $qty_default = $data->qty_used_draft > 0 ? $data->qty_used_draft : $data->qty - $data->qty_used;

                    return '<div id="balance_'.$data->barcode_id.'">
                                <input type="hidden" name="txtBalance" class="clBalance" value="'.$balance.'" >
                                <input type="text"
                                        data-barcode="'.$data->barcode_id.'"
                                        class="form-control balance_filled"
                                        onkeypress="return isNumberKey(event)"
                                        value="'.$qty_default.'" readonly>
                            </div>';

                            
                            // onkeyup="upNumber(this)"
                            // onblur="doneNumber(this)"
                })
                ->rawColumns(['checkbox', 'description', 'qty_used_draft', 'type_pl_draft'])
                ->make(true);
    }

    // get data formulir reject
    public function getDataFormulirReject(Request $request)
    {
        //filtering
        if($request->radio_status == 'po') {

            if($request->po_number == null) {
                $po_number = 'null';
            }
            else {
                $po_number = trim($request->po_number);
            }

            $data = DB::table('cutting_detail')
                ->select(
                        'cutting_detail.barcode_id',
                        'cutting_detail.po_summary_detail_id',
                        'cutting_detail.cut_number',
                        'cutting_detail.cut_info',
                        'cutting_detail.lot',
                        'cutting_detail.komponen',
                        'cutting_detail.part',
                        'cutting_detail.qty',
                        'cutting_detail.sticker_from',
                        'cutting_detail.sticker_to',
                        'cutting_detail.sticker_no',
                        'cutting_detail.cutting_date',
                        'cutting_detail.bundle_id',
                        'cutting_detail.bundle',
                        'cutting_detail.qc',
                        'cutting_detail.cutter',
                        'cutting_detail.current_process',
                        'cutting_detail.current_status',
                        'cutting_detail.process',
                        'cutting_detail.already_process',
                        'cutting_detail.deleted_at',
                        'cutting_detail.is_printed',
                        'cutting_detail.is_printed_formulir',
                        'cutting_detail.is_printed_sj',
                        'cutting_detail.description',
                        'cutting_detail.created_at',
                        'cutting_detail.updated_at',
                        'cutting_detail.uoms',
                        'po_summary.style',
                        'po_summary_detail.po_number',
                        'po_summary_detail.plan_ref',
                        'po_summary_detail.size',
                        'po_summary_detail.color',
                        'po_summary_detail.article',
                        'po_summary_detail.factory_id',
                        'po_summary_detail.mo',
                        'po_summary_detail.qtyordered',
                        'po_summary_detail.kst_joborder',
                        'po_summary_detail.type_id',
                        'po_summary_detail.type_description',
                        'po_summary_detail.category',
                        'po_summary_detail.m_product_id',
                        'po_summary_detail.kode_product',
                        'po_summary_detail.nama_product',
                        'po_summary_detail.from',
                        'po_summary_detail.total_qty',
                        'po_summary_detail.color_edit',
                        'po_summary_detail.article_edit',
                        'po_summary_detail.style_edit',
                        'po_summary_detail.po_number_edit',
                        'po_summary_detail.size_edit',
                        'cutting_detail.qty_used',
                        'cutting_detail.qty_used_draft',
                        'cutting_detail.type_pl_draft',
                        'cutting_detail.barcode_reject',
                        DB::raw('master_komponen.name AS komponen_name'),
                        'master_komponen.name'
                )
                ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail.po_summary_detail_id')
                ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                ->join('master_komponen', 'master_komponen.id', '=', 'cutting_detail.komponen')
                ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
                ->where('po_summary.po_number', $po_number)
                // ->where('cutting_detail.current_process', 'distribusi')
                // ->where('cutting_detail.current_status', 'onprogress')
                ->where(function($query){
                    $query->where(function($query){
                        $query->where('cutting_detail.current_process', 'distribusi')
                                ->where('cutting_detail.current_status', 'onprogress');
                    })
                    ->orWhere(function($query){
                        $query->where('cutting_detail.current_process', 'reject')
                                ->where('cutting_detail.current_status', 'completed');
                    });
                })
                ->where('cutting_detail.is_printed', true)
                // ->where(DB::raw('cutting_detail.qty-cutting_detail.qty_used'), '>', 0)
                ->whereNull('cutting_detail.deleted_at')
                ->orderBy('po_summary.po_number','asc')
                ->orderBy('po_summary_detail.size','asc')
                ->orderBy('cutting_detail.sticker_from','asc');
                
        }elseif ($request->radio_status == 'style') {

            if(empty($request->style)) {
                $style = 'null';
            }
            else {
                $style = $request->style;
            }

            $data = DB::table('cutting_detail')
                ->select(
                        'cutting_detail.barcode_id',
                        'cutting_detail.po_summary_detail_id',
                        'cutting_detail.cut_number',
                        'cutting_detail.cut_info',
                        'cutting_detail.lot',
                        'cutting_detail.komponen',
                        'cutting_detail.part',
                        'cutting_detail.qty',
                        'cutting_detail.sticker_from',
                        'cutting_detail.sticker_to',
                        'cutting_detail.sticker_no',
                        'cutting_detail.cutting_date',
                        'cutting_detail.bundle_id',
                        'cutting_detail.bundle',
                        'cutting_detail.qc',
                        'cutting_detail.cutter',
                        'cutting_detail.current_process',
                        'cutting_detail.current_status',
                        'cutting_detail.process',
                        'cutting_detail.already_process',
                        'cutting_detail.deleted_at',
                        'cutting_detail.is_printed',
                        'cutting_detail.is_printed_formulir',
                        'cutting_detail.is_printed_sj',
                        'cutting_detail.description',
                        'cutting_detail.created_at',
                        'cutting_detail.updated_at',
                        'cutting_detail.uoms',
                        'po_summary.style',
                        'po_summary_detail.po_number',
                        'po_summary_detail.plan_ref',
                        'po_summary_detail.size',
                        'po_summary_detail.color',
                        'po_summary_detail.article',
                        'po_summary_detail.factory_id',
                        'po_summary_detail.mo',
                        'po_summary_detail.qtyordered',
                        'po_summary_detail.kst_joborder',
                        'po_summary_detail.type_id',
                        'po_summary_detail.type_description',
                        'po_summary_detail.category',
                        'po_summary_detail.m_product_id',
                        'po_summary_detail.kode_product',
                        'po_summary_detail.nama_product',
                        'po_summary_detail.from',
                        'po_summary_detail.total_qty',
                        'po_summary_detail.color_edit',
                        'po_summary_detail.article_edit',
                        'po_summary_detail.style_edit',
                        'po_summary_detail.po_number_edit',
                        'po_summary_detail.size_edit',
                        'cutting_detail.qty_used',
                        'cutting_detail.qty_used_draft',
                        'cutting_detail.type_pl_draft',
                        'cutting_detail.barcode_reject',
                        DB::raw('master_komponen.name AS komponen_name'),
                        'master_komponen.name'
                )
                ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail.po_summary_detail_id')
                ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                ->join('master_komponen', 'master_komponen.id', '=', 'cutting_detail.komponen')
                ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
                ->where('po_summary.style', $style)
                ->where(function($query){
                    $query->where(function($query){
                        $query->where('cutting_detail.current_process', 'distribusi')
                                ->where('cutting_detail.current_status', 'onprogress');
                    })
                    ->orWhere(function($query){
                        $query->where('cutting_detail.current_process', 'reject')
                                ->where('cutting_detail.current_status', 'completed');
                    });
                })
                ->where('cutting_detail.is_printed', true)
                ->whereNull('cutting_detail.deleted_at')
                ->orderBy('po_summary.po_number','asc')
                ->orderBy('po_summary_detail.size','asc')
                ->orderBy('cutting_detail.sticker_from','asc');
        }

        $types = DB::table('type_packing_list')
                    ->where('id', '!=', 1)
                    ->whereNull('deleted_at')
                    ->get();
                    
        return Datatables::of($data)
                ->addColumn('checkbox', function ($data) {
                    return '<input type="checkbox" class="clref" name="selector[]" id="Inputselector" value="'.$data->barcode_id.'" data-ponumber="'.$data->po_number.'" data-style="'.$data->style.'" data-size="'.$data->size.'" data-description="'.$data->description.'">';
                })
                ->editColumn('description', function($data){
                    return '<div id="desc_'.$data->barcode_id.'">
                                <input type="text"
                                        data-barcode="'.$data->barcode_id.'"
                                        class="form-control desc_filled"
                                        value="'.$data->description.'">
                            </div>';
                })
                ->editColumn('type_pl_draft', function($data) use($types){
                    
                    $testing = '';
                    foreach ($types as $key => $val){
                       if ($data->type_pl_draft==$val->id) {
                           $testing .= '<option '. $data->type_pl_draft.' data-pl='.$data->type_pl_draft.' data-barcode='.$data->barcode_id.' value='.$val->id.' selected>'.$val->type_name.'</option>';
                        }else{
                            $testing .= '<option '. $data->type_pl_draft.' data-pl='.$data->type_pl_draft.' data-barcode='.$data->barcode_id.' value='.$val->id.'>'.$val->type_name.'</option>';
                       }
                    }

                    return '<div id="typepl_'.$data->barcode_id.'">
                                <select class="form-control typepl_filled" id="type_pl_draft" name="type_pl_draft">
                                '.$testing.'
                                </select>
                            </div>';
                })
                ->editColumn('barcode_reject', function($data){

                    $data_sticker = array();
                    for ($i=$data->sticker_from; $i <= $data->sticker_to; $i++) { 
                        $data_sticker[$i] = $i;
                    }

                    $data_reject = $data->barcode_reject != null ? array_map('trim', explode(',', $data->barcode_reject)) : array();


                    $testing = '';
                    if (count($data_reject) >0) {
                        
                        foreach ($data_sticker as $key => $val){
                            foreach ($data_reject as $key2 => $val2) {
                                if ($val2==$val) {
                                    $testing .= '<option data-barcode='.$data->barcode_id.' data-from='.$data->sticker_from.' data-to='.$data->sticker_to.' value='.$val.' selected>'.$val.'</option>';
                                }else{
                                    $testing .= '<option data-barcode='.$data->barcode_id.' data-from='.$data->sticker_from.' data-to='.$data->sticker_to.' value='.$val.'>'.$val.'</option>';
                                }
                            }
    
                        }
                    }else {

                        foreach ($data_sticker as $key => $val){
    
                           $testing .= '<option data-barcode='.$data->barcode_id.' data-from='.$data->sticker_from.' data-to='.$data->sticker_to.' value='.$val.'>'.$val.'</option>';
                        }
                    }

                    return '<div id="bareject_'.$data->barcode_id.'">
                                
                                <select multiple data-placeholder="Select a State..." class="form-control select-search bareject_filled" name="sticker_reject[]" id="sticker_reject">
                                '.$testing.'
                                </select>
                            </div>';
                })
                ->editColumn('balance', function($data){
                    $balance = 0;
                    $balance = $data->qty - $data->qty_used;

                    return $balance;
                })
                ->editColumn('cut_number', function($data){

                    return $data->cut_number.' '.$data->cut_info;
                })
                ->editColumn('qty_used_draft', function($data){
                    $balance = 0;
                    $qty_default = 0;

                    $balance = $data->qty - $data->qty_used;
                    $qty_default = $data->qty_used_draft > 0 ? $data->qty_used_draft : $data->qty - $data->qty_used;

                    return '<div id="balance_'.$data->barcode_id.'">
                                <input type="hidden" name="txtBalance" class="clBalance" value="'.$balance.'" >
                                <input type="text"
                                        data-barcode="'.$data->barcode_id.'"
                                        class="form-control balance_filled"
                                        onkeypress="return isNumberKey(event)"
                                        onkeyup="upNumber(this)"
                                        onblur="doneNumber(this)"
                                        value="'.$qty_default.'">
                            </div>';
                })
                ->rawColumns(['checkbox', 'description', 'qty_used_draft', 'type_pl_draft', 'barcode_reject'])
                ->make(true);
    }

    // get data surat jalan
    public function getDataSuratJalan(Request $request)
    {
        //filtering
        if($request->radio_status == 'po') {

            if($request->po_number == null) {
                $po_number = 'null';
            }
            else {
                $po_number = trim($request->po_number);
            }

            $data = DB::table('v_surat_jalan')
                        // ->where('po_number', 'like', '%'.$po_number.'%')
                        ->where('po_number', 'like', $po_number)
                        ->orderBy('po_number');
                
        }elseif ($request->radio_status == 'style') {

            if(empty($request->style)) {
                $style = 'null';
            }
            else {
                $style = $request->style;
            }

            $data = DB::table('v_surat_jalan')
                        // ->where('style', 'like', '%'.$style.'%')
                        ->where('style', $style)
                        ->orderBy('po_number');
        }

        $data = $data->where('is_delivery_printed', false)
                        ->whereNull('deleted_at');
        
        return Datatables::of($data)
                ->addColumn('checkbox', function ($data) {
                    return '<input type="checkbox" class="clref" name="selector[]" id="Inputselector" value="'.$data->total_qty.'" data-ponumber="'.$data->po_number.'" data-style="'.$data->style.'" data-create="'.$data->formulir_created.'" data-headerid="'.$data->formulir_artwork_header_id.'">';
                })
                ->editColumn('uoms', function($data){
                    return '<div id="uoms_'.$data->formulir_artwork_header_id.'">
                                <input type="text"
                                        data-id="'.$data->formulir_artwork_header_id.'"
                                        data-po="'.$data->po_number.'"
                                        class="form-control uoms_filled"
                                        value="'.$data->uoms.'">
                            </div>';
                })
                ->rawColumns(['checkbox', 'uoms'])
                ->make(true);
    }

    public function updateDescription(Request $request)
    {
        $barcode_id = $request->barcode_id;
        $description = $request->description;

        $request->validate([
            'barcode_id' => 'required',
        ]);

        try {
            DB::beginTransaction();
                // update cutting detail
                DB::table('cutting_detail')
                        ->where('barcode_id', $barcode_id)
                        ->update([
                            'description' => $description,
                            'updated_at' => Carbon::now()
                        ]);
            
            DB::commit();
            
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        $html = '<input type="text"
                        data-barcode="'.$barcode_id.'"
                        class="form-control desc_filled"
                        value="'.$description.'">';

        return response()->json($html, 200);
    }

    // update qty used (ganti qty used draft)
    public function updateQtyUsed(Request $request)
    {
        $barcode_id = $request->barcode_id;
        $qty_used_draft = !empty($request->qty_used_draft) ? $request->qty_used_draft : 0;

        $get_cd = DB::table('cutting_detail')
                    ->where('barcode_id', $barcode_id)
                    ->first();

        $balance = 0;
        //$qty_default = 0;

        $balance = $get_cd->qty - $get_cd->qty_used;
        //$qty_default = $get_cd->qty_used_draft > 0 ? $get_cd->qty_used_draft : $get_cd->qty - $get_cd->qty_used;

        // cek qty_used_draft
        if ($qty_used_draft>$balance) {
            return response()->json('qty not accepted..!', 422);
        }

        $request->validate([
            'barcode_id' => 'required',
            'qty_used_draft' => 'required',
        ]);

        try {
            DB::beginTransaction();
                // update cutting detail
                DB::table('cutting_detail')
                        ->where('barcode_id', $barcode_id)
                        ->update([
                            'qty_used_draft' => $qty_used_draft
                        ]);
            
            DB::commit();
            
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        $html = '<input name="txtBalance" class="clBalance hidden" value="'.$balance.'" >
                    <input type="text"
                            data-barcode="'.$barcode_id.'"
                            class="form-control balance_filled"
                            onkeypress="return isNumberKey(event)"
                            onkeyup="upNumber(this)"
                            onblur="doneNumber(this)"
                            value="'.$qty_used_draft.'">';

        return response()->json($html, 200);
    }

    // update type pl formulir
    public function updateTypePl(Request $request)
    {
        $barcode_id = $request->barcode_id;
        $type_pl_draft = $request->type_pl_draft;

        $request->validate([
            'barcode_id' => 'required',
            'type_pl_draft' => 'required',
        ]);

        $types = DB::table('type_packing_list')
                    ->where('id', 1)
                    ->whereNull('deleted_at')
                    ->get();

        $testing = '';
        foreach ($types as $key => $val){
            $selected = $val->id == $type_pl_draft ? ' selected' : ' ';

            if ($val->id == $type_pl_draft) {
                $testing .= '<option data-barcode='.$barcode_id.' value='.$val->id.' selected>'.$val->type_name.'</option>';
            }else {
                $testing .= '<option data-barcode='.$barcode_id.' value='.$val->id.'>'.$val->type_name.'</option>';
            }

        }

        try {
            DB::beginTransaction();
                // update cutting detail
                DB::table('cutting_detail')
                        ->where('barcode_id', $barcode_id)
                        ->update([
                            'type_pl_draft' => $type_pl_draft
                        ]);
            
            DB::commit();
            
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        $html = '<select class="form-control typepl_filled" id="type_pl_draft" name="type_pl_draft">
                    '.$testing.'
                    </select>';

        return response()->json($html, 200);
    }

    // update type pl formulir reject
    public function updateTypePlReject(Request $request)
    {
        $barcode_id = $request->barcode_id;
        $type_pl_draft = $request->type_pl_draft;

        $request->validate([
            'barcode_id' => 'required',
            'type_pl_draft' => 'required',
        ]);

        $types = DB::table('type_packing_list')
                    ->where('id', '!=', 1)
                    ->whereNull('deleted_at')
                    ->get();

        $testing = '';
        foreach ($types as $key => $val){
            $selected = $val->id == $type_pl_draft ? ' selected' : ' ';

            if ($val->id == $type_pl_draft) {
                $testing .= '<option data-barcode='.$barcode_id.' value='.$val->id.' selected>'.$val->type_name.'</option>';
            }else {
                $testing .= '<option data-barcode='.$barcode_id.' value='.$val->id.'>'.$val->type_name.'</option>';
            }
        }

        try {
            DB::beginTransaction();
                // update cutting detail
                DB::table('cutting_detail')
                        ->where('barcode_id', $barcode_id)
                        ->update([
                            'type_pl_draft' => $type_pl_draft
                        ]);
            
            DB::commit();
            
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        $html = '<select class="form-control typepl_filled" id="type_pl_draft" name="type_pl_draft">
                    '.$testing.'
                    </select>';

        return response()->json($html, 200);
    }
    
    // update barcode reject
    public function updateBarcodeReject(Request $request)
    {
        $barcode_id = $request->barcode_id;
        $barcode_reject = $request->barcode_reject != 'null' ? implode(',',$request->barcode_reject) : null;
        $sticker_from = $request->sticker_from;
        $sticker_to = $request->sticker_to;

        $request->validate([
            'barcode_id' => 'required',
        ]);

        $data_sticker = array();
        for ($i=$sticker_from; $i <= $sticker_to; $i++) { 
            $data_sticker[$i] = $i;
        }

        $testing = '';
        foreach ($data_sticker as $key => $val){

            $testing .= '<option data-barcode='.$barcode_id.' data-from='.$sticker_from.' data-to='.$sticker_to.' value='.$val.'>'.$val.'</option>';
        }

        try {
            DB::beginTransaction();
                // update cutting detail
                DB::table('cutting_detail')
                        ->where('barcode_id', $barcode_id)
                        ->update([
                            'barcode_reject' => $barcode_reject,
                            'updated_at' => Carbon::now()
                        ]);
            
            DB::commit();
            
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        $html = ' <select multiple data-placeholder="Select a State..." class="form-control select-search bareject_filled" name="sticker_reject[]" id="sticker_reject">
        '.$testing.'
        </select>';

        return response()->json($html, 200);
    }

    // update uoms
    public function updateUoms(Request $request)
    {
        $id = $request->id;
        $uoms = $request->uoms;

        $request->validate([
            'id' => 'required',
        ]);

        try {
            DB::beginTransaction();
                // update formulir_artwork_header
                DB::table('formulir_artwork_header')
                        ->where('id', $id)
                        ->update([
                            'uoms' => $uoms
                        ]);
            
            DB::commit();
            
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        $html = '<input type="text"
                        data-id="'.$id.'"
                        class="form-control uoms_filled"
                        value="'.$uoms.'">';

        return response()->json($html, 200);
    }

    // cancel formulir
    public function cancelFormulir(Request $request)
    {   
        $id = $request->id;

        $request->validate([
            'id' => 'required'
        ]);

        // cek apakah formulir sudah dibuat surat jalan ?
        $cekindelivery = DB::table('delivery_detail')
                            ->where('formulir_id', $id)
                            ->whereNull('deleted_at')
                            ->exists();

        if ($cekindelivery) {
            return response()->json('formulir already in delivery orders, please cancel before..!', 422);
        }

        // data detail
        $data_detail = DB::table('v_formulir_new')
                            ->where('formulir_artwork_header_id', $id)
                            ->whereNull('deleted_at')
                            ->get();

        try {
            DB::beginTransaction();
                // 

                foreach ($data_detail as $key => $value) {
                    $barcode_id = $value->barcode_id;

                    // get qty formulir
                    $get_formulir = DB::table('formulir_artwork')
                                        ->select('formulir_artwork_header_id', 'barcode_id', DB::raw('SUM(qty) as qty_formulir'))
                                        ->where('barcode_id', $barcode_id)
                                        ->where('formulir_artwork_header_id', $value->formulir_artwork_header_id)
                                        ->groupBy('formulir_artwork_header_id', 'barcode_id')
                                        ->get();
                    
                    foreach ($get_formulir as $key1 => $val1) {
                        // qty used - qty formulir, qty used draft + qty formulir (gak jadi)
                        $get_return = DB::table('cutting_detail')
                                        ->selectRaw('qty_used - ? as qty_return, qty_used_draft + ? as qty_return_draft', [$val1->qty_formulir, $val1->qty_formulir])
                                        ->where('barcode_id', $val1->barcode_id)
                                        ->first();

                        $data_cutting_detail = DB::table('cutting_detail')
                                                    ->where('barcode_id', $val1->barcode_id)
                                                    ->first();

                        // update cutting detail
                        DB::table('cutting_detail')
                            ->where('barcode_id', $barcode_id)
                            ->update([
                                'updated_at' => Carbon::now(),
                                'qty_used' => 0,
                                'qty_used_draft' => $data_cutting_detail->qty,
                                'is_printed_formulir' => false,
                                'type_pl_draft' => 0,
                                'barcode_reject' => null
                            ]);
                    }

                }

                // update formulir artwork header
                DB::table('formulir_artwork_header')
                        ->where('id', $id)
                        ->update([
                            'updated_at' => Carbon::now(),
                            'deleted_at' => Carbon::now(),
                            'deleted_by' => Auth::user()->id
                        ]);

                // update formulir artwork
                DB::table('formulir_artwork')
                        ->where('formulir_artwork_header_id', $id)
                        ->update([
                            'updated_at' => Carbon::now(),
                            'deleted_at' => Carbon::now()
                        ]);

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return $this->listFormulir();
    }

    // cancel formulir reject
    public function cancelFormulirReject(Request $request)
    {   
        $id = $request->id;

        $request->validate([
            'id' => 'required'
        ]);

        // cek apakah formulir sudah dibuat surat jalan ?
        $cekindelivery = DB::table('delivery_detail')
                            ->where('formulir_id', $id)
                            ->whereNull('deleted_at')
                            ->exists();

        if ($cekindelivery) {
            return response()->json('formulir already in delivery orders, please cancel before..!', 422);
        }

        // data detail
        $data_detail = DB::table('formulir_artwork')
                            ->where('formulir_artwork_header_id', $id)
                            ->whereNull('deleted_at')
                            ->get();

        try {
            DB::beginTransaction();
                // 
                // update formulir artwork header
                DB::table('formulir_artwork_header')
                        ->where('id', $id)
                        ->update([
                            'updated_at' => Carbon::now(),
                            'deleted_at' => Carbon::now(),
                            'deleted_by' => Auth::user()->id
                        ]);

                // update formulir artwork
                DB::table('formulir_artwork')
                        ->where('formulir_artwork_header_id', $id)
                        ->update([
                            'updated_at' => Carbon::now(),
                            'deleted_at' => Carbon::now()
                        ]);

                foreach ($data_detail as $key => $value) {
                    $barcode_id = $value->barcode_id;

                    // get qty formulir
                    $get_formulir = DB::table('formulir_artwork')
                                        ->select('formulir_artwork_header_id', 'barcode_id', DB::raw('SUM(qty) as qty_formulir'))
                                        ->where('barcode_id', $barcode_id)
                                        ->where('formulir_artwork_header_id', $value->formulir_artwork_header_id)
                                        ->groupBy('formulir_artwork_header_id', 'barcode_id')
                                        ->get();
                    
                    foreach ($get_formulir as $key1 => $val1) {
                        // qty used - qty formulir, qty used draft + qty formulir
                        $get_return = DB::table('cutting_detail')
                                        ->selectRaw('qty_used - ? as qty_return, qty_used_draft + ? as qty_return_draft', [$val1->qty_formulir, $val1->qty_formulir])
                                        ->where('barcode_id', $val1->barcode_id)
                                        ->first();

                        $data_barcode = DB::table('cutting_detail')
                                        ->where('barcode_id', $val1->barcode_id)
                                        ->first();
                        
                        // update cutting detail
                        DB::table('cutting_detail')
                            ->where('barcode_id', $barcode_id)
                            ->update([
                                'updated_at' => Carbon::now(),
                                'qty_used' => 0,  //$get_return->qty_return,
                                'qty_used_draft' => $data_barcode->qty, //$get_return->qty_return_draft,
                                'is_printed_formulir' => false
                            ]);


                        //check if package can be scanned or not
                        $check = $this->_check_scan($barcode_id, 'cancel', 'checkout');
                        if(!$check) {
                            $error = $this->_show_error($barcode_id);
                            return response()->json($error,422);
                        }

                        // prepare table for table cutting detail and cutting_movements
                        $process_to = 'distribusi';
                        $status_to = 'onprogress';
                        $data = array(
                                        'barcode_id' => $barcode_id, 
                                        'process_to' => null, 
                                        'status_to' => 'cancel', 
                                        'created_at' => Carbon::now() 
                                    );

                        //query for table cutting_movements and cutting_detail
                        $query_movement = $this->_query_movement($barcode_id, 'cancel', $data);
                        if(!$query_movement) {
                            throw new \Exception('Movement error');
                        }
                        $query_package = $this->_query_package($barcode_id, 'cancel', $process_to, $status_to);
                        if(!$query_package) {
                            throw new \Exception('Package error');
                        }

                    }

                }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return $this->listFormulirReject();
    }
    // cancel surat jalan
    public function cancelSuratJalan(Request $request)
    {
        $no_suratjalan = $request->no_suratjalan;

        $request->validate([
            'no_suratjalan' => 'required'
        ]);

        // data detail
        $data_detail = DB::table('delivery_detail')
                            ->where('no_suratjalan', $no_suratjalan)
                            ->whereNull('deleted_at')
                            ->get();
        
        $list_formulir = array();

        try {
            DB::beginTransaction();
                // 
                // update delivery header
                DB::table('delivery_header')
                        ->where('no_suratjalan', $no_suratjalan)
                        ->update([
                            'updated_at' => Carbon::now(),
                            'deleted_at' => Carbon::now(),
                            'deleted_by' => Auth::user()->id
                        ]);

                // update delivery detail
                DB::table('delivery_detail')
                        ->where('no_suratjalan', $no_suratjalan)
                        ->update([
                            'updated_at' => Carbon::now(),
                            'deleted_at' => Carbon::now()
                        ]);

                foreach ($data_detail as $key => $value) {
                    $formulir_id = $value->formulir_id;

                    // update formulir artwork header
                    DB::table('formulir_artwork_header')
                            ->where('id', $formulir_id)
                            ->update([
                                'is_delivery_printed' => false,
                                'updated_at' => Carbon::now()
                            ]);

                    $list_formulir[] = $formulir_id;
                }

                // 
                $data_formulir = DB::table('formulir_artwork')
                                            ->whereIn('formulir_artwork_header_id', $list_formulir)
                                            ->get();

                foreach ($data_formulir as $key2 => $val2) {
                    $barcode_id = $val2->barcode_id;

                    // update cutting detail
                    DB::table('cutting_detail')
                            ->where('barcode_id', $barcode_id)
                            ->update([
                                'updated_at' => Carbon::now(),
                                'is_printed_sj' => false
                            ]);
                }

                // update delivery reject
                DB::table('delivery_reject')
                        ->where('no_suratjalan', $no_suratjalan)
                        ->update([
                            'updated_at' => Carbon::now(),
                            'deleted_at' => Carbon::now()
                        ]);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return $this->listSuratJalan();
    }

    public function setCompletedAllFormulir(Request $request)
    {
        $data_req = json_decode(stripslashes($request->data));
        $no_polybag = $request->no_polybag;

        $data = DB::table('cutting_detail')
                    ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail.po_summary_detail_id')
                    ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                    ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
                    ->where('cutting_detail.is_printed', true)
                    ->whereNull('cutting_detail.deleted_at');
        
        if (count($data_req) > 0) {
            foreach ($data_req as $value) {
                $rbarcode['barcode_id'] = $value->barcode_id;
                $arr_barcode[] = $rbarcode;
            }

            $data = $data->whereIn('cutting_detail.barcode_id', $arr_barcode);
        }

        $data = $data->get();

        $data_header = array();
        $data_formulir = array();

        $header['po_number'] = $data[0]->po_number;
        $header['created_at'] = Carbon::now();
        $header['updated_at'] = Carbon::now();
        $header['user_by'] = Auth::user()->id;
        $header['no_polybag'] = $no_polybag;
        $data_header[] = $header;

        $ponumber = $data[0]->po_number;
        $style = $data[0]->style;

        // get type pl id normal
        $types = DB::table('type_packing_list')
                    ->where('type_name', 'NORMAL')
                    ->whereNull('deleted_at')
                    ->first();

        try {
            
            DB::beginTransaction();

                // cek exist no polybag
                $cekpolybagifexist = DB::table('formulir_artwork_header')
                                        ->where('no_polybag', $no_polybag)
                                        ->whereRaw('date(created_at) = ?', [Carbon::now()->format('Y-m-d')])
                                        ->whereNull('deleted_at')
                                        ->exists();

                if ($cekpolybagifexist) {
                    throw new \Exception('Something wrong (polybag already exists)..! ');
                }

                // insert header if exist
                $cekheaderifexist = DB::table('formulir_artwork_header')
                                        ->where('created_at', Carbon::now())
                                        ->where('po_number', $data[0]->po_number)
                                        ->exists();
                if (!$cekheaderifexist) {
                    DB::table('formulir_artwork_header')
                            ->insert($data_header);
                }else {
                    throw new \Exception('Something wrong (formulir already created)..! ');
                }

                $LastInsertId = DB::getPdo()->lastInsertId();

                $qty_used = 0;
                $qty_used_draft = 0;

                foreach ($data as $key => $value) {
                    // if ($value->po_number != $ponumber) {
                    //     throw new \Exception('Something wrong (different PO)..! ');
                    // }
                    if ($value->style != $style) {
                        throw new \Exception('Something wrong (different Style)..! ');
                    }
                    $barcodeid = $value->barcode_id;
                    //
                    $qty_used_draft =  $value->qty_used_draft > 0 ? $value->qty_used_draft : $value->qty-$value->qty_used;

                    $qty_used = $value->qty_used;

                    $qty_used = $qty_used + $qty_used_draft;
        
                    $data_f = array(
                            'barcode_id' => $barcodeid,
                            'description' => $value->description,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                            'formulir_artwork_header_id' => $LastInsertId,
                            'qty' => $qty_used_draft,
                            'type_pl' => $value->type_pl_draft > 0 ? $value->type_pl_draft : $types->id 
                    );
        
                    $data_formulir[] = $data_f;

                    // update cutting detail
                    $update = DB::table('cutting_detail')
                                ->where('barcode_id', $barcodeid)
                                ->update([
                                    'qty_used' => $qty_used,
                                    'qty_used_draft' => $value->qty - $qty_used,
                                    'updated_at' => Carbon::now()
                                ]);
                    
                    if ($update) {
                        // cek qty used
                        $cekqtyused = DB::table('cutting_detail')
                                        ->where('barcode_id', $barcodeid)
                                        ->where(function ($query) {
                                            $query->where('qty_used', '<', 0)
                                                  ->orWhere('qty_used_draft', '<', 0);
                                        })
                                        ->exists();

                        if ($cekqtyused) {
                            throw new \Exception('Something wrong (qty_used invalid)..! ');
                        }
                    }
                    
                }

                
                foreach ($data_formulir as $key2 => $value2) {

                    // update cutting detail
                    DB::table('cutting_detail')
                        ->where('barcode_id', $value2['barcode_id'])
                        ->update([
                            'is_printed_formulir' => true,
                            'type_pl_draft' => 0,
                            'description' => null,
                            'updated_at' => Carbon::now()
                        ]);
                    
                    // cek exists
                    $cekifexist = DB::table('formulir_artwork')
                                    ->where('formulir_artwork_header_id', $LastInsertId)
                                    ->whereNull('deleted_at')
                                    ->exists();
                    
                    if ($cekifexist) {
                        // update formulir artwork
                        DB::table('formulir_artwork')
                            ->where('formulir_artwork_header_id', $LastInsertId)
                            ->update([
                                'updated_at' => Carbon::now()
                            ]);
                    }else{
                        // insert formulir artwork
                        DB::table('formulir_artwork')
                            ->insert($data_formulir);
                    }
                }

                // update factories
                DB::table('factories')
                    ->where('id', Auth::user()->factory_id)
                    ->update([
                        'no_polybag' => (int)$no_polybag,
                        'updated_at' => Carbon::now()
                    ]);

                // cek count no polybag double
                $cekpolybagcount = DB::table('formulir_artwork_header')
                                        ->where('no_polybag', $no_polybag)
                                        ->whereRaw('date(created_at) = ?', [Carbon::now()->format('Y-m-d')])
                                        ->whereNull('deleted_at')
                                        ->count();

                if ($cekpolybagcount>1) {
                    throw new \Exception('Something wrong (polybag double insert)..! ');
                }

            
            DB::commit();
            
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json($LastInsertId, 200);
        
    }

    // formulir reject
    public function setCompletedAllFormulirReject(Request $request)
    {
        $data_req = json_decode(stripslashes($request->data));
        $no_polybag = strtoupper($request->no_polybag);

        $data = DB::table('cutting_detail')
                    ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail.po_summary_detail_id')
                    ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                    ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
                    ->where('cutting_detail.is_printed', true)
                    ->whereNull('cutting_detail.deleted_at');
        
        if (count($data_req) > 0) {
            foreach ($data_req as $value) {
                $rbarcode['barcode_id'] = $value->barcode_id;
                $arr_barcode[] = $rbarcode;
            }

            $data = $data->whereIn('cutting_detail.barcode_id', $arr_barcode);
        }

        $data = $data->get();

        $data_header = array();
        $data_formulir = array();

        // $header['po_number'] = $data[0]->po_number;
        $header['created_at'] = Carbon::now();
        $header['updated_at'] = Carbon::now();
        $header['user_by'] = Auth::user()->id;
        $header['no_polybag'] = $no_polybag;
        $data_header[] = $header;

        $ponumber = $data[0]->po_number;

        // get type pl id reject
        $types = DB::table('type_packing_list')
                    ->where('type_name', 'REJECT')
                    ->whereNull('deleted_at')
                    ->first();

        try {
            
            DB::beginTransaction();

                // cek exist no polybag
                $cekpolybagifexist = DB::table('formulir_artwork_header')
                                        ->where('no_polybag', $no_polybag)
                                        ->whereRaw('date(created_at) = ?', [Carbon::now()->format('Y-m-d')])
                                        ->whereNull('deleted_at')
                                        ->exists();

                if ($cekpolybagifexist) {
                    throw new \Exception('Something wrong (polybag already exists)..! ');
                }

                // insert header if exist
                $cekheaderifexist = DB::table('formulir_artwork_header')
                                        ->whereRaw('date(created_at) = ?', [Carbon::now()->format('Y-m-d')])
                                        ->where('no_polybag', $no_polybag)
                                        ->exists();
                if (!$cekheaderifexist) {
                    DB::table('formulir_artwork_header')
                            ->insert($data_header);
                }else {
                    throw new \Exception('Something wrong (formulir already created)..! ');
                }

                $LastInsertId = DB::getPdo()->lastInsertId();

                $qty_used = 0;
                $qty_used_draft = 0;

                foreach ($data as $key => $value) {
                    // if ($value->po_number != $ponumber) {
                    //     throw new \Exception('Something wrong (different PO)..! ');
                    // }

                    if ($value->barcode_reject == null || $value->barcode_reject == '') {
                        throw new \Exception('Something wrong (sticker reject not found)..! ');
                    }
                    $barcodeid = $value->barcode_id;
                    //
                    $qty_used_draft =  $value->qty_used_draft > 0 ? $value->qty_used_draft : $value->qty-$value->qty_used;

                    $qty_used = $value->qty_used;

                    $qty_used = $qty_used + $qty_used_draft;

                    $qty_barcode_reject = count(explode(',', $value->barcode_reject));
        
                    $data_f = array(
                            'barcode_id' => $barcodeid,
                            'description' => $value->description,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                            'formulir_artwork_header_id' => $LastInsertId,
                            'qty' => $qty_barcode_reject,
                            'type_pl' => $value->type_pl_draft > 0 ? $value->type_pl_draft : $types->id, 
                            'sticker_reject' => $value->barcode_reject, 
                            'po_number' => $value->po_number 
                    );
        
                    $data_formulir[] = $data_f;

                    // update cutting detail
                    $update = DB::table('cutting_detail')
                                ->where('barcode_id', $barcodeid)
                                ->update([
                                    'qty_used' => $qty_used,
                                    'qty_used_draft' => $value->qty - $qty_used,
                                    'updated_at' => Carbon::now()
                                ]);
                    
                    if ($update) {
                        // cek qty used
                        $cekqtyused = DB::table('cutting_detail')
                                        ->where('barcode_id', $barcodeid)
                                        ->where(function ($query) {
                                            $query->where('qty_used', '<', 0)
                                                  ->orWhere('qty_used_draft', '<', 0);
                                        })
                                        ->exists();

                        if ($cekqtyused) {
                            throw new \Exception('Something wrong (qty_used invalid)..! ');
                        }
                    }
                    
                }

                
                foreach ($data_formulir as $key2 => $value2) {

                    // update cutting detail
                    DB::table('cutting_detail')
                        ->where('barcode_id', $value2['barcode_id'])
                        ->update([
                            'is_printed_formulir' => true,
                            'type_pl_draft' => 0,
                            'description' => null,
                            'updated_at' => Carbon::now()
                        ]);
                    
                    // cek exists
                    $cekifexist = DB::table('formulir_artwork')
                                    ->where('formulir_artwork_header_id', $LastInsertId)
                                    ->whereNull('deleted_at')
                                    ->exists();
                    
                    if ($cekifexist) {
                        // update formulir artwork
                        DB::table('formulir_artwork')
                            ->where('formulir_artwork_header_id', $LastInsertId)
                            ->update([
                                'updated_at' => Carbon::now()
                            ]);
                    }else{
                        // insert formulir artwork
                        DB::table('formulir_artwork')
                            ->insert($data_formulir);
                    }

                    // cek hari ini bikin tipe rejectnya apakah double
                    $cektypereject = DB::table('formulir_artwork')
                                    ->where('barcode_id', $value2['barcode_id'])
                                    ->where('type_pl', $value2['type_pl'])
                                    ->whereRaw('date(created_at) = ?', [Carbon::now()->format('Y-m-d')])
                                    ->whereNull('deleted_at')
                                    ->count();

                    if ($cektypereject>1) {
                    throw new \Exception('Something wrong (reject type already exists)..! ');
                    }

                    //check if package can be scanned or not
                    $check = $this->_check_scan($value2['barcode_id'], 'completed', 'checkout');
                    if(!$check) {
                        $error = $this->_show_error($value2['barcode_id']);
                        return response()->json($error,422);
                    }

                    // prepare table for table cutting detail and cutting_movements
                    $process_from = $check->current_process;
                    $status_from = $check->current_status;
                    $status_to = 'completed';
                    $process_to = 'reject';
                    $description = 'rejected on distribusi';
                    $data = array(
                                    'barcode_id' => $value2['barcode_id'],
                                    'process_from' => $process_from,
                                    'status_from' => $status_from,
                                    'process_to' => $process_to,
                                    'status_to' => $status_to,
                                    'user_id' => Auth::user()->id,
                                    'description' => $description,
                                    'ip_address' => \Request::ip(),
                                    'created_at' => Carbon::now() 
                                );

                    //query for table cutting_movements and cutting_detail
                    $query_movement = $this->_query_movement($value2['barcode_id'], 'completed', $data);
                    if(!$query_movement) {
                        throw new \Exception('Movement error');
                    }
                    $query_package = $this->_query_package($value2['barcode_id'], 'completed', $process_to, $status_to);
                    if(!$query_package) {
                        throw new \Exception('Package error');
                    }
                }

                // update factories
                // DB::table('factories')
                //     ->where('id', Auth::user()->factory_id)
                //     ->update([
                //         'no_polybag' => (int)$no_polybag,
                //         'updated_at' => Carbon::now()
                //     ]);

                // cek count no polybag double
                $cekpolybagcount = DB::table('formulir_artwork_header')
                                        ->where('no_polybag', $no_polybag)
                                        ->whereRaw('date(created_at) = ?', [Carbon::now()->format('Y-m-d')])
                                        ->whereNull('deleted_at')
                                        ->count();

                if ($cekpolybagcount>1) {
                    throw new \Exception('Something wrong (polybag double insert)..! ');
                }

            
            DB::commit();
            
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json($LastInsertId, 200);
        
    }

    public function printPreviewFormulir(Request $request)
    {
        // $data_req = json_decode(stripslashes($request->data));

        $formulir_id = $request->data;

        // $data = DB::table('cutting_detail')
        //             ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail.po_summary_detail_id')
        //             ->join('po_summary', 'po_summary.po_number', '=', 'po_summary_detail.po_number')
        //             ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
        //             ->whereNull('cutting_detail.deleted_at');
        
        // if (count($data_req) > 0) {
        //     foreach ($data_req as $value) {
        //         $rbarcode['barcode_id'] = $value->barcode_id;
        //         $arr_barcode[] = $rbarcode;
        //     }

        //     $data = $data->whereIn('cutting_detail.barcode_id', $arr_barcode);
        // }
        
        // $data = $data->orderByRaw('po_summary_detail.po_number, po_summary_detail.size, cutting_detail.cut_number, cutting_detail.part, cutting_detail.sticker_from ASC');

        $data = DB::table('v_formulir_new')
                    ->where('formulir_artwork_header_id', $formulir_id)
                    ->where('factory_id', Auth::user()->factory_id)
                    ->whereNull('deleted_at')
                    ->orderBy('po_number') 
                    ->orderBy('size')
                    ->orderBy('sticker_from');

        $data = $data->get();

        $data_arr = array();
        $newkey = 0;
        $total_qty = 0;

        $data_komponen_arr = array();
        $newkey2 = 0;

        foreach ($data as $key => $value) {
            $total_qty += $value->qty_formulir;
            $data_arr[$value->color.$value->article][$newkey] = $value;
            
            $newkey++;
        }

        foreach ($data as $key2 => $val2) {
            // $data_komponen_arr[$val2->color.$val2->article.$val2->komponen][$newkey2] = $val2;
            $data_komponen_arr[$val2->komponen][$newkey2] = $val2;
            $newkey2++;
        }

        $count_set = count($data_komponen_arr);
        //dd($count_set, $total_qty, $data);

        $factories = Factories::where('id', Auth::user()->factory_id) 
                                ->whereNull('deleted_at')
                                ->first();

        $pdf = new FormulirArtwork2($data, $factories, $data_arr, $total_qty, $count_set);

        $pdf->Output('I', 'formulir'.Carbon::now()->format('d_m_Y H:i:s').'.pdf', true);

        exit;
    }

    // print preview formulir reject
    public function printPreviewFormulirReject(Request $request)
    {
        // $data_req = json_decode(stripslashes($request->data));

        $formulir_id = $request->data;

        $data = DB::table('v_formulir_new')
                    ->where('formulir_artwork_header_id', $formulir_id)
                    ->where('factory_id', Auth::user()->factory_id)
                    ->whereNull('deleted_at')
                    ->orderBy('po_number') 
                    ->orderBy('size')
                    ->orderBy('sticker_from');

        $data = $data->get();

        $data_arr = array();
        $newkey = 0;
        $total_qty = 0;

        $data_komponen_arr = array();
        $newkey2 = 0;

        foreach ($data as $key => $value) {
            $total_qty += $value->qty_formulir;
            $data_arr[$value->color.$value->article][$newkey] = $value;
            
            $newkey++;
        }

        foreach ($data as $key2 => $val2) {
            $data_komponen_arr[$val2->color.$val2->article.$val2->komponen][$newkey2] = $val2;
            $newkey2++;
        }

        $count_set = count($data_komponen_arr);

        $factories = Factories::where('id', Auth::user()->factory_id) 
                                ->whereNull('deleted_at')
                                ->first();

        $pdf = new FormulirArtwork2Reject($data, $factories, $data_arr, $total_qty, $count_set);

        $pdf->Output('I', 'formulir_reject'.Carbon::now()->format('d_m_Y H:i:s').'.pdf', true);

        exit;
    }

    // print surat jalan
    public function setCompletedAllSuratJalan(Request $request)
    {
        $no_suratjalan = $request->no_suratjalan;
        $no_suratjalan_info = $request->no_suratjalan_info;
        $subcont_id = $request->subcont_id;
        $data_req = json_decode(stripslashes($request->data));

        $data = DB::table('v_surat_jalan')
                    ->where('is_delivery_printed', false)
                    ->where('factory_id', Auth::user()->factory_id);
        
        if (count($data_req) > 0) {
            foreach ($data_req as $value) {
                $formulir_artwork_header_id = $value->id;
                $rheader['formulir_artwork_header_id'] = $formulir_artwork_header_id;
                $arr_headerid[] = $rheader;
            }

            $data = $data->whereIn('formulir_artwork_header_id', $arr_headerid);
        }

        $data = $data->get();

        $data_header = array();
        $data_detail = array();

        $header['no_suratjalan'] = $no_suratjalan;
        $header['no_suratjalan_info'] = $no_suratjalan_info;
        $header['subcont_id'] = $subcont_id;
        $header['created_at'] = Carbon::now();
        $header['created_by'] = Auth::user()->id;
        $data_header[] = $header;

        $styles = $data[0]->style;

        $cekexist = DB::table('delivery_header')
                            ->where('no_suratjalan', $no_suratjalan)
                            ->whereNull('deleted_at')
                            ->exists();

        if ($cekexist) {
            return response()->json('No SJ already exist..!', 422);
        }

        try {
            
            DB::beginTransaction();

                // insert header
                $cekheaderifexist = DB::table('delivery_header')
                                        ->where('no_suratjalan', $no_suratjalan)
                                        // ->where('no_suratjalan_info', $no_suratjalan_info)
                                        ->whereNull('deleted_at')
                                        ->exists();
                if (!$cekheaderifexist) {
                    DB::table('delivery_header')
                            ->insert($data_header);
                }else{
                    // cek apakah sudah pernah dibuatkan sj global
                    $cekglobalexist = DB::table('delivery_reject')
                                        ->where('no_suratjalan', $no_suratjalan)
                                        ->whereNull('deleted_at')
                                        ->exists();

                    if ($cekglobalexist) {
                        throw new \Exception('Something wrong (no.sj already exists)..! ');
                    }
                    DB::table('delivery_header')
                            ->where('no_suratjalan', $no_suratjalan)
                            ->update([
                                'updated_at' => Carbon::now()
                            ]);
                }

                foreach ($data as $key => $value) {
                    if ($value->style != $styles) {
                        throw new \Exception('Something wrong (different Style)..! ');
                    }
                    $formulir_artwork_header_id = $value->formulir_artwork_header_id;
        
                    $data_d = array(
                                'no_suratjalan' => $no_suratjalan,
                                'formulir_id' => $formulir_artwork_header_id,
                                'created_at' => Carbon::now(),
                                'no_suratjalan_info' => $no_suratjalan_info
                    );
        
                    $data_detail[] = $data_d;
                    
                }

                foreach ($data_detail as $key2 => $value2) {

                    $gettings = DB::table('formulir_artwork')
                                    ->where('formulir_artwork_header_id', $value2['formulir_id'])
                                    ->get();

                    // update cutting detail
                    foreach ($gettings as $key3 => $val) {
                        DB::table('cutting_detail')
                            ->where('barcode_id', $val->barcode_id)
                            ->update([
                                'is_printed_sj' => true,
                                'updated_at' => Carbon::now()
                            ]);
                    }

                    // update formulir artwork header is printed
                    DB::table('formulir_artwork_header')
                        ->where('id', $value2['formulir_id'])
                        ->update([
                            'is_delivery_printed' => true,
                            'updated_at' => Carbon::now()
                        ]);
                    
                    // cek exists
                    $cekifexist = DB::table('delivery_detail')
                                    ->where('no_suratjalan', $value2['no_suratjalan'])
                                    // ->where('no_suratjalan_info', $no_suratjalan_info)
                                    ->wherenull('deleted_at')
                                    ->exists();
                    
                    if ($cekifexist) {
                        // update delivery detail
                        DB::table('delivery_detail')
                            ->where('no_suratjalan', $value2['no_suratjalan'])
                            ->update([
                                'updated_at' => Carbon::now()
                            ]);
                    }else{
                        // insert delivery detail
                        $insert = DB::table('delivery_detail')
                                    ->insert($data_detail);

                        
                        if ($insert) {
                            
                            // cek double insert
                            $cekformulirid = DB::table('delivery_detail')
                                            ->where('formulir_id', $value2['formulir_id'])
                                            ->whereNull('deleted_at')
                                            ->count();
                            if ($cekformulirid>1) {
                                throw new \Exception('Something wrong (formulir double insert)..! ');
                            }
                        }
                    }
                }

                // update factories
                // $factories = Factories::find(Auth::user()->factory_id);
                // $factories->no_suratjalan = (int)$this->_no_suratjalan()+1;

                // $factories->save();
                DB::table('factories')
                    ->where('id', Auth::user()->factory_id)
                    ->update([
                        'no_suratjalan' => (int)$no_suratjalan,
                        'updated_at' => Carbon::now()
                    ]);

                
                // cek double insert
                $cekheadercount = DB::table('delivery_header')
                                        ->where('no_suratjalan', $no_suratjalan)
                                        ->whereNull('deleted_at')
                                        ->count();
                if ($cekheadercount>1) {
                    throw new \Exception('Something wrong (no surat jalan double insert)..! ');
                }
            
            DB::commit();
            
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
        
    }

    public function printPreviewSuratJalan(Request $request)
    {
        $no_suratjalan = $request->no_suratjalan;
        $no_suratjalan_info = $request->no_suratjalan_info;
        $subcont_id = $request->subcont_id;
        $data_req = json_decode(stripslashes($request->data));

        $data_header = DB::table('delivery_header')
                        ->select(
                            'delivery_header.no_suratjalan',
                            'delivery_header.no_suratjalan_info',
                            'delivery_header.subcont_id',
                            'delivery_header.accepted_by',
                            'delivery_header.approved_by',
                            'delivery_header.delivered_by',
                            'delivery_header.deleted_at',
                            'delivery_header.created_by',
                            'delivery_header.deleted_by',
                            'delivery_header.created_at',
                            'delivery_header.updated_at',
                            'delivery_header.bc_no',
                            'subcont.name'
                        )
                        ->join('subcont', 'subcont.id', '=', 'delivery_header.subcont_id')
                        ->where('delivery_header.no_suratjalan', $no_suratjalan)
                        ->wherenull('delivery_header.deleted_at')
                        ->wherenull('subcont.deleted_at')
                        ->first();

        $data = DB::table('v_surat_jalan')
                    //->where('is_delivery_printed', false)
                    ->where('factory_id', Auth::user()->factory_id);
        
        if (count($data_req) > 0) {
            foreach ($data_req as $value) {
                $formulir_artwork_header_id = $value->id;
                $rheader['formulir_artwork_header_id'] = $formulir_artwork_header_id;
                $arr_headerid[] = $rheader;
            }

            $data = $data->whereIn('formulir_artwork_header_id', $arr_headerid);
        }

        $data = $data->orderBy('no_polybag')->get()->toArray();

        $result = array_reduce($data, function($carry, $item) { 
            if(!isset($carry[$item->po_number.$item->no_polybag])) {
                $carry[$item->po_number.$item->no_polybag] = $item;
            } else {
                $carry[$item->po_number.$item->no_polybag]->total_qty += $item->total_qty;
            }
            return $carry;
        });

        
        $result = $result !=null ? array_values($result) : array();

        $styles = $data[0]->style_edit;
        $type_name = $data[0]->type_name;

        $newkey = 0;
        $data_arr = array();

        foreach ($data as $key => $value) {
            $data_arr[$value->po_number][$newkey] = $value;
            $newkey++;
        }

        $factories = Factories::where('id', Auth::user()->factory_id)
                                ->whereNull('deleted_at')
                                ->first();

        $pdf = new SuratJalan2($data_header, $result, $no_suratjalan, $styles, $data_arr, $factories, $type_name);

        $pdf->Output('I', 'suratJalan'.Carbon::now()->format('d_m_Y H:i:s').'.pdf', true);

        exit;
    }

    // print surat jalan global
    public function setCompletedAllSuratJalanGlobal(Request $request)
    {
        $no_suratjalan = $request->no_suratjalan;
        $no_suratjalan_info = $request->no_suratjalan_info;
        $subcont_id = $request->subcont_id;
        $data = json_decode(stripslashes($request->data));

        $data_header = array();
        $data_detail = array();

        $header['no_suratjalan'] = $no_suratjalan;
        $header['no_suratjalan_info'] = $no_suratjalan_info;
        $header['subcont_id'] = $subcont_id;
        $header['created_at'] = Carbon::now();
        $header['created_by'] = Auth::user()->id;
        $header['is_global'] = true;
        $header['is_washing'] = Auth::user()->is_washing;
        $data_header[] = $header;

        try {
            
            DB::beginTransaction();

                // insert header
                $cekheaderifexist = DB::table('delivery_header')
                                        ->where('no_suratjalan', $no_suratjalan)
                                        // ->where('no_suratjalan_info', $no_suratjalan_info)
                                        ->whereNull('deleted_at')
                                        ->exists();
                if (!$cekheaderifexist) {
                    DB::table('delivery_header')
                            ->insert($data_header);
                }else{
                     // cek apakah sudah pernah dibuatkan sj general
                     $cekgeneralexist = DB::table('delivery_detail')
                                        ->where('no_suratjalan', $no_suratjalan)
                                        ->whereNull('deleted_at')
                                        ->exists();

                    if ($cekgeneralexist) {
                        throw new \Exception('Something wrong (no.sj already exists)..! ');
                    }
                    DB::table('delivery_header')
                            ->where('no_suratjalan', $no_suratjalan)
                            ->update([
                                'updated_at' => Carbon::now()
                            ]);
                }

                for ($i=0; $i < count($data); $i++) {

                    $style = $data[$i][0];
                    $qty = $data[$i][1];
                    //$description = $data[$i][2];
        
                    $data_d = array(
                                'no_suratjalan' => $no_suratjalan,
                                'no_suratjalan_info' => $no_suratjalan_info,
                                'style' => $style,
                                'qty' => $qty,
                                //'description' => $description,
                                'created_at' => Carbon::now(),
                                'subcont_id' => $subcont_id,
                                'is_washing' => Auth::user()->is_washing
                    );
        
                    $data_detail[] = $data_d;
                    
                }

                foreach ($data_detail as $key2 => $value2) {
                    
                    // cek exists
                    $cekifexist = DB::table('delivery_reject')
                                    ->where('no_suratjalan', $value2['no_suratjalan'])
                                    ->wherenull('deleted_at')
                                    ->exists();
                    
                    if ($cekifexist) {
                        // update delivery reject
                        DB::table('delivery_reject')
                            ->where('no_suratjalan', $value2['no_suratjalan'])
                            ->update([
                                'updated_at' => Carbon::now()
                            ]);
                    }else{
                        // insert delivery detail
                        $insert = DB::table('delivery_reject')
                                    ->insert($data_detail);

                        
                        if ($insert) {
                            
                            // cek double insert
                            $cekdouble = DB::table('delivery_reject')
                                            ->where('style', $value2['style'])
                                            ->where('subcont_id', $subcont_id)
                                            // ->whereRaw('date(created_at) = ?', [Carbon::now()->format('Y-m-d')])
                                            ->where('created_at', Carbon::now())
                                            ->whereNull('deleted_at')
                                            ->count();
                            if ($cekdouble>1) {
                                throw new \Exception('Something wrong (surat jalan double insert)..! ');
                            }
                        }
                    }
                }

                // update factories
                DB::table('factories')
                    ->where('id', Auth::user()->factory_id)
                    ->update([
                        'no_suratjalan' => (int)$no_suratjalan,
                        'updated_at' => Carbon::now()
                    ]);

                
                // cek double insert
                $cekheadercount = DB::table('delivery_header')
                                        ->where('no_suratjalan', $no_suratjalan)
                                        ->whereNull('deleted_at')
                                        ->count();
                if ($cekheadercount>1) {
                    throw new \Exception('Something wrong (no surat jalan double insert)..! ');
                }
            
            DB::commit();
            
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
        
    }

    public function printPreviewSuratJalanGlobal(Request $request)
    {
        $no_suratjalan = $request->no_suratjalan;
        $no_suratjalan_info = $request->no_suratjalan_info;
        $subcont_id = $request->subcont_id;

        $data_header = DB::table('delivery_header')
                        ->select(
                            'delivery_header.no_suratjalan',
                            'delivery_header.no_suratjalan_info',
                            'delivery_header.subcont_id',
                            'delivery_header.accepted_by',
                            'delivery_header.approved_by',
                            'delivery_header.delivered_by',
                            'delivery_header.deleted_at',
                            'delivery_header.created_by',
                            'delivery_header.deleted_by',
                            'delivery_header.created_at',
                            'delivery_header.updated_at',
                            'delivery_header.bc_no',
                            'subcont.name'
                        )
                        ->join('subcont', 'subcont.id', '=', 'delivery_header.subcont_id')
                        ->where('delivery_header.no_suratjalan', $no_suratjalan)
                        ->wherenull('delivery_header.deleted_at')
                        ->wherenull('subcont.deleted_at')
                        ->first();

        $data = DB::table('delivery_reject')
                    ->where('no_suratjalan', $request->no_suratjalan)
                    ->whereNull('deleted_at');
    

        $data = $data->orderBy('style')->get()->toArray();

        $factories = Factories::where('id', Auth::user()->factory_id)
                                ->whereNull('deleted_at')
                                ->first();

        $pdf = new SuratJalan2Global($data_header, $no_suratjalan, $data, $factories);

        $pdf->Output('I', 'suratJalan_global'.Carbon::now()->format('d_m_Y H:i:s').'.pdf', true);

        exit;
    }

    public function getNomorSuratJalan()
    {
        $data = $this->_no_suratjalan();

        return response()->json($data, 200);
    }

    // no surat jalan
    static function _no_suratjalan()
    {
        $data = Factories::where('id', Auth::user()->factory_id)
                            ->whereNull('deleted_at')
                            ->first();
        
        // if ($data == null) {
        //     $string = 1;
        // }else {
            $string = $data->no_suratjalan+1;
        // }

        $string = str_pad($string, 4, "0", STR_PAD_LEFT);

        return $string;
    }

    // no polybag
    public function getNomorPolyBag()
    {
        $data = $this->_no_polybag();

        return response()->json($data, 200);
    }

    // no polybag
    static function _no_polybag()
    {
        $data = Factories::where('id', Auth::user()->factory_id)
                            ->whereNull('deleted_at')
                            ->first();
        
        $string = $data->no_polybag+1;

        return (int)$string;
    }

    //check if package can be scanned or not
    private function _check_scan($barcodeid, $check_status, $type) {

        if($type == 'checkin') {
            if($check_status !== 'onprogress') {
                return false;
            }
        }
        elseif($type == 'checkout') {
            if($check_status !== 'completed' && $check_status !== 'cancel') {
                return false;
            }
        }

        //proses check
        if($check_status == 'completed') {
            $check = DB::table('cutting_detail')
                        ->select('cutting_detail.current_status', 'cutting_detail.current_process', 'po_summary.po_number', 'cutting_detail.komponen', 'cutting_detail.cut_number', 'cutting_detail.sticker_no', 'po_summary_detail.po_number_edit', 'po_summary_detail.style_edit', 'po_summary_detail.color_edit', 'po_summary_detail.article_edit', 'po_summary_detail.size_edit', 'cutting_detail.qty', DB::raw('master_komponen.name AS komponen_name'))
                        ->join('master_komponen', 'master_komponen.id', '=', 'cutting_detail.komponen')
                        ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                        ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                        ->where('cutting_detail.barcode_id', $barcodeid)
                        ->where('po_summary.factory_id', Auth::user()->factory_id)
                        ->where(function($query){
                            $query->where(function($query){
                                $query->where('cutting_detail.current_process', 'distribusi')
                                        ->where('cutting_detail.current_status', 'onprogress');
                            })
                            ->orWhere(function($query){
                                $query->where('cutting_detail.current_process', 'reject')
                                        ->where('cutting_detail.current_status', 'completed');
                            });
                        })
                        // ->where('cutting_detail.current_process', 'distribusi')
                        // ->where('cutting_detail.current_status', 'onprogress')
                        ->whereNull('po_summary_detail.deleted_at')
                        ->whereNull('cutting_detail.deleted_at')
                        ->whereNull('po_summary.deleted_at')
                        ->first();

            if(!$check) {
                return false;
            }
            else {
                return $check;
            }
        }
        elseif($check_status == 'onprogress') {
            $check = DB::table('cutting_detail')
                        ->select('cutting_detail.current_status', 'cutting_detail.current_process', 'po_summary.po_number', 'cutting_detail.komponen', 'cutting_detail.cut_number', 'cutting_detail.sticker_no', 'po_summary_detail.po_number_edit', 'po_summary_detail.style_edit', 'po_summary_detail.color_edit', 'po_summary_detail.article_edit', 'po_summary_detail.size_edit', 'cutting_detail.qty', DB::raw('master_komponen.name AS komponen_name'))
                        ->join('master_komponen', 'master_komponen.id', '=', 'cutting_detail.komponen')
                        ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                        ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                        ->where('cutting_detail.barcode_id', $barcodeid)
                        ->where('po_summary.factory_id', Auth::user()->factory_id)
                        ->where('cutting_detail.current_process', 'distribusi')
                        ->where('cutting_detail.current_status', 'completed')
                        ->whereNull('po_summary_detail.deleted_at')
                        ->whereNull('cutting_detail.deleted_at')
                        ->whereNull('po_summary.deleted_at')
                        ->first();

            if(!$check) {
                return false;
            }
            else {
                return $check;
            }
        }
        elseif($check_status == 'cancel') {
            $check = DB::table('cutting_detail')
                        ->select('cutting_detail.current_status', 'cutting_detail.current_process', 'po_summary.po_number', 'cutting_detail.komponen', 'cutting_detail.cut_number', 'cutting_detail.sticker_no', 'po_summary_detail.po_number_edit', 'po_summary_detail.style_edit', 'po_summary_detail.color_edit', 'po_summary_detail.article_edit', 'po_summary_detail.size_edit', 'cutting_detail.qty', DB::raw('master_komponen.name AS komponen_name'))
                        ->join('master_komponen', 'master_komponen.id', '=', 'cutting_detail.komponen')
                        ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                        ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                        ->where('cutting_detail.barcode_id', $barcodeid)
                        ->where('po_summary.factory_id', Auth::user()->factory_id)
                        ->where('cutting_detail.current_process', 'reject')
                        ->where('cutting_detail.current_status', 'completed')
                        ->whereNull('po_summary_detail.deleted_at')
                        ->whereNull('cutting_detail.deleted_at')
                        ->whereNull('po_summary.deleted_at')
                        ->first();

            if(!$check) {
                return false;
            }
            else {
                return $check;
            }
        }
        else {
            return false;
        }
    }

    //show error
    private function _show_error($barcodeid) {
        $error = '';
        $check_current_status = $this->_get_current_status($barcodeid);
        if($check_current_status) {
            if($check_current_status->current_status == 'onprogress') {
                $check_current_status->current_status = 'in';
            }
            elseif($check_current_status->current_status == 'completed') {
                $check_current_status->current_status = 'out';
            }
            $error .= 'Last Scanned (' . $check_current_status->barcode_id . ') : '
                        . $check_current_status->current_process . ' - '
                        . $check_current_status->current_status;
        }
        else {
            $error = 'Barcode not found on system';
        }

        return $error;
    }

    // get current status
    private function _get_current_status($barcodeid)
    {
        $data = DB::table('cutting_detail')
                    ->select('barcode_id', 'current_status', 'current_process')
                    ->where('barcode_id', $barcodeid)
                    ->whereNull('deleted_at')
                    ->first();
        return $data;
    }

    // query movement
    private function _query_movement($barcodeid, $check_status, $data_movement=null)
    {
        if ($check_status == 'completed' || $check_status == 'onprogress') {
            try {
                DB::beginTransaction();
                    // update deleted_at on last cutting movement
                    $last_data = DB::table('cutting_movements')
                                    ->where('barcode_id', $barcodeid)
                                    ->where('is_canceled', false)
                                    ->whereNull('deleted_at')
                                    ->get()
                                    ->toArray();
                    foreach ($last_data as $key => $value) {
                        if (!$value) {
                            throw new Exception('data not found');
                        }else {
                            $update_m = DB::table('cutting_movements')
                                            ->where('id', $value->id)
                                            ->update([
                                                'deleted_at' => Carbon::now()
                                            ]);
                            if (!$update_m) {
                                throw new Exception('the last data nothing updated');
                            }
                        }
                    }
                    // insert data movement
                    $insert = DB::table('cutting_movements')->insert($data_movement);

                    if ($insert) {
                        // cek apakah insert double ?
                        $cek_insert = DB::table('cutting_movements')
                                        ->where('barcode_id', $barcodeid)
                                        ->where('is_canceled', false)
                                        ->whereNull('deleted_at')
                                        ->count();

                        if ($cek_insert > 1) {
                            throw new \Exception('data double inserted');
                        }
                    }
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return true;

        }elseif ($check_status == 'cancel') {
            try {
                DB::beginTransaction();

                /*
                    - get second last data from table cutting_movements
                    - get last data from table cutting_movements
                    - update second last data, deleted_at = null
                    - update last data, deleted_at = now() and is_canceled = true
                */
                $second_last_data = DB::table('cutting_movements')
                                        ->where('barcode_id', $barcodeid)
                                        ->where('is_canceled', false)
                                        ->whereNotNull('deleted_at')
                                        ->orderBy('created_at', 'desc')
                                        ->first();

                $last_data = DB::table('cutting_movements')
                                 ->where('barcode_id', $barcodeid)
                                 ->where('is_canceled', false)
                                 ->whereNull('deleted_at')
                                 ->first();

                DB::table('cutting_movements')
                    ->where('id', $second_last_data->id)
                    ->update([
                        'deleted_at' => null
                    ]);

                DB::table('cutting_movements')
                    ->where('id', $last_data->id)
                    ->update([
                        'is_canceled' => true,
                        'deleted_at' => Carbon::now()
                    ]);

                DB::commit();
            }
            catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return true;

        }else{
            return false;
        }
    }

    //query package
    private function _query_package($barcodeid, $check_status, $process, $status) {

            if($check_status == 'completed' || $check_status == 'onprogress' || $check_status == 'cancel') {
                // get qty
                $data_barcode = DB::table('cutting_detail')
                                    ->where('barcode_id', $barcodeid)
                                    ->first();

                $update_clause = array(
                    'current_process' => $process,
                    'current_status' => $status,
                    'updated_at' => Carbon::now(),
                    'qty_used_draft' => $data_barcode->qty,
                    'qty_used' => 0,
                    'type_pl_draft' => 0,
                    'barcode_reject' => null
                );
            }
            else {
                return false;
            }

        try {
            DB::beginTransaction();

            DB::table('cutting_detail')
                ->where('barcode_id', $barcodeid)
                ->whereNull('deleted_at')
                ->update($update_clause);

            DB::commit();
        }
        catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return true;
    }
}
