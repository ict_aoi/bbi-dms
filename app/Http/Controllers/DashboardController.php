<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;
use Excel;
use Carbon\Carbon;
use App\Models\Factories;

class DashboardController extends Controller
{

    public function index(Request $request)
    {
        //
        $factory = Factories::select('id', 'factory_name')
                ->wherenull('deleted_at')
                ->get();

        $factory_id = $request->factory_id ? $request->factory_id : Auth::user()->factory_id;

        // count sticker barcoding onprogress & completed
        $count_sticker_ready = DB::table('cutting_detail_style')
                                ->select('cutting_detail_style.barcode_id')
                                ->join('po_summary_detail', 'po_summary_detail.id', '=', 'cutting_detail_style.po_summary_detail_id')
                                ->join('master_style_detail', function($join){
                                    $join->on('master_style_detail.style', '=', 'cutting_detail_style.style')
                                        ->on('master_style_detail.komponen', '=', 'cutting_detail_style.komponen');
                                })
                                ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                                ->where('cutting_detail_style.current_process', 'barcoding')
                                ->where(function($query){
                                    $query->where('cutting_detail_style.current_status', 'onprogress')
                                            ->orWhere('cutting_detail_style.current_status', 'completed');
                                })
                                ->whereNull('master_style_detail.deleted_at')
                                ->whereNull('po_summary.deleted_at')
                                ->where('po_summary.factory_id', $factory_id)
                                // ->where('master_style_detail.is_inhouse', false)
                                ->count();
        
        // count sticker onprogress & completed
        $count_sticker = DB::table('cutting_detail_style')
                            ->select('cutting_detail_style.barcode_id')
                            ->join('po_summary_detail', 'po_summary_detail.id', '=', 'cutting_detail_style.po_summary_detail_id')
                            ->join('master_style_detail', function($join){
                                $join->on('master_style_detail.style', '=', 'cutting_detail_style.style')
                                    ->on('master_style_detail.komponen', '=', 'cutting_detail_style.komponen');
                            })
                            ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                            ->where('cutting_detail_style.current_process', 'distribusi')
                            ->where(function($query){
                                $query->where('cutting_detail_style.current_status', 'onprogress')
                                        ->orWhere('cutting_detail_style.current_status', 'completed');
                            })
                            ->whereNull('master_style_detail.deleted_at')
                            ->whereNull('po_summary.deleted_at')
                            ->where('po_summary.factory_id', $factory_id)
                            // ->where('master_style_detail.is_inhouse', false)
                            ->count();

        // count sticker out
        $count_out = DB::table('cutting_detail_style')
                            ->select('cutting_detail_style.barcode_id')
                            ->join('po_summary_detail', 'po_summary_detail.id', '=', 'cutting_detail_style.po_summary_detail_id')
                            ->join('master_style_detail', function($join){
                                $join->on('master_style_detail.style', '=', 'cutting_detail_style.style')
                                    ->on('master_style_detail.komponen', '=', 'cutting_detail_style.komponen');
                            })
                            ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                            ->where('cutting_detail_style.current_process', 'distribusi')
                            ->where('cutting_detail_style.current_status', 'completed')
                            ->whereNull('master_style_detail.deleted_at')
                            ->whereNull('po_summary.deleted_at')
                            ->where('po_summary.factory_id', $factory_id)
                            // ->where('master_style_detail.is_inhouse', false)
                            ->count();

        // count sticker in
        $count_in = DB::table('cutting_detail_style')
                            ->select('cutting_detail_style.barcode_id')
                            ->join('po_summary_detail', 'po_summary_detail.id', '=', 'cutting_detail_style.po_summary_detail_id')
                            ->join('master_style_detail', function($join){
                                $join->on('master_style_detail.style', '=', 'cutting_detail_style.style')
                                    ->on('master_style_detail.komponen', '=', 'cutting_detail_style.komponen');
                            })
                            ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                            ->where('cutting_detail_style.current_process', 'distribusi')
                            ->where('cutting_detail_style.current_status', 'onprogress')
                            ->whereNull('master_style_detail.deleted_at')
                            ->whereNull('po_summary.deleted_at')
                            ->where('po_summary.factory_id', $factory_id)
                            // ->where('master_style_detail.is_inhouse', false)
                            ->count();

        return view('dashboard/index')->with([
                                                'count_sticker_ready' => $count_sticker_ready,
                                                'count_sticker' => $count_sticker,
                                                'count_out' => $count_out,
                                                'count_in' => $count_in
                                            ]);
    }

    public function getData(Request $request)
    {
        $po_number = $request->po;
        $style = $request->style;
        $radio_status = $request->radio_status;

        if ($radio_status) {
            if ($radio_status == 'po') {

                if (empty($po_number)) {
                    $po_number = 'null';
                }

                $data = DB::table('po_summary')
                        ->select(
                            'po_summary.po_number',
                            'po_summary.style',
                            'po_summary.dateordered',
                            'po_summary.datepromised',
                            'po_summary.city_name',
                            'po_summary_detail.color',
                            'po_summary_detail.article',
                            'po_summary_detail.article_edit',
                            'po_summary_detail.style',
                            'po_summary_detail.style_edit',
                            'po_summary_detail.size',
                            'po_summary_detail.c_orderline_id'
                        )                
                        ->join('po_summary_detail', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                        ->where('po_summary.factory_id', Auth::user()->factory_id)
                        ->where('po_summary.po_number', 'like', '%'.$po_number.'%')
                        ->whereNull('po_summary.deleted_at')
                        ->whereNull('po_summary_detail.deleted_at')
                        ->groupBy(
                            'po_summary.po_number',
                            'po_summary.style',
                            'po_summary.dateordered',
                            'po_summary.datepromised',
                            'po_summary.city_name',
                            'po_summary_detail.color',
                            'po_summary_detail.article',
                            'po_summary_detail.article_edit',
                            'po_summary_detail.style',
                            'po_summary_detail.style_edit',
                            'po_summary_detail.size',
                            'po_summary_detail.c_orderline_id'
                        );

            return Datatables::of($data)
                   ->addColumn('qty', function($data) {
                        // count per po    
                        $count_sticker = DB::table('cutting_detail')
                                    ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                                    ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                                    ->where('po_summary.po_number', $data->po_number)
                                    ->where('po_summary_detail.c_orderline_id', $data->c_orderline_id)
                                    ->where('po_summary_detail.style', $data->style)
                                    ->where('po_summary_detail.size', $data->size)
                                    ->whereNull('po_summary_detail.deleted_at')
                                    ->whereNull('cutting_detail.deleted_at')
                                    ->whereNull('po_summary.deleted_at')
                                    ->count();

                        return $count_sticker;

                    })
                    ->addColumn('status_distribusi', function($data){
                         // count out    
                         $count_out = DB::table('cutting_detail')
                                            ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                                            ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                                            ->where('po_summary.po_number', $data->po_number)
                                            ->where('po_summary_detail.c_orderline_id', $data->c_orderline_id)
                                            ->where('po_summary_detail.style', $data->style)
                                            ->where('po_summary_detail.size', $data->size)
                                            ->where('cutting_detail.current_process', 'distribusi')
                                            ->where('cutting_detail.current_status', 'completed')
                                            ->whereNull('po_summary_detail.deleted_at')
                                            ->whereNull('cutting_detail.deleted_at')
                                            ->whereNull('po_summary.deleted_at')
                                            ->count();
                         // count in    
                         $count_in = DB::table('cutting_detail')
                                            ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                                            ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                                            ->where('po_summary.po_number', $data->po_number)
                                            ->where('po_summary_detail.c_orderline_id', $data->c_orderline_id)
                                            ->where('po_summary_detail.style', $data->style)
                                            ->where('po_summary_detail.size', $data->size)
                                            ->where('cutting_detail.current_process', 'distribusi')
                                            ->where('cutting_detail.current_status', 'onprogress')
                                            ->whereNull('po_summary_detail.deleted_at')
                                            ->whereNull('cutting_detail.deleted_at')
                                            ->whereNull('po_summary.deleted_at')
                                            ->count();
                         // count preparation onprogress    
                         $count_preparation_onprogress = DB::table('cutting_detail')
                                                            ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                                                            ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                                                            ->where('po_summary.po_number', $data->po_number)
                                                            ->where('po_summary_detail.c_orderline_id', $data->c_orderline_id)
                                                            ->where('po_summary_detail.style', $data->style)
                                                            ->where('po_summary_detail.size', $data->size)
                                                            ->where('cutting_detail.current_process', 'barcoding')
                                                            ->where('cutting_detail.current_status', 'onprogress')
                                                            ->whereNull('po_summary_detail.deleted_at')
                                                            ->whereNull('cutting_detail.deleted_at')
                                                            ->whereNull('po_summary.deleted_at')
                                                            ->count();
                         // count preparation completed    
                         $count_preparation_completed = DB::table('cutting_detail')
                                                            ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                                                            ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                                                            ->where('po_summary.po_number', $data->po_number)
                                                            ->where('po_summary_detail.c_orderline_id', $data->c_orderline_id)
                                                            ->where('po_summary_detail.style', $data->style)
                                                            ->where('po_summary_detail.size', $data->size)
                                                            ->where('cutting_detail.current_process', 'barcoding')
                                                            ->where('cutting_detail.current_status', 'completed')
                                                            ->whereNull('po_summary_detail.deleted_at')
                                                            ->whereNull('cutting_detail.deleted_at')
                                                            ->whereNull('po_summary.deleted_at')
                                                            ->count();
                        $data = '<span class="badge badge-default position-right">'.$count_preparation_onprogress.'</span>
                                <span class="badge badge-primary position-right">'.$count_preparation_completed.'</span>
                                <span class="badge badge-danger position-right">'.$count_out.'</span>
                                <span class="badge badge-success position-right">'.$count_in.'</span>';

                        return $data;
                    })
                    ->addColumn('action', function($data) {
                        $view_package = route('viewPackageByOrderLine',
                                            [
                                                'style' => $data->style,
                                                'size' => $data->size,
                                                'c_orderline_id' => $data->c_orderline_id
                                            ]
                                        );

                        return '<a type="button" class="btn btn-default" target="_blank" href="' . $view_package .'" title="View Detail"><i class="icon-stack2"></a>';
                    })
                    ->rawColumns(['status_distribusi','qty','action'])
                    ->make(true);

            }elseif ($radio_status == 'style') {

                if (empty($style)) {
                    $style = 'null';
                }
                
                $data = DB::table('po_summary')
                        ->join('po_summary_detail', 'po_summary_detail.po_summary_id', '=', 'po_summary.id')
                        ->where('po_summary.factory_id', Auth::user()->factory_id)
                        ->where('po_summary.style', 'like', '%'.$style.'%')
                        ->whereNull('po_summary.deleted_at')
                        ->whereNull('po_summary_detail.deleted_at');

            return Datatables::of($data)
                   ->addColumn('qty', function($data) {
                        // count per po    
                        $count_sticker = DB::table('cutting_detail')
                                    ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                                    ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                                    ->where('po_summary.style', $data->style)
                                    ->where('po_summary_detail.c_orderline_id', $data->c_orderline_id)
                                    // ->where('po_summary_detail.style', $data->style)
                                    ->where('po_summary_detail.size', $data->size)
                                    ->whereNull('po_summary_detail.deleted_at')
                                    ->whereNull('cutting_detail.deleted_at')
                                    ->whereNull('po_summary.deleted_at')
                                    ->count();

                        return $count_sticker;

                    })
                    ->addColumn('status_distribusi', function($data){
                         // count out    
                         $count_out = DB::table('cutting_detail')
                                            ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                                            ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                                            ->where('po_summary.style', $data->style)
                                            ->where('po_summary_detail.c_orderline_id', $data->c_orderline_id)
                                            ->where('po_summary_detail.size', $data->size)
                                            ->where('cutting_detail.current_process', 'distribusi')
                                            ->where('cutting_detail.current_status', 'completed')
                                            ->whereNull('po_summary_detail.deleted_at')
                                            ->whereNull('cutting_detail.deleted_at')
                                            ->whereNull('po_summary.deleted_at')
                                            ->count();
                         // count in    
                         $count_in = DB::table('cutting_detail')
                                            ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                                            ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                                            ->where('po_summary.style', $data->style)
                                            ->where('po_summary_detail.c_orderline_id', $data->c_orderline_id)
                                            ->where('po_summary_detail.size', $data->size)
                                            ->where('cutting_detail.current_process', 'distribusi')
                                            ->where('cutting_detail.current_status', 'onprogress')
                                            ->whereNull('po_summary_detail.deleted_at')
                                            ->whereNull('cutting_detail.deleted_at')
                                            ->whereNull('po_summary.deleted_at')
                                            ->count();
                         // count preparation onprogress    
                         $count_preparation_onprogress = DB::table('cutting_detail')
                                                            ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                                                            ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                                                            ->where('po_summary.style', $data->style)
                                                            ->where('po_summary_detail.c_orderline_id', $data->c_orderline_id)
                                                            ->where('po_summary_detail.size', $data->size)
                                                            ->where('cutting_detail.current_process', 'barcoding')
                                                            ->where('cutting_detail.current_status', 'onprogress')
                                                            ->whereNull('po_summary_detail.deleted_at')
                                                            ->whereNull('cutting_detail.deleted_at')
                                                            ->whereNull('po_summary.deleted_at')
                                                            ->count();
                         // count preparation completed    
                         $count_preparation_completed = DB::table('cutting_detail')
                                                            ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                                                            ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                                                            ->where('po_summary.style', $data->style)
                                                            ->where('po_summary_detail.c_orderline_id', $data->c_orderline_id)
                                                            ->where('po_summary_detail.size', $data->size)
                                                            ->where('cutting_detail.current_process', 'barcoding')
                                                            ->where('cutting_detail.current_status', 'completed')
                                                            ->whereNull('po_summary_detail.deleted_at')
                                                            ->whereNull('cutting_detail.deleted_at')
                                                            ->whereNull('po_summary.deleted_at')
                                                            ->count();
                        $data = '<span class="badge badge-default position-right">'.$count_preparation_onprogress.'</span>
                                <span class="badge badge-primary position-right">'.$count_preparation_completed.'</span>
                                <span class="badge badge-danger position-right">'.$count_out.'</span>
                                <span class="badge badge-success position-right">'.$count_in.'</span>';

                        return $data;
                    })
                    ->addColumn('action', function($data) {
                        $view_package = route('viewPackageByOrderLine',
                                            [
                                                'style' => $data->style,
                                                'size' => $data->size,
                                                'c_orderline_id' => $data->c_orderline_id
                                            ]
                                        );

                        return '<a type="button" class="btn btn-default" target="_blank" href="' . $view_package .'" title="View Detail"><i class="icon-stack2"></a>';
                    })
                    ->rawColumns(['status_distribusi','qty','action'])
                    ->make(true);
            }else {
                $data = array();
                return Datatables::of($data)
                    ->addColumn('action', function($data) {
                        return null;
                        })
                        ->rawColumns(['status_distribusi','qty','action'])
                        ->make(true);
            }
        }

    }

    //HALAMAN VIEW DETAIL
    public function viewPackage(Request $request) {
        $po_number = trim($request->po_number);
        return view('dashboard/detail')->with('po_number',$po_number);
    }

    //DATA FOR DATATABLES VIEW DETAIL
    public function viewPackageData(Request $request) {
        $ponumber = trim($request->po_number);
        $data     = DB::table('cutting_detail')
                        ->select('cutting_detail.barcode_id', 'cutting_detail.current_status', 'cutting_detail.current_process', 'po_summary.po_number', 'cutting_detail.komponen', 'cutting_detail.cut_number', 'cutting_detail.sticker_no', 'po_summary.style', 'po_summary_detail.color', 'po_summary_detail.article', 'po_summary_detail.size')
                        ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                        ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                        ->where('po_summary.po_number', $ponumber)
                        ->whereNull('po_summary_detail.deleted_at')
                        ->whereNull('cutting_detail.deleted_at')
                        ->whereNull('po_summary.deleted_at')
                        ->orderBy('po_summary_detail.color')
                        ->orderBy('cutting_detail.sticker_from');

        return Datatables::of($data)
                    ->editColumn('current_process', function($data) {
                        $current_process = '<span class="label label-flat border-grey text-grey-600">'.$data->current_process.'</span>';

                        return $current_process;
                    })
                    ->editColumn('current_status', function($data) {
                        if($data->current_status == 'onprogress') {
                            $current_status = '<td><span class="label label-primary" id="current_status_'.$data->barcode_id.'">ON PROGRESS</span></td>';
                        }
                        elseif($data->current_status == 'completed') {
                            $current_status = '<td><span class="label label-success" id="current_status_'.$data->barcode_id.'">COMPLETED</span></td>';
                        }

                        return $current_status;
                    })
                    ->rawColumns(['current_status', 'current_process'])
                    ->make(true);
    }


    //HALAMAN VIEW DETAIL BY ID
    public function viewPackageById(Request $request) {
        $po_summary_id = $request->po_summary_id;
        return view('dashboard/detail_id')->with('po_summary_id',$po_summary_id);
    }

    //DATA FOR DATATABLES VIEW DETAIL BY ID
    public function viewPackageDataById(Request $request) {
        $po_summary_id = $request->po_summary_id;
        $data     = DB::table('cutting_detail')
                        ->select('cutting_detail.barcode_id', 'cutting_detail.current_status', 'cutting_detail.current_process', 'po_summary.po_number', 'cutting_detail.komponen', 'cutting_detail.cut_number', 'cutting_detail.sticker_no', 'po_summary.style', 'po_summary_detail.color', 'po_summary_detail.article', 'po_summary_detail.size', DB::raw('master_komponen.name AS komponen_name'))
                        ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                        ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                        ->join('master_komponen', 'master_komponen.id', '=', 'cutting_detail.komponen')
                        ->where('po_summary.id', $po_summary_id)
                        ->whereNull('po_summary_detail.deleted_at')
                        ->whereNull('cutting_detail.deleted_at')
                        ->whereNull('po_summary.deleted_at')
                        ->whereNull('master_komponen.deleted_at')
                        // ->orderBy('po_summary_detail.barcode_id')
                        ->orderBy('cutting_detail.komponen')
                        ->orderBy('cutting_detail.sticker_from');

        return Datatables::of($data)
                    ->editColumn('current_process', function($data) {
                        $current_process = '<span class="label label-flat border-grey text-grey-600">'.$data->current_process.'</span>';

                        return $current_process;
                    })
                    ->editColumn('current_status', function($data) {
                        if($data->current_status == 'onprogress') {
                            $current_status = '<td><span class="label label-primary" id="current_status_'.$data->barcode_id.'">ON PROGRESS</span></td>';
                        }
                        elseif($data->current_status == 'completed') {
                            $current_status = '<td><span class="label label-success" id="current_status_'.$data->barcode_id.'">COMPLETED</span></td>';
                        }

                        return $current_status;
                    })
                    ->rawColumns(['current_status', 'current_process'])
                    ->make(true);
    }

    //HALAMAN VIEW DETAIL BY ORDERLINE
    public function viewPackageByOrderLine(Request $request) {
        $style = $request->style;
        $size = $request->size;
        $c_orderline_id = $request->c_orderline_id;

        return view('dashboard/detail_orderline')->with([
                                                        'style' => $style,
                                                        'size' => $size,
                                                        'c_orderline_id' => $c_orderline_id
                                                    ]);
    }

    //DATA FOR DATATABLES VIEW DETAIL BY ID
    public function viewPackageDataByOrderLine(Request $request) {
        $style = $request->style;
        $size = $request->size;
        $c_orderline_id = $request->c_orderline_id;

        $data     = DB::table('cutting_detail')
                        ->select(
                            'cutting_detail.barcode_id', 
                            'cutting_detail.current_status', 
                            'cutting_detail.current_process', 
                            'po_summary.po_number', 
                            'cutting_detail.komponen', 
                            'cutting_detail.cut_number', 
                            'cutting_detail.sticker_no', 
                            'po_summary.style', 
                            'po_summary_detail.color', 
                            'po_summary_detail.article', 
                            'po_summary_detail.size', 
                            'po_summary_detail.style_edit', 
                            'po_summary_detail.po_number_edit', 
                            'po_summary_detail.color_edit', 
                            'po_summary_detail.article_edit', 
                            'po_summary_detail.size_edit', 
                            DB::raw('master_komponen.name AS komponen_name')
                        )
                        ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                        ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                        ->join('master_komponen', 'master_komponen.id', '=', 'cutting_detail.komponen')
                        ->where('po_summary_detail.style', $style)
                        ->where('po_summary_detail.size', $size)
                        ->where('po_summary_detail.c_orderline_id', $c_orderline_id)
                        ->whereNull('po_summary_detail.deleted_at')
                        ->whereNull('cutting_detail.deleted_at')
                        ->whereNull('po_summary.deleted_at')
                        ->whereNull('master_komponen.deleted_at')
                        // ->orderBy('po_summary_detail.barcode_id')
                        ->orderBy('cutting_detail.komponen')
                        ->orderBy('cutting_detail.sticker_from');

        return Datatables::of($data)
                    ->editColumn('current_process', function($data) {
                        $current_process = '<span class="label label-flat border-grey text-grey-600">'.$data->current_process.'</span>';

                        return $current_process;
                    })
                    ->editColumn('current_status', function($data) {
                        if($data->current_status == 'onprogress') {
                            $current_status = '<td><span class="label label-primary" id="current_status_'.$data->barcode_id.'">ON PROGRESS</span></td>';
                        }
                        elseif($data->current_status == 'completed') {
                            $current_status = '<td><span class="label label-success" id="current_status_'.$data->barcode_id.'">COMPLETED</span></td>';
                        }

                        return $current_status;
                    })
                    ->rawColumns(['current_status', 'current_process'])
                    ->make(true);
    }

    // update list komponen break
    public function updateListKomponenBreak(Request $request)
    {
        $list_komponen_break = $request->list_komponen_break;

        $request->validate([
            'list_komponen_break' => 'required|numeric',
        ]);

        try {
            DB::beginTransaction();
                // update factories
                DB::table('factories')
                        ->where('id', Auth::user()->factory_id)
                        ->update([
                            'list_komponen_break' => $list_komponen_break,
                            'updated_at' => Carbon::now()
                        ]);
            
            DB::commit();
            
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        $html = '<input type="text"
                        id="list_komponen_break"
                        class="form-control"
                        value="'.$list_komponen_break.'">';

        return response()->json($html, 200);
    }
}
