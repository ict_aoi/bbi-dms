<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use DataTable;
use Carbon\Carbon;
use Auth;
use DataTables;
use App\Models\Factories;

use App\Pdf\MonitoringDistribusi;

class DistribusiController extends Controller
{
    //
    public function index()
    {
        $factory = Factories::whereNull('deleted_at')->get();

        return view('report.distribusi.index')->with('factory', $factory);
    }

    public function getDataDistribusi(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;

        $data = DB::table('v_report_distribusi_2')
                ->where('factory_id', $factory_id);
        
        if ($request->radio_status == 'date') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where(function($query) use ($range) {
                                $query->whereBetween('scanout_date', [$range['from'], $range['to']])
                                        ->orWhereBetween('scanin_date', [$range['from'], $range['to']]);
                            });
        }elseif ($request->radio_status == 'po') {
            $po_number = $request->po_number == null ? ' ' : $request->po_number;
            $data = $data->where('po_number_edit', 'like', '%'.$po_number.'%');
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number_edit', 'like', '%'.$filterby.'%')
                                ->orWhere('size', 'like', '%'.$filterby.'%')
                                ->orWhere('color_edit', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
               ->make(true);
    }

    public function exportDistribusi(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $orderby = $request->orderby;
        $direction = $request->direction;

        $data = DB::table('v_report_distribusi_2')
                ->where('factory_id', $factory_id);
        
        if ($request->radio_status == 'date') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where(function($query) use ($range) {
                                $query->whereBetween('scanout_date', [$range['from'], $range['to']])
                                        ->orWhereBetween('scanin_date', [$range['from'], $range['to']]);
                            });
            
            $f_name = $range['from'] . '_until_' . $range['to'];
        
        }elseif ($request->radio_status == 'po') {
            $po_number = $request->po_number == null ? ' ' : $request->po_number;
            $data = $data->where('po_number_edit', 'like', '%'.$po_number.'%');

            $f_name = $po_number;
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number_edit', 'like', '%'.$filterby.'%')
                                ->orWhere('size', 'like', '%'.$filterby.'%')
                                ->orWhere('color_edit', 'like', '%'.$filterby.'%');
                    });
        }

        //naming file
        $get_factory = Factories::where('id', $factory_id)
                                    ->wherenull('deleted_at')
                                    ->first();

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);

            $filename = $get_factory->factory_name.'_report_distribusi_' . $f_name
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        }
        else {
            $data = $data->orderBy('scanout_date', 'asc')
                        ->orderBy('scanin_date', 'asc');
            
            $filename = $get_factory->factory_name.'_report_distribusi_' . $f_name;
        }

        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($data, $i) {
            $excel->sheet('report', function($sheet) use($data, $i) {
                $sheet->appendRow(array(
                    '#', 'Barcode ID', 'PO Number', 'Style', 'Color', 'Size', 'Komponen',
                    'Qty', 'Sticker No', 'Process', 'Scan Out', 'Scan In'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i)
                {
                    foreach ($rows as $row)
                    {
                        //
                        $sheet->appendRow(array(
                            $i++, '="'.$row->barcode_id.'"', $row->po_number_edit, $row->style_edit, $row->color_edit, $row->size_edit, $row->komponen_name, $row->qty, $row->sticker_no, $row->process_name, $row->scanout_date, $row->scanin_date
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);

    }

    // distribusi monitoring
    public function monitoring()
    {
        $factory = Factories::whereNull('deleted_at')->get();

        $list_po = DB::table('v_distribusi_completed')
                    ->select('po_number_edit')
                    ->groupBy('po_number_edit')
                    ->get();

        return view('report.distribusi.monitoring')->with([
                                                            'list_po'=> $list_po,
                                                            'factory'=> $factory
                                                        ]);
    }

    public function getDataDistribusiMonitoring(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;

        $data = DB::table('v_distribusi_completed')
                ->where('factory_id', $factory_id);
        
        if ($request->radio_status == 'date') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where(function($query) use ($range) {
                                $query->whereBetween('created_at', [$range['from'], $range['to']])
                                        ->orWhereBetween('scanin_date', [$range['from'], $range['to']]);
                            });
        }elseif ($request->radio_status == 'po') {
            $po_number = $request->po_number;
            $data = $data->where('po_number_edit', $po_number);
        }
        elseif ($request->radio_status == 'out') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range_out));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->whereBetween('created_at', [$range['from'], $range['to']]);
        }
        elseif ($request->radio_status == 'in') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range_in));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->whereBetween('scanin_date', [$range['from'], $range['to']]);
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query
                                ->orWhere('po_number_edit', 'like', '%'.$filterby.'%')
                                ->orWhere('size_edit', 'like', '%'.strtoupper($filterby).'%')
                                ->orWhere('color_edit', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
                ->addColumn('tanggal', function($data) use ($request){
                    if ($request->radio_status == 'out') {
                        return Carbon::parse($data->created_at)->format('d/m/Y');
                    }elseif ($request->radio_status == 'in') {
                        return Carbon::parse($data->scanin_date)->format('d/m/Y');
                    }else {
                        return Carbon::parse($data->created_at)->format('d/m/Y');
                    }
                })
                ->make(true);
    }

    public function exportDistribusiMonitoring(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $orderby = $request->orderby;
        $direction = $request->direction;

        $data = DB::table('v_distribusi_monitoring')
                ->where('factory_id', $factory_id);
        
        if ($request->radio_status == 'date') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where(function($query) use ($range) {
                                $query->whereBetween('created_at', [$range['from'], $range['to']])
                                        ->orWhereBetween('scanin_date', [$range['from'], $range['to']]);
                            });
            
            $f_name = $range['from'] . '_until_' . $range['to'];
        
        }elseif ($request->radio_status == 'po') {
            $po_number = $request->po_number;
            $data = $data->where('po_number_edit', $po_number);

            $f_name = $po_number;
        }
        elseif ($request->radio_status == 'out') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range_out));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->whereBetween('created_at', [$range['from'], $range['to']]);
            
            $f_name = $range['from'] . '_until_' . $range['to'].'_Out';
        
        }
        elseif ($request->radio_status == 'in') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range_in));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->whereBetween('scanin_date', [$range['from'], $range['to']]);
            
            $f_name = $range['from'] . '_until_' . $range['to'].'_In';
        
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query
                                // ->orWhere('po_number_edit', 'like', '%'.$filterby.'%')
                                ->orWhere('size_edit', 'like', '%'.strtoupper($filterby).'%')
                                ->orWhere('color_edit', 'like', '%'.$filterby.'%');
                    });
        }

        //naming file
        $get_factory = Factories::where('id', $factory_id)
                                    ->wherenull('deleted_at')
                                    ->first();

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);

            $filename = $get_factory->factory_name.'_monitoring_distribusi_' . $f_name
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        }
        else {
            $data = $data->orderBy('created_at', 'asc')
                        ->orderBy('scanin_date', 'asc');
            
            $filename = $get_factory->factory_name.'_monitoring_distribusi_' . $f_name;
        }

        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($data, $i) {
            $excel->sheet('report', function($sheet) use($data, $i) {
                $sheet->appendRow(array(
                    'Tanggal', 'Cutt', 'Color', 'Size', 'Qty', 'Scan Out', 'Scan In', 'Komponen', 'Sticker No'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i)
                {
                    foreach ($rows as $row)
                    {
                        //
                        $sheet->appendRow(array(
                            $i++, $row->po_number_edit, $row->style_edit, $row->color_edit, $row->size_edit, $row->qty, $row->created_at, $row->scanin_date, $row->komponen_name, $row->sticker_no
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);

    }

    public function exportDistribusiMonitoringPdf(Request $request)
    {
        // $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        // $orderby = $request->orderby;
        // $direction = $request->direction;

        $data = DB::table('v_distribusi_complete_2')
                ->where('factory_id', $factory_id)
                // ->where('po_number_edit', $request->po_number)
                ->where('style_edit', $request->style)
                ->where('color_edit', $request->color);

        // $data = $data->where('po_number_edit', $request->po)
        //         ->where('style_edit', $request->style)
        //         ->where('color_edit', $request->color);
        
        if ($request->radio_status == 'po') {
            $po_number = $request->po_number;
            $data = $data->where('po_number_edit', $po_number);

            $f_name = $po_number;
        }
        elseif ($request->radio_status == 'out') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range_out));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where('po_number_edit', $request->po_number)
                        ->whereBetween('created_at', [$range['from'], $range['to']]);
            
            $f_name = $range['from'] . '_until_' . $range['to'].'_Out';
        
        }

        //naming file
        $get_factory = Factories::where('id', $factory_id)
                                    ->wherenull('deleted_at')
                                    ->first();
            
        $filename = $get_factory->factory_name.'_monitoring_distribusi_' . $f_name;
        // }
        // $data = $data->where('po_number_edit', $request->po)
        //                 ->where('style_edit', $request->style)
        //                 ->where('color_edit', $request->color);

        if($request->process_name!= '' && $request->process_name!= null){
            $data = $data->where('process_name', $request->process_name);
        }


        $data = $data
                ->orderByRaw('date(created_at)')
                ->orderBy('size_edit')
                ->orderBy('cut_number')
                ->get()->toArray();

        $factories = Factories::where('id', Auth::user()->factory_id) 
                                ->whereNull('deleted_at')
                                ->first();
        
        $newkey = 0;
        $newkey2 = 0;
        $newkey3 = 0;
        $data_arr = array();
        $data_arr2 = array();

        foreach ($data as $key => $val) {
            $sco = Carbon::parse($val->created_at)->format('Y-m-d');
            // $sci = Carbon::parse($val->scanin_date)->format('Y-m-d');

            $art = $val->color_edit ? trim($val->color_edit) : trim($val->article_edit);

            if ($request->radio_status == 'out' || $request->radio_status == 'po') {
                $data_arr[$sco][$newkey] = $val;
            }
            // elseif ($request->radio_status == 'in') {
            //     $data_arr[$sci][$newkey] = $val;
            // }

            $newkey++;
        }

        // get size
        $sizes2 = array();
        $sizes = array();

        foreach ($data as $key2 => $item) {
            // $sizes[$item->size_edit][$newkey2] = $item->size_edit;
            // $newkey2++;
            $sizes[$item->size_edit][] = $item->size_edit;
        }

        // ksort($sizes, SORT_NUMERIC);

        foreach ($sizes as $key3 => $val3) {
            $sizes2[$newkey2] = $key3;
            
            $newkey2++;
        }

        $count_sizes = count($sizes2);

        $size_1 = isset($sizes2[0]) ? $sizes2[0] : '';
        $size_2 = isset($sizes2[1]) ? $sizes2[1] : '';
        $size_3 = isset($sizes2[2]) ? $sizes2[2] : '';
        $size_4 = isset($sizes2[3]) ? $sizes2[3] : '';
        $size_5 = isset($sizes2[4]) ? $sizes2[4] : '';
        $size_6 = isset($sizes2[5]) ? $sizes2[5] : '';
        $size_7 = isset($sizes2[6]) ? $sizes2[6] : '';
        $size_8 = isset($sizes2[7]) ? $sizes2[7] : '';
        $size_9 = isset($sizes2[8]) ? $sizes2[8] : '';

        $pdf = new MonitoringDistribusi($data, $data_arr, $factories, $request->radio_status, $request->style, $request->color, $size_1, $size_2, $size_3, $size_4, $size_5, $size_6, $size_7, $size_8, $size_9, $request->po_number, $request->process_name);

        $pdf->Output('I', $filename.'.pdf', true);

        exit;

    }

    public function getDistribusiMonitoringOut(Request $request)
    {
        $data = DB::table('v_distribusi_completed')
                ->select('po_number_edit', 'style_edit', 'color_edit');

        if ($request->radio_status == 'out') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range_out));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->whereBetween('created_at', [$range['from'], $range['to']]);
        
        }elseif ($request->radio_status == 'po') {

            $po_number = $request->po_number;
            $data = $data->where('po_number_edit', $po_number);
        }

        $data = $data->groupBy('po_number_edit', 'style_edit', 'color_edit')
                        ->get();

        return view('report.distribusi.list_distribusi_out')->with('data', $data);
    }

    // per process
    public function getDistribusiMonitoringOutProcess(Request $request)
    {
        $data = DB::table('v_distribusi_completed')
                ->select('po_number_edit', 'style_edit', 'color_edit', 'process_name');

        if ($request->radio_status == 'out') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range_out));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->whereBetween('created_at', [$range['from'], $range['to']]);
        
        }elseif ($request->radio_status == 'po') {

            $po_number = $request->po_number;
            $data = $data->where('po_number_edit', $po_number);
        }

        $data = $data->groupBy('po_number_edit', 'style_edit', 'color_edit', 'process_name')
                        ->get();

        return view('report.distribusi.list_distribusi_out_process')->with('data', $data);
    }


    // distribusi monitoring in
    public function monitoringIn()
    {
        $factory = Factories::whereNull('deleted_at')->get();

        return view('report.distribusi.monitoring_in')->with('factory', $factory);
    }

    public function getDataDistribusiMonitoringIn(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;

        $data = DB::table('v_distribusi_onprogress')
                ->where('factory_id', $factory_id);
        
        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range_in));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = $data->whereBetween('created_at', [$range['from'], $range['to']]);

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query
                                // ->orWhere('po_number_edit', 'like', '%'.$filterby.'%')
                                ->orWhere('size_edit', 'like', '%'.strtoupper($filterby).'%')
                                ->orWhere('color_edit', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
                ->addColumn('tanggal', function($data) use ($request){
                        return Carbon::parse($data->created_at)->format('d/m/Y');
                })
                ->make(true);
    }

    public function exportDistribusiMonitoringIn(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $orderby = $request->orderby;
        $direction = $request->direction;

        $data = DB::table('v_distribusi_onprogress')
                ->where('factory_id', $factory_id);

        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range_in));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
        );

        $data = $data->whereBetween('created_at', [$range['from'], $range['to']]);
        
        $f_name = $range['from'] . '_until_' . $range['to'];

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query
                                // ->orWhere('po_number_edit', 'like', '%'.$filterby.'%')
                                ->orWhere('size_edit', 'like', '%'.strtoupper($filterby).'%')
                                ->orWhere('color_edit', 'like', '%'.$filterby.'%');
                    });
        }

        //naming file
        $get_factory = Factories::where('id', $factory_id)
                                    ->wherenull('deleted_at')
                                    ->first();

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);

            $filename = $get_factory->factory_name.'_monitoring_distribusi_in_' . $f_name
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        }
        else {
            $data = $data->orderBy('created_at', 'asc');
            
            $filename = $get_factory->factory_name.'_monitoring_distribusi_in_' . $f_name;
        }

        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($data, $i) {
            $excel->sheet('report', function($sheet) use($data, $i) {
                $sheet->appendRow(array(
                    'Tanggal', 'Style', 'Color', 'Cut', 'Size', 'Qty', 'Sticker No'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i)
                {
                    foreach ($rows as $row)
                    {
                        //
                        $sheet->appendRow(array(
                            Carbon::parse($row->created_at)->format('d/m/Y'), $row->style_edit, $row->color_edit, $row->cut_number, $row->size_edit, $row->qty, $row->sticker_no
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);

    }
}
