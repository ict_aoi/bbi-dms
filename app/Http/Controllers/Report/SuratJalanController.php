<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use DataTable;
use Carbon\Carbon;
use Auth;
use DataTables;
use App\Models\Factories;
use Rap2hpoutre\FastExcel\FastExcel;

class SuratJalanController extends Controller
{
    //
    public function index()
    {
        $factory = Factories::whereNull('deleted_at')->get();

        return view('report.suratjalan.index')->with('factory', $factory);
    }

    public function getDataSuratJalan(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;

        $data = DB::table('v_monitoring_surat_jalan_new_size')
                ->select(
                    'created_at',
                    'style_edit',
                    'article_edit',
                    'color_edit',
                    'po_number_edit',
                    'qty',
                    'size_edit',
                    'no_polybag',
                    'no_suratjalan'
                )
                ->where('factory_id', $factory_id)
                ->whereNull('deleted_at');
        
        if ($request->radio_status == 'date') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where(function($query) use ($range) {
                                $query->whereBetween('created_at', [$range['from'], $range['to']]);
                            });

        }elseif ($request->radio_status == 'po') {
            $po_number = $request->po_number == null ? ' ' : $request->po_number;
            $data = $data->where('po_number_edit', 'like', '%'.$po_number.'%');
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('po_number_edit', 'like', '%'.$filterby.'%')
                                ->orWhere('style_edit', 'like', '%'.$filterby.'%')
                                ->orWhere('size_edit', 'like', '%'.$filterby.'%')
                                ->orWhere('color_edit', 'like', '%'.$filterby.'%');
                    });
        }

        $data = $data->groupBy([
                            'created_at',
                            'style_edit',
                            'article_edit',
                            'color_edit',
                            'po_number_edit',
                            'qty',
                            'size_edit',
                            'no_polybag',
                            'no_suratjalan'
                        ])
                        ->orderBy('created_at')
                        ->orderBy('po_number_edit');

        return Datatables::of($data)
                ->editColumn('created_at', function($data) {
                    return Carbon::parse($data->created_at)->format('d/m/Y');
                })
               ->make(true);
    }

    public function exportSuratJalan(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $orderby = $request->orderby;
        $direction = $request->direction;

        $data = DB::table('v_monitoring_surat_jalan_new_size')
                    ->select(
                        'created_at',
                        'style_edit',
                        'article_edit',
                        'color_edit',
                        'po_number_edit',
                        'qty',
                        'size_edit',
                        'no_polybag',
                        'no_suratjalan'
                    )
                    ->where('factory_id', $factory_id)
                    ->whereNull('deleted_at');
        
        if ($request->radio_status == 'date') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where(function($query) use ($range) {
                                $query->whereBetween('created_at', [$range['from'], $range['to']]);
                            });
            
            $f_name = $range['from'] . '_until_' . $range['to'];
        
        }elseif ($request->radio_status == 'po') {
            $po_number = $request->po_number == null ? ' ' : $request->po_number;
            $data = $data->where('po_number_edit', 'like', '%'.$po_number.'%');

            $f_name = $po_number;
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                            $query->where('po_number_edit', 'like', '%'.$filterby.'%')
                                    ->orWhere('style_edit', 'like', '%'.$filterby.'%')
                                    ->orWhere('size_edit', 'like', '%'.$filterby.'%')
                                    ->orWhere('color_edit', 'like', '%'.$filterby.'%');
                    });
        }

        //naming file
        $get_factory = Factories::where('id', $factory_id)
                                    ->wherenull('deleted_at')
                                    ->first();

        $data = $data->groupBy([
            'created_at',
            'style_edit',
            'article_edit',
            'color_edit',
            'po_number_edit',
            'qty',
            'size_edit',
            'no_polybag',
            'no_suratjalan'
        ]);

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);

            $filename = $get_factory->factory_name.'_report_monitoring_surat_jalan_' . $f_name
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        }
        else {
            $data = $data->orderBy('created_at')
                            ->orderBy('po_number_edit');
            
            $filename = $get_factory->factory_name.'_report_monitoring_surat_jalan_' . $f_name;
        }

        $i = 1;

        // $export = \Excel::create($filename, function($excel) use ($data, $i) {
        //     $excel->sheet('report', function($sheet) use($data, $i) {
        //         $sheet->appendRow(array(
        //             '#', 'TGL', 'STYLE', 'ARTICLE',  'COLOUR', 'PO', 'QTY', 'Breakdown Size', 'NO POLYBAG', 'NO SJ'
        //         ));
        //         $data->chunk(100, function($rows) use ($sheet, $i)
        //         {
        //             foreach ($rows as $row)
        //             {
        //                 //
        //                 $sheet->appendRow(array(
        //                     $i++, Carbon::parse($row->created_at)->format('d/m/Y'), $row->style_edit, $row->article_edit, $row->color_edit, $row->po_number_edit, $row->qty, $row->size_edit, $row->no_polybag, $row->no_suratjalan
        //                 ));
        //             }
        //         });
        //     });
        // })->download('xlsx');

        // return response()->json('Success exporting', 200);
        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
         }
         if ($data_result->count() > 0) {
            return (new FastExcel($data_result))->download($filename.'.xlsx', function ($row) {

                return 
                        [
                            '#' => $row->no, 
                            'TGL' => Carbon::parse($row->created_at)->format('d/m/Y'), 
                            'STYLE' => $row->style_edit,
                            'ARTICLE' => $row->article_edit,
                            'COLOUR' => $row->color_edit,
                            'PO' => $row->po_number_edit,
                            'QTY' => $row->qty,
                            'Breakdown Size' => $row->size_edit,
                            'NO POLYBAG' => $row->no_polybag,
                            'NO SJ' => $row->no_suratjalan
                        ];
                        
            });
         }else {

            $list = collect([
                [ 
                    '#' => '', 
                    'TGL' => '', 
                    'STYLE' => '',
                    'ARTICLE' => '',
                    'COLOUR' => '',
                    'PO' => '',
                    'QTY' => '',
                    'Breakdown Size' => '',
                    'NO POLYBAG' => '',
                    'NO SJ' => ''
                 ],
            ]);
            
            return (new FastExcel($list))->download($filename.'.xlsx');

         }

    }

    // surat jalan in
    public function suratjalanin()
    {
        $factory = Factories::whereNull('deleted_at')->get();

        return view('report.suratjalan.suratjalanin')->with('factory', $factory);
    }

    public function getDataSuratJalanIn(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;

        $data = DB::table('v_monitoring_surat_jalan_in_new')
                ->select(
                    'style_edit',
                    'article_edit',
                    'color_edit',
                    'po_number_edit',
                    'size_edit',
                    'no_polybag',
                    'no_suratjalan',
                    'sj_cutting',
                    DB::raw('date(checkin) AS checkin, sum(qty) AS qty')
                )
                ->where('factory_id', $factory_id)
                ->whereNull('deleted_at');
        
        if ($request->radio_status == 'date') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where(function($query) use ($range) {
                                $query->whereBetween('checkin', [$range['from'], $range['to']]);
                            });

        }elseif ($request->radio_status == 'po') {
            $po_number = $request->po_number == null ? ' ' : $request->po_number;
            $data = $data->where('po_number_edit', 'like', '%'.$po_number.'%');
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('po_number_edit', 'like', '%'.$filterby.'%')
                                ->orWhere('style_edit', 'like', '%'.$filterby.'%')
                                ->orWhere('size_edit', 'like', '%'.$filterby.'%')
                                ->orWhere('color_edit', 'like', '%'.$filterby.'%');
                    });
        }

        $data = $data->groupBy([
                            DB::raw('date(checkin)'),
                            'style_edit',
                            'article_edit',
                            'color_edit',
                            'po_number_edit',
                            'size_edit',
                            'no_polybag',
                            'no_suratjalan',
                            'sj_cutting'
                        ])
                        ->orderBy('checkin')
                        ->orderBy('po_number_edit');

        return Datatables::of($data)
                ->editColumn('created_at', function($data) {
                    return Carbon::parse($data->checkin)->format('d/m/Y');
                })
               ->make(true);
    }

    public function exportSuratJalanIn(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $orderby = $request->orderby;
        $direction = $request->direction;

        $data = DB::table('v_monitoring_surat_jalan_in_new')
                    ->select(
                        'style_edit',
                        'article_edit',
                        'color_edit',
                        'po_number_edit',
                        'size_edit',
                        'no_polybag',
                        'no_suratjalan',
                        'sj_cutting',
                        DB::raw('date(checkin) AS checkin, sum(qty) AS qty')
                    )
                    ->where('factory_id', $factory_id)
                    ->whereNull('deleted_at');
        
        if ($request->radio_status == 'date') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where(function($query) use ($range) {
                                $query->whereBetween('checkin', [$range['from'], $range['to']]);
                            });
            
            $f_name = $range['from'] . '_until_' . $range['to'];
        
        }elseif ($request->radio_status == 'po') {
            $po_number = $request->po_number == null ? ' ' : $request->po_number;
            $data = $data->where('po_number_edit', 'like', '%'.$po_number.'%');

            $f_name = $po_number;
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                            $query->where('po_number_edit', 'like', '%'.$filterby.'%')
                                    ->orWhere('style_edit', 'like', '%'.$filterby.'%')
                                    ->orWhere('size_edit', 'like', '%'.$filterby.'%')
                                    ->orWhere('color_edit', 'like', '%'.$filterby.'%');
                    });
        }

        //naming file
        $get_factory = Factories::where('id', $factory_id)
                                    ->wherenull('deleted_at')
                                    ->first();
        $data = $data->groupBy([
                            DB::raw('date(checkin)'),
                            'style_edit',
                            'article_edit',
                            'color_edit',
                            'po_number_edit',
                            'size_edit',
                            'no_polybag',
                            'no_suratjalan',
                            'sj_cutting'
                        ]);

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);

            $filename = $get_factory->factory_name.'_report_monitoring_surat_jalan_in_' . $f_name
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        }
        else {
            $data = $data->orderBy('checkin')
                            ->orderBy('po_number_edit');
            
            $filename = $get_factory->factory_name.'_report_monitoring_surat_jalan_in_' . $f_name;
        }

        $i = 1;

        // $export = \Excel::create($filename, function($excel) use ($data, $i) {
        //     $excel->sheet('report', function($sheet) use($data, $i) {
        //         $sheet->appendRow(array(
        //             '#', 'TGL', 'STYLE', 'ARTICLE',  'COLOUR', 'PO', 'QTY', 'Breakdown Size', 'NO POLYBAG', 'NO SJ'
        //         ));
        //         $data->chunk(100, function($rows) use ($sheet, $i)
        //         {
        //             foreach ($rows as $row)
        //             {
        //                 //
        //                 $sheet->appendRow(array(
        //                     $i++, Carbon::parse($row->created_at)->format('d/m/Y'), $row->style_edit, $row->article_edit, $row->color_edit, $row->po_number_edit, $row->qty, $row->size_edit, $row->no_polybag, $row->no_suratjalan
        //                 ));
        //             }
        //         });
        //     });
        // })->download('xlsx');

        // return response()->json('Success exporting', 200);
        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
         }
         if ($data_result->count() > 0) {
            return (new FastExcel($data_result))->download($filename.'.xlsx', function ($row) {

                return 
                        [
                            '#' => $row->no, 
                            'TGL' => Carbon::parse($row->checkin)->format('d/m/Y'), 
                            'STYLE' => $row->style_edit,
                            'ARTICLE' => $row->article_edit,
                            'COLOUR' => $row->color_edit,
                            'PO' => $row->po_number_edit,
                            'QTY' => $row->qty,
                            'Breakdown Size' => $row->size_edit,
                            'NO POLYBAG' => $row->no_polybag,
                            'NO SJ Cut' => $row->sj_cutting,
                            'NO SJ Exim' => $row->no_suratjalan
                        ];
                        
            });
         }else {

            $list = collect([
                [ 
                    '#' => '', 
                    'TGL' => '', 
                    'STYLE' => '',
                    'ARTICLE' => '',
                    'COLOUR' => '',
                    'PO' => '',
                    'QTY' => '',
                    'Breakdown Size' => '',
                    'NO POLYBAG' => '',
                    'NO SJ' => ''
                 ],
            ]);
            
            return (new FastExcel($list))->download($filename.'.xlsx');

         }

    }
    
    // surat jalan in exim
    public function suratjalaninexim()
    {
        $factory = Factories::whereNull('deleted_at')->get();

        return view('report.suratjalan.suratjalaninexim')->with('factory', $factory);
    }

    public function getDataSuratJalanInExim(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;

        $data = DB::table('delivery_in')
                ->select(
                        'delivery_in.id',
                        'delivery_in.no_suratjalan',
                        'delivery_in.no_suratjalan_info',
                        'delivery_in.subcont_id',
                        'delivery_in.accepted_by',
                        'delivery_in.approved_by',
                        'delivery_in.delivered_by',
                        'delivery_in.deleted_at',
                        'delivery_in.created_by',
                        'delivery_in.deleted_by',
                        'delivery_in.bc_no',
                        'delivery_in.type_bc',
                        'delivery_in.no_aju',
                        'delivery_in.no_daftar_bc',
                        'delivery_in.document_date',
                        'delivery_in.created_at',
                        'delivery_in.updated_at',
                        'delivery_in.qty',
                        DB::raw('subcont.name as subcont_name')
                )
                ->join('subcont', 'subcont.id','=','delivery_in.subcont_id')
                ->wherenull('delivery_in.deleted_at')
                ->wherenull('subcont.deleted_at');
        
        if ($request->radio_status == 'waiting') {
        
            $data = $data->whereNotIn('delivery_in.id', function($q){
                            $q->select('delivery_in_id')->from('delivery_in_detail');
                        });

        }
        // elseif ($request->radio_status == 'po') {
        //     $po_number = $request->po_number == null ? ' ' : $request->po_number;
        //     $data = $data->where('po_number_edit', 'like', '%'.$po_number.'%');
        // }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('delivery_in.no_suratjalan', 'like', '%'.$filterby.'%')
                                ->orWhere('delivery_in.bc_no', 'like', '%'.$filterby.'%')
                                ->orWhere('delivery_in.no_daftar_bc', 'like', '%'.$filterby.'%')
                                ->orWhere('delivery_in.no_aju', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
                ->editColumn('document_date', function($data) {
                    return Carbon::parse($data->document_date)->format('d/m/Y');
                })
                ->addColumn('status', function($data) {
                    $get_detail = DB::table('delivery_in_detail')
                                    ->where('delivery_in_id', $data->id)
                                    ->whereNull('deleted_at')
                                    ->sum('qty');

                    if($get_detail > 0) {
                        $is_completed = '<span class="label label-success label-rounded">
                                            <i class="icon-checkmark2"></i>
                                        </span><span class="badge badge-success position-right">'.$get_detail.'</span>';
                    }else{
                        $is_completed = '<span class="label label-default label-rounded">
                                            <i class="icon-cross3"></i>
                                        </span>';
                    }

                    return $is_completed;
                })
                ->rawColumns(['status'])
               ->make(true);
    }

    public function exportSuratJalanInExim(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $orderby = $request->orderby;
        $direction = $request->direction;

        $data = DB::table('delivery_in')
                ->select(
                        'delivery_in.id',
                        'delivery_in.no_suratjalan',
                        'delivery_in.no_suratjalan_info',
                        'delivery_in.subcont_id',
                        'delivery_in.accepted_by',
                        'delivery_in.approved_by',
                        'delivery_in.delivered_by',
                        'delivery_in.deleted_at',
                        'delivery_in.created_by',
                        'delivery_in.deleted_by',
                        'delivery_in.bc_no',
                        'delivery_in.type_bc',
                        'delivery_in.no_aju',
                        'delivery_in.no_daftar_bc',
                        'delivery_in.document_date',
                        'delivery_in.created_at',
                        'delivery_in.updated_at',
                        'delivery_in.qty',
                        DB::raw('subcont.name as subcont_name')
                )
                ->join('subcont', 'subcont.id','=','delivery_in.subcont_id')
                ->wherenull('delivery_in.deleted_at')
                ->wherenull('subcont.deleted_at');

        
        $f_name = 'all';
        
        if ($request->radio_status == 'waiting') {
        
            $data = $data->whereNotIn('delivery_in.id', function($q){
                            $q->select('delivery_in_id')->from('delivery_in_detail');
                        });

            $f_name = 'waiting_checkin';
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                            $query->where('delivery_in.no_suratjalan', 'like', '%'.$filterby.'%')
                                    ->orWhere('delivery_in.bc_no', 'like', '%'.$filterby.'%')
                                    ->orWhere('delivery_in.no_daftar_bc', 'like', '%'.$filterby.'%')
                                    ->orWhere('delivery_in.no_aju', 'like', '%'.$filterby.'%');
                    });
        }

        //naming file
        $get_factory = Factories::where('id', $factory_id)
                                    ->wherenull('deleted_at')
                                    ->first();

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);

            $filename = $get_factory->factory_name.'_report_monitoring_sj_in_exim_' . $f_name
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        }
        else {
            $data = $data->orderBy('document_date');
            
            $filename = $get_factory->factory_name.'_report_monitoring_sj_in_exim_' . $f_name;
        }

        $i = 1;

        $data_result = $data->get();
        foreach ($data_result as $data_results) {
            $data_results->no = $i++;
         }
         if ($data_result->count() > 0) {
            return (new FastExcel($data_result))->download($filename.'.xlsx', function ($row) {
                
                $get_detail = DB::table('delivery_in_detail')
                                    ->where('delivery_in_id', $row->id)
                                    ->whereNull('deleted_at')
                                    ->count();

                $qty_cutting = 0;

                if($get_detail > 0) {
                    $get_detail_qty = DB::table('delivery_in_detail')
                                    ->where('delivery_in_id', $row->id)
                                    ->whereNull('deleted_at')
                                    ->sum('qty');

                    $status = 'Already Checkin';

                    $qty_cutting = $get_detail_qty;

                }else{
                    $status = 'Waiting Checkin';
                }

                return 
                        [
                            '#' => $row->no, 
                            'Tgl Dokumen' => Carbon::parse($row->document_date)->format('d/m/Y'), 
                            'No. SJ' => $row->no_suratjalan,
                            'Subcont' => $row->subcont_name,
                            'No. BC' => $row->bc_no,
                            'Type BC' => $row->type_bc,
                            'No Pendaftaran' => $row->no_daftar_bc,
                            'Qty Exim' => $row->qty,
                            'Status Cutting' => $status,
                            'Qty Cutting' => $qty_cutting
                        ];
                        
            });
         }else {

            $list = collect([
                [ 
                    '#' => '', 
                    'Tgl Dokumen' => '', 
                    'No. SJ' => '',
                    'Subcont' => '',
                    'No. BC' => '',
                    'Type BC' => '',
                    'No Pendaftaran' => '',
                    'Qty Exim' => '',
                    'Status Cutting' => '',
                    'Qty Cutting' => ''
                ],
            ]);
            
            return (new FastExcel($list))->download($filename.'.xlsx');

         }

    }
}
