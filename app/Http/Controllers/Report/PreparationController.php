<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use DataTable;
use Carbon\Carbon;
use Auth;
use DataTables;
use App\Models\Factories;

class PreparationController extends Controller
{
    //
    public function index()
    {
        $factory = Factories::whereNull('deleted_at')->get();

        return view('report.preparation.index')->with('factory', $factory);
    }

    public function getDataPreparation(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;

        $data = DB::table('v_report_preparation')
                ->where('factory_id', $factory_id);
        
        if ($request->radio_status == 'date') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where(function($query) use ($range) {
                                $query->whereBetween('in_date', [$range['from'], $range['to']])
                                        ->orWhereBetween('print_date', [$range['from'], $range['to']])
                                        ->orWhereBetween('out_date', [$range['from'], $range['to']]);
                            });
        }elseif ($request->radio_status == 'po') {
            $po_number = $request->po_number == null ? ' ' : $request->po_number;
            $data = $data->where('po_number_edit', 'like', '%'.$po_number.'%');
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number_edit', 'like', '%'.$filterby.'%')
                                ->orWhere('size', 'like', '%'.$filterby.'%')
                                ->orWhere('color_edit', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
               ->editColumn('status', function($data){
                    if ($data->is_inhouse) {
                        $str = '<span class="badge badge-success position-center">INHOUSE</span>';
                    }else {
                        $str = '<span class="badge badge-primary position-center">SUBCONT</span>';
                    }

                    return $str;
               })
               ->rawColumns(['status'])
               ->make(true);
    }

    public function exportPreparation(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $orderby = $request->orderby;
        $direction = $request->direction;

        $data = DB::table('v_report_preparation')
                ->where('factory_id', $factory_id);
        
        if ($request->radio_status == 'date') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where(function($query) use ($range) {
                                $query->whereBetween('in_date', [$range['from'], $range['to']])
                                        ->orWhereBetween('print_date', [$range['from'], $range['to']])
                                        ->orWhereBetween('out_date', [$range['from'], $range['to']]);
                            });
            
            $f_name = $range['from'] . '_until_' . $range['to'];
        
        }elseif ($request->radio_status == 'po') {
            $po_number = $request->po_number == null ? ' ' : $request->po_number;
            $data = $data->where('po_number_edit', 'like', '%'.$po_number.'%');

            $f_name = $po_number;
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number_edit', 'like', '%'.$filterby.'%')
                                ->orWhere('size', 'like', '%'.$filterby.'%')
                                ->orWhere('color_edit', 'like', '%'.$filterby.'%');
                    });
        }

        //naming file
        $get_factory = Factories::where('id', $factory_id)
                                    ->wherenull('deleted_at')
                                    ->first();

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);

            $filename = $get_factory->factory_name.'_report_preparation_' . $f_name
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        }
        else {
            $data = $data->orderBy('in_date', 'asc')
                        ->orderBy('print_date', 'asc')
                        ->orderBy('out_date', 'asc');
            
            $filename = $get_factory->factory_name.'_report_preparation_' . $f_name;
        }

        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($data, $i) {
            $excel->sheet('report', function($sheet) use($data, $i) {
                $sheet->appendRow(array(
                    '#', 'Barcode ID', 'PO Number', 'Style', 'Color', 'Size', 'Komponen',
                    'Qty', 'Sticker No', 'Create Date', 'Print Date', 'Scan Out', 'Status'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i)
                {
                    foreach ($rows as $row)
                    {
                        if ($row->is_inhouse) {
                            $status = 'INHOUSE';
                        }else {
                            $status = 'SUBCONT';
                        }
                        //
                        $sheet->appendRow(array(
                            $i++, '="'.$row->barcode_id.'"', $row->po_number_edit, $row->style_edit, $row->color_edit, $row->size_edit, $row->komponen_name, $row->qty, $row->sticker_no, $row->in_date, $row->print_date, $row->out_date, $status
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);

    }
}
