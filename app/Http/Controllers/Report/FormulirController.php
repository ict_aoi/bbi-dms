<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use DataTable;
use Carbon\Carbon;
use Auth;
use DataTables;
use App\Models\Factories;

class FormulirController extends Controller
{
    //
    public function index()
    {
        $factory = Factories::whereNull('deleted_at')->get();

        return view('report.formulir.index')->with('factory', $factory);
    }

    public function getDataFormulir(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;

        $data = DB::table('v_formulir_new')
                ->where('factory_id', $factory_id)
                ->whereNull('deleted_at');
        
        if ($request->radio_status == 'date') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where(function($query) use ($range) {
                                $query->whereBetween('formulir_created', [$range['from'], $range['to']]);
                            });

        }elseif ($request->radio_status == 'po') {
            $po_number = $request->po_number == null ? ' ' : $request->po_number;
            $data = $data->where('po_number', 'like', '%'.$po_number.'%');
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number', 'like', '%'.$filterby.'%')
                                ->orWhere('size', 'like', '%'.$filterby.'%')
                                ->orWhere('color', 'like', '%'.$filterby.'%');
                    });
        }

        $data = $data->orderBy('formulir_created')
                        ->orderBy('no_polybag')
                        ->orderBy('sticker_from');

        return Datatables::of($data)
               ->make(true);
    }

    public function exportFormulir(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $orderby = $request->orderby;
        $direction = $request->direction;

        $data = DB::table('v_formulir_new')
                ->where('factory_id', $factory_id)
                ->whereNull('deleted_at');
        
        if ($request->radio_status == 'date') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where(function($query) use ($range) {
                                $query->whereBetween('formulir_created', [$range['from'], $range['to']]);
                            });
            
            $f_name = $range['from'] . '_until_' . $range['to'];
        
        }elseif ($request->radio_status == 'po') {
            $po_number = $request->po_number == null ? ' ' : $request->po_number;
            $data = $data->where('po_number_edit', 'like', '%'.$po_number.'%');

            $f_name = $po_number;
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('barcode_id', 'like', '%'.$filterby.'%')
                                ->orWhere('po_number_edit', 'like', '%'.$filterby.'%')
                                ->orWhere('size', 'like', '%'.$filterby.'%')
                                ->orWhere('color_edit', 'like', '%'.$filterby.'%');
                    });
        }

        //naming file
        $get_factory = Factories::where('id', $factory_id)
                                    ->wherenull('deleted_at')
                                    ->first();

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);

            $filename = $get_factory->factory_name.'_report_formulir_' . $f_name
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        }
        else {
            $data = $data->orderBy('formulir_created', 'asc')
                            ->orderBy('no_polybag', 'asc')
                            ->orderBy('sticker_from', 'asc');
            
            $filename = $get_factory->factory_name.'_report_formulir_' . $f_name;
        }

        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($data, $i) {
            $excel->sheet('report', function($sheet) use($data, $i) {
                $sheet->appendRow(array(
                    '#', 'Barcode ID', 'PO', 'Style', 'Color', 'Size', 'Komponen',
                    'Qty', 'Sticker No', 'Created At', 'No Polybag'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i)
                {
                    foreach ($rows as $row)
                    {
                        //
                        $sheet->appendRow(array(
                            $i++, '="'.$row->barcode_id.'"', $row->po_number_edit, $row->style_edit, $row->color_edit, $row->size, $row->komponen_name, $row->qty_formulir, $row->sticker_no, $row->formulir_created, $row->no_polybag
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);

    }
}
