<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Hash;
use Validator;
use DataTables;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\Factories;
use App\Models\Customers;
use Redirect;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    public function index(Request $request){
        return view('admin.user.index');
    }

    public function getDatatables(Request $request){

        if ($request->ajax()){
            $users = User::whereNull('deleted_at')->get();
            //datatables
            return DataTables::of($users)
                ->addColumn('action', function($users){ return view('_action', [
                    'edit' => route('user.edit', $users->id),
                    'userid' => $users->id,
                    'delete_user' => route('user.delete', $users->id)]);})
                ->make(true);
        }
    }

    public function create(){

        $users = User::whereNull('deleted_at')->get();
        $roles = Role::get();
        $factory = Factories::whereNull('deleted_at')->get();
        
        return view('admin.user.create', compact('roles', 'users', 'factory'));
    }

    public function accountSetting($id)
    {
        $user = User::find($id);
        return view('admin.user.account_setting')
            ->with('user',$user);
    }

    public function resetPassword(Request $request)
    {
        $password = bcrypt('1234');

        $users = DB::table('users')
                        ->where('id', $request['id'])
                        ->update(['password' => $password]);

        return '1234';
    }

    public function updatepassword(Request $request, $id)
    {
        $validator =  Validator::make($request->all(), [
            'password_old' => 'required',
            'password_new' => 'required|different:password_old',
            'password_confirm' => 'required|same:password_new',
        ]);

        if ($validator->passes()) {
            try{
                db::beginTransaction();
                if (Hash::check($request->get('password_old'), Auth::user()->password)) {
                    $user = User::find($id);
                    $user->password = bcrypt($request->password_new);

                    if($user->update()){
                        db::commit();
                        return response()->json('Success', 200);
                    }
                }

            }catch (Exception $ex){
                db::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

        }else{
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ],400);
        }
    }


    public function store(Request $request){

        $data=$request->all();

        $data = new User;
        $data->name = $request['name'];
        $data->nik = $request['nik'];
        $data->password = bcrypt($request['password']);
        $data->email = $request['email'];
        $data->factory_id = $request['factory'];

        $data->created_at = carbon::now();

        if($data->save()) {
            $role_user = DB::table('role_user') 
                ->insert([
                    'user_id' => $data->id,
                    'role_id' => $request->role_id
                ]);
        }

        return Redirect::back()->with('alert','Save Successful !');
    }

    public function edit($id){
        $roles = DB::table('roles')
            ->select('id', 'name', 'display_name', 'description', 'created_at', 'updated_at')
            ->orderBy('created_at', 'desc')
            ->get();

        $user_roles = DB::table('role_user')
                          ->select('role_id')
                          ->where('user_id', $id)
                          ->first();

        $user = User::find($id);

        $factory = Factories::wherenull('deleted_at')
                    ->get();
        
        $get_users = DB::table('users')
                    ->where('id', $id)
                    ->wherenull('deleted_at')
                    ->first();

        return view('admin.user.edit')
                    ->with([
                            'roles' => $roles, 
                            'user' => $user, 
                            'factory' => $factory,
                            'user_roles' => $user_roles->role_id
                    ]);
    }

    public function update(Request $request){
        $data = $request->all();

        $users['nik'] = $data['nik'];
        $users['name'] = $data['name'];
        $users['email'] = $data['email'];
        $users['factory_id'] = $data['factory'];

        if(isset($data['resetpassword'])) {
            if($data['resetpassword'] == "1"){
                $users['password'] = bcrypt($data['nik'].'123');
            }
        }

        $users = DB::table('users')
                        ->where('id', $data['id'])
                        ->update($users);

        $role_user = DB::table('role_user')
                      ->where('user_id', $data['id'])
                      ->update(['role_id' => $data['role_id']]);
                      
        
        return redirect('admin/user');

    }

    public function destroy(Request $request)
    {   //For Deleting users
        $id = $request->id;
        // $Users = User::find($id)->delete();
        $user = User::find($id);
        $user->deleted_at = carbon::now();
        $user->save();
    }

}
