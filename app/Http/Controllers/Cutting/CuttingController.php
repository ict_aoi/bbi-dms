<?php

namespace App\Http\Controllers\Cutting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Process;
use App\Models\Types;

use DB;
use Auth;
use DataTables;
use Carbon\Carbon;

use App\Pdf\Barcode;

use Illuminate\Support\Facades\Input;

class CuttingController extends Controller
{
    //
    public function barcode()
    {
        $process = Process::whereNull('deleted_at')->get();
        $types = Types::whereNull('deleted_at')->get();

        return view('cutting/barcode')->with([
                                                'process' => $process,
                                                'types' => $types
                                            ]);
    }

    public function getData(Request $request) {

        //api
        if($request->radio_status) {
            //filtering
            if($request->radio_status == 'po') {
                if(empty($request->po_number)) {
                   $po_number = 'null';
                }
                else {
                   $po_number = $request->po_number;
                }

               //  $params =  $this->api_url . $this->parameter[2] . '=' . $po_number;
               $params = DB::table('breakdown_size')
                            ->where('poreference', 'like', '%'.strtoupper($po_number).'%')
                            ->get();
            }
            elseif($request->radio_status == 'fstyle') {
                if(empty($request->style)) {
                   $style = 'null';
                }
                else {
                   $style = $request->style;
                }

                $params = DB::table('breakdown_size')
                            ->where('style', 'like', '%'.strtoupper($style).'%')
                            ->whereDate('datepromise', '>=', Carbon::now()->subDays(60)->format('Y-m-d'))
                            ->get();
            }
           //
            elseif($request->radio_status == 'promise') {
                $counted_day = 0;
                if($request->date_range) {
                    $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
                    $from = date_format(date_create($date_range[0]), 'Y-m-d');
                    $to = date_format(date_create($date_range[1]), 'Y-m-d');
                    //check date range
                    $date_to = Carbon::parse($date_range[0]);
                    $date_from = Carbon::parse($date_range[1]);
                    $counted_day = $date_to->diffInDays($date_from);
                }
                else {
                    $from = date_format(Carbon::now(), 'Y-m-d');
                    $to = date_format(Carbon::now(), 'Y-m-d');
                }

                if($counted_day > 30) {
                    return response()->json('Date range should be less than 30 days',422);
                }

               $params = DB::table('breakdown_size')
                            ->whereBetween('datepromise', [$from, $to])
                            ->get();
            }

               $data = json_decode(json_encode($params), true);

            return Datatables::of($data)
                   ->editColumn('poreference', function($data){

                        if (isset($data['poreference'])) {
                            $str = $data['poreference'];
                        }elseif (empty($data['poreference'])) {
                            $str = $data['poreference'];
                        }else{
                            $str = trim($data['po_number']);
                        }

                        return $str;

                   })
                   ->editColumn('datepromise', function($data){
                       return isset($data['datepromise']) ? $data['datepromise'] : $data['datepromised'];
                   })
                   ->editColumn('size', function($data){
                       return trim($data['size']);
                   })
                   ->editColumn('style', function($data){
                       if ($data['source_erp'] == 'bima') {
                           $style = trim($data['style']);
                       }else {
                            $style = $data['style_so'] ? trim($data['style_so']) : trim($data['style']);
                       }

                       return '<a onclick="detailStyle(this)" data-style="'.$style.'">'.$style.'</a>';
                   })
                   ->editColumn('total_package', function($data) {
                    //    $ponumber = isset($data['poreference']) ? trim($data['poreference']) : trim($data['po_number']);
                       if (isset($data['poreference'])) {
                            $ponumber = $data['poreference'];
                        }elseif (empty($data['poreference'])) {
                            $ponumber = $data['poreference'];
                        }else{
                            $ponumber = trim($data['po_number']);
                        }
                       $size = trim($data['size']);

                       if ($data['source_erp'] == 'bima') {
                            $style = trim($data['style']);
                        }else {
                            $style = $data['style_so'] ? trim($data['style_so']) : trim($data['style']);
                        }
                    //   $style = $data['style_so'] ? trim($data['style_so']) : trim($data['style']);
                       $color = $data['color'] ? trim($data['color']) : null;
                       $article = $data['article'] ? trim($data['article']) : null;

                       $m_product_id = $data['m_product_id'];
                       $c_order_id = $data['c_order_id'];
                       $c_orderline_id = $data['c_orderline_id'];
                       $is_recycle = $data['is_recycle'];

                       $total_package    = DB::table('cutting_detail')
                                            ->select('barcode_id')
                                            ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail.po_summary_detail_id')
                                            ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                                            // ->where('po_summary_detail.po_number', $ponumber)
                                            ->where('po_summary_detail.size', $size)
                                            ->where('po_summary_detail.c_orderline_id', $c_orderline_id)
                                            ->where('po_summary_detail.m_product_id', $m_product_id)
                                            // ->where('po_summary_detail.color', $color)
                                            // ->where('po_summary_detail.article', $article)
                                            ->where('po_summary.style', $style)
                                            ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
                                            ->whereNull('cutting_detail.deleted_at')
                                            ->count();

                       if($total_package == 0) {
                           $total_package = '-';
                       }

                        // cek po summary detail apakah sudah pernah dibuat atau belum
                        $cekpodetail = DB::table('po_summary_detail')
                                            ->where('c_orderline_id', $c_orderline_id)
                                            ->where('factory_id', Auth::user()->factory_id)
                                            ->whereNull('deleted_at')
                                            ->orderBy('created_at', 'DESC')
                                            ->first();

                        $size_edit = $size;
                        $po_number_edit = $ponumber;
                        $style_edit = $style;
                        $color_edit = $color;
                        $article_edit = $article;

                        if ($cekpodetail) {
                            $size_edit = $cekpodetail->size_edit;
                            $po_number_edit = $cekpodetail->po_number_edit;
                            $style_edit = $cekpodetail->style_edit;
                            $color_edit = $cekpodetail->color_edit;
                            $article_edit = $cekpodetail->article_edit;
                        }

                        //
                       $mo = null;
                       $pcd = null;
                       $podd1 = null;
                       $ctyname = null;
                       $kst_joborder = null;
                       $qtyordered = $data['qtyordered'];
                        //

                       if (isset($data['poreference'])) {
                           $returned_value = '<span id="total_package_'.$c_orderline_id.'">' . $total_package . '</span>
                                          <input type="hidden" id="pcd_'.$c_orderline_id.'" value="'.$pcd.'">
                                          <input type="hidden" id="mo_'.$c_orderline_id.'" value="'.$mo.'">
                                          <input type="hidden" id="style_'.$c_orderline_id.'" value="'.$style.'">
                                          <input type="hidden" id="podd_'.$c_orderline_id.'" value="'.$podd1.'">
                                          <input type="hidden" id="size_'.$c_orderline_id.'" value="'.$data['size'].'">
                                          <input type="hidden" id="ctyname_'.$c_orderline_id.'" value="'.$ctyname.'">
                                          <input type="hidden" id="article_'.$c_orderline_id.'" value="'.$article.'">
                                          <input type="hidden" id="joborder_'.$c_orderline_id.'" value="'.$kst_joborder.'">
                                          <input type="hidden" id="qtyordered_'.$c_orderline_id.'" value="'.$qtyordered.'">
                                          <input type="hidden" id="category_'.$c_orderline_id.'" value="'.$data['category'].'">
                                          <input type="hidden" id="dateorder_'.$c_orderline_id.'" value="'.$data['dateorder'].'">
                                          <input type="hidden" id="datepromise_'.$c_orderline_id.'" value="'.$data['datepromise'].'">
                                          <input type="hidden" id="color_'.$c_orderline_id.'" value="'.$color.'">
                                          <input type="hidden" id="category_'.$c_orderline_id.'" value="'.$data['category'].'">
                                          <input type="hidden" id="dateorder_'.$c_orderline_id.'" value="'.$data['dateorder'].'">
                                          <input type="hidden" id="datepromise_'.$c_orderline_id.'" value="'.$data['datepromise'].'">
                                          <input type="hidden" id="productid_'.$c_orderline_id.'" value="'.$data['m_product_id'].'">
                                          <input type="hidden" id="productkode_'.$c_orderline_id.'" value="'.$data['kode_product'].'">
                                          <input type="hidden" id="productname_'.$c_orderline_id.'" value="'.$data['nama_product'].'">
                                          <input type="hidden" id="buyer_'.$c_orderline_id.'" value="'.$data['buyer'].'">
                                          <input type="hidden" id="orderid_'.$c_orderline_id.'" value="'.$data['c_order_id'].'">
                                          <input type="hidden" id="orderlineid_'.$c_orderline_id.'" value="'.$data['c_orderline_id'].'">
                                          <input type="hidden" id="size_edit_'.$c_orderline_id.'" value="'.$size_edit.'">
                                          <input type="hidden" id="po_number_edit_'.$c_orderline_id.'" value="'.$po_number_edit.'">
                                          <input type="hidden" id="color_edit_'.$c_orderline_id.'" value="'.$color_edit.'">
                                          <input type="hidden" id="style_edit_'.$c_orderline_id.'" value="'.$style_edit.'">
                                          <input type="hidden" id="article_edit_'.$c_orderline_id.'" value="'.$article_edit.'">
                                          <input type="hidden" id="is_recycle_'.$c_orderline_id.'" value="'.$is_recycle.'">
                                         ';
                       }elseif (empty($data['poreference'])) {
                        $returned_value = '<span id="total_package_'.$c_orderline_id.'">' . $total_package . '</span>
                                       <input type="hidden" id="pcd_'.$c_orderline_id.'" value="'.$pcd.'">
                                       <input type="hidden" id="mo_'.$c_orderline_id.'" value="'.$mo.'">
                                       <input type="hidden" id="style_'.$c_orderline_id.'" value="'.$style.'">
                                       <input type="hidden" id="podd_'.$c_orderline_id.'" value="'.$podd1.'">
                                       <input type="hidden" id="size_'.$c_orderline_id.'" value="'.$data['size'].'">
                                       <input type="hidden" id="ctyname_'.$c_orderline_id.'" value="'.$ctyname.'">
                                       <input type="hidden" id="article_'.$c_orderline_id.'" value="'.$article.'">
                                       <input type="hidden" id="joborder_'.$c_orderline_id.'" value="'.$kst_joborder.'">
                                       <input type="hidden" id="qtyordered_'.$c_orderline_id.'" value="'.$qtyordered.'">
                                       <input type="hidden" id="category_'.$c_orderline_id.'" value="'.$data['category'].'">
                                       <input type="hidden" id="dateorder_'.$c_orderline_id.'" value="'.$data['dateorder'].'">
                                       <input type="hidden" id="datepromise_'.$c_orderline_id.'" value="'.$data['datepromise'].'">
                                       <input type="hidden" id="color_'.$c_orderline_id.'" value="'.$color.'">
                                       <input type="hidden" id="category_'.$c_orderline_id.'" value="'.$data['category'].'">
                                       <input type="hidden" id="dateorder_'.$c_orderline_id.'" value="'.$data['dateorder'].'">
                                       <input type="hidden" id="datepromise_'.$c_orderline_id.'" value="'.$data['datepromise'].'">
                                       <input type="hidden" id="productid_'.$c_orderline_id.'" value="'.$data['m_product_id'].'">
                                       <input type="hidden" id="productkode_'.$c_orderline_id.'" value="'.$data['kode_product'].'">
                                       <input type="hidden" id="productname_'.$c_orderline_id.'" value="'.$data['nama_product'].'">
                                       <input type="hidden" id="buyer_'.$c_orderline_id.'" value="'.$data['buyer'].'">
                                       <input type="hidden" id="orderid_'.$c_orderline_id.'" value="'.$data['c_order_id'].'">
                                       <input type="hidden" id="orderlineid_'.$c_orderline_id.'" value="'.$data['c_orderline_id'].'">
                                       <input type="hidden" id="size_edit_'.$c_orderline_id.'" value="'.$size_edit.'">
                                       <input type="hidden" id="po_number_edit_'.$c_orderline_id.'" value="'.$po_number_edit.'">
                                       <input type="hidden" id="color_edit_'.$c_orderline_id.'" value="'.$color_edit.'">
                                       <input type="hidden" id="style_edit_'.$c_orderline_id.'" value="'.$style_edit.'">
                                       <input type="hidden" id="article_edit_'.$c_orderline_id.'" value="'.$article_edit.'">
                                       <input type="hidden" id="is_recycle_'.$c_orderline_id.'" value="'.$is_recycle.'">
                                      ';
                    }else{
                           $returned_value = '<span id="total_package_'.$c_orderline_id.'">' . $total_package . '</span>
                                        <input type="hidden" id="pcd_'.$c_orderline_id.'" value="'.$data['pcd'].'">
                                        <input type="hidden" id="mo_'.$c_orderline_id.'" value="'.$data['mo'].'">
                                        <input type="hidden" id="style_'.$c_orderline_id.'" value="'.$data['style'].'">
                                        <input type="hidden" id="podd_'.$c_orderline_id.'" value="'.$data['podd'].'">
                                        <input type="hidden" id="size_'.$c_orderline_id.'" value="'.$data['size'].'">
                                        <input type="hidden" id="ctyname_'.$c_orderline_id.'" value="'.$data['city_name'].'">
                                        <input type="hidden" id="article_'.$c_orderline_id.'" value="'.$article.'">
                                        <input type="hidden" id="joborder_'.$c_orderline_id.'" value="'.$data['kst_joborder'].'">
                                        <input type="hidden" id="qtyordered_'.$c_orderline_id.'" value="'.$data['qtyordered'].'">
                                        <input type="hidden" id="category_'.$c_orderline_id.'" value="'.$data['category'].'">
                                        <input type="hidden" id="dateorder_'.$c_orderline_id.'" value="'.$data['dateordered'].'">
                                        <input type="hidden" id="datepromise_'.$c_orderline_id.'" value="'.$data['datepromised'].'">
                                        <input type="hidden" id="color_'.$c_orderline_id.'" value="'.$color.'">
                                        <input type="hidden" id="productid_'.$c_orderline_id.'" value="'.$data['m_product_id'].'">
                                        <input type="hidden" id="productkode_'.$c_orderline_id.'" value="'.$data['kode_product'].'">
                                        <input type="hidden" id="productname_'.$c_orderline_id.'" value="'.$data['nama_product'].'">
                                        <input type="hidden" id="buyer_'.$c_orderline_id.'" value="'.$data['buyer'].'">
                                        <input type="hidden" id="orderid_'.$c_orderline_id.'" value="'.$data['c_order_id'].'">
                                        <input type="hidden" id="orderlineid_'.$c_orderline_id.'" value="'.$data['c_orderline_id'].'">
                                        <input type="hidden" id="size_edit_'.$c_orderline_id.'" value="'.$size_edit.'">
                                        <input type="hidden" id="po_number_edit_'.$c_orderline_id.'" value="'.$po_number_edit.'">
                                        <input type="hidden" id="color_edit_'.$c_orderline_id.'" value="'.$color_edit.'">
                                        <input type="hidden" id="style_edit_'.$c_orderline_id.'" value="'.$style_edit.'">
                                        <input type="hidden" id="article_edit_'.$c_orderline_id.'" value="'.$article_edit.'">
                                        <input type="hidden" id="is_recycle_'.$c_orderline_id.'" value="'.$is_recycle.'">
                                         ';
                       }

                       return $returned_value;
                   })
                   ->addColumn('action', function($data) {

                       if (isset($data['poreference'])) {
                            $ponumber = $data['poreference'];
                        }elseif (empty($data['poreference'])) {
                            $ponumber = $data['poreference'];
                        }else{
                            $ponumber = trim($data['po_number']);
                        }
                       $size =  trim($data['size']);

                       if ($data['source_erp'] == 'bima') {
                            $style = trim($data['style']);
                        }else {
                            $style = $data['style_so'] ? trim($data['style_so']) : trim($data['style']);
                        }

                    //   $style = $data['style_so'] ? trim($data['style_so']) : trim($data['style']);
                       $color = $data['color'] ? trim($data['color']) : null;
                       $article = $data['article'] ? trim($data['article']) : null;

                       $m_product_id = $data['m_product_id'];
                       $c_orderline_id = $data['c_orderline_id'];

                        $check = DB::table('po_summary_detail')
                                    ->where('c_orderline_id', $c_orderline_id)
                                    ->where('m_product_id', $m_product_id)
                                    ->where('factory_id', Auth::user()->factory_id)
                                    ->exists();

                       if(!$check) {

                            return view('_action', [
                                'model'  => $data,
                                'upload' => $ponumber.'|'.$style.'|'.$size.'|'.$color.'|'.$article.'|'.$m_product_id.'|'.$c_orderline_id
                            ]);

                       }
                       else {

                            return view('_action', [
                                'model' => $data,
                                'upload' => $ponumber.'|'.$style.'|'.$size.'|'.$color.'|'.$article.'|'.$m_product_id.'|'.$c_orderline_id,
                                'detail' => route('cutting.viewPackageByProduct',
                                                [
                                                    'po_number'  => $ponumber,
                                                    'style'  => str_slug($style),
                                                    'size'  => $size,
                                                    'color'  => $color,
                                                    'article'  => $article,
                                                    'm_product_id'  => $m_product_id,
                                                    'c_orderline_id'  => $c_orderline_id,
                                                    'status_print' => 'all_print'
                                                ]),
                                'delete_modal' => route('cutting.ajaxGetDataSummaryDetail',
                                                [
                                                    'po_number'  => $ponumber,
                                                    'style'  => $style,
                                                    'size'  => $size,
                                                    'color'  => $color,
                                                    'article'  => $article,
                                                    'm_product_id'  => $m_product_id,
                                                    'c_orderline_id'  => $c_orderline_id
                                                ])
                            ]);

                       }
                    })
                    ->addColumn('is_completed', function($data)
                    {

                        if (isset($data['poreference'])) {
                            $ponumber = $data['poreference'];
                        }elseif (empty($data['poreference'])) {
                            $ponumber = $data['poreference'];
                        }else{
                            $ponumber = trim($data['po_number']);
                        }
                       $size = trim($data['size']);
                       $style = $data['style_so'] ? trim($data['style_so']) : trim($data['style']);
                       $color = $data['color'] ? $data['color'] : null;
                       $article = $data['article'] ? $data['article'] : null;
                       $m_product_id = $data['m_product_id'];
                       $c_orderline_id = $data['c_orderline_id'];

                        $get_package = DB::table('cutting_detail')
                                           ->select('barcode_id')
                                           ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail.po_summary_detail_id')
                                           ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                                           ->where('po_summary_detail.c_orderline_id', $c_orderline_id)
                                           ->where('po_summary_detail.m_product_id', $m_product_id)
                                           ->where('po_summary_detail.size', $size)
                                        //   ->where('po_summary.style', $style)
                                           ->where(function($q) use ($style, $data){
                                                $q->where('po_summary.style', $style);
                                                $q->orwhere('po_summary.style', '*'.$style);
                                                $q->orwhere('po_summary.style', trim($data['style']));
                                            })
                                           ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
                                           ->where('cutting_detail.is_printed', true)
                                           ->whereNull('cutting_detail.deleted_at')
                                           ->count();

                       if($get_package > 0) {
                           $is_completed = '<span class="label label-success label-rounded">
                                               <i class="icon-checkmark2"></i>
                                           </span><span class="badge badge-success position-right">'.$get_package.'</span>';
                       }else{
                           $is_completed = '<span class="label label-default label-rounded">
                                               <i class="icon-cross3"></i>
                                           </span>';
                       }

                       return $is_completed;
                    })
                    ->addColumn('checkbox', function ($data) {

                        if (isset($data['poreference'])) {
                            $ponumber = $data['poreference'];
                        }elseif (empty($data['poreference'])) {
                            $ponumber = $data['poreference'];
                        }else{
                            $ponumber = trim($data['po_number']);
                        }
                        $size = trim($data['size']);
                        $style = $data['style_so'] ? trim($data['style_so']) : trim($data['style']);
                        $color = $data['color'] ? $data['color'] : null;
                        $article = $data['article'] ? $data['article'] : null;
                        $c_orderline_id = $data['c_orderline_id'];

                        return '<input type="checkbox" class="clref" name="selector[]" id="Inputselector" data-ponumber="'.$ponumber.'" data-style="'.$style.'" data-size="'.$size.'" data-color="'.$color.'" data-article="'.$article.'" data-orderline="'.$c_orderline_id.'">';
                    })
                    ->editColumn('size', function($data){
                        // cek size
                        $ceksize = DB::table('po_summary_detail')
                                        ->select('size', 'size_edit')
                                        ->where('c_orderline_id', $data['c_orderline_id'])
                                        ->first();

                        if ($ceksize) {
                            return $ceksize->size_edit;
                        }else {
                            return $data['size'];
                        }
                    })
                    ->rawColumns(['total_package', 'action', 'is_completed', 'checkbox', 'style', 'size'])
                    ->make(true);
        }
        else {
            $data = array();
            return Datatables::of($data)
                   ->editColumn('total_package', function($data) {
                       return null;
                   })
                   ->editColumn('style', function($data) {
                       return null;
                   })
                   ->addColumn('action', function($data) {
                       return null;
                    })
                   ->addColumn('is_completed', function($data) {
                       return null;
                    })
                   ->addColumn('checkbox', function($data) {
                       return null;
                    })
                   ->addColumn('size', function($data) {
                       return null;
                    })
                    ->rawColumns(['total_package', 'action', 'is_completed', 'checkbox', 'style', 'size'])
                    ->make(true);
        }
   }

    // check list komponen
    public function checkKomponen()
    {
        $factories = DB::table('factories')
                        ->where('id', Auth::user()->factory_id)
                        ->whereNull('deleted_at')
                        ->first();

        return view('cutting/check_komponen')->with(['factories'=>$factories]);
    }

    public function getDataCheckKomponen(Request $request) {

        //api
        if($request->radio_status) {
            //filtering
            if($request->radio_status == 'po') {
                if(empty($request->po_number)) {
                   $po_number = 'null';
                }
                else {
                   $po_number = $request->po_number;
                }

               $data = DB::table('v_list_komponen')
                            ->select('style', 'po_number', 'color', 'size', 'c_orderline_id', 'sticker_no', 'cut_number')
                            ->where('po_number_edit', 'like', '%'.strtoupper($po_number).'%');
            }
            elseif($request->radio_status == 'style') {
                if(empty($request->style)) {
                   $style = 'null';
                }
                else {
                   $style = $request->style;
                }

                $data = DB::table('v_list_komponen')
                            ->select('style', 'po_number', 'color', 'size', 'c_orderline_id', 'sticker_no', 'cut_number')
                            ->where('style_edit', 'like', '%'.strtoupper($style).'%');
            }

            // $data = json_decode(json_encode($params), true);
            $data = $data->groupBy('style', 'po_number', 'color', 'size', 'c_orderline_id', 'sticker_no', 'cut_number');
            $data = $data->orderBy('po_number');

            return Datatables::of($data)
                   ->editColumn('po_number', function($data){
                        $strings = DB::table('po_summary_detail')
                                        ->where('c_orderline_id', $data->c_orderline_id)
                                        ->first();

                        $str = $strings->po_number_edit;

                        return $str;
                   })
                   ->editColumn('style', function($data){
                        $strings = DB::table('po_summary_detail')
                                        ->where('c_orderline_id', $data->c_orderline_id)
                                        ->first();

                        $str = $strings->style_edit;

                        return $str;
                   })
                   ->editColumn('article', function($data){
                        $strings = DB::table('po_summary_detail')
                                        ->where('c_orderline_id', $data->c_orderline_id)
                                        ->first();

                        $str = $strings->article_edit;

                        return $str;
                   })
                   ->editColumn('color', function($data){
                        $strings = DB::table('po_summary_detail')
                                        ->where('c_orderline_id', $data->c_orderline_id)
                                        ->first();

                        $str = $strings->color_edit;

                        return $str;
                   })
                   ->addColumn('action', function($data) {
                        return view('_action', [
                            'model' => $data,
                            'button_mode' => true,
                            'is_printed' => route('cutting.printPreviewKomponen', [
                                                'c_orderline_id' => $data->c_orderline_id,
                                                'style' => $data->style,
                                                'po_number' => $data->po_number,
                                                'sticker_no' => $data->sticker_no
                                            ])
                        ]);

                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        else {
            $data = array();
            return Datatables::of($data)
                   ->editColumn('total_package', function($data) {
                       return null;
                   })
                   ->editColumn('style', function($data) {
                       return null;
                   })
                   ->addColumn('action', function($data) {
                       return null;
                    })
                   ->addColumn('is_completed', function($data) {
                       return null;
                    })
                   ->addColumn('checkbox', function($data) {
                       return null;
                    })
                    ->rawColumns(['total_package', 'action', 'is_completed', 'checkbox', 'style'])
                    ->make(true);
        }
    }

   public function generateBarcode(Request $request)
   {
        $ponumber = trim($request->po_number);
        $style = $request->style;

        // if($ponumber == '') {
        //     return response()->json('PO Number not found', 422);
        // }

        // cek style sudah diinput komponen atau belum
        $cekkomponen = DB::table('v_master_style_new')
                            ->where('style', $style)
                            ->exists();

        if (!$cekkomponen) {
            return response()->json('please input komponen in style first..!', 422);
        }

        $master_style = DB::table('v_master_style_new')
                            ->where('style', $style)
                            // ->where('type_id', $request->type_id)
                            ->get();

        $pcd = $request->pcd != null ? Carbon::parse($request->pcd)->format('Y-m-d') : null;
        $podd = $request->podd != null ? Carbon::parse($request->podd)->format('Y-m-d') : null;
        $mo = $request->mo;
        $city_name = $request->city_name;
        $kst_joborder = $request->kst_joborder;
        $qtyordered = $request->qtyordered;

        $article = $request->article;
        $size = $request->size;
        $color = $request->color;
        $cut_number = $request->cut_number;
        $cut_info = strtoupper($request->cut_info);
        $lot = strtoupper($request->lot);
        $part = $request->part;
        $komponen = trim(strtoupper($request->komponen));
        $process_id = $request->process_id != 'null' ? $request->process_id : null;
        $qty = $request->qty;
        $sticker_from = $request->sticker_from;
        $sticker_to = $request->sticker_to;
        $sticker_no = trim($request->sticker_no);
        $cutting_date = Carbon::parse($request->cutting_date)->format('Y-m-d');
        $bundle = strtoupper($request->bundle);
        $qc = strtoupper($request->qc);
        $cutter = strtoupper($request->cutter);
        $type_id = $request->type_id;
        $type_description = strtoupper($request->type_description);

        $category = $request->category;
        $dateordered = $request->dateordered;
        $datepromised = $request->datepromised;
        $m_product_id = $request->m_product_id;
        $kode_product = $request->kode_product;
        $nama_product = $request->nama_product;

        $total_qty = $request->total_qty;
        $from = $request->from;

        $color_edit = strtoupper($request->color_edit);
        $article_edit = strtoupper($request->article_edit);

        $style_edit = strtoupper($request->style_edit);
        $po_number_edit = strtoupper($request->po_number_edit);

        $size_edit = strtoupper($request->size_edit);

        $buyer = $request->buyer;
        $c_order_id = $request->c_order_id;
        $c_orderline_id = $request->c_orderline_id;

        $no_bundle = $request->no_bundle;
        $is_user_nagai = Auth::user()->is_nagai;

        $is_recycle = $request->is_recycle;

        $is_inhouse = false;

        if (isset($request->is_inhouse)) {
            if ($request->is_inhouse == 'on') {
                $is_inhouse = true;
            }else{
                $is_inhouse = false;
            }
        }

        // cek apakah po, size, article, color sudah ada ?
        // $cek_size = DB::table('po_summary_detail')
        //                 ->where('po_number', $ponumber)
        //                 ->where('size', $size)
        //                 ->where(function($query) use($article, $color){
        //                     $query->where('article', $article)
        //                             ->orwhere('color', $color);
        //                 })
        //                 ->exists();

        // if ($cek_size) {
        //     return response()->json('PO, Size, article/color already exists..!', 422);
        // }

        // cek apakah komponen, size, po, sticker_no udah ada
        // $cek_komponen = DB::table('cutting_detail')
        //                     ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
        //                     ->where('cutting_detail.komponen', $komponen)
        //                     // ->where('cutting_detail.sticker_no', $sticker_no)
        //                     ->where('po_summary_detail.size', $size)
        //                     ->where('po_summary_detail.po_number', $ponumber)
        //                     ->where('po_summary_detail.color', $color)
        //                     ->where('po_summary_detail.article', $article)
        //                     ->get()->toArray();

        // if (count($cek_komponen) > 0) {

        //     $max_sticker = max(array_column($cek_komponen,'total_qty'));
        //     $min_sticker = min(array_column($cek_komponen,'from'));

        //     $max_from = max(array_column($cek_komponen,'from'))-1;

        //     $max_sticker = $max_sticker+$max_from;


        //     if ($from >= $min_sticker && $from <= $max_sticker ) {
        //         return response()->json('Sticker already exists..!', 422);
        //     }

        // }

        $data_po_summary = array(
                            'po_number' => $ponumber,
                            'style' => $style,
                            'podd' => $podd,
                            'pcd' => $pcd,
                            'factory_id' => Auth::user()->factory_id,
                            'city_name' => $city_name,
                            'dateordered' => $dateordered,
                            'datepromised' => $datepromised,
                            'created_at' => Carbon::now(),
                            'is_inhouse' => $is_inhouse,
                            'buyer' => $buyer,
                            'c_order_id' => $c_order_id,
                            'is_user_nagai' => $is_user_nagai
                        );

        //
        $data_po_summary_detail = array();
        $data_cutting_detail = array();
        $data_cutting_movement = array();

        $podetail['po_number'] = $ponumber;
        $podetail['size'] = $size;
        $podetail['color'] = $color;
        $podetail['article'] = $article;
        $podetail['factory_id'] = Auth::user()->factory_id;
        $podetail['mo'] = $mo;
        $podetail['qtyordered'] = (int)$qtyordered;
        $podetail['kst_joborder'] = $kst_joborder;
        $podetail['type_id'] = $type_id;
        $podetail['type_description'] = $type_description;
        $podetail['created_at'] = Carbon::now();
        $podetail['category'] = $category;
        $podetail['m_product_id'] = $m_product_id;
        $podetail['kode_product'] = $kode_product;
        $podetail['nama_product'] = $nama_product;
        $podetail['total_qty'] = $total_qty;
        $podetail['qty'] = $qty;
        $podetail['from'] = $from;
        $podetail['color_edit'] = $color_edit;
        $podetail['article_edit'] = $article_edit;
        $podetail['is_inhouse'] = $is_inhouse;
        $podetail['po_number_edit'] = $po_number_edit;
        $podetail['style_edit'] = $style_edit;
        $podetail['style'] = $style;
        $podetail['c_order_id'] = $c_order_id;
        $podetail['c_orderline_id'] = $c_orderline_id;
        $podetail['size_edit'] = $size_edit;
        $podetail['no_bundle'] = $no_bundle;
        $podetail['is_recycle'] = $is_recycle;

        try {
            DB::beginTransaction();

            // insert po_summary
            //  $cek_po = DB::table('po_summary')
            //             // ->where('po_number', $ponumber)
            //             ->where('c_order_id', $c_order_id)
            //             ->exists();

            // if (!$cek_po) {
                DB::table('po_summary')->insert($data_po_summary);
            // }

            $LastInsertId = DB::getPdo()->lastInsertId();

            // insert po_summary_detail
            $podetail['po_summary_id'] = $LastInsertId;
            $data_po_summary_detail[] = $podetail;

            $insert1 = DB::table('po_summary_detail')->insert($data_po_summary_detail);

            // cek double c_orderline_id & size & from
            // if ($insert1) {
            //     $cekin = DB::table('po_summary_detail')
            //                 ->where('c_orderline_id', $c_orderline_id)
            //                 ->where('style', $style)
            //                 ->where('size', $size)
            //                 ->where('from', $from)
            //                 ->count();

            //     if ($cekin > 1) {
            //         throw new \Exception("error duplikat start from..!");
            //     }
            // }

            $no = 1;
            $to = $qty - 1;
            $totalan = $total_qty + ($from-1);
            $qty_modulus = $total_qty%$qty;

            //dd($total_qty, $qty);

            if ($total_qty > $qty) {

                foreach ($master_style as $key => $val) {

                    if ($val->is_special) {

                        if (in_array($size, \Config::get('constants.BIG_SIZE'))) {
                            if (!$val->is_big_size) {
                                continue;
                            }
                        }else {
                            if ($val->is_big_size) {
                                continue;
                            }
                        }
                    }

                    for ($i=$from; $i <= $totalan; $i+=$qty) {

                        $panel = $i;
                        $panel_to = $panel+$to;

                        if ($panel_to > $totalan) {
                            $panel_to = $totalan;
                        }

                        $qty = ($panel_to-$panel)+1;

                    //}

                    //foreach (range($from, $totalan, $qty) AS $panel) {
                        //$panel_to = $panel+$to;

                        //if ($panel_to > $totalan) {
                        //    $panel_to = $totalan;
                        //    //$qty = $qty_modulus;
                        //}

                        $sticker_number = $panel.' - '.$panel_to;

                        $barcode_id = $this->generateBarcodeNumber($no);

                        // data cutting movement
                        $move['barcode_id'] = $barcode_id;
                        $move['process_from'] = null;
                        $move['status_from'] = null;
                        $move['process_to'] = 'barcoding';
                        $move['status_to'] = 'onprogress';
                        $move['user_id'] = Auth::user()->id;
                        $move['description'] = 'barcoding';
                        $move['ip_address'] = \Request::ip();
                        $move['created_at'] = Carbon::now();

                        $data_cutting_movement[] = $move;

                        // data cutting detail
                        $detail['barcode_id'] = $barcode_id;
                        $detail['po_summary_detail_id'] = $LastInsertId;
                        $detail['cut_number'] = $cut_number;
                        $detail['cut_info'] = $cut_info;
                        $detail['lot'] = $lot;
                        $detail['komponen'] = $val->komponen;
                        $detail['part'] = $val->part;
                        $detail['qty'] = $qty;
                        $detail['sticker_from'] = $panel;
                        $detail['sticker_to'] = $panel_to;
                        $detail['sticker_no'] = $sticker_number;
                        $detail['cutting_date'] = $cutting_date;
                        $detail['bundle'] = $bundle;
                        $detail['qc'] = $qc;
                        $detail['cutter'] = $cutter;
                        $detail['current_process'] = 'barcoding';
                        $detail['current_status'] = 'onprogress';
                        //$detail['process'] = //$process_id;
                        $detail['process'] = $val->process;
                        $detail['created_at'] = Carbon::now();

                        $detail['qty_used_draft'] = $qty;

                        $detail['is_recycle'] = $is_recycle;

                        $data_cutting_detail[] = $detail;

                        $no++;
                    }

                }


            }elseif ($total_qty == $qty) {

                $sticker_number = $from.' - '.$totalan;


                foreach ($master_style as $key => $val) {

                    if ($val->is_special) {

                        if (in_array($size, \Config::get('constants.BIG_SIZE'))) {
                            if (!$val->is_big_size) {
                                continue;
                            }
                        }else {
                            if ($val->is_big_size) {
                                continue;
                            }
                        }
                    }

                    $barcode_id = $this->generateBarcodeNumber($no);

                    // data cutting movement
                    $move['barcode_id'] = $barcode_id;
                    $move['process_from'] = null;
                    $move['status_from'] = null;
                    $move['process_to'] = 'barcoding';
                    $move['status_to'] = 'onprogress';
                    $move['user_id'] = Auth::user()->id;
                    $move['description'] = 'barcoding';
                    $move['ip_address'] = \Request::ip();
                    $move['created_at'] = Carbon::now();

                    $data_cutting_movement[] = $move;

                    // data cutting detail
                    $detail['barcode_id'] = $barcode_id;
                    $detail['po_summary_detail_id'] = $LastInsertId;
                    $detail['cut_number'] = $cut_number;
                    $detail['cut_info'] = $cut_info;
                    $detail['lot'] = $lot;
                    $detail['komponen'] = $val->komponen;
                    $detail['part'] = $val->part;
                    $detail['qty'] = $qty;
                    $detail['sticker_from'] = $from;
                    $detail['sticker_to'] = $totalan;
                    $detail['sticker_no'] = $sticker_number;
                    $detail['cutting_date'] = $cutting_date;
                    $detail['bundle'] = $bundle;
                    $detail['qc'] = $qc;
                    $detail['cutter'] = $cutter;
                    $detail['current_process'] = 'barcoding';
                    $detail['current_status'] = 'onprogress';
                    // $detail['process'] = $process_id;
                    $detail['process'] = $val->process;
                    $detail['created_at'] = Carbon::now();

                    $detail['qty_used_draft'] = $qty;

                    $detail['is_recycle'] = $is_recycle;

                    $data_cutting_detail[] = $detail;

                    $no++;
                }


            }else {
                throw new \Exception("something wrong..!");
            }

            // insert cutting detail
            $insert = DB::table('cutting_detail')->insert($data_cutting_detail);

            // insert movement
            DB::table('cutting_movements')->insert($data_cutting_movement);

            // update master style
            DB::table('master_style')
                ->where('style', $style)
                ->update([
                    'is_used' => true,
                    'updated_at' => Carbon::now()
                ]);

            DB::commit();

        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        $total_package    = DB::table('cutting_detail')
                            ->select('barcode_id')
                            ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail.po_summary_detail_id')
                            ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                            // ->where('po_summary_detail.po_number', $ponumber)
                            ->where('po_summary_detail.c_orderline_id', $c_orderline_id)
                            ->where('po_summary_detail.size', $size)
                            ->where('po_summary_detail.m_product_id', $m_product_id)
                            ->where('po_summary.style', $style)
                            ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
                            ->whereNull('cutting_detail.deleted_at')
                            ->count();

        if($total_package == 0) {
            $total_package = '-';
        }

        return response()->json($total_package, 200);
   }

    //viewPackageDetail
    public function viewPackage(Request $request)
    {
        $po_number = trim($request->po_number);
        $style = trim($request->style);
        $size = trim($request->size);
        $color = $request->color;
        $article = $request->article;
        return view('cutting/detail')->with([
                                                'po_number' => $po_number,
                                                'style' => $style,
                                                'size' => $size,
                                                'color' => $color,
                                                'article' => $article,
                                            ]);
    }

     //DATA FOR DATATABLES VIEW PACKAGE
     public function viewPackageData(Request $request) {
        $ponumber = trim($request->po_number);
        $style = trim($request->style);
        $size = trim($request->size);
        $color = $request->color;
        $article = $request->article;

        $data     = DB::table('cutting_detail')
                        ->select(
                            'cutting_detail.barcode_id',
                            'cutting_detail.po_summary_detail_id',
                            'cutting_detail.cut_number',
                            'cutting_detail.cut_info',
                            'cutting_detail.lot',
                            'cutting_detail.komponen',
                            'cutting_detail.part',
                            'cutting_detail.qty',
                            'cutting_detail.sticker_from',
                            'cutting_detail.sticker_to',
                            'cutting_detail.sticker_no',
                            'cutting_detail.cutting_date',
                            'cutting_detail.bundle_id',
                            'cutting_detail.bundle',
                            'cutting_detail.qc',
                            'cutting_detail.cutter',
                            'cutting_detail.current_process',
                            'cutting_detail.current_status',
                            'cutting_detail.process',
                            'cutting_detail.already_process',
                            'cutting_detail.deleted_at',
                            'cutting_detail.is_printed',
                            'cutting_detail.is_printed_formulir',
                            'cutting_detail.is_printed_sj',
                            'cutting_detail.description',
                            'cutting_detail.created_at',
                            'cutting_detail.updated_at',
                            'cutting_detail.uoms',
                            'po_summary.style',
                            'po_summary_detail.po_number',
                            'po_summary_detail.plan_ref',
                            'po_summary_detail.size',
                            'po_summary_detail.color',
                            'po_summary_detail.article',
                            'po_summary_detail.factory_id',
                            'po_summary_detail.mo',
                            'po_summary_detail.qtyordered',
                            'po_summary_detail.kst_joborder',
                            'po_summary_detail.type_id',
                            'po_summary_detail.type_description',
                            'po_summary_detail.category',
                            'po_summary_detail.m_product_id',
                            'po_summary_detail.kode_product',
                            'po_summary_detail.nama_product',
                            'po_summary_detail.from',
                            'po_summary_detail.total_qty',
                            'po_summary_detail.color_edit',
                            'po_summary_detail.article_edit',
                            DB::raw('master_komponen.name AS komponen_name')
                        )
                        ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail.po_summary_detail_id')
                        ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                        ->join('master_komponen', 'master_komponen.id', '=', 'cutting_detail.komponen')
                        ->where('po_summary_detail.po_number', $ponumber)
                        ->where('po_summary_detail.size', $size)
                        ->where('po_summary_detail.color', $color)
                        ->where('po_summary_detail.article', $article)
                        ->where('po_summary.style', $style)
                        ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
                        ->whereNull('cutting_detail.deleted_at')
                        ->orderBy('cutting_detail.sticker_from');

        return Datatables::of($data)
                    ->addColumn('checkbox', function ($data) {
                        return '<input type="checkbox" class="clplanref" name="selector[]" id="Inputselector" value="'.$data->barcode_id.'" data-ponumber="'.$data->po_number.'" data-style="'.$data->style.'" data-size="'.$data->size.'" data-color="'.$data->color.'" data-article="'.$data->article.'">';
                    })
                    ->editColumn('is_printed', function($data) {
                        if($data->is_printed) {
                            $iscompleted = '<span class="label label-success label-rounded"
                                            id="completed_'.$data->barcode_id.'">
                                            <i class="icon-checkmark2"></i>
                                           </span>';
                        }
                        else {
                            $iscompleted = '<span class="label label-default label-rounded"
                                            id="completed_'.$data->barcode_id.'">
                                            <i class="icon-cross3"></i>
                                           </span>';
                        }
                        return $iscompleted;
                    })
                    ->addColumn('action', function($data) {
                        return view('_action', [
                                     'model' => $data,
                                     'button_mode' => true,
                                     'is_printed' => route('cutting.ajaxSetCompleted', [
                                                         'barcodeid' => $data->barcode_id,
                                                         'current_is_printed' => $data->is_printed
                                                        ])
                                    ]);
                    })
                    ->rawColumns(['checkbox', 'is_printed', 'action'])
                    ->make(true);
    }

    //viewPackageByProduct
    public function viewPackageByProduct(Request $request)
    {
        $po_number = trim($request->po_number);
        $style = strtoupper(trim($request->style));
        $size = trim($request->size);
        $color = $request->color;
        $article = $request->article;
        $m_product_id = $request->m_product_id;
        $c_orderline_id = $request->c_orderline_id;
        $status_print = $request->status_print;


        // $style = DB::table('po_summary_detail')
        //             ->where('c_orderline_id', $c_orderline_id)
        //             ->first()->style;

        // factories
        $factories = DB::table('factories')
                        ->where('id', Auth::user()->factory_id)
                        ->whereNull('deleted_at')
                        ->first();

        // types
        $types = DB::table('types')
                    ->whereNull('deleted_at')
                    ->get();

        return view('cutting/detail_by_product')->with([
                                                'po_number' => $po_number,
                                                'style' => $style,
                                                'size' => $size,
                                                'color' => $color,
                                                'article' => $article,
                                                'm_product_id' => $m_product_id,
                                                'c_orderline_id' => $c_orderline_id,
                                                'factories' => $factories,
                                                'types' => $types,
                                                'status_print' => $status_print
                                            ]);
    }

     //DATA FOR DATATABLES VIEW PACKAGE
     public function viewPackageDataByProduct(Request $request) {

        $po_number = trim($request->po_number);
        $style = trim($request->style);
        $size = trim($request->size);
        $color = $request->color;
        $article = $request->article;
        $m_product_id = trim($request->m_product_id);
        $c_orderline_id = $request->c_orderline_id;

        $status_print = request()->status_print;

        $data     = DB::table('cutting_detail')
                        ->select(
                            'cutting_detail.barcode_id',
                            'cutting_detail.po_summary_detail_id',
                            'cutting_detail.cut_number',
                            'cutting_detail.cut_info',
                            'cutting_detail.lot',
                            'cutting_detail.komponen',
                            'cutting_detail.part',
                            'cutting_detail.qty',
                            'cutting_detail.sticker_from',
                            'cutting_detail.sticker_to',
                            'cutting_detail.sticker_no',
                            'cutting_detail.cutting_date',
                            'cutting_detail.bundle_id',
                            'cutting_detail.bundle',
                            'cutting_detail.qc',
                            'cutting_detail.cutter',
                            'cutting_detail.current_process',
                            'cutting_detail.current_status',
                            'cutting_detail.process',
                            'cutting_detail.already_process',
                            'cutting_detail.deleted_at',
                            'cutting_detail.is_printed',
                            'cutting_detail.is_printed_formulir',
                            'cutting_detail.is_printed_sj',
                            'cutting_detail.description',
                            'cutting_detail.created_at',
                            'cutting_detail.updated_at',
                            'cutting_detail.uoms',
                            'po_summary.style',
                            'po_summary_detail.po_number',
                            'po_summary_detail.plan_ref',
                            'po_summary_detail.size',
                            'po_summary_detail.color',
                            'po_summary_detail.article',
                            'po_summary_detail.factory_id',
                            'po_summary_detail.mo',
                            'po_summary_detail.qtyordered',
                            'po_summary_detail.kst_joborder',
                            'po_summary_detail.type_id',
                            'po_summary_detail.type_description',
                            'po_summary_detail.category',
                            'po_summary_detail.m_product_id',
                            'po_summary_detail.kode_product',
                            'po_summary_detail.nama_product',
                            'po_summary_detail.from',
                            'po_summary_detail.total_qty',
                            'po_summary_detail.color_edit',
                            'po_summary_detail.article_edit',
                            DB::raw('master_komponen.name AS komponen_name'),
                            'po_summary_detail.c_orderline_id',
                            'po_summary_detail.no_bundle'
                        )
                        ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail.po_summary_detail_id')
                        ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                        ->join('master_komponen', 'master_komponen.id', '=', 'cutting_detail.komponen')
                        ->where('po_summary_detail.c_orderline_id', $c_orderline_id)
                        ->where('po_summary_detail.m_product_id', $m_product_id)
                        ->where('po_summary_detail.size', $size)
                        // ->where('po_summary_detail.color', $color)
                        // ->where('po_summary_detail.article', $article)
                        //->where('po_summary.style', $style)
                        ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
                        ->whereNull('cutting_detail.deleted_at');

        if ($status_print=='print_complete') {
            $data = $data->where('cutting_detail.is_printed', true);
        }elseif ($status_print=='print_onprogress') {
            $data = $data->where('cutting_detail.is_printed', false);
        }
        // if (Auth::user()->is_nagai) {
        //     $data = $data->where('cutting_detail.is_printed', false);
        // }

        $data = $data->orderBy('cutting_detail.sticker_from');

        return Datatables::of($data)
                    ->addColumn('checkbox', function ($data) {
                        return '<input type="checkbox" class="clplanref" name="selector[]" id="Inputselector" value="'.$data->barcode_id.'" data-ponumber="'.$data->po_number.'" data-style="'.$data->style.'" data-size="'.$data->size.'" data-color="'.$data->color.'" data-article="'.$data->article.'" data-product="'.$data->m_product_id.'" data-orderline="'.$data->c_orderline_id.'">';
                    })
                    ->editColumn('is_printed', function($data) {
                        if($data->is_printed) {
                            $iscompleted = '<span class="label label-success label-rounded"
                                            id="completed_'.$data->barcode_id.'">
                                            <i class="icon-checkmark2"></i>
                                           </span>';
                        }
                        else {
                            $iscompleted = '<span class="label label-default label-rounded"
                                            id="completed_'.$data->barcode_id.'">
                                            <i class="icon-cross3"></i>
                                           </span>';
                        }
                        return $iscompleted;
                    })
                    ->editColumn('lot', function($data) {
                        if(Auth::user()->is_nagai) {
                            $str = $data->no_bundle;
                        }
                        else {
                            $str = $data->lot;
                        }
                        return $str;
                    })
                    ->addColumn('action', function($data) {
                        return view('_action', [
                                     'model' => $data,
                                     'button_mode' => true,
                                     'is_printed' => route('cutting.ajaxSetCompleted', [
                                                         'barcodeid' => $data->barcode_id,
                                                         'current_is_printed' => $data->is_printed
                                                        ])
                                    ]);
                    })
                    ->rawColumns(['checkbox', 'is_printed', 'action', 'lot'])
                    ->make(true);
    }

    //set package as completed
    public function setCompleted(Request $request) {
        $barcodeid = trim($request->barcodeid);
        // $current_ = $request->current_is_printed;
        $process_from = 'barcoding';
        $process_to = 'barcoding';
        $status_to = 'completed';
        $description = 'print barcode' ;

        //check status package dan is printed
        $check = DB::table('cutting_detail')
                    ->select('current_status', 'is_printed')
                    ->where('barcode_id', $barcodeid)
                    ->whereNull('deleted_at')
                    ->first();
        if(!$check) {
            return response()->json('Something wrong, system cannot proccess this request', 422);
        }
        else {
            //jika data sudah di print sebelumnya, maka tidak perlu update database
            if($check->is_printed === TRUE) {
                return response()->json('GOOD', 200);
            }
            $status_from = $check->current_status;
        }

        //data movement
        $data = array(
            'barcode_id' => $barcodeid,
            'process_from' => $process_from,
            'process_to' => $process_to,
            'status_from' => $status_from,
            'status_to' => $status_to,
            'user_id' => Auth::user()->id,
            'description' => $description,
            'ip_address' => \Request::ip(),
            'created_at' => Carbon::now()
        );

        try {
            db::beginTransaction();

            //update is_printed
            DB::table('cutting_detail')
            ->where('barcode_id', $barcodeid)
            ->whereNull('deleted_at')
            ->update([
                'current_process' => $process_to,
                'current_status' => $status_to,
                'is_printed' => true,
                'updated_at' => Carbon::now()
            ]);

            //update deleted_at on the last package movement based on package
            $checkifexist = DB::table('cutting_movements')
                                ->where('barcode_id', $barcodeid)
                                ->where('is_canceled', false)
                                ->whereNull('deleted_at')
                                ->first();
            if($checkifexist) {
                DB::table('cutting_movements')
                    ->where('barcode_id', $barcodeid)
                    ->where('is_canceled', false)
                    ->whereNull('deleted_at')
                    ->update([
                        'deleted_at' => Carbon::now()
                    ]);
            }

            //insert cutting movement
            DB::table('cutting_movements')->insert($data);

            db::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json('GOOD', 200);
    }

    //set package as completed
    public function setCompletedAll(Request $request) {
        // $barcodeid = trim($request->barcodeid);
        // $current_ = $request->current_is_printed;
        $ponumber = trim($request->po_number);
        $style = trim($request->style);
        $size = trim($request->size);
        $color = $request->color;
        $article = $request->article;
        $m_product_id = $request->m_product_id;

        $data_req = json_decode(stripslashes($request->data));

        $status_print = request()->status_print;

        $process_from = 'barcoding';
        $process_to = 'barcoding';
        $status_to = 'completed';
        $description = 'print barcode' ;


        $data     = DB::table('cutting_detail')
                    ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail.po_summary_detail_id')
                    ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                    ->where('po_summary_detail.m_product_id', $m_product_id)
                    ->where('po_summary_detail.size', $size)
                    ->where('po_summary_detail.color', $color)
                    ->where('po_summary_detail.article', $article)
                    //->where('po_summary.style', $style)
                    ->where(function($q) use ($style){
                        $q->where('po_summary.style', $style);
                        $q->orwhere('po_summary.style', '*'.$style);
                    })
                    ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
                    ->whereNull('cutting_detail.deleted_at');
                    //->get();


        if ($status_print=='print_complete') {
            $data = $data->where('cutting_detail.is_printed', true);
        }elseif ($status_print=='print_onprogress') {
            $data = $data->where('cutting_detail.is_printed', false);
        }

        if (count($data_req) > 0) {
            foreach ($data_req as $value) {
                $rbarcode['barcode_id'] = $value->barcode_id;
                $arr_barcode[] = $rbarcode;
            }

            $data = $data->whereIn('cutting_detail.barcode_id', $arr_barcode);
        }

        $data = $data->get();

        $data_movement = array();

        foreach ($data as $key => $value) {
            $barcodeid = $value->barcode_id;
            $status_from = $value->current_status;

            if($value->is_printed === TRUE) {
                continue;
            }
            //data movement
            $data_m = array(
                'barcode_id' => $barcodeid,
                'process_from' => $process_from,
                'process_to' => $process_to,
                'status_from' => $status_from,
                'status_to' => $status_to,
                'user_id' => Auth::user()->id,
                'description' => $description,
                'ip_address' => \Request::ip(),
                'created_at' => Carbon::now()
            );

            $data_movement[] = $data_m;
        }


        try {
            db::beginTransaction();

            foreach ($data_movement as $key2 => $value2) {

                //update is_printed
                DB::table('cutting_detail')
                ->where('barcode_id', $value2['barcode_id'])
                ->whereNull('deleted_at')
                ->update([
                    'current_process' => $process_to,
                    'current_status' => $status_to,
                    'is_printed' => true,
                    'updated_at' => Carbon::now()
                ]);

                //update deleted_at on the last package movement based on package
                $checkifexist = DB::table('cutting_movements')
                                    ->where('barcode_id', $value2['barcode_id'])
                                    ->where('is_canceled', false)
                                    ->whereNull('deleted_at')
                                    ->first();
                if($checkifexist) {
                    DB::table('cutting_movements')
                    ->where('barcode_id', $value2['barcode_id'])
                    ->where('is_canceled', false)
                    ->whereNull('deleted_at')
                    ->update([
                        'deleted_at' => Carbon::now()
                    ]);
                }
            }

            //insert cutting movement
            DB::table('cutting_movements')->insert($data_movement);

            db::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json('GOOD', 200);
    }
    //set package as completed filter po
    public function setCompletedAllFilterPo(Request $request) {
        // $barcodeid = trim($request->barcodeid);
        // $current_ = $request->current_is_printed;
        $ponumber = trim($request->po_number);
        $style = trim($request->style);
        $size = trim($request->size);
        $color = $request->color;
        $article = $request->article;

        $data_req = json_decode(stripslashes($request->data));

        $process_from = 'barcoding';
        $process_to = 'barcoding';
        $status_to = 'completed';
        $description = 'print barcode' ;


        $data     = DB::table('cutting_detail')
                    ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail.po_summary_detail_id')
                    ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                    ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
                    ->whereNull('cutting_detail.deleted_at');

        if (isset($request->filter_po)) {
            $data = $data->where('po_summary.po_number', 'like', '%'.$request->filter_po.'%');
        }else{

            $data = $data->where('po_summary_detail.po_number', $ponumber)
                        ->where('po_summary_detail.size', $size)
                        ->where('po_summary_detail.color', $color)
                        ->where('po_summary_detail.article', $article)
                        //->where('po_summary.style', $style);
                        ->where(function($q) use ($style){
                            $q->where('po_summary.style', $style);
                            $q->orwhere('po_summary.style', '*'.$style);
                        });

            if (count($data_req) > 0) {
                foreach ($data_req as $value) {
                    $rbarcode['barcode_id'] = $value->barcode_id;
                    $arr_barcode[] = $rbarcode;
                }

                $data = $data->whereIn('cutting_detail.barcode_id', $arr_barcode);
            }
        }


        $data = $data->get();

        $data_movement = array();

        foreach ($data as $key => $value) {
            $barcodeid = $value->barcode_id;
            $status_from = $value->current_status;

            if($value->is_printed === TRUE) {
                continue;
            }
            //data movement
            $data_m = array(
                'barcode_id' => $barcodeid,
                'process_from' => $process_from,
                'process_to' => $process_to,
                'status_from' => $status_from,
                'status_to' => $status_to,
                'user_id' => Auth::user()->id,
                'description' => $description,
                'ip_address' => \Request::ip(),
                'created_at' => Carbon::now()
            );

            $data_movement[] = $data_m;
        }


        try {
            db::beginTransaction();

            foreach ($data_movement as $key2 => $value2) {

                //update is_printed
                DB::table('cutting_detail')
                ->where('barcode_id', $value2['barcode_id'])
                ->whereNull('deleted_at')
                ->update([
                    'current_process' => $process_to,
                    'current_status' => $status_to,
                    'is_printed' => true,
                    'updated_at' => Carbon::now()
                ]);

                //update deleted_at on the last package movement based on package
                $checkifexist = DB::table('cutting_movements')
                                    ->where('barcode_id', $value2['barcode_id'])
                                    ->where('is_canceled', false)
                                    ->whereNull('deleted_at')
                                    ->first();
                if($checkifexist) {
                    DB::table('cutting_movements')
                    ->where('barcode_id', $value2['barcode_id'])
                    ->where('is_canceled', false)
                    ->whereNull('deleted_at')
                    ->update([
                        'deleted_at' => Carbon::now()
                    ]);
                }
            }

            //insert cutting movement
            DB::table('cutting_movements')->insert($data_movement);

            db::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json('GOOD', 200);
    }

    //print preview
    public function printPreview(Request $request) {

        $data = DB::table('cutting_detail_style')
                    ->select(
                            'cutting_detail_style.barcode_id',
                            'cutting_detail_style.po_summary_detail_id',
                            'cutting_detail_style.cut_number',
                            'cutting_detail_style.cut_info',
                            'cutting_detail_style.lot',
                            'cutting_detail_style.komponen',
                            'cutting_detail_style.part',
                            'cutting_detail_style.qty',
                            'cutting_detail_style.sticker_from',
                            'cutting_detail_style.sticker_to',
                            'cutting_detail_style.sticker_no',
                            'cutting_detail_style.cutting_date',
                            'cutting_detail_style.bundle_id',
                            'cutting_detail_style.bundle',
                            'cutting_detail_style.qc',
                            'cutting_detail_style.cutter',
                            'cutting_detail_style.current_process',
                            'cutting_detail_style.current_status',
                            'cutting_detail_style.process',
                            'cutting_detail_style.already_process',
                            'cutting_detail_style.deleted_at',
                            'cutting_detail_style.is_printed',
                            'cutting_detail_style.is_printed_formulir',
                            'cutting_detail_style.is_printed_sj',
                            'cutting_detail_style.description',
                            'cutting_detail_style.created_at',
                            'cutting_detail_style.updated_at',
                            'cutting_detail_style.uoms',
                            'v_cutting_process_detail.process_name',
                            'po_summary.style',
                            'po_summary_detail.po_number',
                            'po_summary_detail.plan_ref',
                            'po_summary_detail.size',
                            'po_summary_detail.color',
                            'po_summary_detail.article',
                            'po_summary_detail.factory_id',
                            'po_summary_detail.mo',
                            'po_summary_detail.qtyordered',
                            'po_summary_detail.kst_joborder',
                            'po_summary_detail.type_id',
                            'po_summary_detail.type_description',
                            'po_summary_detail.category',
                            'po_summary_detail.m_product_id',
                            'po_summary_detail.kode_product',
                            'po_summary_detail.nama_product',
                            'po_summary_detail.from',
                            'po_summary_detail.total_qty',
                            'po_summary_detail.color_edit',
                            'po_summary_detail.article_edit',
                            'po_summary_detail.style_edit',
                            'po_summary_detail.size_edit',
                            'v_master_style.komponen_name',
                            'v_master_style.total',
                            'v_master_style.is_color_white_split_collar',
                            'v_master_style.type_name',
                            'po_summary_detail.po_number_edit',
                            'po_summary_detail.no_bundle',
                            'po_summary_detail.is_recycle'
                            // ,
                            // 'v_master_style.type_id',
                            // 'v_master_style.type_name'
                    )
                    ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail_style.po_summary_detail_id')
                    ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                    ->join('v_master_style', function($join){
                        $join->on('v_master_style.style', '=', 'cutting_detail_style.style');
                        $join->on('v_master_style.komponen', '=', 'cutting_detail_style.komponen');
                        // $join->on('v_master_style.type_id', '=', 'cutting_detail_style.type_id');
                    })
                    ->leftjoin('v_cutting_process_detail', 'v_cutting_process_detail.barcode_id', '=', 'cutting_detail_style.barcode_id')
                    ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
                    ->whereNull('cutting_detail_style.deleted_at');

        if (isset($request->filter_po)) {
            $ponumber = $request->filter_po;
            $data = $data->where('po_summary.po_number', 'like', '%'.$ponumber.'%');

            // get list komponen
            $list_komponen = DB::table('v_list_komponen')
                                ->where('po_number', 'like', '%'.$ponumber.'%')
                                ->get();
        }else{
            $ponumber = $request->po_number;
            $style = $request->style;
            $size = $request->size;
            $color = $request->color;
            $article = $request->article;
            $m_product_id = $request->m_product_id;

            $data = $data->where('po_summary_detail.po_number', $ponumber)
                            ->where('po_summary_detail.m_product_id', $m_product_id)
                            ->where('po_summary_detail.size', $size)
                            ->where('po_summary_detail.color', $color)
                            ->where('po_summary_detail.article', $article)
                            ->where(function($q) use ($style) {
                                $q->where('po_summary.style', $style);
                                $q->orWhere('po_summary.style', '*'.$style);
                            });
                            //->where('po_summary.style', $style);
            //
            if(isset($request->barcode_id) && !empty($request->barcode_id)) {
                $barcodeid = $request->barcode_id;
                $data = $data->where('cutting_detail_style.barcode_id', $barcodeid);
            }
            else {
                $barcodeid = null;
                $data_req = json_decode(stripslashes($request->data));

                $arr_barcode = [];

                if (count($data_req) > 0) {
                    foreach ($data_req as $value) {
                        $rbarcode['barcode_id'] = $value->barcode_id;
                        $arr_barcode[] = $rbarcode;
                    }

                    $data = $data->whereIn('cutting_detail_style.barcode_id', $arr_barcode);
                }
            }

            // get list komponen
            $list_komponen = DB::table('v_list_komponen')
                                //->where('style', $style)
                                ->where(function($q) use ($style) {
                                    $q->where('style', $style);
                                    $q->orWhere('style', '*'.$style);
                                })
                                ->where('po_number', $ponumber)
                                ->get();
        }

            $filename = 'po_number_#' . $ponumber . '.pdf';
            //config layout
            if (Auth::user()->is_nagai) {

                if ($request->komponen_break == 1) {
                    if ($request->paper == 'a3') {
                        $orientation = 'landscape';
                        $papersize = 'a3';

                        $load_view = 'cutting.print.index_barcode_nagai';

                    }elseif ($request->paper == 'a4') {
                        $orientation = 'potrait';
                        $papersize = 'a4';

                        $load_view = 'cutting.print.index_barcode_nagai_a4';
                    }else{
                        $orientation = 'landscape';
                        $papersize = 'a3';

                        $load_view = 'cutting.print.index_barcode_nagai';
                    }
                }else {
                    if ($request->paper == 'a3') {
                        $orientation = 'landscape';
                        $papersize = 'a3';

                        $load_view = 'cutting.print.index_nagai';

                    }elseif ($request->paper == 'a4') {
                        $orientation = 'potrait';
                        $papersize = 'a4';

                        $load_view = 'cutting.print.index_nagai_a4';
                    }else{
                        $orientation = 'landscape';
                        $papersize = 'a3';

                        $load_view = 'cutting.print.index_nagai';
                    }
                }
            }else{

                if ($request->komponen_break == 1) {
                    if ($request->paper == 'a3') {
                        $orientation = 'landscape';
                        $papersize = 'a3';

                        $load_view = 'cutting.print.index_barcode';

                    }elseif ($request->paper == 'a4') {
                        $orientation = 'potrait';
                        $papersize = 'a4';

                        $load_view = 'cutting.print.index_barcode_a4';
                    }else{
                        $orientation = 'landscape';
                        $papersize = 'a3';

                        $load_view = 'cutting.print.index_barcode';
                    }
                }else {
                    if ($request->paper == 'a3') {
                        $orientation = 'landscape';
                        $papersize = 'a3';

                        $load_view = 'cutting.print.index';

                    }elseif ($request->paper == 'a4') {
                        $orientation = 'potrait';
                        $papersize = 'a4';

                        $load_view = 'cutting.print.index_a4';
                    }else{
                        $orientation = 'landscape';
                        $papersize = 'a3';

                        $load_view = 'cutting.print.index';
                    }
                }
            }


            $data = $data
                            ->orderBy('cutting_detail_style.style')
                            ->orderBy('cutting_detail_style.sticker_from')
                            ->orderBy('cutting_detail_style.sticker_to')
                            ->orderBy('cutting_detail_style.cut_number')
                            ->orderByRaw("NULLIF(regexp_replace(v_master_style.que, '\D', '', 'g'), '')::int")
                            ->orderBy('cutting_detail_style.part')
                            ->orderBy('v_master_style.komponen_name')
                            ->get();

            // dd($data);

            $tempsticker = 0;
            $newkey = 0;
            // $list_komponen_arr[$tempsticker] = '';
            $list_komponen_arr = array();

            $detail_komponen = array();

            foreach ($list_komponen as $key => $value) {
                $list_komponen_arr[$value->sticker_no][$newkey] = $value;

                $newkey++;
            }

            foreach ($list_komponen_arr as $key1 => $val1) {
                foreach ($val1 as $key2 => $val2) {
                    $detail_komponen[] = $val1;
                }
            }

            // count komponen
            $count_komponen = DB::table('master_style_detail')
                                    ->where(function($q) use ($style) {
                                        $q->where('style', $style);
                                        $q->orWhere('style', '*'.$style);
                                    })
                                    //->where('style', $request->style)
                                    ->whereNull('deleted_at')
                                    ->get();


            foreach ($count_komponen as $kex => $vax) {
                if($vax->is_special){
                    if (in_array($request->size, \Config::get('constants.BIG_SIZE'))) {
                        if (!$vax->is_big_size) {
                            unset($count_komponen[$kex]);
                        }
                    }else {
                        if ($vax->is_big_size) {
                            unset($count_komponen[$kex]);
                        }
                    }
                }
            }

            $pdf = \PDF::loadView($load_view,
                                    [
                                        'data' => $data,
                                        'list_komponen' => $list_komponen,
                                        'list_komponen_arr' => $list_komponen_arr,
                                        'detail_komponen' => $detail_komponen,
                                        'showbarcode' => true,
                                        'count_komponen' => count($count_komponen)
                                    ])
                        ->setPaper($papersize, $orientation);

            return $pdf->stream($filename);

            // $pdf = new Barcode($data);

            // $pdf->Output('I', $filename.".pdf", true);

            // exit;
    }
    //print preview list komponen
    public function printPreviewKomponen(Request $request) {

        if (isset($request->filter_po)) {
            $ponumber = $request->filter_po;
            // header
            $list_komponen_header = DB::table('v_list_komponen')
                                ->where('po_number', 'like', '%'.$ponumber.'%')
                                ->first();

            // count komponen by style
            $count_komponen = DB::table('master_style_detail')
                                ->where('style', $list_komponen_header->style)
                                ->whereNull('deleted_at')
                                ->count();
            // get list komponen
            $list_komponen = DB::table('v_list_komponen')
                                ->where('po_number', 'like', '%'.$ponumber.'%');
                                // ->get();
        }else{
            $ponumber = $request->po_number;

            $size = $request->size;
            $color = $request->color;
            $article = $request->article;
            $m_product_id = $request->m_product_id;
            $c_orderline_id = $request->c_orderline_id;

             // komponen
            $list_komponen_header = DB::table('v_list_komponen')
                                    //->where('style', $style)
                                    ->where('po_number', $ponumber)
                                    ->where('c_orderline_id', $c_orderline_id);
                                    //->first();

            $style = $list_komponen_header->first()->style;

            $barcodeid = null;
            $data_req = json_decode(stripslashes($request->data));

            $arr_barcode = [];

            if (count($data_req) > 0) {
                foreach ($data_req as $value) {
                    $rbarcode['barcode_id'] = $value->barcode_id;
                    $arr_barcode[] = $rbarcode;
                }
                //get cut number
                $data_cut_detail = DB::table('cutting_detail')->where('barcode_id', $arr_barcode[0]['barcode_id']);

                if ($data_cut_detail->exists()) {
                    $list_komponen_header = $list_komponen_header->where('cut_number', $data_cut_detail->first()->cut_number);
                }
            }


            $list_komponen_header = $list_komponen_header->first();

            // count komponen by style
            $count_komponen = DB::table('master_style_detail')
                                ->where('style', $style)
                                ->whereNull('deleted_at')
                                ->count();

            // get list komponen
            $list_komponen = DB::table('v_list_komponen')
                                ->where('style', $style)
                                ->where('po_number', $ponumber)
                                ->where('c_orderline_id', $c_orderline_id);
                                // ->orderBy('sticker_from');
                                // ->get();
        }


        if (isset($request->sticker_no)) {
            $list_komponen = $list_komponen->where('sticker_no', $request->sticker_no)
                                            ->orderBy('komponen_name')
                                            ->orderBy('sticker_no')
                                            ->get();
        }else {
            //
            if(isset($request->barcode_id) && !empty($request->barcode_id)) {
                $barcodeid = $request->barcode_id;
                $get_sticker = DB::table('cutting_detail')
                                    ->where('barcode_id', $barcodeid)
                                    ->first();

                $list_komponen = $list_komponen->where('sticker_no', $get_sticker->sticker_no);
            }
            elseif (isset($request->data)) {
                $barcodeid = null;
                $data_req = json_decode(stripslashes($request->data));

                $arr_barcode = [];
                $arr_sticker = [];
                $arr_cuts = [];

                if (count($data_req) > 0) {
                    foreach ($data_req as $value) {
                        $rbarcode['barcode_id'] = $value->barcode_id;
                        $arr_barcode[] = $rbarcode;
                    }

                    $get_sticker = DB::table('cutting_detail')
                                        ->whereIn('barcode_id', $arr_barcode)
                                        ->get();

                    foreach ($get_sticker as $val) {
                        $rSticker['sticker_no'] = $val->sticker_no;
                        $rCuts['cut_number'] = $val->cut_number;
                        $arr_sticker[] = $rSticker;
                        $arr_cuts[] = $rCuts;
                    }

                    $list_komponen = $list_komponen
                                                    ->whereIn('sticker_no', $arr_sticker)
                                                    ->whereIn('cut_number', $arr_cuts);
                }
            }

            $list_komponen = $list_komponen->orderBy('komponen_name')
                                            ->orderBy('sticker_no')
                                            ->get();
        }

            $filename = 'list_po_number_#' . $ponumber . '.pdf';

            //config layout
            if ($request->paper == 'a3') {
                $orientation = 'landscape';
                $papersize = 'a3';

                $load_view = 'cutting.print.list_komponen';

            }elseif ($request->paper == 'a4') {
                $orientation = 'potrait';
                $papersize = 'a4';

                $load_view = 'cutting.print.list_komponen_a4';
            }else{
                $orientation = 'landscape';
                $papersize = 'a3';

                $load_view = 'cutting.print.list_komponen';
            }

            $data = array();

            $tempsticker = 0;
            $newkey = 0;

            $list_komponen_arr = array();

            $detail_komponen = array();

            $list_sticker = array();
            $list_part = array();

            if ($request->part == 1) {
                foreach ($list_komponen as $key => $value) {
                    $list_komponen_arr[$value->sticker_no.$value->part.$value->cut_number][$newkey] = $value;
                    $list_part[$value->sticker_no.$value->part.$value->cut_number] = $value->part;
                    $list_sticker[$value->sticker_no.$value->part.$value->cut_number] = $value->sticker_no;

                    $newkey++;
                }
            }else {
                foreach ($list_komponen as $key => $value) {
                    $list_komponen_arr[$value->sticker_no.$value->cut_number][$newkey] = $value;
                    $list_sticker[$value->sticker_no.$value->cut_number] = $value->sticker_no;

                    $newkey++;
                }
            }


            foreach ($list_komponen_arr as $key1 => $val1) {
                foreach ($val1 as $key2 => $val2) {
                    $detail_komponen[] = $val1;
                }
            }

            $chunk = array_chunk($list_komponen_arr,2);
            $chunk_new = array();
            foreach ($chunk as $key2 => $val2) {
                foreach ($val2 as $key3 => $val3) {
                        $chunk_new[] = $val3;
                }
            }
            // array_multisort(array_map('count', $chunk_new), SORT_DESC, $chunk_new);

            $list_sticker_new = array();
            $keyy = 0;
            foreach ($list_sticker as $vals) {
                $list_sticker_new[$keyy] = $vals;
                $keyy++;
            }


            $list_part_new = array();
            $keyy2 = 0;
            foreach ($list_part as $vals) {
                $list_part_new[$keyy2] = $vals;
                $keyy2++;
            }

            $factories = DB::table('factories')
                            ->whereNull('deleted_at')
                            ->first();

            $pdf = \PDF::loadView($load_view,
                                    [
                                        'data' => $data,
                                        'list_komponen' => $list_komponen,
                                        'list_komponen_arr' => $list_komponen_arr,
                                        'detail_komponen' => $detail_komponen,
                                        'list_komponen_header' => $list_komponen_header,
                                        'chunk_new' => $chunk_new,
                                        'list_sticker_new' => $list_sticker_new,
                                        'factories' => $factories,
                                        'count_komponen' => $count_komponen,
                                        'showbarcode' => true,
                                        'list_part_new' => $list_part_new
                                    ])
                        ->setPaper($papersize, $orientation);

            return $pdf->stream($filename);

            // $pdf = new Barcode($data);

            // $pdf->Output('I', $filename.".pdf", true);

            // exit;
    }

    //print preview by formulir
    public function printPreviewByFormulir(Request $request) {

        $data = DB::table('cutting_detail_style')
                    ->select(
                            'cutting_detail_style.barcode_id',
                            'cutting_detail_style.po_summary_detail_id',
                            'cutting_detail_style.cut_number',
                            'cutting_detail_style.cut_info',
                            'cutting_detail_style.lot',
                            'cutting_detail_style.komponen',
                            'cutting_detail_style.part',
                            'formulir_artwork.qty',
                            'cutting_detail_style.sticker_from',
                            'cutting_detail_style.sticker_to',
                            'cutting_detail_style.sticker_no',
                            'cutting_detail_style.cutting_date',
                            'cutting_detail_style.bundle_id',
                            'cutting_detail_style.bundle',
                            'cutting_detail_style.qc',
                            'cutting_detail_style.cutter',
                            'cutting_detail_style.current_process',
                            'cutting_detail_style.current_status',
                            'cutting_detail_style.process',
                            'cutting_detail_style.already_process',
                            'cutting_detail_style.deleted_at',
                            'cutting_detail_style.is_printed',
                            'cutting_detail_style.is_printed_formulir',
                            'cutting_detail_style.is_printed_sj',
                            'cutting_detail_style.description',
                            'cutting_detail_style.created_at',
                            'cutting_detail_style.updated_at',
                            'cutting_detail_style.uoms',
                            'v_cutting_process_detail.process_name',
                            'po_summary.style',
                            'po_summary_detail.po_number',
                            'po_summary_detail.plan_ref',
                            'po_summary_detail.size',
                            'po_summary_detail.color',
                            'po_summary_detail.article',
                            'po_summary_detail.factory_id',
                            'po_summary_detail.mo',
                            'po_summary_detail.qtyordered',
                            'po_summary_detail.kst_joborder',
                            'po_summary_detail.type_id',
                            'po_summary_detail.type_description',
                            'po_summary_detail.category',
                            'po_summary_detail.m_product_id',
                            'po_summary_detail.kode_product',
                            'po_summary_detail.nama_product',
                            'po_summary_detail.from',
                            'po_summary_detail.total_qty',
                            'po_summary_detail.color_edit',
                            'po_summary_detail.article_edit',
                            'po_summary_detail.style_edit',
                            'v_master_style.komponen_name',
                            'v_master_style.total',
                            'po_summary_detail.po_number_edit',
                            'po_summary_detail.size_edit',
                            'formulir_artwork.sticker_reject',
                            DB::raw('cutting_detail_style.qty AS qty_all')
                    )
                    ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail_style.po_summary_detail_id')
                    ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                    ->join('v_master_style', function($join){
                        $join->on('v_master_style.style', '=', 'cutting_detail_style.style');
                        $join->on('v_master_style.komponen', '=', 'cutting_detail_style.komponen');
                    })
                    ->join('formulir_artwork', 'formulir_artwork.barcode_id', '=', 'cutting_detail_style.barcode_id')
                    ->join('formulir_artwork_header', 'formulir_artwork_header.id', '=', 'formulir_artwork.formulir_artwork_header_id')
                    ->leftjoin('v_cutting_process_detail', 'v_cutting_process_detail.barcode_id', '=', 'cutting_detail_style.barcode_id')
                    ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
                    ->where('formulir_artwork_header.id', $request->formulir_id)
                    ->whereNull('cutting_detail_style.deleted_at')
                    ->whereNull('formulir_artwork_header.deleted_at')
                    ->whereNull('formulir_artwork.deleted_at');

            $filename = 'formulir_#_.pdf';

            //config layout
            $orientation = 'landscape';
            $papersize = 'a3';
            $load_view = 'cutting.print.panel_formulir';

            if ($request->paper == 'a4') {
                $orientation = 'potrait';
                $papersize = 'a4';
                $load_view = 'cutting.print.panel_formulir_a4';
            }

            $data = $data->get();

            $pdf = \PDF::loadView($load_view,
                                    [
                                        'data' => $data,
                                        'showbarcode' => true
                                    ])
                        ->setPaper($papersize, $orientation);

            return $pdf->stream($filename);

    }

    //print preview by formulir
    public function printPreviewByFormulirReject(Request $request) {

        $data = DB::table('cutting_detail_style')
                    ->select(
                            'cutting_detail_style.barcode_id',
                            'cutting_detail_style.po_summary_detail_id',
                            'cutting_detail_style.cut_number',
                            'cutting_detail_style.cut_info',
                            'cutting_detail_style.lot',
                            'cutting_detail_style.komponen',
                            'cutting_detail_style.part',
                            'formulir_artwork.qty',
                            'cutting_detail_style.sticker_from',
                            'cutting_detail_style.sticker_to',
                            'cutting_detail_style.sticker_no',
                            'cutting_detail_style.cutting_date',
                            'cutting_detail_style.bundle_id',
                            'cutting_detail_style.bundle',
                            'cutting_detail_style.qc',
                            'cutting_detail_style.cutter',
                            'cutting_detail_style.current_process',
                            'cutting_detail_style.current_status',
                            'cutting_detail_style.process',
                            'cutting_detail_style.already_process',
                            'cutting_detail_style.deleted_at',
                            'cutting_detail_style.is_printed',
                            'cutting_detail_style.is_printed_formulir',
                            'cutting_detail_style.is_printed_sj',
                            'cutting_detail_style.description',
                            'cutting_detail_style.created_at',
                            'cutting_detail_style.updated_at',
                            'cutting_detail_style.uoms',
                            'v_cutting_process_detail.process_name',
                            'po_summary.style',
                            'po_summary_detail.po_number',
                            'po_summary_detail.plan_ref',
                            'po_summary_detail.size',
                            'po_summary_detail.color',
                            'po_summary_detail.article',
                            'po_summary_detail.factory_id',
                            'po_summary_detail.mo',
                            'po_summary_detail.qtyordered',
                            'po_summary_detail.kst_joborder',
                            'po_summary_detail.type_id',
                            'po_summary_detail.type_description',
                            'po_summary_detail.category',
                            'po_summary_detail.m_product_id',
                            'po_summary_detail.kode_product',
                            'po_summary_detail.nama_product',
                            'po_summary_detail.from',
                            'po_summary_detail.total_qty',
                            'po_summary_detail.color_edit',
                            'po_summary_detail.article_edit',
                            'po_summary_detail.style_edit',
                            'v_master_style.komponen_name',
                            'v_master_style.total',
                            'po_summary_detail.po_number_edit',
                            'po_summary_detail.size_edit',
                            'formulir_artwork.sticker_reject',
                            DB::raw('cutting_detail_style.qty AS qty_all'),
                            'type_packing_list.type_name'
                    )
                    ->join('po_summary_detail','po_summary_detail.id','=','cutting_detail_style.po_summary_detail_id')
                    ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                    ->join('v_master_style', function($join){
                        $join->on('v_master_style.style', '=', 'cutting_detail_style.style');
                        $join->on('v_master_style.komponen', '=', 'cutting_detail_style.komponen');
                    })
                    ->join('formulir_artwork', 'formulir_artwork.barcode_id', '=', 'cutting_detail_style.barcode_id')
                    ->join('formulir_artwork_header', 'formulir_artwork_header.id', '=', 'formulir_artwork.formulir_artwork_header_id')
                    ->join('type_packing_list', 'type_packing_list.id','=','formulir_artwork.type_pl')
                    ->leftjoin('v_cutting_process_detail', 'v_cutting_process_detail.barcode_id', '=', 'cutting_detail_style.barcode_id')
                    ->where('po_summary_detail.factory_id', Auth::user()->factory_id)
                    ->where('formulir_artwork_header.id', $request->formulir_id)
                    ->whereNull('cutting_detail_style.deleted_at')
                    ->whereNull('formulir_artwork_header.deleted_at')
                    ->whereNull('formulir_artwork.deleted_at')
                    ->whereNull('type_packing_list.deleted_at');

            $filename = 'formulir_#_.pdf';

            //config layout
            $orientation = 'landscape';
            $papersize = 'a3';
            $load_view = 'cutting.print.panel_formulir_reject';

            if ($request->paper == 'a4') {
                $orientation = 'potrait';
                $papersize = 'a4';
                $load_view = 'cutting.print.panel_formulir_reject_a4';
            }

            $data = $data->get();

            $pdf = \PDF::loadView($load_view,
                                    [
                                        'data' => $data,
                                        'showbarcode' => true
                                    ])
                        ->setPaper($papersize, $orientation);

            return $pdf->stream($filename);

    }

    //MASTER SUBCONT
    public function masterSubcont()
    {
        return view('cutting.master.index');
    }

    public function ajaxGetDataMasterSubcont(Request $request)
    {
        if ($request->ajax()){
            $data = DB::table('subcont')
                ->wherenull('deleted_at')
                ->orderBy('created_at', 'desc')
                ->get();

            //datatables

            return DataTables::of($data)
                ->addColumn('action', function($data){ return view('_action', [
                        // 'edit' => route('cutting.editSubcont', $data->id),
                        'edit_modal' => route('cutting.editSubcont', $data->id),
                        'id' => $data->id,
                        'delete_subcont' => route('cutting.deleteSubcont', $data->id)
                    ]);
                })
            ->make(true);
        }
    }

    //add new master subcont
    public function addSubcont(Request $request)
    {

        $this->validate($request, [
            'name' => 'required'
        ]);

        $data = array([
            'name' => $request->name,
            'created_at' => Carbon::now()
        ]);

        try {
            DB::begintransaction();

            //insert table sewing
            DB::table('subcont')->insert($data);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    //edit master subcont
    public function editSubcont($id)
    {
        $subcont = DB::table('subcont')
                        ->where('id', $id)
                        ->first();
        // return view('cutting.master.edit', compact('subcont'));

        return response()->json($subcont, 200);
    }

    //update master subcont
    public function updateSubcont(Request $request)
    {
        $data = $request->all();

                    DB::table('subcont')
                        ->where('id', $data['id_update'])
                        ->update([
                            'name' => $data['name_update'],
                            'updated_at' => Carbon::now()
                        ]);

        return view('cutting.master.index');

    }

    //delete master subcont
    public function deleteSubcont(Request $request)
    {
        $id = $request->id;
        DB::table('subcont')
                ->where('id', $id)
                ->update([
                    'deleted_at' => Carbon::now()
                ]);

        return view('cutting.master.index');
    }

    //END OF MASTER SUBCONT

    //MASTER STYLE
    public function masterStyle()
    {
        $process = DB::table('process')
                    ->whereNull('deleted_at')
                    ->get();

        $komponen = DB::table('master_komponen')
                    ->whereNull('deleted_at')
                    ->get();

        $types = DB::table('types')
                    ->whereNull('deleted_at')
                    ->get();

        $styles = DB::table('v_breakdown_size_2')
                    ->select('style')
                    // ->select(DB::raw('COALESCE(style_so, style) style'))
                    // ->groupBy(DB::raw('COALESCE(style_so, style)'))
                    ->groupBy('style')
                    //->limit(10)
                    ->get();

        return view('cutting.master.style.index', compact('process', 'komponen', 'types', 'styles'));
    }

    public function ajaxGetDataMasterStyle(Request $request)
    {
        //api
        if($request->radio_status) {
            //filtering
            if($request->radio_status == 'style') {

                if(empty($request->style)) {
                   $style = 'null';
                }
                else {
                   $style = $request->style;
                }

               $params = DB::table('v_breakdown_size')
                            ->select('style')
                            // ->where('style', 'like', '%'.strtoupper($style).'%')
                            ->where('style', $style)
                            ->groupBy('style')
                            ->get();
            }
           //  BBI gak ada lc
            elseif($request->radio_status == 'lc') {

            }

            // cek style exists
            $cekifstyleexist = DB::table('v_master_style_new')
                                    // ->where('style', 'like', '%'.strtoupper($style).'%')
                                    ->where('style', $style)
                                    // ->whereNull('deleted_at')
                                    ->exists();

            if ($cekifstyleexist) {
                $dataxx = DB::table('v_master_style_new')
                            // ->where('style', 'like', '%'.strtoupper($style).'%')
                            ->where('style', $style)
                            ->get();

                $data = json_decode(json_encode($dataxx), true);
            }else{

                $data = json_decode(json_encode($params), true);
            }

            return Datatables::of($data)
                    ->editColumn('part', function($data){
                        $part = isset($data['part']) ? $data['part'] : '-';

                        return $part;
                    })
                    ->editColumn('komponen_name', function($data){
                        $komponen_name = isset($data['komponen_name']) ? $data['komponen_name'] : '-';

                        return $komponen_name;
                    })
                    ->editColumn('process_name', function($data){
                        $process_name = isset($data['process_name']) ? $data['process_name'] : '-';

                        return $process_name;
                    })
                    ->editColumn('total', function($data){
                        $total = isset($data['total']) ? $data['total'] : '-';

                        return $total;
                    })
                    ->editColumn('type_name', function($data){
                        $type_name = isset($data['type_name']) ? $data['type_name'] : '-';

                        return $type_name;
                    })
                    ->editColumn('action', function($data){

                        $style = $data['style'];
                        $id = isset($data['id']) ? $data['id'] : null;
                        $part = isset($data['part']) ? $data['part'] : null;
                        $komponen = isset($data['komponen']) ? $data['komponen'] : null;
                        $komponen_name = isset($data['komponen_name']) ? $data['komponen_name'] : null;
                        $total = isset($data['total']) ? $data['total'] : null;
                        $process = isset($data['process']) ? $data['process'] : null;
                        $process_name = isset($data['process_name']) ? $data['process_name'] : null;

                        $check = DB::table('v_master_style_new')
                                    // ->where('style', 'like', '%'.strtoupper($style).'%')
                                    ->where('style', $style)
                                    ->exists();

                        if(!$check) {
                                return view('_action', [
                                    'model'  => $data,
                                    'upload' => $style,
                                    'upload_sync' => $style
                                ]);

                        }
                        else {

                                return view('_action', [
                                    'model' => $data,
                                    'upload' => $style,
                                    'style' => $style,
                                    'id' => $id,
                                    'komponen' => $komponen,
                                    'edit_modal' => route('cutting.editStyle',
                                                [
                                                    // 'style' => $style,
                                                    'id' => $id,
                                                    'komponen' => $komponen
                                                ]),
                                    'delete_style' => route('cutting.deleteStyle',
                                                [
                                                    // 'style' => $style,
                                                    'id' => $id,
                                                    'komponen' => $komponen
                                                ])
                                ]);

                        }
                    })
                    ->rawColumns(['part', 'komponen', 'action'])
                    ->make(true);
        }
        else {
            $data = array();
            return Datatables::of($data)
                   ->editColumn('style', function($data) {
                       return null;
                   })
                   ->editColumn('part', function($data) {
                       return null;
                   })
                   ->editColumn('komponen', function($data) {
                       return null;
                   })
                   ->editColumn('type_name', function($data) {
                       return null;
                   })
                   ->editColumn('action', function($data) {
                       return null;
                   })
                   ->rawColumns(['part', 'komponen', 'action'])
                   ->make(true);
        }
    }

    //add new master style
    public function addStyle(Request $request)
    {

        $this->validate($request, [
            'modalstyle' => 'required',
            'part' => 'required',
            'komponen' => 'required',
            'total' => 'required'
        ]);

        // cek apakah style sudah digunakan
        $cekifused = DB::table('master_style')
                        ->where('style', $request->modalstyle)
                        ->where('is_used', true)
                        ->whereNull('deleted_at')
                        ->exists();

        $cekifused2 = DB::table('po_summary')
                        ->where('style', $request->modalstyle)
                        ->whereNull('deleted_at')
                        ->exists();
        // sementara dilepas
        // if ($cekifused || $cekifused2) {
        //     return response()->json('style already used..!', 422);
        // }

        $komponen = $request->komponen;

        $is_inhouse = false;

        if (isset($request->is_inhouse)) {
            if ($request->is_inhouse == 'on') {
                $is_inhouse = true;
            }else{
                $is_inhouse = false;
            }
        }

        $is_big_size = $request->is_big_size === "true" ? true : false;
        $is_special = $request->is_special === "true" ? true : false;

        // cek style & komponen
        $cekifexist = DB::table('master_style')
                        ->join('master_style_detail', 'master_style_detail.style', '=', 'master_style.style')
                        ->where('master_style_detail.style', $request->modalstyle)
                        ->where('master_style_detail.komponen', $komponen)
                        ->whereNull('master_style.deleted_at')
                        ->whereNull('master_style_detail.deleted_at')
                        ->exists();

        if ($cekifexist) {
            return response()->json('komponen already exists..!', 422);
        }

        $process = $request->process_id != 'null' ? $request->process_id : null;

        $data = array();

        $det['style'] = $request->modalstyle;
        $det['part'] = trim($request->part);
        $det['komponen'] = $komponen;
        $det['process'] = $process;
        $det['total'] = (int)$request->total;
        $det['created_at'] = Carbon::now();
        $det['is_inhouse'] = $is_inhouse;
        $det['type_id'] = $request->type_id;
        $det['is_big_size'] = $is_big_size;
        $det['is_special'] = $is_special;

        $data[] = $det;

        try {
            DB::begintransaction();

            //insert table master style
            // cek if style exists
            $cekifstyleexist = DB::table('master_style')
                                    ->where('style', $request->modalstyle)
                                    ->whereNull('deleted_at')
                                    ->exists();

            if (!$cekifstyleexist) {

                DB::table('master_style')->insert([
                    'style' => $request->modalstyle,
                    'created_at' => Carbon::now()
                ]);
            }

            //insert table master style detail
            DB::table('master_style_detail')->insert($data);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    //edit master style
    public function editStyle(Request $request)
    {
        // $style = $request->style;
        $id = $request->id;
        $komponen = $request->komponen;

        $data = DB::table('v_master_style_new')
                    // ->where('style', $style)
                    ->where('id', $id)
                    ->where('komponen', $komponen)
                    ->first();

        return response()->json($data, 200);
    }

    //update master style
    public function updateStyle(Request $request)
    {
        $data = $request->all();

        $is_inhouse = false;

        if (isset($data['is_inhouse_update'])) {
            if ($data['is_inhouse_update'] == 'on') {
                $is_inhouse = true;
            }else{
                $is_inhouse = false;
            }
        }

        $is_big_size = $data['is_big_size_update'] === "true" ? true : false;
        $is_special = $data['is_special_update'] === "true" ? true : false;

        // // cek apakah style sudah digunakan
        // $cekifused = DB::table('master_style')
        //                 ->where('style', $data['styleUpdate'])
        //                 ->where('is_used', true)
        //                 ->whereNull('deleted_at')
        //                 ->exists();

        // $cekifused2 = DB::table('po_summary')
        //                 ->where('style', $data['styleUpdate'])
        //                 ->whereNull('deleted_at')
        //                 ->exists();

        // if ($cekifused || $cekifused2) {
        //     return response()->json('style already used..!', 422);
        // }

        try {
            DB::beginTransaction();

                DB::table('master_style')
                    ->where('style', $data['styleUpdate'])
                    ->update([
                        'updated_at' => Carbon::now()
                    ]);

                DB::table('master_style_detail')
                    ->where('style', $data['styleUpdate'])
                    ->where('komponen', $data['komponenUpdate'])
                    ->update([
                        'part' => $data['partUpdate'],
                        'process' => $data['process_id_update'] != 'null' ? $data['process_id_update'] : null,
                        'total' => $data['totalUpdate'],
                        'is_inhouse' => $is_inhouse,
                        'is_big_size' => $is_big_size,
                        'is_special' => $is_special,
                        'type_id' => $data['type_id_update'],
                        'updated_at' => Carbon::now()
                    ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        // return view('cutting.master.index');
    }

    //delete master style
    public function deleteStyle(Request $request)
    {
        // $style = $request->style;
        $id = $request->id;
        $komponen = $request->komponen;

        // get data style
        $style = DB::table('master_style')
                    ->where('id', $id)
                    ->first()->style;

        try {
            DB::beginTransaction();

            DB::table('master_style')
                ->where('id', $id)
                ->update([
                    'updated_at' => Carbon::now()
                ]);

            DB::table('master_style_detail')
                ->where('style', $style)
                ->where('komponen', $komponen)
                ->update([
                    'deleted_at' => Carbon::now()
                ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        //return view('cutting.master.index');
    }

    //END OF MASTER STYLE

    //MASTER KOMPONEN
    public function masterKomponen()
    {
        return view('cutting.master.komponen.index');
    }

    public function ajaxGetDataMasterKomponen(Request $request)
    {
        if ($request->ajax()){
            $data = DB::table('master_komponen')
                ->wherenull('deleted_at')
                ->orderBy('created_at', 'desc')
                ->get();

            //datatables

            return DataTables::of($data)
                ->addColumn('action', function($data){ return view('_action', [
                        'edit' => route('cutting.editKomponen', $data->id),
                        'id' => $data->id,
                        'deletes' => route('cutting.deleteKomponen', $data->id)
                    ]);
                })
            ->make(true);
        }
    }

    //add new master komponen
    public function addKomponen(Request $request)
    {

        $this->validate($request, [
            'name' => 'required'
        ]);

        $name = trim(strtoupper($request->name));

        $data = array([
            'name' => $name,
            'created_at' => Carbon::now()
        ]);

        // cek komponen exists
        $cekifexist = DB::table('master_komponen')
                        ->where('name', $name)
                        ->whereNull('deleted_at')
                        ->exists();

        if ($cekifexist) {
            return response()->json('Komponen already exists..!', 422);
        }

        try {
            DB::begintransaction();

            //insert table komponen
            DB::table('master_komponen')->insert($data);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    //edit master komponen
    public function editKomponen($id)
    {
        $komponen = DB::table('master_komponen')
                        ->where('id', $id)
                        ->first();
        return view('cutting.master.komponen.edit', compact('komponen'));
    }

    //update master komponen
    public function updateKomponen(Request $request)
    {
        $data = $request->all();

        $name = trim(strtoupper($data['name']));

        // cek komponen exists
        $cekifexist = DB::table('master_komponen')
                        ->where('name', $name)
                        ->whereNull('deleted_at')
                        ->exists();

        if ($cekifexist) {
            return response()->json('Komponen already exists..!', 422);
        }


        DB::table('master_komponen')
            ->where('id', $data['id'])
            ->update([
                'name' => $name,
                'updated_at' => Carbon::now()
            ]);

        return view('cutting.master.komponen.index');

    }

    //delete master komponen
    public function deleteKomponen(Request $request)
    {
        $id = $request->id;
        DB::table('master_komponen')
                ->where('id', $id)
                ->update([
                    'deleted_at' => Carbon::now()
                ]);

        return view('cutting.master.komponen.index');
    }

    //END OF MASTER KOMPONEN

    //MASTER PROCESS
    public function masterProcess()
    {
        return view('cutting.master.process.index');
    }

    public function ajaxGetDataMasterProcess(Request $request)
    {
        if ($request->ajax()){
            $data = DB::table('process')
                ->wherenull('deleted_at')
                ->orderBy('created_at', 'desc')
                ->get();

            //datatables

            return DataTables::of($data)
                ->addColumn('action', function($data){ return view('_action', [
                        // 'edit' => route('cutting.editProcess', $data->id),
                        'edit_modal' => route('cutting.editProcess', $data->id),
                        'id' => $data->id,
                        'deletes' => route('cutting.deleteProcess', $data->id)
                    ]);
                })
            ->make(true);
        }
    }

    //add new master process
    public function addProcess(Request $request)
    {

        $this->validate($request, [
            'process_name' => 'required'
        ]);

        $process_name = trim(strtoupper($request->process_name));

        $data = array([
            'process_name' => $process_name,
            'created_at' => Carbon::now()
        ]);

        // cek process exists
        $cekifexist = DB::table('process')
                        ->where('process_name', $process_name)
                        ->whereNull('deleted_at')
                        ->exists();

        if ($cekifexist) {
            return response()->json('Master Process already exists..!', 422);
        }

        try {
            DB::begintransaction();

            //insert table process
            DB::table('process')->insert($data);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    //edit master process
    public function editProcess($id)
    {
        $process = DB::table('process')
                        ->where('id', $id)
                        ->first();
        // return view('cutting.master.process.edit', compact('process'));

        return response()->json($process, 200);
    }

    //update master process
    public function updateProcess(Request $request)
    {
        $data = $request->all();

        $process_name = trim(strtoupper($data['process_name_update']));

        // cek process exists
        $cekifexist = DB::table('process')
                        ->where('process_name', $process_name)
                        ->whereNull('deleted_at')
                        ->exists();

        if ($cekifexist) {
            return response()->json('Master process already exists..!', 422);
        }


        DB::table('process')
            ->where('id', $data['id_update'])
            ->update([
                'process_name' => $process_name,
                'updated_at' => Carbon::now()
            ]);

        return view('cutting.master.process.index');

    }

    //delete master process
    public function deleteProcess(Request $request)
    {
        $id = $request->id;
        DB::table('process')
                ->where('id', $id)
                ->update([
                    'deleted_at' => Carbon::now()
                ]);

        return view('cutting.master.process.index');
    }

    //END OF MASTER PROCESS

    // ajax get style
    public function ajaxGetDataGetListStyle()
    {
        $data = DB::table('master_style')
                    ->whereNull('deleted_at')
                    ->get();

        return view('cutting/master/style/list_style')->with('data', $data);
    }

    // sync style
    public function styleSync(Request $request)
    {
        $style = $request->style;
        $style_id = $request->style_id;

        $style_sync = $request->style_sync;

        $data_style_detail = array();

        $get_style = DB::table('master_style_detail')
                            ->where('style', $style)
                            ->whereNull('deleted_at')
                            ->get();

        foreach ($get_style as $key => $val) {
            $detail['style'] = $style_sync;
            $detail['part'] = $val->part;
            $detail['komponen'] = $val->komponen;
            $detail['process'] = $val->process;
            $detail['total'] = $val->total;
            $detail['created_at'] = Carbon::now();
            $detail['is_inhouse'] = $val->is_inhouse;
            $detail['is_big_size'] = $val->is_big_size;
            $detail['is_special'] = $val->is_special;

            $data_style_detail[] = $detail;
        }

        try {
            DB::begintransaction();


            $cekexist = DB::table('master_style')
                            ->where('style', $style_sync)
                            ->whereNull('deleted_at')
                            ->exists();

            if(!$cekexist){
                // insert style header
                DB::table('master_style')->insert(
                        [
                            'style' => $style_sync,
                            'created_at' => Carbon::now()
                        ]
                    );
            }

            // insert style detail
            DB::table('master_style_detail')->insert($data_style_detail);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json('success', 200);
    }

    public function getDataKomponen(Request $request)
    {
        $style = $request->style;

        $data = DB::table('v_master_style')
                    ->where('style', $style)
                    ->get();

        return view('cutting.list_komponen', compact('data'));
    }

    // ajax get po summary detail
    public function ajaxGetDataSummaryDetail(Request $request)
    {
        $data = DB::table('po_summary_detail')
                    ->select(
                        'po_summary_detail.id',
                        'po_summary_detail.po_number',
                        'po_summary_detail.plan_ref',
                        'po_summary_detail.style',
                        'po_summary_detail.size',
                        'po_summary_detail.color',
                        'po_summary_detail.article',
                        'po_summary_detail.deleted_at',
                        'po_summary_detail.factory_id',
                        'po_summary_detail.mo',
                        'po_summary_detail.qtyordered',
                        'po_summary_detail.kst_joborder',
                        'po_summary_detail.type_id',
                        'po_summary_detail.type_description',
                        'po_summary_detail.category',
                        'po_summary_detail.m_product_id',
                        'po_summary_detail.kode_product',
                        'po_summary_detail.nama_product',
                        'po_summary_detail.from',
                        'po_summary_detail.qty',
                        'po_summary_detail.total_qty',
                        'po_summary_detail.color_edit',
                        'po_summary_detail.article_edit',
                        'po_summary_detail.created_at',
                        'po_summary_detail.updated_at',
                        'po_summary_detail.is_inhouse',
                        'po_summary_detail.po_number_edit',
                        'po_summary_detail.style_edit',
                        'po_summary_detail.po_summary_id',
                        'po_summary_detail.c_order_id',
                        'po_summary_detail.c_orderline_id',
                        'po_summary_detail.size_edit',
                        'po_summary_detail.no_bundle',
                        'cutting_detail.cut_number'
                    )
                    ->join('cutting_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                    ->where('po_summary_detail.c_orderline_id', $request->c_orderline_id)
                    ->where('po_summary_detail.style', $request->style)
                    ->whereNull('po_summary_detail.deleted_at')
                    ->groupBy([
                        'po_summary_detail.id',
                        'po_summary_detail.po_number',
                        'po_summary_detail.plan_ref',
                        'po_summary_detail.style',
                        'po_summary_detail.size',
                        'po_summary_detail.color',
                        'po_summary_detail.article',
                        'po_summary_detail.deleted_at',
                        'po_summary_detail.factory_id',
                        'po_summary_detail.mo',
                        'po_summary_detail.qtyordered',
                        'po_summary_detail.kst_joborder',
                        'po_summary_detail.type_id',
                        'po_summary_detail.type_description',
                        'po_summary_detail.category',
                        'po_summary_detail.m_product_id',
                        'po_summary_detail.kode_product',
                        'po_summary_detail.nama_product',
                        'po_summary_detail.from',
                        'po_summary_detail.qty',
                        'po_summary_detail.total_qty',
                        'po_summary_detail.color_edit',
                        'po_summary_detail.article_edit',
                        'po_summary_detail.created_at',
                        'po_summary_detail.updated_at',
                        'po_summary_detail.is_inhouse',
                        'po_summary_detail.po_number_edit',
                        'po_summary_detail.style_edit',
                        'po_summary_detail.po_summary_id',
                        'po_summary_detail.c_order_id',
                        'po_summary_detail.c_orderline_id',
                        'po_summary_detail.size_edit',
                        'po_summary_detail.no_bundle',
                        'cutting_detail.cut_number'
                    ])
                    ->get();

        return view('cutting/list_orderline')->with('data', $data);
    }

    //delete summary detail
    public function deleteSummaryDetail(Request $request)
    {
        $po_summary_detail_id = $request->id;
        $po_summary_id = $request->po_summary_id;
        $c_orderline_id = $request->c_orderline_id;
        $style = $request->style;

        $data_history_po_summary = array();
        $data_history_po_summary_detail = array();
        $data_history_cutting_detail = array();
        $data_history_cutting_movements = array();

        $data_po_summary = DB::table('po_summary')
                                ->where('id', $po_summary_id)
                                ->whereNull('deleted_at')
                                ->get();

        foreach ($data_po_summary as $key => $val) {

                $pos['id'] = $val->id;
                $pos['po_number'] = $val->po_number;
                $pos['plan_ref'] = $val->plan_ref;
                $pos['style'] = $val->style;
                $pos['dateordered'] = $val->dateordered;
                $pos['datepromised'] = $val->datepromised;
                $pos['deleted_at'] = $val->deleted_at;
                $pos['psd'] = $val->psd;
                $pos['podd'] = $val->podd;
                $pos['pcd'] = $val->pcd;
                $pos['is_cancel_order'] = $val->is_cancel_order;
                $pos['factory_id'] = $val->factory_id;
                $pos['city_name'] = $val->city_name;
                $pos['created_at'] = $val->created_at;
                $pos['updated_at'] = $val->updated_at;
                $pos['is_inhouse'] = $val->is_inhouse;
                $pos['buyer'] = $val->buyer;
                $pos['c_order_id'] = $val->c_order_id;
                $pos['deleted_by'] = Auth::user()->id;
                $pos['move_date'] = Carbon::now();
                $data_history_po_summary[] = $pos;
        }

        $data_po_summary_detail = DB::table('po_summary_detail')
                                    ->where('id', $po_summary_detail_id)
                                    ->whereNull('deleted_at')
                                    ->get();

        foreach ($data_po_summary_detail as $key2 => $val2) {
                $posd['id'] = $val2->id;
                $posd['po_number'] = $val2->po_number;
                $posd['plan_ref'] = $val2->plan_ref;
                $posd['style'] = $val2->style;
                $posd['size'] = $val2->size;
                $posd['color'] = $val2->color;
                $posd['article'] = $val2->article;
                $posd['deleted_at'] = $val2->deleted_at;
                $posd['factory_id'] = $val2->factory_id;
                $posd['mo'] = $val2->mo;
                $posd['qtyordered'] = $val2->qtyordered;
                $posd['kst_joborder'] = $val2->kst_joborder;
                $posd['type_id'] = $val2->type_id;
                $posd['type_description'] = $val2->type_description;
                $posd['category'] = $val2->category;
                $posd['m_product_id'] = $val2->m_product_id;
                $posd['kode_product'] = $val2->kode_product;
                $posd['nama_product'] = $val2->nama_product;
                $posd['from'] = $val2->from;
                $posd['qty'] = $val2->qty;
                $posd['total_qty'] = $val2->total_qty;
                $posd['color_edit'] = $val2->color_edit;
                $posd['article_edit'] = $val2->article_edit;
                $posd['is_inhouse'] = $val2->is_inhouse;
                $posd['po_number_edit'] = $val2->po_number_edit;
                $posd['style_edit'] = $val2->style_edit;
                $posd['po_summary_id'] = $val2->po_summary_id;
                $posd['c_order_id'] = $val2->c_order_id;
                $posd['c_orderline_id'] = $val2->c_orderline_id;
                $posd['size_edit'] = $val2->size_edit;
                $posd['deleted_by'] = Auth::user()->id;
                $posd['move_date'] = Carbon::now();
                $posd['created_at'] = $val2->created_at;
                $posd['updated_at'] = $val2->updated_at;
                $data_history_po_summary_detail[] = $posd;
        }

        $data_cutting_detail = DB::table('cutting_detail')
                                    ->where('po_summary_detail_id', $po_summary_detail_id)
                                    ->whereNull('deleted_at')
                                    ->get();

        $list_barcode = array();

        foreach ($data_cutting_detail as $key3 => $val3) {
                $cutd['barcode_id'] = $val3->barcode_id;
                $cutd['po_summary_detail_id'] = $val3->po_summary_detail_id;
                $cutd['cut_number'] = $val3->cut_number;
                $cutd['cut_info'] = $val3->cut_info;
                $cutd['lot'] = $val3->lot;
                $cutd['komponen'] = $val3->komponen;
                $cutd['part'] = $val3->part;
                $cutd['qty'] = $val3->qty;
                $cutd['sticker_from'] = $val3->sticker_from;
                $cutd['sticker_to'] = $val3->sticker_to;
                $cutd['sticker_no'] = $val3->sticker_no;
                $cutd['cutting_date'] = $val3->cutting_date;
                $cutd['bundle_id'] = $val3->bundle_id;
                $cutd['bundle'] = $val3->bundle;
                $cutd['qc'] = $val3->qc;
                $cutd['cutter'] = $val3->cutter;
                $cutd['current_process'] = $val3->current_process;
                $cutd['current_status'] = $val3->current_status;
                $cutd['process'] = $val3->process;
                $cutd['already_process'] = $val3->already_process;
                $cutd['deleted_at'] = $val3->deleted_at;
                $cutd['is_printed'] = $val3->is_printed;
                $cutd['is_printed_formulir'] = $val3->is_printed_formulir;
                $cutd['is_printed_sj'] = $val3->is_printed_sj;
                $cutd['description'] = $val3->description;
                $cutd['uoms'] = $val3->uoms;
                $cutd['qty_used'] = $val3->qty_used;
                $cutd['qty_used_draft'] = $val3->qty_used_draft;
                $cutd['type_pl_draft'] = $val3->type_pl_draft;
                $cutd['created_at'] = $val3->created_at;
                $cutd['updated_at'] = $val3->updated_at;
                $data_history_cutting_detail[] = $cutd;

                $list_barcode[] = $val3->barcode_id;
        }

        $data_cutting_movement = DB::table('cutting_movements')
                                    ->whereIn('barcode_id', $list_barcode)
                                    ->get();

        foreach ($data_cutting_movement as $key4 => $val4) {
                $cutm['id'] = $val4->id;
                $cutm['barcode_id'] = $val4->barcode_id;
                $cutm['process_from'] = $val4->process_from;
                $cutm['status_from'] = $val4->status_from;
                $cutm['process_to'] = $val4->process_to;
                $cutm['status_to'] = $val4->status_to;
                $cutm['deleted_at'] = $val4->deleted_at;
                $cutm['is_canceled'] = $val4->is_canceled;
                $cutm['user_id'] = $val4->user_id;
                $cutm['description'] = $val4->description;
                $cutm['ip_address'] = $val4->ip_address;
                $cutm['created_at'] = $val4->created_at;
                $cutm['updated_at'] = $val4->updated_at;

                $data_history_cutting_movements[] = $cutm;
        }

        try {
            DB::beginTransaction();

            // insert history
            DB::table('history_po_summary')->insert($data_history_po_summary);

            DB::table('history_po_summary_detail')->insert($data_history_po_summary_detail);

            DB::table('history_cutting_detail')->insert($data_history_cutting_detail);

            DB::table('history_cutting_movements')->insert($data_history_cutting_movements);

            // delete table
            DB::table('cutting_movements')
                ->whereIn('barcode_id', $list_barcode)
                ->delete();

            DB::table('cutting_detail')
                ->whereIn('barcode_id', $list_barcode)
                ->delete();

            DB::table('po_summary_detail')
                ->where('id', $po_summary_detail_id)
                ->whereNull('deleted_at')
                ->delete();

            DB::table('po_summary')
                ->where('id', $po_summary_id)
                ->whereNull('deleted_at')
                ->delete();



            $cekifused2 = DB::table('po_summary')
                    ->where('style', $style)
                    ->whereNull('deleted_at')
                    ->exists();

            if (!$cekifused2) {
                // update master style
                DB::table('master_style')
                    ->whereNull('deleted_at')
                    ->update([
                        'is_used' => false,
                        'updated_at' => Carbon::now()
                    ]);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

    }

    public function updatePoNumberEdit(Request $request)
    {
        $id = $request->id;
        $po_number_edit = $request->po_number_edit;

        $request->validate([
            'id' => 'required',
        ]);

        try {
            DB::beginTransaction();
                // update po_summary_detail
                DB::table('po_summary_detail')
                        ->where('id', $id)
                        ->update([
                            'po_number_edit' => $po_number_edit,
                            'updated_at' => Carbon::now()
                        ]);

            DB::commit();

        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        $html = '<input type="text"
                        data-id="'.$id.'"
                        class="form-control po_filled"
                        value="'.$po_number_edit.'">';

        return response()->json($html, 200);
    }

   private function generateBarcodeNumber($string) {
        $string = str_pad($string, 4, "0", STR_PAD_LEFT);
        // $number = Carbon::now()->format('ym').mt_rand(10000000, 99999999).$string; // better than rand()
        $number = Carbon::now()->format('ymdhis').$string; // better than rand()

        // call the same function if the barcode exists already
        if ($this->barcodeNumberExists($number)) {
            return generateBarcodeNumber((int)$string);
        }

        // otherwise, it's valid and can be used
        return $number;
    }

    static function barcodeNumberExists($number) {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return DB::table('cutting_detail')
                    ->where('barcode_id', $number)
                    ->exists();
    }

}
