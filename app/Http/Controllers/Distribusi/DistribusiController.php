<?php

namespace App\Http\Controllers\Distribusi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use DB;
use Carbon\Carbon;

class DistribusiController extends Controller
{
    //checkout
    public function checkout()
    {
        return view('distribusi/checkout/index');
    }

    //checkin
    public function checkin()
    {
        $delivery_in = DB::table('delivery_in')
                        ->select('id', 'no_suratjalan', 'bc_no', 'qty')
                        ->whereNull('deleted_at')
                        ->get();

        return view('distribusi/checkin/index', compact('delivery_in'));
    }

    /*
    Constraint :
        yg boleh di scan completed
        a. process :barcoding, status :completed
        b. process :distribusi, status :completed

        yg boleh di cancel
        a. process :distribusi, status :completed
    */

    public function postCheckout(Request $request)
    {
        $type = 'checkout';
        $barcodeid = trim($request->barcodeid);
        $status = strtolower(trim($request->status));

        //allowed status
        if($status !== 'completed' && $status !== 'cancel') {
            return response()->json('Status unidentified', 422);
        }

        //check if package can be scanned or not
        $check = $this->_check_scan($barcodeid, $status, $type);
        if(!$check) {
            $error = $this->_show_error($barcodeid);
            return response()->json($error,422);
        }

        // prepare table for table cutting detail and cutting_movements
        if ($status == 'completed') {
           $process_from = $check->current_process;
           $status_from = $check->current_status;
           $status_to = $status;
           $process_to = 'distribusi';
           $description = 'set as completed on distribusi';
           $data = array(
                            'barcode_id' => $barcodeid,
                            'process_from' => $process_from,
                            'status_from' => $status_from,
                            'process_to' => $process_to,
                            'status_to' => $status_to,
                            'user_id' => Auth::user()->id,
                            'description' => $description,
                            'ip_address' => \Request::ip(),
                            'created_at' => Carbon::now()
                        );
        }elseif ($status == 'cancel') {
            // cek apakah barcode sudah di buat surat jalan ?
            $cekexist = DB::table('cutting_detail')
                            ->where('barcode_id', $barcodeid)
                            ->where('is_printed_sj', true)
                            ->exists();
            if ($cekexist) {
                return response()->json('delivery orders already created, please cancel before!',422);
            }
            $process_to = 'barcoding';
            $status_to = 'completed';
            $data = array(
                            'barcode_id' => $barcodeid,
                            'process_to' => null,
                            'status_to' => 'cancel',
                            'created_at' => Carbon::now()
                        );
        }

        // do database action
        try {
            DB::beginTransaction();

                //query for table cutting_movements and cutting_detail
                $query_movement = $this->_query_movement($barcodeid, $status, $data);
                if(!$query_movement) {
                    throw new \Exception('Movement error');
                }
                $query_package = $this->_query_package($barcodeid, $status, $process_to, $status_to);
                if(!$query_package) {
                    throw new \Exception('Package error');
                }

            DB::commit();
        } catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return view('distribusi.ajax_list')
                    ->with('data', $data)
                    ->with('po_number', $check->po_number)
                    ->with('komponen', $check->komponen)
                    ->with('cut_number', $check->cut_number)
                    ->with('sticker_no', $check->sticker_no)
                    ->with('komponen_name', $check->komponen_name)
                    ->with('po_number_edit', $check->po_number_edit)
                    ->with('color_edit', $check->color_edit)
                    ->with('article_edit', $check->article_edit)
                    ->with('size_edit', $check->size_edit)
                    ->with('style_edit', $check->style_edit)
                    ->with('qty', $check->qty);
    }

    /*
    Constraint :
        yg boleh di scan onprogress
        a. process :distribusi, status :completed
    */

    public function postCheckin(Request $request)
    {
        $type = 'checkin';
        $barcodeid = trim($request->barcodeid);
        $status = strtolower(trim($request->status));

        //allowed status
        if($status !== 'onprogress') {
            return response()->json('Status unidentified', 422);
        }

        //check if package can be scanned or not
        $check = $this->_check_scan($barcodeid, $status, $type);
        if(!$check) {
            $error = $this->_show_error($barcodeid);
            return response()->json($error,422);
        }

        // prepare table for table cutting detail and cutting_movements
        $process_from = $check->current_process;
        $status_from = $check->current_status;
        $status_to = $status;
        $process_to = 'distribusi';
        $description = 'accepted on checkin distribusi';
        $data = array(
                        'barcode_id' => $barcodeid,
                        'process_from' => $process_from,
                        'status_from' => $status_from,
                        'process_to' => $process_to,
                        'status_to' => $status_to,
                        'user_id' => Auth::user()->id,
                        'description' => $description,
                        'ip_address' => \Request::ip(),
                        'created_at' => Carbon::now()
                    );

        $data_formulir = DB::table('formulir_artwork')
                            ->where('barcode_id', $barcodeid)
                            ->whereNull('deleted_at')
                            ->orderBy('created_at', 'desc')
                            ->first();
        if (!$data_formulir) {
            return response()->json('formulir/polybag not found..!', 422);
        }

        $data_delivery_detail = DB::table('delivery_detail')
                            ->where('formulir_id', $data_formulir->formulir_artwork_header_id)
                            ->first();
        if (!$data_delivery_detail) {
            return response()->json('SJ Out Cutting not found..!', 422);
        }
        // delivery in
        $data_detail = array([
                    'no_suratjalan' => $data_delivery_detail->no_suratjalan,
                    'no_suratjalan_info' => $data_delivery_detail->no_suratjalan_info,
                    'delivery_in_id' => $request->delivery_in_id,
                    'formulir_artwork_id' => $data_formulir->id,
                    'barcode_id' => $barcodeid,
                    'qty' => $data_formulir->qty,
                    'created_at' => Carbon::now(),
                    'user_id' => Auth::user()->id
        ]);

        // // get qty total
        $qty_total = DB::table('delivery_in')
                        ->where('id', $request->delivery_in_id)
                        ->first()->qty;

        // do database action
        try {
            DB::beginTransaction();

                //query for table cutting_movements and cutting_detail
                $query_movement = $this->_query_movement($barcodeid, $status, $data);
                if(!$query_movement) {
                    throw new \Exception('Movement error');
                }
                $query_package = $this->_query_package($barcodeid, $status, $process_to, $status_to);
                if(!$query_package) {
                    throw new \Exception('Package error');
                }

                // insert delivery in detail
                $insert = DB::table('delivery_in_detail')->insert($data_detail);

                if ($insert) {

                    // cek apakah over qty ?
                    $qtyin = DB::table('delivery_in_detail')
                                        ->where('delivery_in_id', $request->delivery_in_id)
                                        ->whereNull('deleted_at')
                                        ->sum('qty');

                    if ($qtyin > $qty_total) {
                        return response()->json('No. SJ over QTY..!', 422);
                    }
                    // cek double
                    $cekifexist = DB::table('delivery_in_detail')
                                    ->where('formulir_artwork_id',$data_formulir->id)
                                    ->whereNull('deleted_at')
                                    ->count();

                    if ($cekifexist>1) {
                        throw new \Exception('Double input');
                    }
                }

                // update formulir artwork
                DB::table('formulir_artwork')
                        ->where('id', $data_formulir->id)
                        ->update([
                            'checkin' => Carbon::now(),
                            'updated_at' => Carbon::now()
                        ]);

            DB::commit();
        } catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return view('distribusi.ajax_list_checkin')
                    ->with('data', $data)
                    ->with('po_number', $check->po_number)
                    ->with('komponen', $check->komponen)
                    ->with('cut_number', $check->cut_number)
                    ->with('sticker_no', $check->sticker_no)
                    ->with('komponen_name', $check->komponen_name)
                    ->with('po_number_edit', $check->po_number_edit)
                    ->with('color_edit', $check->color_edit)
                    ->with('article_edit', $check->article_edit)
                    ->with('size_edit', $check->size_edit)
                    ->with('style_edit', $check->style_edit)
                    ->with('qty', $check->qty);
    }

    //check if package can be scanned or not
    private function _check_scan($barcodeid, $check_status, $type) {

        if($type == 'checkin') {
            if($check_status !== 'onprogress') {
                return false;
            }
        }
        elseif($type == 'checkout') {
            if($check_status !== 'completed' && $check_status !== 'cancel') {
                return false;
            }
        }

        //proses check
        if($check_status == 'completed') {
            $check = DB::table('cutting_detail')
                        ->select('cutting_detail.current_status', 'cutting_detail.current_process', 'po_summary.po_number', 'cutting_detail.komponen', 'cutting_detail.cut_number', 'cutting_detail.sticker_no', 'po_summary_detail.po_number_edit', 'po_summary_detail.style_edit', 'po_summary_detail.color_edit', 'po_summary_detail.article_edit', 'po_summary_detail.size_edit', 'cutting_detail.qty', DB::raw('master_komponen.name AS komponen_name'))
                        ->join('master_komponen', 'master_komponen.id', '=', 'cutting_detail.komponen')
                        ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                        ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                        ->where('cutting_detail.barcode_id', $barcodeid)
                        ->where('po_summary.factory_id', Auth::user()->factory_id)
                        ->where(function($query)
                        {
                            $query->where(function ($query)
                            {
                                $query->where('cutting_detail.current_process', 'barcoding')
                                        ->where('cutting_detail.current_status', 'completed');
                            })
                            ->orWhere(function($query){
                                $query->where('cutting_detail.current_process', 'distribusi')
                                        ->where('cutting_detail.current_status', 'onprogress');

                            });
                        })
                        //->where('cutting_detail.current_status', 'completed')
                        ->whereNull('po_summary_detail.deleted_at')
                        ->whereNull('cutting_detail.deleted_at')
                        ->whereNull('po_summary.deleted_at')
                        ->first();

            if(!$check) {
                return false;
            }
            else {
                return $check;
            }
        }
        elseif($check_status == 'onprogress') {
            $check = DB::table('cutting_detail')
                        ->select('cutting_detail.current_status', 'cutting_detail.current_process', 'po_summary.po_number', 'cutting_detail.komponen', 'cutting_detail.cut_number', 'cutting_detail.sticker_no', 'po_summary_detail.po_number_edit', 'po_summary_detail.style_edit', 'po_summary_detail.color_edit', 'po_summary_detail.article_edit', 'po_summary_detail.size_edit', 'cutting_detail.qty', DB::raw('master_komponen.name AS komponen_name'))
                        ->join('master_komponen', 'master_komponen.id', '=', 'cutting_detail.komponen')
                        ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                        ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                        ->where('cutting_detail.barcode_id', $barcodeid)
                        ->where('po_summary.factory_id', Auth::user()->factory_id)
                        ->where(function($query)
                        {
                            $query->where(function ($query)
                            {
                                $query->where('cutting_detail.current_process', 'reject')
                                        ->where('cutting_detail.current_status', 'completed');
                            })
                            ->orWhere(function($query){
                                $query->where('cutting_detail.current_process', 'distribusi')
                                        ->where('cutting_detail.current_status', 'completed');

                            });
                        })
                        // ->where('cutting_detail.current_process', 'distribusi')
                        // ->where('cutting_detail.current_status', 'completed')
                        ->whereNull('po_summary_detail.deleted_at')
                        ->whereNull('cutting_detail.deleted_at')
                        ->whereNull('po_summary.deleted_at')
                        ->first();

            if(!$check) {
                return false;
            }
            else {
                return $check;
            }
        }
        elseif($check_status == 'cancel') {
            $check = DB::table('cutting_detail')
                        ->select('cutting_detail.current_status', 'cutting_detail.current_process', 'po_summary.po_number', 'cutting_detail.komponen', 'cutting_detail.cut_number', 'cutting_detail.sticker_no', 'po_summary_detail.po_number_edit', 'po_summary_detail.style_edit', 'po_summary_detail.color_edit', 'po_summary_detail.article_edit', 'po_summary_detail.size_edit', 'cutting_detail.qty', DB::raw('master_komponen.name AS komponen_name'))
                        ->join('master_komponen', 'master_komponen.id', '=', 'cutting_detail.komponen')
                        ->join('po_summary_detail', 'cutting_detail.po_summary_detail_id', '=', 'po_summary_detail.id')
                        ->join('po_summary', 'po_summary.id', '=', 'po_summary_detail.po_summary_id')
                        ->where('cutting_detail.barcode_id', $barcodeid)
                        ->where('po_summary.factory_id', Auth::user()->factory_id)
                        ->where('cutting_detail.current_process', 'distribusi')
                        ->where('cutting_detail.current_status', 'completed')
                        ->whereNull('po_summary_detail.deleted_at')
                        ->whereNull('cutting_detail.deleted_at')
                        ->whereNull('po_summary.deleted_at')
                        ->first();

            if(!$check) {
                return false;
            }
            else {
                return $check;
            }
        }
        else {
            return false;
        }
    }

    //show error
    private function _show_error($barcodeid) {
        $error = '';
        $check_current_status = $this->_get_current_status($barcodeid);
        if($check_current_status) {
            if($check_current_status->current_status == 'onprogress') {
                $check_current_status->current_status = 'in';
            }
            elseif($check_current_status->current_status == 'completed') {
                $check_current_status->current_status = 'out';
            }
            $error .= 'Last Scanned (' . $check_current_status->barcode_id . ') : '
                        . $check_current_status->current_process . ' - '
                        . $check_current_status->current_status;
        }
        else {
            $error = 'Barcode not found on system';
        }

        return $error;
    }

    // get current status
    private function _get_current_status($barcodeid)
    {
        $data = DB::table('cutting_detail')
                    ->select('barcode_id', 'current_status', 'current_process')
                    ->where('barcode_id', $barcodeid)
                    ->whereNull('deleted_at')
                    ->first();
        return $data;
    }

    // query movement
    private function _query_movement($barcodeid, $check_status, $data_movement=null)
    {
        if ($check_status == 'completed' || $check_status == 'onprogress') {
            try {
                DB::beginTransaction();
                    // update deleted_at on last cutting movement
                    $last_data = DB::table('cutting_movements')
                                    ->where('barcode_id', $barcodeid)
                                    ->where('is_canceled', false)
                                    ->whereNull('deleted_at')
                                    ->get()
                                    ->toArray();
                    foreach ($last_data as $key => $value) {
                        if (!$value) {
                            throw new Exception('data not found');
                        }else {
                            $update_m = DB::table('cutting_movements')
                                            ->where('id', $value->id)
                                            ->update([
                                                'deleted_at' => Carbon::now()
                                            ]);
                            if (!$update_m) {
                                throw new Exception('the last data nothing updated');
                            }
                        }
                    }
                    // insert data movement
                    $insert = DB::table('cutting_movements')->insert($data_movement);

                    if ($insert) {
                        // cek apakah insert double ?
                        $cek_insert = DB::table('cutting_movements')
                                        ->where('barcode_id', $barcodeid)
                                        ->where('is_canceled', false)
                                        ->whereNull('deleted_at')
                                        ->count();

                        if ($cek_insert > 1) {
                            throw new \Exception('data double inserted');
                        }
                    }
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return true;

        }elseif ($check_status == 'cancel') {
            try {
                DB::beginTransaction();

                /*
                    - get second last data from table cutting_movements
                    - get last data from table cutting_movements
                    - update second last data, deleted_at = null
                    - update last data, deleted_at = now() and is_canceled = true
                */
                $second_last_data = DB::table('cutting_movements')
                                        ->where('barcode_id', $barcodeid)
                                        ->where('is_canceled', false)
                                        ->whereNotNull('deleted_at')
                                        ->orderBy('created_at', 'desc')
                                        ->first();

                $last_data = DB::table('cutting_movements')
                                 ->where('barcode_id', $barcodeid)
                                 ->where('is_canceled', false)
                                 ->whereNull('deleted_at')
                                 ->first();

                DB::table('cutting_movements')
                    ->where('id', $second_last_data->id)
                    ->update([
                        'deleted_at' => null
                    ]);

                DB::table('cutting_movements')
                    ->where('id', $last_data->id)
                    ->update([
                        'is_canceled' => true,
                        'deleted_at' => Carbon::now()
                    ]);

                DB::commit();
            }
            catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return true;

        }else{
            return false;
        }
    }

    //query package
    private function _query_package($barcodeid, $check_status, $process, $status) {

            if($check_status == 'completed' || $check_status == 'onprogress' || $check_status == 'cancel') {
                // get qty
                $data_barcode = DB::table('cutting_detail')
                                    ->where('barcode_id', $barcodeid)
                                    ->first();

                $update_clause = array(
                    'current_process' => $process,
                    'current_status' => $status,
                    'updated_at' => Carbon::now(),
                    'qty_used_draft' => $data_barcode->qty,
                    'qty_used' => 0,
                    'type_pl_draft' => 0,
                    'barcode_reject' => null
                );
            }
            else {
                return false;
            }

        try {
            DB::beginTransaction();

            DB::table('cutting_detail')
                ->where('barcode_id', $barcodeid)
                ->whereNull('deleted_at')
                ->update($update_clause);

            DB::commit();
        }
        catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return true;
    }

    // get data delivery reject
    public function ajaxGetDeliveryReject(Request $request)
    {
        // $data = DB::table('delivery_reject')
        //             ->select(
        //                 'delivery_reject.id',
        //                 'delivery_reject.style',
        //                 'delivery_reject.qty',
        //                 'delivery_reject.created_at',
        //                 'delivery_reject.updated_at',
        //                 'delivery_reject.no_suratjalan',
        //                 'delivery_reject.no_suratjalan_info',
        //                 'delivery_reject.deleted_at',
        //                 'delivery_reject.subcont_id',
        //                 'delivery_reject.checkin',
        //                 DB::raw('subcont.name as subcont_name')
        //             )
        //             ->join('subcont', 'subcont.id', '=', 'delivery_reject.subcont_id')
        //             ->whereNull('delivery_reject.checkin')
        //             ->whereNull('delivery_reject.deleted_at')
        //             ->whereNull('subcont.deleted_at')
        //             ->get();

        $data = DB::table('v_list_delivery_reject')
                        ->get();

        return view('distribusi/checkin/list_style')->with('datax', $data);
        // return response()->json($data, 200);
    }

    // get data delivery in move
    public function ajaxGetDeliveryInMove(Request $request)
    {
        $delivery_in_id = $request->delivery_in_id;

        $data = DB::table('delivery_in')
                        ->where('id', '!=', $delivery_in_id)
                        ->whereNull('deleted_at')
                        ->get();

        return view('distribusi/checkin/list_delivery_in')->with('dataxx', $data);
        // return response()->json($data, 200);
    }

    // checkin delivery reject
    public function checkinDeliveryReject(Request $request)
    {
       $request->validate([
            'delivery_in_id' => 'required',
            'delivery_reject_id' => 'required'
       ]);

       $delivery_reject_id = $request->delivery_reject_id;
       $data_reject = DB::table('delivery_reject')
                            ->where('id', $delivery_reject_id)
                            ->whereNull('deleted_at')
                            ->first();

       $data_detail = array([
                'no_suratjalan' => $data_reject->no_suratjalan,
                'no_suratjalan_info' => $data_reject->no_suratjalan_info,
                'delivery_in_id' => $request->delivery_in_id,
                'delivery_reject_id' => $delivery_reject_id,
                'qty' => $request->qty,
                'created_at' => Carbon::now(),
                'user_id' => Auth::user()->id
       ]);

       // get qty total
       $qty_total = DB::table('delivery_in')
            ->where('id', $request->delivery_in_id)
            ->first()->qty;

        // get qty yg diedit apakah melebihi qty seharusnya tau tidak
        $data_qty_now =  DB::table('v_list_delivery_reject')
                            ->where('id', $delivery_reject_id)
                            ->first()->qty;

        if ($request->qty > $data_qty_now) {
            return response()->json('Qty not accepted for SJ..!', 422);
        }

       try {
           DB::beginTransaction();
                // insert
                $insert = DB::table('delivery_in_detail')->insert($data_detail);

                if ($insert) {
                    // cek apakah over qty ?
                    $qtyin = DB::table('delivery_in_detail')
                                        ->where('delivery_in_id', $request->delivery_in_id)
                                        ->whereNull('deleted_at')
                                        ->sum('qty');

                    if ($qtyin > $qty_total) {
                        return response()->json('No. SJ over QTY..!', 422);
                    }

                    // cek double
                    $data_rejectx = DB::table('delivery_reject')
                                    ->select('qty')
                                    ->where('id', $delivery_reject_id)
                                    ->first();

                    $total_qty = $data_rejectx->qty;

                    $total_qty_in = DB::table('delivery_in_detail')
                                        ->where('delivery_reject_id', $delivery_reject_id)
                                        ->whereNull('deleted_at')
                                        ->sum('qty');

                    // cek total qty dengan total qty in
                    if ($total_qty_in > $total_qty) {
                        // return response()->json('invalid update qty (qty in > total qty)', 422);
                        throw new \Exception('invalid update qty (qty in > total qty)');
                    }

                    // $cekifexist = DB::table('delivery_in_detail')
                    //                 ->where('delivery_reject_id',$delivery_reject_id)
                    //                 ->whereNull('deleted_at')
                    //                 ->count();

                    // if ($cekifexist>1) {
                    //     throw new \Exception('Double input');
                    // }
                }

                // update delivery reject
                DB::table('delivery_reject')
                        ->where('id', $delivery_reject_id)
                        ->update([
                            'checkin' => Carbon::now(),
                            'updated_at' => Carbon::now()
                        ]);

           DB::commit();
       } catch (Exception $ex) {
           DB::rollback();

           $message = $ex->getMessage();
           ErrorHandler::db($message);
       }

    }

    // checkin delivery move
    public function checkinDeliveryMove(Request $request)
    {
       $request->validate([
            'delivery_in_id_old' => 'required',
            'delivery_in_id_new' => 'required',
            'data' => 'required'
       ]);

       $data_req = json_decode(stripslashes($request->data));

        if (count($data_req)<=0) {
            return response()->json('barcode/style not found..!', 422);
        }

        $qty_total = DB::table('delivery_in')
                        ->where('id', $request->delivery_in_id_new)
                        ->first()->qty;

       try {
           DB::beginTransaction();

           foreach ($data_req as $key => $val) {

                $data_movement = array(
                                        'delivery_in_detail_id' => $val->id,
                                        'delivery_in_id_from' => $request->delivery_in_id_old,
                                        'delivery_in_id_to' => $request->delivery_in_id_new,
                                        'user_id' => Auth::user()->id,
                                        'description' => 'moving to another sj',
                                        'ip_address' => \Request::ip(),
                                        'created_at' => Carbon::now(),
                                        'updated_at' => Carbon::now()
                                    );

                // get last data
                $data_last = DB::table('delivery_in_movements')
                                ->whereNull('deleted_at')
                                ->first();

                if ($data_last) {
                    DB::table('delivery_in_movements')
                        ->where('id', $data_last->id)
                        ->update([
                            'deleted_at' => Carbon::now()
                        ]);
                }

                // insert data movement
                DB::table('delivery_in_movements')->insert($data_movement);

                // update delivery in detail
                $update = DB::table('delivery_in_detail')
                            ->where('id', $val->id)
                            ->update([
                                'delivery_in_id' => $request->delivery_in_id_new,
                                'updated_at' => Carbon::now()
                            ]);

                if ($update) {
                    // cek apakah over qty ?
                    $qtyin = DB::table('delivery_in_detail')
                                        ->where('delivery_in_id', $request->delivery_in_id_new)
                                        ->whereNull('deleted_at')
                                        ->sum('qty');

                    if ($qtyin > $qty_total) {
                        return response()->json('No. SJ over QTY..!', 422);
                    }
                }
            }

           DB::commit();
       } catch (Exception $ex) {
           DB::rollback();

           $message = $ex->getMessage();
           ErrorHandler::db($message);
       }

    }

}
