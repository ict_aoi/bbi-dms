<?php namespace App\Http\Controllers;

use Hash;
use File;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\User;
use App\Models\Factories;
use Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home');
    }

    public function accountSetting()
    {
        return view('account_setting');
    }

    public function showAvatar($filename)
    {
        $path = Config::get('storage.avatar');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function updateAccount(request $request,$id)
    {
        $avatar = Config::get('storage.avatar');
        if (!File::exists($avatar)) File::makeDirectory($avatar, 0777, true);

        $old_password = $request->old_password;
        $password = $request->new_password;
        $retype_password = $request->retype_password;
        $user = User::find($id);
        $image = null;
        if($password)
        {
            if (!Hash::check($old_password, $user->password)) return redirect()->back()
            ->withErrors([
                'old_password' => 'Password tidak sama dengan yang saat ini',
            ]);

            if ($password != $retype_password) return redirect()->back()
            ->withErrors([
                'retype_password' => 'Password yang anda masukan tidak sama dengan password baru',
            ]);
        }

       
        if ($request->hasFile('photo')) 
        {
            if ($request->file('photo')->isValid()) 
            {
                $old_file = $avatar . '/' . $user->photo;
                
                if(File::exists($avatar)) File::delete($old_file);
                
                $file = $request->file('photo');
                $now = Carbon::now()->format('u');
                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize = Image::make($file->getRealPath());
                $height = $image_resize->height();
                $width = $image_resize->width();

                $newWidth = 130;
                $newHeight = 130;
                $image_resize->resize($newWidth, $newHeight);
                $image_resize->save($avatar.'/'.$image);
            }
        }

        if ($password) $user->password = bcrypt($password);
        if ($image) $user->photo = $image;
        $user->save();

        return redirect()->route('accountSetting'); //->withSuccess();
        
        
    }

    public function factorySetting()
    {
        $factories = Factories::where('id', Auth::user()->factory_id)
                                ->whereNull('deleted_at')
                                ->first();

        return view('factory_setting')->with('factories', $factories);
    }

    public function updateFactory(request $request)
    {
        $factories = Factories::where('id', Auth::user()->factory_id)
                                ->whereNull('deleted_at')
                                ->first();

        $factory = Factories::find(Auth::user()->factory_id);

        $public_images = public_path().'/images';
        if (!File::exists($public_images)) File::makeDirectory($public_images, 0777, true);

        $factory->no_suratjalan = $request->no_suratjalan;
        $factory->no_polybag = $request->no_polybag;
        $factory->list_komponen_break = $request->list_komponen_break;
        // $image = null;
        $image = $factories->logo_factory;

        if ($request->hasFile('logo_factory')) 
        {
            if ($request->file('logo_factory')->isValid()) 
            {
                $old_file = $public_images . '/' . $factories->logo_factory;
                
                if(File::exists($public_images)) File::delete($old_file);
                
                $file = $request->file('logo_factory');
                $now = Carbon::now()->format('u');
                // $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $filename = 'logo-bbi';
                // $image = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();
                $image = $filename. '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize = Image::make($file->getRealPath());
                $height = $image_resize->height();
                $width = $image_resize->width();

                $newWidth = 800;
                $newHeight = 303;
                $image_resize->resize($newWidth, $newHeight);
                $image_resize->save($public_images.'/'.$image);
            }
        }

        $factory->logo_factory = $image;
        $factory->save();

        return redirect()->route('factorySetting'); //->withSuccess();
           
    }

    public function showDoc()
    {
        $file_path = public_path().'/file/Beginning_Android_Programming_with_Android_Studio_4th_Edition.pdf';

        return response()->file($file_path);
    }
}
