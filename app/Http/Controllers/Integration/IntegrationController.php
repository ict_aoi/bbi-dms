<?php

namespace App\Http\Controllers\Integration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Carbon\Carbon;
use Symfony\Component\Debug\ErrorHandler;

class IntegrationController extends Controller
{
    //
    static function BreakdownSizeUpdate()
    {
        $data_erp = DB::connection('erp_bbi_live')
                        ->table('jz_breakdown_size')
                        ->where(DB::raw('extract(year FROM datepromise)'), '>=', '2019')
                        ->get();
                        
        $data_dms = DB::table('breakdown_size')
                        ->where(DB::raw('extract(year FROM datepromise)'), '>=', '2019')
                        ->get();

        foreach ($data_erp as $key => $val) {
            $c_orderline_id = $val->c_orderline_id;

            // check data dms
            $checkifexist = DB::table('breakdown_size')
                                ->where('c_orderline_id', $c_orderline_id)
                                ->exists();

            if ($checkifexist) {
                DB::table('breakdown_size')
                    ->where('c_orderline_id', $c_orderline_id)
                    ->update([
                            'documentno' => $val->documentno,
                            'poreference' => $val->poreference,
                            'm_product_id' => $val->m_product_id,
                            'kode_product' => $val->kode_product,
                            'nama_product' => $val->nama_product,
                            'description' => $val->description,
                            'style' => $val->style,
                            'size' => $val->size,
                            'article' => $val->article,
                            'color' => $val->color,
                            'kst_season' => $val->kst_season,
                            'category' => $val->category,
                            'dateorder' => $val->dateorder,
                            'datepromise' => $val->datepromise,
                            'buyer' => $val->buyer,
                            'qtyordered' => (int)$val->qtyordered,
                            'updated_at' => Carbon::now()
                    ]);
            }
        }

    }
}
