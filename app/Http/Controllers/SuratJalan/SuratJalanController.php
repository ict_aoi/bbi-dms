<?php

namespace App\Http\Controllers\SuratJalan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use DataTable;
use Carbon\Carbon;
use Auth;
use DataTables;
use App\Models\Factories;

use Rap2hpoutre\FastExcel\FastExcel;

class SuratJalanController extends Controller
{
    //
    public function list()
    {
        $factory = Factories::whereNull('deleted_at')->get();

        $list_sj = DB::table('delivery_header')
                        ->select(DB::raw("no_suratjalan, no_suratjalan_info, concat(no_suratjalan, ' ', no_suratjalan_info) as no_suratjalan_concat"))
                        ->whereNull('deleted_at')
                        ->get();

        $type_bc = \Config::get('constants.TYPE_BC');

        return view('bea_cukai.index')->with([
                                                'list_sj'=> $list_sj,
                                                'factory'=> $factory,
                                                'type_bc'=> $type_bc
                                            ]);
    }

    public function getDataSuratJalan(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;

        $data = DB::table('v_surat_jalan_bc')
                ->where('factory_id', $factory_id)
                ->whereNull('deleted_at');
        
        if ($request->radio_status == 'date') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where(function($query) use ($range) {
                                $query->whereBetween('created_at', [$range['from'], $range['to']]);
                                        // ->orWhereBetween('print_date', [$range['from'], $range['to']])
                                        // ->orWhereBetween('out_date', [$range['from'], $range['to']]);
                            });
        }elseif ($request->radio_status == 'sj') {
            $no_suratjalan = $request->no_suratjalan == null ? ' ' : $request->no_suratjalan;
            $data = $data->where('no_suratjalan', $no_suratjalan);
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('no_suratjalan_concat', 'like', '%'.$filterby.'%')
                                ->orWhere('bc_no', 'like', '%'.$filterby.'%')
                                ->orWhere('subcont_name', 'like', '%'.$filterby.'%')
                                ->orWhere('no_daftar_bc', 'like', '%'.$filterby.'%');
                    });
        }

        return Datatables::of($data)
               ->editColumn('is_integrated', function($data){

                    if ($data->is_integrated) {
                        $str = '<span class="label label-success label-rounded">
                                    <i class="icon-checkmark2"></i>
                                </span>';
                    }else {
                        $str = '<span class="label label-default label-rounded">
                                    <i class="icon-cross3"></i>
                                </span>';
                    }

                    return $str;
               })
               ->editColumn('no_suratjalan_concat', function($data){
                   return '<a onclick="detailSuratJalan(this)" data-suratjalan="'.$data->no_suratjalan.'" data-suratjalanconcat="'.$data->no_suratjalan_concat.'">'.$data->no_suratjalan_concat.'</a>';
               })
               ->editColumn('action', function($data){
                   return '<a class="btn btn-danger" onclick="editSuratJalan(this)" data-suratjalan="'.$data->no_suratjalan.'" data-suratjalanconcat="'.$data->no_suratjalan_concat.'">Update</a>';
               })
               ->setRowAttr([
                    'style' => function($data)
                    {
                        if($data->is_washing) {
                            return  'background-color: #eaff74;';
                        }
                    },
                ])
            //    ->editColumn('bc_no', function($data){
                   
            //         if ($data->is_integrated) {
            //            $readonly = " readonly";
            //         }else {
            //            $readonly = " ";
            //         }


            //         return '<div id="bc_'.$data->no_suratjalan.'">
            //                     <input type="text"
            //                             data-id="'.$data->no_suratjalan.'"
            //                             class="form-control bc_filled"
            //                             value="'.$data->bc_no.'" '.$readonly.'>
            //                 </div>';
            //     })
               ->rawColumns(['is_integrated', 'bc_no', 'no_suratjalan_concat', 'action'])
               ->make(true);
    }

    public function exportSuratJalan(Request $request)
    {
        $filterby = $request->filterby;
        $factory_id = $request->factory_id;
        $orderby = $request->orderby;
        $direction = $request->direction;

        $data = DB::table('v_surat_jalan_bc')
                ->where('factory_id', $factory_id)
                ->whereNull('deleted_at');
        
        if ($request->radio_status == 'date') {
            $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
            $range = array(
                'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create($date_range[1]), 'Y-m-d 23:59:59')
            );

            $data = $data->where(function($query) use ($range) {
                                $query->whereBetween('created_at', [$range['from'], $range['to']]);
                            });
            
            $f_name = $range['from'] . '_until_' . $range['to'];
        
        }elseif ($request->radio_status == 'sj') {
            $no_suratjalan = $request->no_suratjalan == null ? ' ' : $request->no_suratjalan;
            $data = $data->where('no_suratjalan', $no_suratjalan);

            $f_name = $no_suratjalan;
        }

        //jika filterby tidak kosong
        if(!empty($filterby)) {
            $data = $data->where(function($query) use ($filterby) {
                        $query->where('no_suratjalan_concat', 'like', '%'.$filterby.'%')
                                ->orWhere('no_daftar_bc', 'like', '%'.$filterby.'%')
                                ->orWhere('subcont_name', 'like', '%'.$filterby.'%');
                    });
        }

        //naming file
        $get_factory = Factories::where('id', $factory_id)
                                    ->wherenull('deleted_at')
                                    ->first();

        //jika orderby tidak undefined
        if($orderby != 'undefined') {
            $data = $data->orderBy($orderby, $direction);

            $filename = $get_factory->factory_name.'_list_surat_jalan_' . $f_name
                    . '_orderby_' . $orderby . '_' . $direction . '_filterby_' . $filterby;
        }
        else {
            $data = $data->orderBy('created_at', 'asc');
            
            $filename = $get_factory->factory_name.'_list_surat_jalan_' . $f_name;
        }

        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($data, $i) {
            $excel->sheet('report', function($sheet) use($data, $i) {
                $sheet->appendRow(array(
                    '#', 'Date', 'No. Surat jalan', 'Subcont', 'Type BC', 'No Aju', 'No Pendaftaran', 'Tgl Dokumen', 'Washing'
                ));
                $data->chunk(100, function($rows) use ($sheet, $i)
                {
                    foreach ($rows as $row)
                    {
                        //
                        $sheet->appendRow(array(
                            $i++, $row->created_at, $row->no_suratjalan_concat, $row->subcont_name, $row->type_bc, $row->no_aju, $row->no_daftar_bc, $row->document_date, $row->is_washing
                        ));
                    }
                });
            });
        })->download('xlsx');

        return response()->json('Success exporting', 200);

    }

    public function editBcNo(Request $request)
    {
        $no_suratjalan = $request->no_suratjalan;

       
        $data = DB::table('delivery_header')
                        ->where('no_suratjalan', $no_suratjalan)
                        ->whereNull('deleted_at')
                        ->first();

        return response()->json($data, 200);
    }
    public function updateBcNo(Request $request)
    {
        $no_suratjalan = $request->no_suratjalan;
        $type_bc = $request->type_bc;
        $no_aju = trim($request->no_aju);
        $no_daftar_bc = trim($request->no_daftar_bc);
        $document_date = $request->document_date;
        $bc_no = $request->bc_no;

        // $request->validate([
        //     'no_suratjalan' => 'required',
        // ]);

        try {
            DB::beginTransaction();
                // update delivery header
                DB::table('delivery_header')
                        ->where('no_suratjalan', $no_suratjalan)
                        ->update([
                            'bc_no' => $bc_no,
                            'type_bc' => $type_bc,
                            'no_aju' => $no_aju,
                            'no_daftar_bc' => $no_daftar_bc,
                            'document_date' => $document_date,
                            'updated_at' => Carbon::now()
                        ]);

                // insert bea cukai docs
                DB::table('bea_cukai_docs')
                        ->insert([
                            'no_suratjalan' => $no_suratjalan,
                            'type_bc' => $type_bc,
                            'no_aju' => $no_aju,
                            'no_daftar_bc' => $no_daftar_bc,
                            'document_date' => $document_date,
                            'user_id' => Auth::user()->id,
                            'ip_address' => \Request::ip(),
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now()
                        ]);
            
            DB::commit();
            
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

    }

    // get detail
    public function getDetailSuratJalan(Request $request)
    {
        $data_formulir = DB::table('delivery_detail')
                            ->where('no_suratjalan', $request->no_suratjalan)
                            ->whereNull('deleted_at')
                            ->get();

        $data = DB::table('v_surat_jalan');

        $arr_headerid = array();
        foreach ($data_formulir as $value) {
            $formulir_artwork_header_id = $value->formulir_id;
            $rheader['formulir_artwork_header_id'] = $formulir_artwork_header_id;
            $arr_headerid[] = $rheader;
        }
        if (count($arr_headerid) > 0) {
            $data = $data->whereIn('formulir_artwork_header_id', $arr_headerid);
        }

        $data = $data->orderBy('no_polybag')
                    ->get();

        $status = 'normal';

        // cek reject
        $cekifreject =  DB::table('delivery_reject')
                            ->where('no_suratjalan', $request->no_suratjalan)
                            ->whereNull('deleted_at')
                            ->exists();

        if ($cekifreject) {
            $status = 'global';
            $data = DB::table('delivery_reject')
                        ->where('no_suratjalan', $request->no_suratjalan)
                        ->whereNull('deleted_at')
                        ->get();
        }

        return response()->json([
            'status' => $status,
            'data' => $data
        ], 200);
    }

    //SURAT JALAN IN
    public function suratJalanIn()
    {
        $type_bc = \Config::get('constants.TYPE_BC');
        $subcont = DB::table('subcont')
                        ->wherenull('deleted_at')
                        ->get();

        return view('bea_cukai.surat_jalan_in.index',compact('type_bc', 'subcont'));
    }

    public function ajaxGetDataSuratJalanIn(Request $request)
    {
        if ($request->ajax()){
            $data = DB::table('delivery_in')
                ->select(
                        'delivery_in.id',
                        'delivery_in.no_suratjalan',
                        'delivery_in.no_suratjalan_info',
                        'delivery_in.subcont_id',
                        'delivery_in.accepted_by',
                        'delivery_in.approved_by',
                        'delivery_in.delivered_by',
                        'delivery_in.deleted_at',
                        'delivery_in.created_by',
                        'delivery_in.deleted_by',
                        'delivery_in.bc_no',
                        'delivery_in.type_bc',
                        'delivery_in.no_aju',
                        'delivery_in.no_daftar_bc',
                        'delivery_in.document_date',
                        'delivery_in.created_at',
                        'delivery_in.updated_at',
                        'delivery_in.qty',
                        DB::raw('subcont.name as subcont_name')
                )
                ->join('subcont', 'subcont.id','=','delivery_in.subcont_id')
                ->wherenull('delivery_in.deleted_at')
                ->wherenull('subcont.deleted_at')
                ->orderBy('delivery_in.created_at', 'desc')
                ->get();

            //datatables

            return DataTables::of($data)
                ->addColumn('action', function($data){ return view('_action', [
                        'edit_modal' => route('suratjalanin.editSuratJalanIn', $data->id),
                        'id' => $data->id,
                        'deletes' => route('suratjalanin.deleteSuratJalanIn', $data->id)
                    ]);
                })
            ->make(true);
        }
    }

    //add new surat jalan in
    public function addSuratJalanIn(Request $request)
    {

        $this->validate($request, [
            'no_suratjalan' => 'required'
        ]);

        // $name = trim(strtoupper($request->name));

        $data = array([
                'no_suratjalan' => $request->no_suratjalan,
                // 'no_suratjalan_info' => $request->,
                'subcont_id' => $request->subcont_id,
                'bc_no' => $request->bc_no,
                'type_bc' => $request->type_bc,
                'no_aju' => $request->no_aju,
                'no_daftar_bc' => $request->no_daftar_bc,
                'document_date' => $request->document_date,
                'qty' => $request->qty,
                'created_at' => Carbon::now()
        ]);

        // cek exists
        $cekifexist = DB::table('delivery_in')
                        ->where('no_suratjalan', $request->no_suratjalan)
                        ->whereNull('deleted_at')
                        ->exists();
        
        if ($cekifexist) {
            return response()->json('surat jalan already exists..!', 422);
        }

        try {
            DB::begintransaction();

            //insert table
            DB::table('delivery_in')->insert($data);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    //edit surat jalan in
    public function editSuratJalanIn($id)
    {
        $data = DB::table('delivery_in')
                        ->where('id', $id)
                        ->first();

        return response()->json($data, 200);
    }

    //update surat jalan in
    public function updateSuratJalanIn(Request $request)
    {
        $data = $request->all();

        $no_suratjalan = $data['no_suratjalan_update'];

        try {
            DB::begintransaction();

            //update table
            $update = DB::table('delivery_in')
                            ->where('id', $data['id_update'])
                            ->update([
                                'no_suratjalan' => $no_suratjalan,
                                'subcont_id' => $data['subcont_id_update'],
                                'bc_no' => $data['bc_no_update'],
                                'type_bc' => $data['type_bc_update'],
                                'no_aju' => $data['no_aju_update'],
                                'no_daftar_bc' => $data['no_daftar_bc_update'],
                                'document_date' => $data['document_date_update'],
                                'qty' => $data['qty_update'],
                                'updated_at' => Carbon::now()
                            ]);
            if ($update) {
                // cek exists
                $cekifexist = DB::table('delivery_in')
                        ->where('no_suratjalan', $no_suratjalan)
                        ->whereNull('deleted_at')
                        ->count();

                if ($cekifexist>1) {
                        return response()->json('surat jalan already exists..!', 422);
                }
            }

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        // return view('bea_cukai.surat_jalan_in.index');

    }

    //delete
    public function deleteSuratJalanIn(Request $request)
    {
        $id = $request->id;
        DB::table('delivery_in')
                ->where('id', $id)
                ->update([
                    'deleted_at' => Carbon::now()
                ]);

       // return view('bea_cukai.surat_jalan_in.index');
    }

    //END OF SURAT JALAN IN

    // get data delivery in
    public function ajaxGetDataDeliveryIn(Request $request)
    {
        $no_suratjalan = $request->no_suratjalan;
        $delivery_in_id = $request->delivery_in_id;

        if ($request->ajax()){
            $data = DB::table('v_surat_jalan_bc_detail_in')
                        ->where('delivery_in_id', $delivery_in_id)
                        ->whereNull('deleted_at');

            $data = $data->orderBy('created_at', 'desc')->get();  

            //datatables
            return DataTables::of($data)
                ->addColumn('checkbox', function ($data) {
                    return '<input type="checkbox" class="clplanref" name="selector[]" id="Inputselector" value="'.$data->id.'">';
                })
                ->addColumn('action', function($data){ return view('_action', [
                        'id' => $data->id,
                        'deletes' => route('suratjalanin.deleteDeliveryInDetail', $data->id)
                    ]);
                })
                ->editColumn('qty', function($data){
                    // $readonly = $data->barcode_id != null ? ' disabled' : ' ';
                    $readonly = ' disabled';

                    return '<div id="qty_'.$data->qty.'">
                                <input type="text"
                                        data-id="'.$data->id.'"
                                        class="form-control qty_filled"
                                        value="'.$data->qty.'"
                                        onkeypress="return isNumberKey(event)"
                                        onfocus="return $(this).select();"
                                        '.$readonly.'>
                            </div>';
                })
            ->rawColumns(['qty', 'action', 'checkbox'])
            ->make(true);
        }
    }

    // export data delivery in
    public function exportDataDeliveryIn(Request $request)
    {
        $delivery_in_id = $request->delivery_in_id;

        $data = DB::table('v_surat_jalan_bc_detail_in')
                    ->where('delivery_in_id', $delivery_in_id)
                    ->whereNull('deleted_at')
                    ->get();

        if ($data->count()<1) {
            return response()->json('data not found', 422);
        }

        $filename = $data[0]->bc_no;
        $i= 1;
        // $export = \Excel::create($filename, function($excel) use ($data, $i) {
        //     $excel->sheet('report', function($sheet) use($data, $i) {
        //         $sheet->appendRow(array(
        //             '#', 'Barcode ID', 'STYLE', 'CUT',  'Komponen', 'No.Sticker', 'QTY', 'Date'
        //         ));
        //         $data->chunk(100, function($rows) use ($sheet, $i)
        //         {
        //             foreach ($rows as $row)
        //             {
        //                 //
        //                 $sheet->appendRow(array(
        //                     $i++, '="'.$row->barcode_id.'"', $row->style_edit, $row->cut_number, $row->komponen_name, $row->sticker_no, $row->qty, $row->checkin
        //                 ));
        //             }
        //         });
        //     });
        // })->download('xlsx');
        foreach ($data as $data_results) {
            $data_results->no = $i++;
         }
         if ($data->count() > 0) {
            return (new FastExcel($data))->download($filename.'.xlsx', function ($row) {

                return 
                        [
                            '#' => $row->no, 
                            'Barcode ID' => $row->barcode_id, 
                            'STYLE' => $row->style_edit,
                            'CUT' => $row->cut_number,
                            'Komponen' => $row->komponen_name,
                            'No. Sticker' => $row->sticker_no,
                            'Size' => $row->size_edit,
                            'QTY' => $row->qty,
                            'Date' => $row->checkin
                        ];
                        
            });
         }else {

            $list = collect([
                [ 
                    '#' => '', 
                    'Barcode ID' => '', 
                    'STYLE' => '',
                    'CUT' => '',
                    'Komponen' => '',
                    'No. Sticker' => '',
                    'Size' => '',
                    'QTY' => '',
                    'Date' => ''
                 ],
            ]);
            
            return (new FastExcel($list))->download($filename.'.xlsx');

         }
    }

    public function deleteDeliveryInDetail(Request $request)
    {
        $id = $request->id;

        $request->validate([
            'id' => 'required'
        ]);

        $data_delivery_in_detail = DB::table('delivery_in_detail')
                                        ->where('id', $id)
                                        ->first();

    }
    // update qty delivery in detail
    public function updateQty(Request $request)
    {
        $id = $request->id;
        $qty = $request->qty;

        $request->validate([
            'id' => 'required',
            'qty' => 'required|numeric',
        ]);

        if ($qty<=0 || $qty == '') {
            return response()->json('invalid qty..!', 422);
        }


        try {
            DB::beginTransaction();
                // update
                $update = DB::table('delivery_in_detail')
                            ->where('id', $id)
                            ->update([
                                'qty' => $qty,
                                'updated_at' => Carbon::now()
                            ]);

                if ($update) {
                    // get data delivery in detail
                    $data_in_detail = DB::table('delivery_in_detail')
                                        ->where('id', $id)
                                        ->first();

                    if ($data_in_detail->barcode_id != null) {

                        throw new \Exception('invalid update qty');

                        $data_formulir = DB::table('v_formulir')
                                        ->select('qty_formulir')
                                        ->where('barcode_id', $data_in_detail->barcode_id)
                                        ->first();

                        $total_qty = $data_formulir->qty_formulir;

                        $total_qty_in = DB::table('delivery_in_detail')
                                            ->where('barcode_id', $data_in_detail->barcode_id)
                                            ->whereNull('deleted_at')
                                            ->sum('qty');

                        // cek total qty dengan total qty in
                        if ($total_qty_in > $total_qty) {
                            return response()->json('invalid update qty (qty in > total qty)', 422);
                        }
                    }else {
                        $data_formulir = DB::table('delivery_reject')
                                        ->select('qty')
                                        ->where('id', $data_in_detail->delivery_reject_id)
                                        ->first();

                        $total_qty = $data_formulir->qty;

                        $total_qty_in = DB::table('delivery_in_detail')
                                            ->where('delivery_reject_id', $data_in_detail->delivery_reject_id)
                                            ->whereNull('deleted_at')
                                            ->sum('qty');

                        // cek total qty dengan total qty in
                        if ($total_qty_in > $total_qty) {
                            return response()->json('invalid update qty (qty in > total qty)', 422);
                        }
                    }
                }
            
            DB::commit();
            
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

    }
}
