<?php namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    protected $fillable = [
        'id','name', 'nik', 'email', 'password', 'warehouse_id', 'division', 'factory_id', 'created_at', 'updated_at', 'deleted_at', 'sex', 'photo', 'admin_role', 'is_nagai', 'is_washing'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles(){
        return $this->belongsToMany('App\Models\Role','role_user','user_id','role_id');
    }
}
