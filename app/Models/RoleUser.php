<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole_User;

class RoleUser extends Model
{
    //
    protected $table = 'role_user';

    protected $fillable = ['user_id','role_id'];
}
