<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Factories extends Model
{
    protected $table = 'factories';
	protected $fillable = ['id','factory_name','created_at','updated_at','deleted_at', 'no_suratjalan', 'no_polybag', 'list_komponen_break', 'logo_factory'];
    protected $guarded = ['id'];
    protected $dates = ['created_at','updated_at','deleted_at'];
}
