<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Process extends Model
{
    protected $table = 'process';
	protected $fillable = ['id','process_name','created_at','updated_at','deleted_at'];
    protected $guarded = ['id'];
    protected $dates = ['created_at','updated_at','deleted_at'];
}
