<?php

use Illuminate\Database\Seeder;

use App\Models\TypePackingList;
use Carbon\Carbon;

class TypePackingListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $types = [
            [
                'type_name' => 'NORMAL',
                'created_at' => Carbon::now()
            ],
            [
                'type_name' => 'REJECT',
                'created_at' => Carbon::now()
            ],
            [
                'type_name' => 'RETURN',
                'created_at' => Carbon::now()
            ],
            [
                'type_name' => 'MINUS',
                'created_at' => Carbon::now()
            ],
        ];

        foreach ($types as $key => $type) {
            TypePackingList::create([
                                'type_name' => $type['type_name'],
                                'created_at' => $type['created_at']
                            ]);
        }
    }
}
