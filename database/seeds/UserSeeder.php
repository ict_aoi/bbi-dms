<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    public function run()
    {
        $roles  = Role::Select('id')->get();

        $user = new User();
        $user->name = 'Admin ICT BBI';
        $user->nik = '11111111';
        $user->warehouse_id = '1000013';
        $user->email = 'admin_ict@bbi.co.id';
        $user->email_verified_at = carbon::now();
        $user->password =  bcrypt('password1');
        $user->sex = 'laki';
        $user->factory_id = '3';
        $user->admin_role = TRUE;

        if($user->save())
            $user->attachRoles($roles); 

        $user1 = new User();
        $user1->name = 'Admin ICT BBI';
        $user1->nik = 'admin';
        $user1->warehouse_id = '1000013';
        $user1->email = 'admin_ict2@bbi.co.id';
        $user1->email_verified_at = carbon::now();
        $user1->password =  bcrypt('admin08');
        $user1->sex = 'laki';
        $user1->factory_id = '3';
        $user1->admin_role = TRUE;

        if($user1->save())
            $user1->attachRoles($roles);   
    }
}
