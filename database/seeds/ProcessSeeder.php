<?php

use Illuminate\Database\Seeder;
use App\Models\Process;

use Carbon\Carbon;

class ProcessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $process = [
            [
                'process_name' => 'ARTWORK',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'process_name' => 'HE',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'process_name' => 'PAD',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'process_name' => 'PPA',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'process_name' => 'FUSE',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'process_name' => 'BOBOK',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'process_name' => 'PRINTING',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ];

        foreach ($process as $key => $proc) {
            Process::create([
                'process_name' => $proc['process_name'],
                'created_at' => $proc['created_at'],
                'updated_at' => $proc['updated_at']
            ]);
        }
    }
}
