<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('permissions')->truncate();
        DB::table('roles')->truncate();
        DB::table('users')->truncate();
        DB::table('factories')->truncate();
        DB::table('process')->truncate();
        DB::table('types')->truncate();
        DB::table('type_packing_list')->truncate();
        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(FactoriesSeeder::class);
        $this->call(ProcessSeeder::class);
        $this->call(TypeSeeder::class);
        $this->call(TypePackingListSeeder::class);
    }
}
