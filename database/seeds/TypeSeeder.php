<?php

use Illuminate\Database\Seeder;

use App\Models\Types;
use Carbon\Carbon;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $types = [
            [
                'type_name' => 'Non',
                'created_at' => Carbon::now()
            ],
            [
                'type_name' => 'TOP',
                'created_at' => Carbon::now()
            ],
            [
                'type_name' => 'BOTTOM',
                'created_at' => Carbon::now()
            ],
        ];

        foreach ($types as $key => $type) {
            Types::create([
                'type_name' => $type['type_name'],
                'created_at' => $type['created_at']
            ]);
        }
    }
}
