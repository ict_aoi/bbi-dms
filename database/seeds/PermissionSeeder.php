<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'menu-user',
                'display_name' => 'menu user',
                'description' => 'menu user'
            ],
            [
                'name' => 'menu-role',
                'display_name' => 'menu role',
                'description' => 'menu role'
            ],
            [
                'name' => 'menu-permission',
                'display_name' => 'menu permission',
                'description' => 'menu permission'
            ],
            [
                'name' => 'menu-dashboard',
                'display_name' => 'menu dashboard',
                'description' => 'menu dashboard'
            ],
            [
                'name' => 'menu-user-management',
                'display_name' => 'menu user management',
                'description' => 'menu user management'
            ],
            [
                'name' => 'menu-master',
                'display_name' => 'menu master',
                'description' => 'menu master'
            ],
            [
                'name' => 'menu-report',
                'display_name' => 'menu report',
                'description' => 'menu report'
            ],
            [
                'name' => 'menu-barcoding',
                'display_name' => 'menu barcoding',
                'description' => 'menu barcoding'
            ],
            [
                'name' => 'menu-packinglist',
                'display_name' => 'menu packing list',
                'description' => 'menu packing list'
            ],
            [
                'name' => 'menu-distribusi-out',
                'display_name' => 'menu distribusi out',
                'description' => 'menu distribusi out'
            ],
            [
                'name' => 'menu-subcont',
                'display_name' => 'menu subcont',
                'description' => 'menu subcont'
            ],
            [
                'name' => 'menu-distribusi-in',
                'display_name' => 'menu distribusi in',
                'description' => 'menu distribusi in'
            ],
        ];

        foreach ($permissions as $key => $permission) {
            Permission::create([
                'name' => $permission['name'],
                'display_name' => $permission['display_name'],
                'description' => $permission['description']
            ]);
        }
    }
}
