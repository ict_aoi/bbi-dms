<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Factories;

class FactoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $factories = array(
            [
                'factory_name' => 'AOI1', 
                'created_at' => Carbon::now(), 
                'deleted_at' => Carbon::now(), 
            ],
            [
                'factory_name' => 'AOI2', 
                'created_at' => Carbon::now(), 
                'deleted_at' => Carbon::now(), 
            ],
            [
                'factory_name' => 'BBIS', 
                'created_at' => Carbon::now(), 
                'deleted_at' => null, 
            ],
        );

        foreach ($factories as $key => $value) {
            Factories::create([
                            'factory_name' => $value['factory_name'], 
                            'created_at' => $value['created_at'], 
                            'deleted_at' => $value['deleted_at']
            ]);
        }
    }
}
