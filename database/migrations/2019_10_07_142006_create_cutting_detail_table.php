<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuttingDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cutting_detail', function (Blueprint $table) {
            $table->string('barcode_id',200)->unique();
            $table->integer('po_summary_detail_id');
            $table->integer('cut_number')->nullable();
            $table->string('cut_info', 200)->nullable();
            $table->string('lot', 100)->nullable();
            $table->integer('komponen')->nullable();
            $table->string('part', 50)->nullable();
            $table->integer('qty')->nullable();
            $table->integer('sticker_from')->nullable();
            $table->integer('sticker_to')->nullable();
            $table->string('sticker_no', 100)->nullable();
            $table->date('cutting_date')->nullable();
            $table->integer('bundle_id')->nullable();
            $table->string('bundle', 200)->nullable();
            $table->string('qc', 200)->nullable();
            $table->string('cutter', 200)->nullable();
            $table->string('current_process', 100)->nullable();
            $table->string('current_status', 100)->nullable();
            $table->string('process', 200)->nullable();
            $table->string('already_process', 200)->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->boolean('is_printed')->default(false);
            $table->boolean('is_printed_formulir')->default(false);
            $table->boolean('is_printed_sj')->default(false);
            $table->string('description')->nullable();
            $table->primary('barcode_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cutting_detail');
    }
}
