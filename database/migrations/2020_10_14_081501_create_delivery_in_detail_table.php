<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryInDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_in_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_suratjalan');
            $table->string('no_suratjalan_info')->nullable();
            $table->integer('delivery_in_id')->nullable();
            $table->string('barcode_id', 200)->nullable();
            $table->integer('delivery_reject_id')->nullable();
            $table->integer('qty')->nullable();
            $table->integer('formulir_artwork_id')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_in_detail');
    }
}
