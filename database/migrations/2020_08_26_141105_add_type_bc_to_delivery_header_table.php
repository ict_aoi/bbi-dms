<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeBcToDeliveryHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_header', function (Blueprint $table) {
            //
            $table->string('type_bc', 100)->nullable();
            $table->string('no_aju', 200)->nullable();
            $table->string('no_daftar_bc', 200)->nullable();
            $table->date('document_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_header', function (Blueprint $table) {
            //
        });
    }
}
