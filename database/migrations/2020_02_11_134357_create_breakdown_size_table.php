<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBreakdownSizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('breakdown_size', function (Blueprint $table) {
            $table->increments('id');
            $table->string('documentno')->nullable();
            $table->string('poreference')->nullable();
            $table->integer('m_product_id')->nullable();
            $table->string('kode_product')->nullable();
            $table->string('nama_product')->nullable();
            $table->string('description')->nullable();
            $table->string('style')->nullable();
            $table->string('size')->nullable();
            $table->string('article')->nullable();
            $table->string('color')->nullable();
            $table->string('kst_season')->nullable();
            $table->string('category')->nullable();
            $table->date('dateorder')->nullable();
            $table->date('datepromise')->nullable();
            $table->string('buyer')->nullable();
            $table->integer('c_order_id')->nullable();
            $table->integer('c_orderline_id')->nullable();
            $table->integer('qtyordered')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('breakdown_size');
    }
}
