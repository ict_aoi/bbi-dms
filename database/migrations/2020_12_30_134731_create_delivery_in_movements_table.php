<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryInMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_in_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('delivery_in_detail_id');
            $table->integer('delivery_in_id_from');
            $table->integer('delivery_in_id_to');
            $table->dateTime('deleted_at')->nullable();
            $table->boolean('is_canceled')->default(false);
            $table->integer('user_id')->nullable();
            $table->string('description', 255)->nullable();
            $table->string('ip_address', 200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_in_movements');
    }
}
