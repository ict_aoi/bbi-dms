<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterStyleDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_style_detail', function (Blueprint $table) {
            $table->string('style', 200)->nullable();
            $table->string('part', 50)->nullable();
            $table->integer('komponen')->nullable();
            $table->string('process', 200)->nullable();
            $table->integer('total')->default(1);
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_style_detail');
    }
}
