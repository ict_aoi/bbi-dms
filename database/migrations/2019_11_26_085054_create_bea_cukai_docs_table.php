<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeaCukaiDocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bea_cukai_docs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_suratjalan', 200)->nullable();
            $table->string('type_bc', 100)->nullable();
            $table->string('no_aju', 200)->nullable();
            $table->string('no_daftar_bc', 200)->nullable();
            $table->date('document_date')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('ip_address', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bea_cukai_docs');
    }
}
