<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulirArtworkHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulir_artwork_header', function (Blueprint $table) {
            $table->increments('id');
            $table->string('po_number',100)->nullable();
            $table->string('uoms')->nullable();
            $table->integer('user_by')->nullable();
            $table->string('created_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->dateTime('create_date')->nullable();
            $table->string('position_created_by')->nullable();
            $table->string('approved_by')->nullable();
            $table->dateTime('approve_date')->nullable();
            $table->string('position_approved_by')->nullable();
            $table->string('delivered_by')->nullable();
            $table->dateTime('delivery_date')->nullable();
            $table->string('position_delivered_by')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->boolean('is_delivery_printed')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulir_artwork_header');
    }
}
