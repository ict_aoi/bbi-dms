<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryPoSummaryDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_po_summary_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('po_number', 100);
            $table->string('plan_ref', 100)->nullable();
            $table->string('style', 200)->nullable();
            $table->string('size', 200)->nullable();
            $table->string('color', 200)->nullable();
            $table->string('article', 200)->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->integer('factory_id')->nullable();
            $table->string('mo', 100)->nullable();
            $table->integer('qtyordered')->nullable();
            $table->string('kst_joborder', 200)->nullable();
            $table->integer('type_id')->nullable();
            $table->string('type_description', 100)->nullable();
            $table->string('category', 200)->nullable();
            $table->string('m_product_id', 100)->nullable();
            $table->string('kode_product', 255)->nullable();
            $table->string('nama_product', 255)->nullable();
            $table->integer('from');
            $table->integer('qty');
            $table->integer('total_qty');
            $table->string('color_edit', 200)->nullable();
            $table->string('article_edit', 200)->nullable();
            $table->boolean('is_inhouse')->default(false);
            $table->string('po_number_edit', 100)->nullable();
            $table->string('style_edit', 200)->nullable();
            $table->integer('po_summary_id')->nullable();
            $table->integer('c_order_id');
            $table->integer('c_orderline_id');
            $table->string('size_edit')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->dateTime('move_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_po_summary_detail');
    }
}
