<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryCuttingMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_cutting_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('barcode_id',200);
            $table->string('process_from',200)->nullable();
            $table->string('status_from',200)->nullable();
            $table->string('process_to',200)->nullable();
            $table->string('status_to',200)->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->boolean('is_canceled')->default(false);
            $table->integer('user_id')->nullable();
            $table->string('description', 255)->nullable();
            $table->string('ip_address', 200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_cutting_movements');
    }
}
