<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSpecialSizeToMasterStyleDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_style_detail', function (Blueprint $table) {
            //
            $table->boolean('is_special')->nullable()->default(false);
            $table->boolean('is_big_size')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_style_detail', function (Blueprint $table) {
            //
        });
    }
}
