<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryInTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_in', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_suratjalan');
            $table->string('no_suratjalan_info')->nullable();
            $table->integer('subcont_id')->nullable();
            $table->string('accepted_by')->nullable();
            $table->string('approved_by')->nullable();
            $table->string('delivered_by')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->string('bc_no')->nullable();
            $table->string('type_bc', 100)->nullable();
            $table->string('no_aju')->nullable();
            $table->string('no_daftar_bc')->nullable();
            $table->date('document_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_in');
    }
}
